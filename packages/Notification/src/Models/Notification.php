<?php
namespace Saegeul\Notification\Models ;

class Notification extends \Eloquent
{
    protected $fillable = ['receiver', 'series_id', 'post_id','commit_uuid', 'user_id', 'command','is_confirmed','pull_request_id','notified_at'] ;

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }

    public function series()
    {
        return $this->belongsTo('Saegeul\Series\Models\Series');
    }
}
