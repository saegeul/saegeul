<?php

namespace spec\Saegeul\Permission;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Database\Eloquent\Model ;
use Saegeul\Series\Models\Series as SeriesModel ;

class PermissionGeneratorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Permission\PermissionGenerator');
    }

    public function it_generate_slug(SeriesModel $series)
    {
        $series->getTable()->shouldBeCalled()->willReturn('series');
        $series->getKey()->shouldBeCalled()->willReturn(1);
        $slug = 'series/1/owner';
        $roleName = 'owner';
        $this->generateSlug($series, $roleName)->shouldReturn($slug);
    }

    public function it_generate_permissions(SeriesModel $series)
    {
        $series->getTable()->shouldBeCalled()->willReturn('series');
        $series->getKey()->shouldBeCalled()->willReturn(1);
 
        $permissions = ['create','view','update','delete','write'];
        $returnData = [
            'series.1.create' => true,
            'series.1.view' => true,
            'series.1.update' => true,
            'series.1.delete' => true,
            'series.1.write' => true
        ];
        $this->generatePermissions($series,$permissions)->shouldReturn($returnData);
    }

    public function it_generate_permissions_to_array()
    {
        $role = [
            'name' => 'owner',
            'documentId' => 1,
            'documentType' => 'series',
            'permissions' => ['create','view','update','delete','write']
        ];
        $permissions = [
            'series.1.create',
            'series.1.view',
            'series.1.update',
            'series.1.delete',
            'series.1.write'
        ]; 
        $this->generatePermissionsToArray($role)->shouldReturn($permissions);
    }
}
