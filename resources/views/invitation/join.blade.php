@extends('layouts.default')

@section('contents')
<br/>
<div class="container">

    <div class="mini-form-wrapper">
        <div> 
            Complete To join for {{ $invitedModel->getTable() }}
            <br>Title : {{ $invitedModel->title }}
            <br>Description : {{ $invitedModel->description }}
            <br>OwnerName : {{ $invitedModel->user->first_name }} {{ $invitedModel->user->last_name }}
            <br>OwnerEmail : {{ $invitedModel->user->email }}
        </div> 
    </div>
</div> 
@stop 
