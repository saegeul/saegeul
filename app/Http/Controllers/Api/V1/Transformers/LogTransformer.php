<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Logger\Models\Log;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;

class LogTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user'];

    public function transform(Log $log)
    {
        return [
            'id' => (int) $log->id,
            'command' => $log->command,
            'series_id' => $log->series_id,
            'invitation_id' => $log->invitation_id,
            'post_id' => $log->post_id,
            'commit_uuid' => $log->commit_uuid,
            'created_at' => $log->created_at,
            'updated_at' => $log->updated_at
        ];
    }

    public function includeUser(Log $log)
    {
        $user = $log->user;

        return $this->item($user, new UserTransformer);
    }
}
