<?php

Route::group(array('prefix'=>'workspace/series','namespace'=>'Workspace', 'middleware' =>'auth.glauth' ),function(){
    Route::get('/', array('as'=>'workspace.series.index','uses'=>'SeriesController@index'));
    Route::get('/coworking', array('as'=>'workspace.series.getCoworkingSeries','uses'=>'SeriesController@getCoworkingSeries'));
    Route::get('/create', array('as'=>'workspace.series.create','uses'=>'SeriesController@editForm'));
    Route::post('/create', array('as'=>'workspace.series.store','uses'=>'SeriesController@store'));
    Route::get('/{id}',array('as'=>'workspace.series.show','uses'=>'SeriesController@show')); 
    Route::get('/{id}/posts',array('as'=>'workspace.series.posts','uses'=>'SeriesController@posts')); 
    Route::get('/{id}/posts/create',array('as'=>'workspace.series.posts.create','uses'=>'PostsController@create')); 
    Route::get('/{id}/posts/{postId}',array('as'=>'workspace.series.posts.show','uses'=>'PostsController@show')); 
    Route::get('/{id}/posts/{postId}/edit',array('as'=>'workspace.series.posts.edit','uses'=>'PostsController@edit')); 
    Route::get('/{id}/posts/{postId}/edit-or-view',array('as'=>'workspace.series.posts.editOrView','uses'=>'PostsController@editOrView')); 
    Route::get('/{id}/collaborators',array('as'=>'workspace.series.collaborators','uses'=>'SeriesController@collaborators')); 
    Route::get('/{id}/followers',array('as'=>'workspace.series.followers','uses'=>'SeriesController@followers')); 
}); 
