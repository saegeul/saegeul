@extends('layouts.default')

@section('content')
<br/>
<div class="container">

    <div class="mini-form-wrapper">
        {!! Form::open(['class' =>'clearfix signup-form', 'method' => 'post', 'url' => '/subscription/join']) !!}
        <input type="hidden" name="subscription_code" value="{{ $subscriptionCode }}">
        <div> 
            Hi Welcome to {{ $subscription->series->getTable() }}
            <br>Title : {{ $subscription->series->title }}
            <br>Description : {{ $subscription->series->description }}
            <br>OwnerName : {{ $subscription->series->user->first_name }} {{ $subscription->series->user->last_name }}
            <br>OwnerEmail : {{ $subscription->series->user->email }}
        </div> 
        <button class="btn btn-block ghost-btn" type="submit">Join </button> 
        {!! Form::close() !!}
    </div>
</div> 
@stop 
