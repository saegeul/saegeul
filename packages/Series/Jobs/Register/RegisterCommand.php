<?php

namespace Saegeul\Series\Jobs\Register;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Series\Events\SeriesWasRegistered ;

class RegisterCommand implements SelfHandling
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data ;
    }

    public function handle(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository)
    {
        $series = $seriesRepository->store($this->data);

        $dispatcher->fire(new SeriesWasRegistered($series));

        return $series ;
    }
}
