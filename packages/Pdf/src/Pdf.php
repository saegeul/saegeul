<?php 
namespace Saegeul\Pdf;

use Artgrafii\Glauth\Glauth ;
use Artgrafii\Glauth\Contracts\UserInterface ;
use Saegeul\Post\Post;
use Saegeul\Series\Series;
use Saegeul\Pdf\MYPDF;
use Saegeul\Pdf\Services\DirectoryService;

class Pdf 
{
    protected $glauth ; 
    protected $seriesService;
    protected $postService;
    protected $directoryService;
    protected $user;

    private $fontFamily;
    private $fontFamilyKo;
    private $marginTop;
    private $marginBottom;
    private $marginRight;
    private $marginLeft;
    private $htmlTag;
    private $html;
    private $prveNode;
    private $nextNode;
    private $nodes;

    public function __construct(
        Glauth $glauth,
        Series $seriesService,
        Post $postService,
        DirectoryService $directoryService
    )
    {
        $this->glauth = $glauth;
        $this->seriesService = $seriesService;
        $this->postService = $postService;
        $this->directoryService = $directoryService;
        $this->user = $this->glauth->check();
        $this->setConfig();
    }

    private function setConfig()
    {
        $this->fontFamily = \Config::get('pdf.font.family');
        $this->fontFamilyKo = \Config::get('pdf.font.family_ko');
        $this->marginTop = \Config::get('pdf.margin.top');
        $this->marginBottom = \Config::get('pdf.margin.bottom');
        $this->marginRight = \Config::get('pdf.margin.right');
        $this->marginLeft = \Config::get('pdf.margin.left');
        $this->htmlTag = \Config::get('pdf.tag');
    }

    public function createForSeries($seriesId)
    {
        $series = $this->seriesService->show($seriesId);       
        
        $postRepository = $this->postService->getRepository();
        $posts = $postRepository->findBySeries($series->getKey(),$series->user_id);

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = $this->setPdf($pdf);
        $pdf->SetTitle($series->title);
        $pdf->SetSubject($series->description);

        $pdf->AddPage();
        $html = '<h2>'.$series->title.'<h2>';
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->SetXY(0, 270);
        $pdf->Cell(0, 0, 'Author : '.$series->user->email, 0, 0, 'R', 0, '', 1);

        // Create a fixed link to the first page using the * character
        $index_link = $pdf->AddLink();
        $pdf->SetLink($index_link, 0, '*1');

        $i = 1;
        foreach($posts as $post){
            if($post->pull_request_status != 'merged'){
                $pdf->AddPage();
                $pdf->SetFillColor(200, 220, 255);
                $pdf->Cell(0, 6, 'Chapter '.$i.' : '.$post->title, 0, 1, '', 1);
                $pdf->Ln(4);

                if(isset($post->post_image_url)){
                    $html = '<img src="http://'.$post->post_image_url.'">';
                    $pdf->writeHTML($html, true, false, true, false, '');
                }
            /*
            $index_link = $pdf->AddLink();
            $pdf->SetLink($index_link, 0, '*1');
            $pdf->Cell(0, 0, $post->title, 0, 1, 'C', false, '');
             */

                $pdf->Bookmark('Chapter '.$i.' '.$post->title, 0, 0, '', 'B', array(0,64,128));
                $index_link = $pdf->AddLink();

                $html = $this->parse($post->content,$pdf);
                $pdf->writeHTML($html, true, false, true, false, '');
                ++$i;
            }
        } 
        $pdf->addTOCPage();

        //write the TOC title
        $pdf->SetFont('malgungothic', '', 12);
        $pdf->MultiCell(0, 0, 'Table Of Content', 0, 'C', 0, 1, '', '', true, 0);
        $pdf->Ln();

        // add a simple Table Of Content at first page
        $pdf->addTOC(2, 'courier', '.', 'INDEX', 'B', array(128,0,0));

        // end of TOC page
        $pdf->endTOCPage();

        $fileName = $this->directoryService->getFileName($series);
        return $pdf->output($fileName, 'D');
    }

    private function setPdf($pdf)
    {
        $pdf->SetCreator(PDF_CREATOR);
        //$author = $this->user->last_name.''.$this->user->first_name;
        //$pdf->SetAuthor($author);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

        // remove default header
        $pdf->setPrintHeader(false);

        //set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins($this->marginLeft,$this->marginTop,$this->marginRight);
        $pdf->SetHeaderMargin($this->marginTop);
        $pdf->SetFooterMargin($this->marginBottom);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE,$this->marginBottom);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('nanumgothic','',11);
        $pdf->SetDefaultMonospacedFont('nanumgothic'); 

        // set line heigh
        $noMargins = array(
            0 => array('h' => '', 'n' => 0),
            1 => array('h' => '', 'n' => 0)
        );
        $lineHeight = [
            'br' => [['n' => 0.01]],
            'blockquote' => [['n' => 0.01]],
            'table' => $noMargins,
            'div' => [['n' => 0.01]]
        ];
        $pdf->setHtmlVSpace($lineHeight);
        $pdf->setHtmlLinksStyle(array(0,0,255),'N');
        $pdf->setCellHeightRatio(1.6);

        return $pdf;
    }

    public function createForPost($postId)
    {
        $post = $this->postService->show($postId);       

        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = $this->setPdf($pdf);
        $pdf->SetTitle($post->title);
        $pdf->SetSubject($post->description);
        $pdf->AddPage();

        $html = $this->parse($post->content);
        $pdf->writeHTML($html, true, 0, true, 0);
        $pdf->lastPage();

        $fileName = $this->directoryService->getFileName($post);
        //$pdf->output($fileName, 'FI');
        return $pdf->output($fileName, 'D');
    }

    private function parse($content)
    {
        $jsonText = json_decode($content);
        $document = $jsonText->document;

        $this->recursive($document);

        $html = $this->getHtmlTag();

        //dd($html);
        $this->initHtmlTag();

        return $html;
    }

    private function recursive($document)
    { 
        $newArr = [];
        foreach ($document as $key => $node) {
            if(!isset($node->type)){
                if(!isset($node->attributes[0])){
                    if(count($newArr) > 0){
                        $this->createCoverTag($newArr);
                        $newArr = [];
                    }
                    $this->recursive($node->text);
                }else if($node->attributes[0] == 'quote'){
                    if(count($newArr) > 0){
                        $this->createCoverTag($newArr);
                        $newArr = [];
                    }
                    $html = '<blockquote style="padding:0;margin:0;border-left:1px solid #ccc;"><table><tr><td width="12"></td><td width="500">';
                    foreach($node->text as $key => $child){ 
                        if(count($node->text) != ($key+1)){
                            $str = $this->createTag($child);
                            $html .=  $str;
                        }
                    }
                    $this->setHtmlTag($html.'</td></tr></table></blockquote>');
                }else { 
                    $newArr[] = $node; 
                }
            }else{
                $html = $this->createTag($node);
                $this->setHtmlTag($html);
            }
        }
        if(count($newArr) > 0){
            $this->createCoverTag($newArr);
            $newArr = [];
        }
    }

    private function createCoverTag($nodes)
    {
        $cover = $this->getCoverTag(end($nodes[0]->attributes));
        $this->setHtmlTag('<'.$cover.'>');
        foreach($nodes as $key => $node){ 
            $html ='';
            foreach($node->text as $child){
                $str = $this->createTag($child);
                $html .=  $str;
            }
            $this->setHtmlTag('<li>'.$html.'</li>');
            if(!isset($nodes[$key +1])){
                $cover = $this->getCoverTag(end($node->attributes));
                $this->setHtmlTag('</'.$cover.'>');
            }else{
                if(count($node->attributes) < count($nodes[$key +1]->attributes)){
                    $cover = $this->getCoverTag(end($nodes[$key +1]->attributes));
                    $this->setHtmlTag('<'.$cover.'>');
                }else if(count($node->attributes) > count($nodes[$key +1]->attributes)){
                    $cover = $this->getCoverTag(end($node->attributes));
                    $this->setHtmlTag('</'.$cover.'>');
                }else if(count($node->attributes) == count($nodes[$key +1]->attributes) && $node->attributes != $nodes[$key +1]->attributes){
                    $cover = $this->getCoverTag(end($node->attributes));
                    $this->setHtmlTag('</'.$cover.'>');
                    $cover = $this->getCoverTag(end($nodes[$key +1]->attributes));
                    $this->setHtmlTag('<'.$cover.'>');
                }
            }
        }

        if(count(end($nodes)->attributes) > count($nodes[0]->attributes)){
            foreach(end($nodes)->attributes as $key => $attribute){
                if($key&1 && count(end($nodes)->attributes) != ($key+1)){
                    $cover = $this->getCoverTag($attribute);
                    $this->setHtmlTag('</'.$cover.'>');
                }
            }
        }
    }

    private function getCoverTag($attribute)
    {
        if($attribute == 'bullet'){
            return 'ul';
        }else if($attribute == 'number'){
            return 'ol';
        }else{
            return '';
        }
    }

    private function createTag($node)
    {
        $html = '';
        if($node->type == 'string'){
            if(isset($node->attributes->href)){
                $str = $this->setFontStyle($node->attributes,$node->string);
                $html = '<a href="'.$node->attributes->href.'">'.$str.'</a>';
            }else if(isset($node->string)){
                $html = $this->setFontStyle($node->attributes,$node->string);
            }
        }else if($node->type == 'attachment' && isset($node->attachment->url)){
            $style = $this->getImageStyle($node->attachment);
            $html = '<div><img src="'.$node->attachment->url.'"style="'.$style.'"/></div>';
        }
        return $html;
    }

    private function setFontStyle($attributes, $str)
    {
        $str = preg_replace("/\r\n|\r|\n/",'<br/>',$str);

        if(isset($attributes->bold)){
            $str = '<b>'.$str.'</b>';
        }else if(isset($attributes->italic)){
            $str = '<i>'.$str.'</i>';
        }else if(isset($attributes->blockBreak)){
            $str = $str.'<br/>';
        }

        return $str;
    }

    private function getImageStyle($attachment)
    {
        $style = '';
        foreach ($attachment as $key => $val) {
            if($key == 'height' || $key == 'width'){
                $style .= $key .':'.$val.'px;';
            }
        }
        return $style;
    } 

    private function initHtmlTag()
    {
        $this->html = '';
    }
    private function setHtmlTag($tag)
    {
        $this->html .= $tag;
    }

    public function getHtmlTag()
    {
        return $this->html;
    }
}
