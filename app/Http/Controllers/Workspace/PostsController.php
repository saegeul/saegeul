<?php namespace App\Http\Controllers\Workspace;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request ;
use Carbon\Carbon ;
use Saegeul\Series\Series ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Post\Post ;
use Artgrafii\Glauth\Glauth ;

class PostsController extends Controller 
{
    protected $postService ; 
    protected $glauth ; 

    public function __construct(
        Post $postService,
        Glauth $glauth
    )
    {
        $this->postService = $postService ;
        $this->glauth = $glauth ;
    }

    public function editOrView($seriesId, $postId)
    {
        $post = $this->postService->show($postId);
        $user = $this->glauth->check() ;

        if($post->isCreatableByUser($user)){
            return redirect()->route('workspace.series.posts.edit', array($seriesId,$postId)) ;
        }else if($post->series->isWriteableByUser($user)) {
            return redirect()->route('workspace.series.posts.show', array($seriesId,$postId)) ;
        }
    }

    public function show($seriesId, $postId)
    { 
        $post = $this->postService->show($postId);
        return view('workspace.posts.show',compact('post'));
    }

    public function edit($seriesId, $postId)
    { 
        $post = $this->postService->show($postId);
        return view('posts.edit',compact('post'));
    }

    public function create($seriesId)
    {
        $user = $this->glauth->check()  ;

        $formData = [];
        $formData['title'] = 'title';
        $formData['description'] = 'description';
        $formData['content'] = 'content';
        $formData['series_id'] = $seriesId;
        $formData['user_id'] = $user->getKey(); 

        $post = $this->postService->register($formData);  

        return redirect()->route('workspace.series.posts.edit', array($seriesId,$post->getKey())) ; 
    }
}
