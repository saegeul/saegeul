<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Illuminate\Http\Request;
use Artgrafii\Glauth\Glauth ;

class RelayController extends ApiController 
{
    protected $glauth;
    protected $user;

    public function __construct(Glauth $glauth)
	{
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
	}

    public function get(Request $request)
    {
        try{
            $data = $request->all(); 
            $items = new \stdClass;
            foreach($data as $value){
                $resource = $value['resource'];
                $url = '/api/v1'.$value['url'];
                $param = $value['data'];

                $request = Request::create($url, 'GET', $param);
                $response = json_decode($this->getRouter()->dispatch($request)->getContent());
                $items->{$resource} = $response;
            }
            $result = json_encode($items);
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        }catch(\Exception $e){
            return $this->respondInternalError() ; 
        }
    }
}
