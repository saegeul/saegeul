<?php
namespace Saegeul\Logger ;
use Saegeul\Logger\Repositories\LogRepository ;
use Saegeul\Logger\Contracts\LoggableInterface ;
use Artgrafii\Glauth\Glauth ;

class Logger
{
    protected $repo ;
    protected $glauth ; 

    public function __construct(LogRepository $repo, Glauth $glauth)
    {
        $this->repo = $repo ;
        $this->glauth = $glauth ;
    }

    public function log(LoggableInterface $data , $eventName)
    { 
        $loggedData = $data->getLoggedData() ;
        $loggedData['command'] = $eventName  ;
        $loggedData['user_id'] = $this->glauth->check()->getKey()  ;

        $ret = $this->repo->store($loggedData) ; 

        return $ret ; 
    }

    public function findBySeries($seriesId, $page=1)
    {
        return $this->repo->findBySeriesId($seriesId,$page); 
    }

    public function findByPost($postId, $page=1)
    {
        return $this->repo->findByPostId($postId,$page); 
    }
}
