import React from 'react' ; 
import Request from 'superagent' ;

export default class Timemachine extends React.Component {

    constructor(props){
        super(props) ;

        this.state = { 
            revisions : [ 1,2,3,4] 
        }; 
    }

    render() {
        return (
            <div className={this.props.isOpen ? 'right-fixed-bar open' : 'right-fixed-bar close'} >
                <div className="header">
                    <h6><a onClick={this.props.toggler}>History</a></h6>
                </div>
                <div>
                    <ul className="items">
                        {this.state.revisions.map((obj,index) => {
                            var that = this.props ;
                            var reload =  function(){
                                that.reload(obj.id) ;
                            }
                            return (
                            <li key={index}> 
                                <a onClick={reload} revision_id={obj.id}><span className="label">Updated</span> {obj.title}</a>
                            </li>
                            )
                        })} 
                    </ul>
                </div>
            </div> 
        )
    } 

    componentDidMount() {
        Request.get('/api/v1/posts/'+this.props.postId+'/revisions')
                .send({
                })
                .end((err,response)=>{
                    let data = JSON.parse(response.text).data ; 
                    console.log(data) ;
                    this.setState({
                        revisions : data
                    });
                }) ;

    }
}
