import React , { PropTypes , Component } from 'react' ; 
import {DragSource} from 'react-dnd' ; 
import ItemTypes from './ItemTypes.jsx' ; 

function collect(connect, monitor){ 
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging() 
    }
}

const postItemSource = {
    beginDrag(props) {
        const item = props.post ; 
        return item ; 
    },

    endDrag(props, monitor, component) {
        if(!monitor.didDrop()) {
            return ; 
        }
    }
}


class PostItem  extends React.Component {
    render() {
        const {post} = this.props ;
        const {isDragging, connectDragSource } = this.props ; 
        const draggingStyle = {
            backgroundColor: '#000'
        }; 

        return connectDragSource(
            <li> 
                <a style={ isDragging ? draggingStyle : null }>{post.title}</a>
            </li>
        ); 
    }
}

export default DragSource(ItemTypes.POST, postItemSource ,collect)(PostItem) ;
