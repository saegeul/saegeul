<?php

namespace Saegeul\Vcs\Events ;

use Saegeul\Post\Models\Post ;
use Saegeul\Vcs\Models\PullRequest;

class PostWasImported
{
    public $post;
    public $pullRequest;

    public function __construct(Post $post, PullRequest $pullRequest)
    {
        $this->post = $post;
        $this->pullRequest = $pullRequest;
    }
}
