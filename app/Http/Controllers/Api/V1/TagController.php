<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Series\Series ;
use Saegeul\Post\Post;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\TagTransformer;
use App\Http\Controllers\Api\V1\Transformers\SeriesTransformer;

class TagController extends ApiController 
{
    protected $glauth ;
    protected $user ;
    protected $seriesService;
    protected $fractal;

    public function __construct(
        Glauth $glauth,
        Series $seriesService,
        Manager $fractal
    )
	{
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->seriesService = $seriesService ;
        $this->fractal = $fractal;
	}

    public function findByTagname(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'tag' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $tag = $request->input('tag');
            $seriesRepository = $this->seriesService->getRepository();
            $items = $seriesRepository->getTags($tag);

            $resource = new Fractal\Resource\Collection($items, new TagTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function findByTagForSeries(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'tag' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $tag = $request->input('tag');
            $seriesRepository = $this->seriesService->getRepository();
            $items = $seriesRepository->findByTagname($tag);

            $resource = new Fractal\Resource\Collection($items, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            dd($e->getMessage());
            return $this->respondInternalError() ; 
        }
    }

    public function registerForSeries(Request $request, $seriesId)
    {
        try{
            $v = \Validator::make($request->all(), [
                'tags' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $tags = $request->input('tags');
            $series = $this->seriesService->show($seriesId);
            \DB::beginTransaction();
            $series->setTags($tags);
            \DB::commit();
            $series = $this->seriesService->show($seriesId);

            $resource = new Fractal\Resource\Item($series, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            dd($e->getMessage());
            return $this->respondInternalError() ; 
        }
    }
}
