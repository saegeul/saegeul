<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class TagTest extends TestCase
{
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    public function testRegister()
    {
        #given
        $this->signin();

        $data = Factory::build('Saegeul\Series\Models\Series');
        $response = $this->call('POST','api/v1/series',['title' => $data['title'], 'description' => $data['description']], []);        
        $this->assertEquals(201, $response->status());
        $result = $response->getContent();
        $arr = json_decode($result, true);

        #when
        $response = $this->call('PUT','api/v1/series/'.$arr['data']['id'].'/tag',['tags' => '1,2,5,11,111,123'], []);        
        #then
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['tag']['data'][2]['name'], 5);
    }

    public function test_failed_case_to_register_no_login()
    {
        #given
        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $series = $seriesRepository->lists(1,'')->first();

        #when
        $response = $this->call('PUT','api/v1/series/'.$series->getKey().'/tag',['tags' => '1,2,5,11,111,123'], []);        
        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_register_invalid_data()
    {
        #given
        $this->signin();
        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $series = $seriesRepository->lists(1,'')->first();

        #when
        $response = $this->call('PUT','api/v1/series/'.$series->getKey().'/tag',[], []);        

        #then
        $this->assertEquals(422, $response->status());
    }

    public function testUpdate()
    {
        #given
        $this->signin();
        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $series = $seriesRepository->lists(1,'')->first();

        #then
        $response = $this->call('PUT','api/v1/series/'.$series->getKey().'/tag',['tags' => '1,2,123'], []);        
        #then
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['tag']['data'][2]['name'], 123);
    }

    public function testGet()
    {
        #when
        $response = $this->call('GET','api/v1/tags',['tag' => '2']);        
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data'][1]['name'], 123);
    }

    public function testGetSeries()
    {
        #when
        $response = $this->call('GET','api/v1/tags/series',['tag' => '2']);        
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data'][0]['tag']['data'][1]['name'], 2);
    }
}
