<div class="container mini-container">
    <h6 class="title"><a href="/pdf/series/{{$series->getKey()}}"><i class="material-icons">cloud_download</i></a>&nbsp;{{$series->title}}</h6>
    <div>
        <ul class="tabs simple-tabs">
            <li class="tab-title @if($currentTab == 'cover') active @endif"><a href="{{route('workspace.series.show',array($series->getKey()))}}">COVER</a></li>
            <li class="tab-title @if($currentTab == 'posts') active @endif"><a href="{{route('workspace.series.posts',array($series->getKey()))}}">POSTS</a></li>
            <li class="tab-title @if($currentTab == 'collaborators') active @endif"><a href="{{route('workspace.series.collaborators',array($series->getKey()))}}">COLLABORATORS</a></li>
            <li class="tab-title"><a>TIMELINE</a></li>
            <li class="tab-title"><a>FOLLOWERS</a></li>
        </ul>
    </div>
</div>
