<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class RelayTest extends TestCase
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    }

    public function testGetCase1()
    {
        // get post page
        
        #given
        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;

        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postArray['series_id'] = 1;
        $postService = $this->app->make('Saegeul\Post\Post') ;

        $this->signin();
        $originPost = $postService->register($postArray) ;
        $this->signout();

        $user = $this->signinByCollaborator();
        $newPost = $vcsService->fork($originPost->getKey(),$user->getKey());
        $data = [
            'post_id' => $newPost->getKey(),
            'target_id' => $newPost->origin_id,
                'user_id' => $user->getKey(),
                'series_id' => 1,
                'status' => 'merge',
                'comment' => 'post 1 에 입력을 원합니다.'
            ];
        $pullRequest = $vcsService->pullRequest($data);
        $this->signout();
        
        $this->signin();
        $prefix = 'api/v1/relay';
        $data = [];
        $param = [
            'page' => 1,
            'param' => 'abc'
        ];
        $temp = [
            'resource'=>'posts',
            'url'=>'/series/1/posts',
            'data'=> $param
        ];
        $data[] = $temp ;
        $temp = [
            'resource'=>'pullrequest',
            'url'=>'/series/1/pullrequests',
            'data'=> $param
        ];
        $data[] = $temp ;

        #when
        $response = $this->call('GET',$prefix,$data);        

        #then
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result);
    }    
}
