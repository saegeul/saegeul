<?php

namespace spec\Saegeul\Post;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Bus\Dispatcher ;
use Saegeul\Post\Jobs\Register\RegisterCommand ;
use Saegeul\Post\Jobs\Update\UpdateCommand ;
use Saegeul\Post\Jobs\Show\ShowCommand ;
use Saegeul\Post\Jobs\Delete\DeleteCommand ;
use Saegeul\Post\Models\Post as PostModel ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;

class PostSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Post\Post');
    }

    public function let(Dispatcher $dispatcher, PostRepositoryInterface $postRepository)
    {
        $this->beConstructedWith($dispatcher,$postRepository);
    }

    public function it_register(Dispatcher $dispatcher, PostModel $post)
    {
        $data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
            'user_id' => 1,
            'series_id' => 1
        ];
        $command = new RegisterCommand($data) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->register($data)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_update(Dispatcher $dispatcher, PostModel $post)
    {
        $postId = 1;
        $data = [
            'content' => "Laravel utilizes Composer to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine."
        ];
        $command = new UpdateCommand($postId,$data) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->update($postId,$data)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_show(Dispatcher $dispatcher, PostModel $post)
    {
        $postId = 1;
        $command = new ShowCommand($postId) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->show($postId)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_delete(Dispatcher $dispatcher, PostModel $post)
    {
        $postId = 1;
        $command = new DeleteCommand($postId) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->delete($postId)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_get_repository(PostRepositoryInterface $postRepository)
    {
        $this->getRepository()->shouldReturnAnInstanceOf('Saegeul\Post\Contracts\PostRepositoryInterface');
    }
}
