<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class LinkParseTest extends TestCase
{
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    public function testParseForVimeo()
    {
        #given
        $this->signin();

        $prefix = 'api/v1/links/parse';
        $data = [
            'uri' => 'https://vimeo.com/channels/lexusshortfilms/108968882'
            ];

        #when
        $response = $this->call('GET',$prefix,$data, []);

        #then
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);
        $this->assertEquals($arr['data']['type'],'vimeo');
        $this->assertEquals($arr['data']['imageUrl'],'http://i.vimeocdn.com/video/494828514_200x150.jpg');
    }
 
    public function testParse()
    {
        #given
        $this->signin();

        $prefix = 'api/v1/links/parse';
        $data = [
            'uri' => 'https://www.youtube.com/watch?v=QmwEHmFv3zs'
            ];

        #when
        $response = $this->call('GET',$prefix,$data, []);

        #then
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);
        $this->assertEquals($arr['data']['type'],'youtube');
        $this->assertEquals($arr['data']['imageUrl'],'https//img.youtube.com/vi/QmwEHmFv3zs/default.jpg');
    }

    public function test_failed_case_to_get_parse_invalid_no_login()
    {
        $prefix = 'api/v1/links/parse';
        $data = [
            'uri' => 'https://www.youtube.com/watch?v=QmwEHmFv3zs'
            ];
        $response = $this->call('GET',$prefix,$data,[]);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_parse_invalid_host()
    {
        #given
        $this->signin();

        $prefix = 'api/v1/links/parse';
        $data = [
            'uri' => 'https://nate.com/watch?v=QmwEHmFv3zs'
            ];

        #when
        $response = $this->call('GET',$prefix,$data, []);
        $this->assertEquals(404, $response->status());
    }
}
