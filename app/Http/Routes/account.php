<?php

Route::group(array('prefix'=>'account'),function(){
    Route::get('/profile', array('as'=>'account.profile','uses'=>'AccountController@profile'));
    Route::get('/password',array('as'=>'account.password','uses'=>'AccountController@password')); 
    Route::post('/change',array('as'=>'account.change','uses'=>'AccountController@change')); 
});
