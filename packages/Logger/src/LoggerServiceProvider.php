<?php
namespace Saegeul\Logger ;

class LoggerServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->app->bind('Saegeul\Logger\Logger') ;
    }
}
