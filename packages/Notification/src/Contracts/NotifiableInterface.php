<?php
namespace Saegeul\Notification\Contracts ; 

interface NotifiableInterface
{
    public function getNotificationData() ;
}
