<?php

namespace spec\Saegeul\Vcs;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Bus\Dispatcher ;
use Saegeul\Vcs\Jobs\Commit\CommitCommand ;
use Saegeul\Vcs\Jobs\Fork\ForkCommand ;
use Saegeul\Vcs\Jobs\PullRequest\PullRequestCommand ;
use Saegeul\Vcs\Jobs\Merge\MergeCommand ;
use Saegeul\Vcs\Jobs\Import\ImportCommand ;
use Saegeul\Vcs\Models\Revision as RevisionModel ;
use Saegeul\Vcs\Models\PullRequest as PullRequestModel ;
use Saegeul\Post\Models\Post as PostModel;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface;
use Saegeul\Vcs\Contracts\ActivityRepositoryInterface;

class VcsSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Vcs\Vcs');
    }

    public function let(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository, 
        PullRequestRepositoryInterface $pullRequestRepository,
        ActivityRepositoryInterface $activityRepository
    )
    {
        $this->beConstructedWith($dispatcher,$revisionRepository,$pullRequestRepository,$activityRepository);
    }

    public function it_commit(Dispatcher $dispatcher, PostModel $post)
    {
        $postId = 1;
        $command = new CommitCommand($postId);
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->commit($postId)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_fork(Dispatcher $dispatcher, PostModel $post)
    {
        $postId = 2;
        $userId = 2;
        $command = new ForkCommand($postId,$userId);
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->fork($postId,$userId)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_pull_request(Dispatcher $dispatcher, PullRequestModel $pullRequestModel)
    {
        $data = [
            'post_id' => 2,
            'series_id' => 1,
            'status' => 'pull-request',
            'user_id' => 2,
            'comment' => 'post 1 에 입력을 원합니다.'
        ];
        $command = new PullRequestCommand($data);
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($pullRequestModel) ; 
        $this->pullRequest($data)->shouldReturnAnInstanceOf('Saegeul\Vcs\Models\PullRequest');
    }

    public function it_merge(Dispatcher $dispatcher, PostModel $toPost, PostModel $fromPost, PostModel $post)
    {
        $data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
        ];
        $command = new MergeCommand($toPost->getWrappedObject(),$fromPost->getWrappedObject(),$data);
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->merge($toPost,$fromPost,$data)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_import(Dispatcher $dispatcher, PostModel $post)
    {
        $pullRequestId = 1;
        $userId = 2;
        $command = new ImportCommand($pullRequestId,$userId);
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->import($pullRequestId,$userId)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_rollback(Dispatcher $dispatcher, PostModel $post)
    {
        $postId = 1;
        $fromUuid = '1111-222';
        $command = new ImportCommand($postId,$fromUuid);
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($post) ; 
        $this->import($postId,$fromUuid)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }

    public function it_get_revision_repository(RevisionRepositoryInterface $revisionRepository)
    {
        $this->getRevisionRepository()->shouldReturnAnInstanceOf('Saegeul\Vcs\Contracts\RevisionRepositoryInterface');
    }

    public function it_get_pullRequest_repository(PullRequestRepositoryInterface $pullRequestRepository)
    {
        $this->getPullRequestRepository()->shouldReturnAnInstanceOf('Saegeul\Vcs\Contracts\PullRequestRepositoryInterface');
    }
}
