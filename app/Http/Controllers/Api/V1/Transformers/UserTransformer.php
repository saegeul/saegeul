<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Artgrafii\Glauth\Models\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => (int) $user->id,
            'email' => $user->email,
            'last_login' => $user->last_login,
            'username' => $user->last_name.' '.$user->first_name,
            'unique_link' => $user->unique_link,
            'uuid' => $user->uuid,
            'profile_image' => $user->profile_image,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at
        ];
    }
}
