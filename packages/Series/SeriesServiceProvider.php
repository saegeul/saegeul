<?php

namespace Saegeul\Series;

use Illuminate\Support\ServiceProvider ;

class SeriesServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->app->bind('Saegeul\Series\Contracts\SeriesRepositoryInterface',
            'Saegeul\Series\Repositories\SeriesRepository');

        $this->registerEventListener();
    }

    private function registerEventListener()
    {
        $registerConf = $this->app['config']->get('event.register-series'); 
        foreach($registerConf['handler'] as $val){
            \Event::listen($registerConf['name'],$val);
        }

        $updateConf = $this->app['config']->get('event.update-series'); 
        \Event::listen($updateConf['name'],$updateConf['handler']);

        $deleteConf = $this->app['config']->get('event.delete-series'); 
        foreach($deleteConf['handler'] as $val){
            \Event::listen($deleteConf['name'],$val);
        }
    }
}
