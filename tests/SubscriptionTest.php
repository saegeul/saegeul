<?php

use Artgrafii\Glauth\Services\HashCodeService ;

class SubscriptionTest extends TestCase 
{
    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    public function testRegister()
    {
        $user = $this->signinByCollaborator();

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $seriesId = $seriesRepository->lists(1,'')->first()->getKey();

        $data = [
            'series_id' => $seriesId
        ];
        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscription = $subscriptionService->register($data,$user);

        $this->assertInstanceOf('Saegeul\Subscription\Models\Subscription',$subscription);
        $this->assertEquals($subscription->email,$user->email) ; 
        $this->assertEquals($subscription->series_id,$seriesId) ; 
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_register_subscription_invalid_id()
    {
        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesId = 1112;

        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        $data = [
            'series_id' => $seriesId
        ];

        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscription = $subscriptionService->register($data,$user);
    }

    public function testConfirm()
    {
        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscriptionId = 1;

        $hashCodeService = $this->app->make('Artgrafii\Glauth\Services\HashCodeService') ;
        $hashCode = $hashCodeService->encode($subscriptionId);

        $subscription = $subscriptionService->confirm($hashCode);

        $this->assertInstanceOf('Saegeul\Subscription\Models\Subscription',$subscription);
        $this->assertEquals($subscription->getKey(),$subscriptionId) ; 
    }

    /**
     * @expectedException Artgrafii\Glauth\Exceptions\InvalidHashCodeException
     **/
    public function test_failed_case_to_confirm_with_invalid_invitation_code()
    {
        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscriptionId = 1;

        $hashCode = 'aaaa';

        $subscription = $subscriptionService->confirm($hashCode);
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_confirm_with_invalid_invitation_id()
    {
        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscriptionId = 10;

        $hashCodeService = $this->app->make('Artgrafii\Glauth\Services\HashCodeService') ;
        $hashCode = $hashCodeService->encode($subscriptionId);

        $subscription = $subscriptionService->confirm($hashCode);
    }

    public function testCancel()
    {
        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $series = $seriesRepository->lists(1,'')->first();

        $user = $this->signinByCollaborator();

        $email = 'root@saegeul.com';
        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscription = $subscriptionService->cancel($user->email,$series->getKey());

        $this->assertInstanceOf('Saegeul\Subscription\Models\Subscription',$subscription);
        $this->assertEquals($subscription->email,$user->email) ; 
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_cancel_with_invalid_invitation_id()
    {
        $seriesId = 10;

        $email = 'root@saegeul.com';
        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscription = $subscriptionService->cancel($email,$seriesId);
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_cancel_with_invalid_invitation_email()
    {
        $seriesId = 1;

        $email = 'root1@saegeul.com';
        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscription = $subscriptionService->cancel($email,$seriesId);
    }
}
