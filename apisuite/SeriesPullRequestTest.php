<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class SeriesPullRequestTest extends TestCase
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    }

    public function testGetPullRequests()
    {
        #given

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $this->signin();

        #when
        $prefix = 'api/v1/series/'.$series->getKey().'/pullrequests';
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(200, $response->status());

        /*
        $result = $response->getContent();
        $arr = json_decode($result, true);
         */
    }

    public function test_failed_case_to_get_pull_requests_no_login()
    {
        #given
        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        #when
        $prefix = 'api/v1/series/'.$series->getKey().'/pullrequests';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_get_pull_requests_invalid_series_id()
    {
        #given
        $seriesId = 111;

        #when
        $prefix = 'api/v1/series/'.$seriesId.'/pullrequests';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function testShow()
    {
        #given
        $user = $this->signinByCollaborator();

        $series = Factory::build('Saegeul\Series\Models\Series')->first();
        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postArray['user_id'] = $user->getKey();
        $postArray['series_id'] = $series->getKey();
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->register($postArray) ;
        $data = [
            'post_id' => $post->getKey(),
                'series_id' => $series->getKey(),
                'status' => 'import',
                'user_id' => $user->getKey(),
                'comment' => 'post 1 에 입력을 원합니다.'
            ];
        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;
        $pullRequest = $vcsService->pullRequest($data);
        $this->signout();

        $this->signin();
        #when
        $prefix = 'api/v1/series/'.$series->getKey().'/pullrequests/'.$pullRequest->getKey();
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(200, $response->status());

        /*
        $result = $response->getContent();
        $arr = json_decode($result, true);
         */
    }

    public function test_failed_case_to_show_no_login()
    {
        #given
        $pullRequestId = 111;

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        #when
        $prefix = 'api/v1/series/'.$series->getKey().'/pullrequests/'.$pullRequestId;
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_get_show_invalid_series_id()
    {
        #given
        $this->signin();

        $pullRequestId = 111;
        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        #when
        $prefix = 'api/v1/series/'.$series->getKey().'/pullrequests/'.$pullRequestId;
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(404, $response->status());
    }
}
