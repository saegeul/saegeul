<?php

namespace spec\Saegeul\Series\Jobs\Show;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Series\Models\Series as SeriesModel ;

class ShowCommandSpec extends ObjectBehavior
{
    protected $seriesId ;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Series\Jobs\Show\ShowCommand');
    }

    public function let()
    {
        $seriesId = 1;
        $this->seriesId = $seriesId;
        $this->beConstructedWith($seriesId);
    }

    public function it_trigger_handle(SeriesRepositoryInterface $seriesRepository, SeriesModel $series)
    {
        $seriesRepository->find($this->seriesId)->shouldBeCalled()->willReturn($series);
        $this->handle($seriesRepository)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }
}
