<?php

Route::group(array('prefix'=>'guide'),function(){ 


    Route::group(array('prefix'=>'account'),function(){
        Route::get('/profile', array('as'=>'guide.account.profile','uses'=>'Guide\AccountManager@profile'));
        Route::get('/password',array('as'=>'guide.account.password','uses'=>'Guide\AccountManager@password')); 
    }); 

    Route::group(array('prefix'=>'components','namespace'=>'Guide'),function(){
        Route::get('/', array('as'=>'guide.components.index','uses'=>'ComponentController@index'));

    }); 



    Route::group(array('prefix'=>'workspace/series','namespace'=>'Guide\Workspace'),function(){
        Route::get('/', array('as'=>'guide.workspace.series.index','uses'=>'SeriesController@index'));
        Route::get('/emptry', array('as'=>'guide.workspace.series.empty','uses'=>'SeriesController@writeNow'));
        Route::get('/', array('as'=>'guide.workspace.series.index','uses'=>'SeriesController@index'));
        Route::get('/create', array('as'=>'guide.workspace.series.create','uses'=>'SeriesController@create'));
        Route::get('/{id}',array('as'=>'guide.workspace.series.show','uses'=>'SeriesController@show')); 
        Route::get('/{id}/posts',array('as'=>'guide.workspace.series.posts','uses'=>'PostController@bySeries')); 
        Route::get('/{id}/posts/write',array('as'=>'guide.workspace.series.posts.write','uses'=>'PostController@writeForm')); 

    }); 

    Route::group(array('prefix'=>'series','namespace'=>'Guide\Series'),function(){
        Route::get('/{seriesId}',array('as'=>'guide.series.show','uses'=>'SeriesController@show'));
        Route::get('/{seriesId}/posts/{postId}',array('as'=>'guide.series.posts','uses'=>'PostController@show')); 
    });

    Route::get('/signup',array('as' => 'guide.signup' , 'uses' => 'Guide\SignController@signup')) ;
    Route::get('/signin',array('as' => 'guide.signin' , 'uses' => 'Guide\SignController@signin')) ;
    Route::get('/forgot-password',array('as' => 'guide.forgot-password' , 'uses' => 'Guide\SignController@forgotPassword')) ;
    Route::get('/activation',array('as' => 'guide.activation' , 'uses' => 'Guide\SignController@activationForm')) ;

    Route::get('/@{nickname}',array('as' => 'guide.mypage' ,'uses' => 'Guide\MypageController@index')) ;

});
