<?php

namespace spec\Saegeul\Subscription;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Bus\Dispatcher;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface ;
use Saegeul\Subscription\Models\Subscription as SubscriptionModel ;
use Saegeul\Subscription\Jobs\Register\RegisterCommand ;
use Saegeul\Subscription\Jobs\Confirm\ConfirmCommand ;
use Saegeul\Subscription\Jobs\Cancel\CancelCommand ;
use Artgrafii\Glauth\Contracts\UserInterface ;
use Cartalyst\Sentinel\Users\EloquentUser as UserModel ;

class SubscriptionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Subscription\Subscription');
    }

    public function let(Dispatcher $dispatcher, SubscriptionRepositoryInterface $subscriptionRepository)
    {
        $this->beConstructedWith($dispatcher,$subscriptionRepository);
    }

    public function it_register(Dispatcher $dispatcher, SubscriptionModel $series, UserInterface $user)
    {
        $email = 'sihyeong@artgrafii.com';
        $data = [
            'email' => $email,
            'series_id' => 1,
            'user_id' => 1
        ];

        $user->getKey()->shouldBeCalled()->willReturn(1);
        $user->getEmail()->shouldBeCalled()->willReturn($email);
        $command = new RegisterCommand($data) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series) ; 
        $this->register($data,$user)->shouldReturnAnInstanceOf('Saegeul\Subscription\Models\Subscription');
    }

    public function it_confirm(Dispatcher $dispatcher, SubscriptionModel $series)
    {
        $subscriptionCode = 'RDO1aV4l0r42kxXW';
        $command = new ConfirmCommand($subscriptionCode) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series) ; 
        $this->confirm($subscriptionCode)->shouldReturnAnInstanceOf('Saegeul\Subscription\Models\Subscription');
    }

    public function it_cancel(Dispatcher $dispatcher, SubscriptionModel $series)
    {
        $email = 'sihyeong@artgrafii.com';
        $seriesId = 1;
        $command = new CancelCommand($email,$seriesId); 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series); 
        $this->cancel($email,$seriesId)->shouldReturnAnInstanceOf('Saegeul\Subscription\Models\Subscription');
    }

    public function it_get_repository(SubscriptionRepositoryInterface $seriesRepository)
    {
        $this->getRepository()->shouldReturnAnInstanceOf('Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface');
    }
}
