<?php

use Laracasts\TestDummy\Factory;

class NotificationTest extends TestCase 
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        $result = $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $glauth->signout();
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $result = $glauth->signin($user->email,'1q2w3e') ;
    }

	public function testRegister()
	{
        #given 
        $this->signinByCollaborator();

        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->register($postArray) ;

        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;
        $data = [
            'post_id' => $post->getKey(),
                'series_id' => $postArray['series_id'],
                'status' => 'import',
                'user_id' => 2,
                'comment' => 'post 3 에 입력을 원합니다.'
            ];

        
        #when
        $pullRequest = $vcsService->pullRequest($data);

        #then
        $this->assertInstanceOf('Saegeul\Vcs\Models\PullRequest',$pullRequest);
    }
    
    public function testGetNotificationsWithReceiver()
    {
        #given 
        $this->signin();
        $notificationService = $this->app->make('Saegeul\Notification\Notification') ;

        #when
        $notifications = $notificationService->getNotificationWithReceiver(1);

        #then
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator',$notifications);
    }

    public function testShow()
    {
        #given 
        $notificationService = $this->app->make('Saegeul\Notification\Notification') ;

        #when
        $notification = $notificationService->show(1);

        #then
        $this->assertInstanceOf('Saegeul\Notification\Models\Notification',$notification);
        $this->assertEquals($notification->is_confirmed,1) ; 
    }
}
