<?php

namespace Saegeul\Subscription\Jobs\Register;

use Illuminate\Events\Dispatcher;
use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Subscription\Events\SubscriptionWasRegistered ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class RegisterCommand implements SelfHandling
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle(
        Dispatcher $dispatcher, 
        SubscriptionRepositoryInterface $subscriptionRepository,
        SeriesRepositoryInterface $seriesRepository
    )
    {
        $series = $seriesRepository->find($this->data['series_id']);
        if(!$series){
            throw New ModelNotFoundException('Not found series id');
        }

        $subscription = $subscriptionRepository->store($this->data);

        $dispatcher->fire(new SubscriptionWasRegistered($subscription,$series));

        return $subscription ;  
    }
}
