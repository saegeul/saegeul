@extends('layouts.default')

@section('content')
<br/>
<div class="container">

    <div class="mini-form-wrapper">
        <div> 
            Complete To Subscribe for {{ $subscription->series->getTable() }}
            <br>Title : {{ $subscription->series->title }}
            <br>Description : {{ $subscription->series->description }}
            <br>OwnerName : {{ $subscription->series->user->first_name }} {{ $subscription->series->user->last_name }}
            <br>OwnerEmail : {{ $subscription->series->user->email }}
        </div> 
    </div>
</div> 
@stop 
