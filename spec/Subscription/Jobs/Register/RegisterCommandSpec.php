<?php

namespace spec\Saegeul\Subscription\Jobs\Register;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Subscription\Models\Subscription as SubscriptionModel ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Subscription\Events\SubscriptionWasRegistered ;

class RegisterCommandSpec extends ObjectBehavior
{
    protected $data;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Subscription\Jobs\Register\RegisterCommand');
    }

    public function let()
    {
        $email = 'sihyeong@artgrafii.com';
        $data = [
            'email' => $email,
            'series_id' => 1,
            'user_id' => 1
        ];
        $this->data = $data;
        $this->beConstructedWith($data);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        SubscriptionRepositoryInterface $subscriptionRepository, 
        SubscriptionModel $subscription,
        SeriesRepositoryInterface $seriesRepository,
        SeriesModel $series
    )
    {
        $seriesRepository->find($this->data['series_id'])->shouldBeCalled()->willReturn($series);

        $subscriptionRepository->store($this->data)->shouldBeCalled()->willReturn($subscription);

        $dispatcher->fire(new SubscriptionWasRegistered($subscription->getWrappedObject(),$series->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$subscriptionRepository,$seriesRepository)->shouldReturnAnInstanceOf('Saegeul\Subscription\Models\Subscription');
    }
}
