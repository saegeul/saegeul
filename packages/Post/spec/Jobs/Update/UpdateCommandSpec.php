<?php

namespace spec\Saegeul\Post\Jobs\Update;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Post\Models\Post as PostModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Post\Events\PostWasUpdated;

class UpdateCommandSpec extends ObjectBehavior
{
    protected $postId;
    protected $data;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Post\Jobs\Update\UpdateCommand');
    }

    public function let()
    {
        $postId = 1;
        $this->postId = $postId;
        $data = [
            'content' => "Laravel utilizes Composer to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine."
        ];
        $this->data = $data;
        $this->beConstructedWith($postId,$data);
    }

    public function it_trigger_handle(PostRepositoryInterface $postRepository, PostModel $post)
    {
        $postRepository->find($this->postId)->shouldBeCalled()->willReturn($post);
        $postRepository->update($this->postId,$this->data)->shouldBeCalled()->willReturn($post);

        $dispatcher->fire(new PostWasUpdated($post->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$postRepository)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
