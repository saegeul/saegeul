<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class CollaboratorTest extends TestCase
{
    private $prefix = 'api/v1/series/1/collaborators';
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    public function testGetCollaborators()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix);        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);

        $this->assertNotEmpty($arr['data']);
    } 

    public function test_failed_case_to_get_collaborators_invalid_series_id()
    {
        $this->signin();

        $prefix = 'api/v1/series/10/collaborators';
        $response = $this->call('GET',$prefix);        
        $this->assertEquals(404, $response->status());
    } 

    public function testGetWaitings()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix.'/waitings');
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $series = $seriesRepository->find(1);

        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;

        $role = $roleRepository->findBySlug($series->slugForCollaborator());
        $this->assertEquals($arr['data']['0']['role_id'],$role->getKey());
    }

    public function test_failed_case_to_get_waitings_invalid_series_id()
    {
        $this->signin();

        $prefix = 'api/v1/series/10/collaborators';
        $response = $this->call('GET',$prefix.'/waitings');
        $this->assertEquals(404, $response->status());
    }

    public function test_failed_case_to_get_waitings_no_sign()
    {
        $response = $this->call('GET',$this->prefix.'/waitings');
        $this->assertEquals(401, $response->status());
    }

    public function testShowCollaborator()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix.'/2');
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);

        $this->assertEquals($arr['data']['id'], 2);
    }

    public function test_failed_case_to_show_invalid_series_id()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix.'/11');
        $this->assertEquals(404, $response->status());
    }

    public function test_failed_case_to_show_invalid_collaboartor_id()
    {
        $this->signin();

        $prefix = 'api/v1/series/10/collaborators';
        $response = $this->call('GET',$prefix.'/1');
        $this->assertEquals(404, $response->status());
    }

    public function testProcessCollaborators()
    {
        $this->signin();

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $series = $seriesRepository->find(1);

        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;

        $role = $roleRepository->findBySlug($series->slugForCollaborator());  

        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();

        $collaborator = $userRepository->findById(4) ;  
        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $invitation = $invitationService->inviteRole($role->getKey(),$collaborator->email,$series);
        $hashCodeService = $this->app->make('Artgrafii\Glauth\Services\HashCodeService');
        $invitationCode = $hashCodeService->encode($invitation->getKey());
        $invitationService->joinRole($invitationCode,$collaborator->getKey());

        $invitator = $userRepository->findById(5) ;  
        $invitationService->inviteRole($role->getKey(),$invitator->email,$series);

        $response = $this->call('POST',$this->prefix.'/process', ['deletion_user_ids' => [$collaborator->getKey()], 'invitation_emails' => ['sihyeong@artgrafii.com'], 'deletion_waiting_emails' => [$invitator->email]]);

        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);
        $this->assertEquals($arr['data']['id'], 1);
    }

    public function test_failed_case_to_process_collaborators_no_signin()
    {
        $response = $this->call('POST',$this->prefix.'/process', ['deletion_user_ids' => [1], 'invitation_emails' => ['sihyeong@artgrafii.com'], 'deletion_waiting_emails' => ['aaa@artgrafii.com']]);
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_process_collaborators_invalid_series_id()
    {
        $this->signin();

        $prefix = 'api/v1/series/10/collaborators';
        $response = $this->call('POST',$prefix.'/process', ['deletion_user_ids' => [1], 'invitation_emails' => ['sihyeong@artgrafii.com'], 'deletion_waiting_emails' => ['aaa@artgrafii.com']]);
        $this->assertEquals(404, $response->status());
    }
}
