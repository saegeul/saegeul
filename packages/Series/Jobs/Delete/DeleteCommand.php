<?php

namespace Saegeul\Series\Jobs\Delete;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Series\Events\SeriesWasDeleted ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class DeleteCommand implements SelfHandling
{

    public $seriesId ;

    public function __construct($seriesId)
    {
        $this->seriesId = $seriesId ;
    }

    public function handle(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository)
    {
        $series = $seriesRepository->find($this->seriesId);
        if(!$series){
            throw New ModelNotFoundException('Not found series id');
        }
        $deletedSeries = $seriesRepository->delete($this->seriesId);
        if($deletedSeries){
            $dispatcher->fire(new SeriesWasDeleted($deletedSeries));
        }

        return $deletedSeries;
    }
}
