<html>
	<head>
		<title>Laravel</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
            
            .menu li {
                color: #ccc;
                font-weight: 900;
                font-size: 20px;
                padding: 5px 20px;
                border-right: 1px solid #fff;
                float:left;
            }
            .menu ul {
                display: none;
                position: absolute;
            }
            .submenu li {
                color: #fff;
                height: 20px;
                background-color: #ddd;
                font-weight: bold;
                font-size: 12px;
                padding: 5px;
                border-right: 1px solid #fff;
                float:left;
            }
            .current {
                display: block;
            }
		</style>
	</head>
	<body>
		<div class="container">
            <ul class="menu">
                <li id="main-menu-1">About us
                    <ul class="submenu">
                        <li><a href="http://www.hillsfive.co.kr/subpage/about/aboutus.html">동화마을 소개</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/about/aboutus_movie.html">보도자료</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/about/aboutus_exterior.html">전경보기</a></li>
                    </ul>
                </a></li>
                <li id="main-menu-2">Villa Station
                    <ul class="submenu">
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/villa_view.html">동 배치도</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/vip_view.html">VIP</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/rod_view.html">로뎅</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/swe_view.html">스위트</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/bea_view.html">비틀즈</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/moz_view.html">모짜르트</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/cho_view.html">쇼팽</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/mon_view.html">모네</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/bac_view.html">바흐</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/mic_view.html">미켈란젤로</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/pic_view.html">피카소</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/room/dav_view.html">다빈치</a></li>
                    </ul>
                </li>
                <li id="main-menu-3">Enjoy Facility
                    <ul class="submenu">
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_cafe.html">카페</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_sky.html">스카이파크</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_par.html">세미나실</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_kar.html">노래방</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_sau.html">옛날식 황토방</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_swim.html">연못(수영장)</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_outsau.html">야외족욕탕</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_outsau.html">야외온천(VIP)</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_jokgu.html">족구장</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_bbq.html">바베큐</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_vally.html">계곡</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_walk.html">산책/등산</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/enjoy/enjoy_garden.html">자연농원</a></li>
                    </ul>
                </li>
                <li id="main-menu-4">Reservation
                    <ul class="submenu">
                        <li><a href="http://www.hillsfive.co.kr/subpage/reser/reser_guide.html">예약방법/입퇴실안내</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/reser/reser_reser.html">유의사항/환불안내</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/reser/reser_check.html">이용금액</a></li>
                    </ul>
                </a></li>
                <li id="main-menu-5">Traffic Info
                    <ul class="submenu">
                        <li><a href="http://www.hillsfive.co.kr/subpage/traffic/traffic_map.html">동화속마을 약도</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/traffic/traffic_car.html">자가차량</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/traffic/traffic_bus.html">버스안내</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/traffic/traffic_train.html">기차안내</a></li>
                    </ul>
                </li>
                <li id="main-menu-6">Tour Guide
                    <ul class="submenu">
                        <li><a href="http://www.hillsfive.co.kr/subpage/tour/tour_guid.html">주변관광</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/tour/tour_even.html">축제와 행사</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/tour/tour_eat.html">먹거리/특산물</a></li>
                    </ul>
                </li>
                <li id="main-menu-7">Community
                    <ul class="submenu">
                        <li><a href="http://www.hillsfive.co.kr/subpage/commu/commu_eve.html">이벤트</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/commu/commu_not.html">공지사항</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/commu/commu_qna.html">질문과 답변</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/commu/commu_travel.html">여행후기</a></li>
                        <li><a href="http://www.hillsfive.co.kr/subpage/commu/commu_gallery.html">펜션갤러리</a></li>
                    </ul>
                </li>
            </ul>
		</div> 
	</body>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>
        jQuery(function($){
            var selectedId = "";
            $(".menu li").has(".submenu").hover(function(){
                $('#'+selectedId).removeClass("current").children("ul").stop(true,true).css("display","none");
                $(this).children("ul").addClass("current").fadeIn();
            },function(){
                selectedId = $(this).attr('id');
            });
        });
    </script>
</html>
