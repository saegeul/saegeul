<?php

namespace spec\Saegeul\Invitation\Jobs\InviteRole;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Events\Dispatcher;
use Artgrafii\Glauth\Glauth ;
use Artgrafii\Glauth\Services\RoleService ;
use Cartalyst\Sentinel\Sentinel ;
use Cartalyst\Sentinel\Roles\IlluminateRoleRepository ;
use Cartalyst\Sentinel\Roles\EloquentRole ;
use Saegeul\Invitation\Events\UserWasInvitedRole;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface ;
use Saegeul\Invitation\Models\Invitation ;
use Saegeul\Invitation\Contracts\InvitableInterface ;

class InviteRoleCommandSpec extends ObjectBehavior
{
    protected $roleId = 1;
    protected $email = 'sihyeong@artgrafii.com';
    protected $invitable;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Invitation\Jobs\InviteRole\InviteRoleCommand');
    }

    public function let(InvitableInterface $invitable)
    {
        $this->invitable = $invitable ;
        $this->beConstructedWith($this->roleId,$this->email,$invitable);
    }


    public function it_trigger_handle(
        Dispatcher $dispatcher,
        Glauth $glauth, 
        RoleService $roleService,
        IlluminateRoleRepository $roleRepository,
        EloquentRole $roleModel,
        InvitationRepositoryInterface $invitationRepository,
        InvitableInterface $invitableModel,
        Invitation $invitation 
    )
    {
        $this->beConstructedWith($this->roleId,$this->email,$invitableModel);
        
        $glauth->getRoleService()->shouldBeCalled()->willReturn($roleService);
        $roleService->getRoleRepository()->shouldBeCalled()->willReturn($roleRepository);
        $roleRepository->findById($this->roleId)->shouldBeCalled()->willReturn($roleModel);

        $invitableModel->getKey()->shouldBeCalled()->willReturn(1) ;
        $invitableModel->getClassName()->shouldBeCalled()->willReturn('Saegeul\Series\Models\Series') ;

        $formatter =[
            'email' => $this->email,
            'role_id' => $this->roleId,
            'invited_id' =>  1,
            'invited_type' => 'Saegeul\Series\Models\Series'
        ];

        $invitationRepository->findOnWaitingByEmail($formatter['email'], $formatter['role_id'])->shouldBeCalled()->willReturn(null);

        $invitationRepository->store($formatter)->shouldBeCalled()->willReturn($invitation);
        $dispatcher->fire(new UserWasInvitedRole($invitation->getWrappedObject(),$roleModel->getWrappedObject(),$invitableModel->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$glauth,$invitationRepository)->shouldReturnAnInstanceOf('Saegeul\Invitation\Models\Invitation');
    }
}
