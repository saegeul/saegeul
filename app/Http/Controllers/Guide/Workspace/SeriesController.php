<?php
namespace App\Http\Controllers\Guide\Workspace ;

use App\Http\Controllers\Controller ;

class SeriesController extends Controller { 

    public function index()
    {
        return view('guides.workspace.series.index') ;
    }

    public function writeNow()
    {
        return view('guides.workspace.series.empty') ;
    }

    public function create()
    {
        return view('guides.workspace.series.create') ;
    }
}
