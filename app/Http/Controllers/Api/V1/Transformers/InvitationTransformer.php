<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Invitation\Models\Invitation;

class InvitationTransformer extends TransformerAbstract
{
    public function transform(Invitation $invitation)
    {
        return [
            'id' => (int) $invitation->id,
            'email' => $invitation->email,
            'role_id' => $invitation->role_id,
            'response' => $invitation->response,
            'created_at' => $invitation->created_at,
            'updated_at' => $invitation->updated_at
        ];
    }
}
