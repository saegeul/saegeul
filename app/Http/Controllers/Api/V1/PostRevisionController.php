<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Post\Post;
use Saegeul\Vcs\Vcs;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\RevisionTransformer;

class PostRevisionController extends ApiController 
{
    protected $postService;
    protected $vcsService;
    protected $glauth;
    protected $user;
    protected $fractal;

    public function __construct(
        Post $postService, 
        Vcs $vcsService,
        Glauth $glauth, 
        Manager $fractal
    )
	{
        $this->postService = $postService;
        $this->vcsService = $vcsService;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->fractal = $fractal;
	}

    public function index(Request $request, $postId)
    {
        try{
            $post = $this->postService->show($postId);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $page = $request->input('page')?$request->input('page'):1;
            $revisionRepository = $this->vcsService->getRevisionRepository();
            $revisions = $revisionRepository->history($postId,$page);

            $resource = new Fractal\Resource\Collection($revisions, new RevisionTransformer);
            $resource->setPaginator(new IlluminatePaginatorAdapter($revisions));
            $result = $this->fractal->createData($resource)->toJson();

            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function show(Request $request, $postId, $revisionId)
    {
        try{
            $post = $this->postService->show($postId);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $revisionRepository = $this->vcsService->getRevisionRepository();
            $revision = $revisionRepository->find($revisionId);
            if(!$revision){
                return $this->respondNotFound();
            }

            $resource = new Fractal\Resource\Item($revision, new RevisionTransformer);
            $result = $this->fractal->createData($resource)->toJson();

            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
