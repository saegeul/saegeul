@extends("layouts.default")

@section('content')
<br/>
<div class="container">

    <div class="mini-form-wrapper">
        {!! Form::open(['class' =>'clearfix signup-form', 'method' => 'post', 'url' => '/user/nickname']) !!}
            <input type="hidden" class="form-control" name="id" required="required" value="{{ $user->getKey() }}">
            <div class="form-group">
                <label>Your Site Link</label>
                <div class="site-link-wrapper">
                    <span class="placeholder">saegeul.com/</span>
                    <input type="text" class="form-control" name="nickname" required="required">
                </div>
                <p class="alert"> 
                    Your site link is unique. But you can not correct.
                </p>
            </div> 
            <button class="btn btn-block ghost-btn" type="submit">Register a url</button>
        {!! Form::close() !!}
    </div>
</div> 
@stop 
