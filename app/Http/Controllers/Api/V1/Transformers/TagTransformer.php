<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Cartalyst\Tags\IlluminateTag;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;

class TagTransformer extends TransformerAbstract
{
    public function transform(IlluminateTag $tag)
    {
        return [
            'id' => (int) $tag->id,
            'name' => $tag->name,
            'slug' => $tag->slug,
            'count' => $tag->count,
            'namespace' => $tag->namespace,
            'created_at' => $tag->created_at,
            'updated_at' => $tag->updated_at
        ];
    }
}
