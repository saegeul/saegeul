<?php

namespace Saegeul\Vcs\Jobs\PullRequest;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasPullRequested;

class PullRequestCommand implements SelfHandling
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle(
        Dispatcher $dispatcher, 
        PullRequestRepositoryInterface $pullRequestRepository, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository
    )
    {
        $pullRequest = $pullRequestRepository->store($this->data);

        $post = $postRepository->find($pullRequest->post_id);
        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        }

        $post->pull_request_status = $pullRequest->status;
        $post->save();

        $revision = $revisionRepository->findByUuid($post->commit_uuid);
        $revision->pull_request_status = $pullRequest->status;
        $revision->save();

        $dispatcher->fire(new PostWasPullRequested($pullRequest));

        return $pullRequest;
    }
}
