import React from 'react' ; 
import ReactDOM from 'react-dom' ; 
import CreateSeriesDialog from './../CreateSeriesDialog.jsx';

export default class CreateSeriesButton extends React.Component {
    constructor(props){
        super(props) ;
        this.div = null ;
        this.state = {
            dialogStatus : {
                isOpen :false
            }
        }

        this.dialogWasInstantiated = false ;
    }

    render() { 
        return (
            <a className="button round icon first-btn" onClick={this.showDialog.bind(this)}>
                <i className="material-icons icon">create</i>
            </a> 
        )
    }
    
    componentDidMount() {

        let bodyElement = document.body ;
        this.div = document.createElement('div') ;
        bodyElement.appendChild(this.div) ;
        ReactDOM.render(<CreateSeriesDialog dialogStatus={this.state.dialogStatus} closeDialog={this.closeDialog.bind(this)}></CreateSeriesDialog>, this.div) ;
    }

    showDialog() { 
        this.setState({
            dialogStatus : {
                isOpen : true
            }
        }); 

        /*
           if(!this.dialogWasInstantiated){ 
           console.log('init') ;
           let bodyElement = document.body ;
           this.div = document.createElement('div') ;
           bodyElement.appendChild(this.div) ;

           this.dialogWasInstantiated = true ;
           } 
           */
    }

    closeDialog() {
        this.setState({
            dialogStatus : {
                isOpen : false
            }
        }); 
    }
}
