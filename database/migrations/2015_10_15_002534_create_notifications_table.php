<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('receiver');
			$table->string('command');
			$table->integer('pull_request_id')->nullable();
			$table->integer('series_id')->nullable();
			$table->integer('post_id')->nullable();
			$table->string('commit_uuid')->nullable();
			$table->integer('is_confirmed')->default(0);
			$table->integer('user_id');
			$table->timestamp('notified_at');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
