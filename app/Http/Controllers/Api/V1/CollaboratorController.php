<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Series\Series ;
use Saegeul\Invitation\Invitation;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;
use App\Http\Controllers\Api\V1\Transformers\InvitationTransformer;
use App\Http\Controllers\Api\V1\Transformers\CollaboratorTransformer;
use App\Http\Controllers\Api\V1\Transformers\SeriesTransformer;

class CollaboratorController extends ApiController 
{
    protected $glauth ;
    protected $seriesService;
    protected $invitationService;
    protected $roleService;
    protected $userRepository;
    protected $user;
    protected $fractal;

    public function __construct(
        Glauth $glauth, 
        Series $seriesService,
        Invitation $invitationService,
        Manager $fractal
    )
    {
        $this->glauth = $glauth;
        $this->user = $this->glauth->check() ;
        $this->seriesService = $seriesService ;
        $this->invitationService = $invitationService;
        $this->fractal = $fractal;
    }

    public function index(Request $request, $seriesId)
    {
        try{
            $series = $this->seriesService->show($seriesId);

            $this->roleService = $this->glauth->getRoleService();
            $roleRepository = $this->roleService->getRoleRepository() ;
            $role = $roleRepository->findBySlug($series->slugForCollaborator());

            $resource = new Fractal\Resource\Collection($role->users, new UserTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }

    }

    public function show(Request $request, $seriesId, $collaboratorId)
    {
        try{
            $series = $this->seriesService->show($seriesId);

            $userService = $this->glauth->getUserService();
            $userRepository = $userService->getUserRepository();
            $collaborator = $userRepository->find($collaboratorId);

            $this->roleService = $this->glauth->getRoleService();
            $roleRepository = $this->roleService->getRoleRepository() ;
            $role = $roleRepository->findBySlug($series->slugForCollaborator());
            if(!$collaborator || !$collaborator->inRole($role)){
                return $this->respondNotFound('not found collaborator');
            }

            $resource = new Fractal\Resource\Item($collaborator, new UserTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function processCollaborators(Request $request, $id)
    {
        try{
            $series = $this->seriesService->show($id);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $userService = $this->glauth->getUserService();
            $this->userRepository = $userService->getUserRepository();

            $this->roleService = $this->glauth->getRoleService();
            $roleRepository = $this->roleService->getRoleRepository() ;
            $role = $roleRepository->findBySlug($series->slugForCollaborator());  

            \DB::beginTransaction();

            $deleteUserIds = $request->input('deletion_user_ids');
            if($deleteUserIds != "" && is_array($deleteUserIds)){
                $this->deleteRole($deleteUserIds,$role);
            }

            $invitationEmails = $request->input('invitation_emails');
            if($invitationEmails != "" && is_array($invitationEmails)){
                $this->inviteRole($invitationEmails,$role,$series);
            }

            $deletionWaitingEmails = $request->input('deletion_waiting_emails');
            if($deletionWaitingEmails != "" && is_array($deletionWaitingEmails)){
                $this->deleteInvitation($deletionWaitingEmails,$role,$series);
            }
            \DB::commit();

            $resource = new Fractal\Resource\Item($series, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    private function deleteRole($deleteUserIds,$role)
    {
        foreach($deleteUserIds as $id){
            $this->invitationService->detachRole($id,$role->getKey());
        }
    }

    private function inviteRole($invitationEmails,$role,$series)
    {
        foreach($invitationEmails as $email){
            $this->invitationService->inviteRole($role->getKey(),$email,$series);
        }
    }

    private function deleteInvitation($deletionWaitingEmails,$role,$series)
    {
        $invitationRepository = $this->invitationService->getRepository();
        foreach($deletionWaitingEmails as $email){
            $invitation = $invitationRepository->findOnWaitingByEmail($email, $role->getKey());
            if($invitation){
                $invitationRepository->delete($invitation->getKey());
            }
        }
    }

    public function getWaitings(Request $request, $seriesId)
    {
        try{
            $series = $this->seriesService->show($seriesId);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $this->roleService = $this->glauth->getRoleService();
            $roleRepository = $this->roleService->getRoleRepository() ;
            $role = $roleRepository->findBySlug($series->slugForCollaborator());

            $invitationRepository = $this->invitationService->getRepository();
            $waitings = $invitationRepository->findOnWaiting($role->getKey());

            $resource = new Fractal\Resource\Collection($waitings, new InvitationTransformer);

            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }         
}
