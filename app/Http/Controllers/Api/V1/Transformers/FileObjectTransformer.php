<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Artgrafii\SgDrive\Models\FileObject;

class FileObjectTransformer extends TransformerAbstract
{
    public function transform(FileObject $fileObject)
    {
        return [
            'id' => (int) $fileObject->id,
            'encrypted_name' => $fileObject->encrypted_name,
            'original_name' => $fileObject->original_name,
            'size' => $fileObject->size,
            'full_path' => $fileObject->full_path,
            'cdn_full_path' => config('filesystems.disks.softlayer-cdn.cdn-prefix').'/CDN/'.$fileObject->full_path,
            'is_image' => $fileObject->is_image,
            'type' => $fileObject->type,
            'mime_type' => $fileObject->mime_type,
            'height' => $fileObject->height,
            'width' => $fileObject->width,
            'is_cdn' => $fileObject->is_cdn,
            'success' => true,
            'created_at' => $fileObject->created_at,
            'updated_at' => $fileObject->updated_at
        ];
    }
}
