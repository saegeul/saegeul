<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Favorite\Models\Favorite;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;

class FavoriteTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user'];

    public function transform(Favorite $favorite)
    {
        return [
            'id' => (int) $favorite->id,
            'user_id' => $favorite->user_id,
            'favorite_id' => $favorite->favorite_id,
            'favorite_type' => $favorite->favorite_type,
            'created_at' => $favorite->created_at,
            'updated_at' => $favorite->updated_at
        ];
    }

    public function includeUser(Favorite $favorite)
    {
        $user = $favorite->user;

        return $this->item($user, new UserTransformer);
    }
}
