<?php

namespace Saegeul\Series\Events\Handlers ;

use Saegeul\Series\Events\SeriesWasRegistered ;
use Saegeul\Series\Events\SeriesWasDeleted ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Permission\PermissionGenerator ;
use Saegeul\Series\SeriesRoleCollection ;
use Artgrafii\SgDrive\DriveFactory ;
use Saegeul\Favorite\Favorite;

class SeriesEventHandler
{
    protected $glauth;
    protected $permissionGenerator;
    protected $roleCollection ;
    protected $factory ;
    protected $favoriteService;

    public function __construct(
        Glauth $glauth, 
        PermissionGenerator $permissionGenerator, 
        SeriesRoleCollection $roleCollection,
        DriveFactory $factory,
        Favorite $favoriteService
    )
    {
        $this->glauth = $glauth;
        $this->permissionGenerator = $permissionGenerator ;
        $this->roleCollection = $roleCollection ;
        $this->factory = $factory ;
        $this->favoriteService = $favoriteService;
    }

    public function whenSeriesWasRegistered(SeriesWasRegistered $event)
    {
        $series = $event->series;
        $this->addRole($series);
    }

    private function addRole($series)
    {
        $roleService = $this->glauth->getRoleService();

        foreach($this->roleCollection->get() as $roleName => $val){
            $slug = $this->permissionGenerator->generateSlug($series, $roleName);
            $permissions = $this->permissionGenerator->generatePermissions($series, $val);
            $createdRole = $roleService->addRole($roleName,$slug,$permissions);
            if($createdRole->name == $this->roleCollection->getOwner()){
                $roleService->attachRole($series->user_id,$createdRole->getKey());
            }
        }
    }

    public function whenSeriesWasDeleted(SeriesWasDeleted $event)
    {
        $series = $event->series;

        $userService = $this->glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find($series->user_id);

        if($user){
            $drive = $this->factory->makeDrive($user);
            if(isset($series->series_image)){
                $temp = $drive->delete($series->series_image);
            }
        }

        $roleService = $this->glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;

        foreach($this->roleCollection->get() as $roleName => $val){
            $slug = $this->permissionGenerator->generateSlug($series, $roleName);
            $role = $roleRepository->findBySlug($slug);  
            if(count($role->users) > 0){
                foreach($role->users as $key => $user){
                    $roleService->detachRole($user->getKey(),$role->getKey());
                }
            }
            $role->delete();
        }

        $favoriteRepository = $this->favoriteService->getRepository();
        $favorites = $favoriteRepository->findByFavoriteIdOnType($series->getKey(),$series->getClassName());
        foreach($favorites as $key => $favorite){
            $favoriteRepository->delete($favorite->getKey());
        }
    }
}
