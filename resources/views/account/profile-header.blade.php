<div class="profile-header-wrapper">
    <div class="row">
        <div class="small-12">
            <div class="profile-header">
                <h1>Hello, {{ $user->unique_link }}</h1>
            </div>
        </div>
    </div>
    <div class="row"> 
        <ul class="tabs simple-tabs">
            <li class="tab-title @if($action == 'profile') active @endif"><a aria-selected="false" tabindex="-1" href="{{route('account.profile')}}">Profile</a></li>
            <li class="tab-title @if($action == 'password') active @endif"><a aria-selected="false" tabindex="-1" href="{{route('account.password')}}">Password</a></li>
        </ul>
    </div>
</div>
