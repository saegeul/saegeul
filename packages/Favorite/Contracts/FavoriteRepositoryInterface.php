<?php

namespace Saegeul\Favorite\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface FavoriteRepositoryInterface extends RepositoryInterface 
{
}
