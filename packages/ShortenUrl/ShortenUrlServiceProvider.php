<?php

namespace Saegeul\ShortenUrl;

use Illuminate\Support\ServiceProvider;
use Saegeul\ShortenUrl\ShortenUrl;

class ShortenUrlServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $credentials = $this->app['config']->get('finder.youtube'); 

        $this->app->bind('Saegeul\ShortenUrl\Contracts\ShortenUrlInterface',function() use($credentials) {
            $client = new \Google_Client();
            $client->setDeveloperKey($credentials);
            $service = new \Google_Service_Urlshortener($client);
            $url = new \Google_Service_Urlshortener_Url();

            return new ShortenUrl($service,$url) ;
        });
    }
}

