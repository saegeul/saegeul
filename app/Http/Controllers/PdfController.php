<?php namespace App\Http\Controllers;

use Saegeul\Pdf\Pdf;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Saegeul\Post\Post ;
use Saegeul\Series\Series ;

class PdfController extends Controller 
{
    protected $glauth;
    private $pdfService; 
    protected $seriesService;
    protected $postService ; 

    public function __construct(
        Glauth $glauth, 
        Series $seriesService,
        Post $postService,
        Pdf $pdfService
    )
	{
        $this->pdfService = $pdfService;
        $this->seriesService = $seriesService;
        $this->postService = $postService ;
        $this->glauth = $glauth;
        $this->user = $this->glauth->check() ;
	}

	public function getPdfForSeries(Request $request, $seriesId)
	{
        try {
            $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
            if(!$this->user){
                return redirect()->to('/signin?return_url='.$returnUrl);
            }

            $series = $this->seriesService->show($seriesId);
            if($series->status == 'draft' && !$series->isViewableByUser($this->user)){
                return redirect()->back()->withErrors($e->getMessage());
            }

            return $this->pdfService->createForSeries($series->getKey()) ;
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function getPdfForPost(Request $request, $postId)
	{
        try {
            $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
            if(!$this->user){
                return redirect()->to('/signin?return_url='.$returnUrl);
            }

            $post = $this->postService->show($postId);
            $series = $this->seriesService->show($post->series_id);
            if($series->status == 'draft' && !$series->isCreatableByUser($this->user)){
                return redirect()->back()->withErrors($e->getMessage());
            }

            return $this->pdfService->createForPost($post->getKey()) ;
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } 
    }
}
