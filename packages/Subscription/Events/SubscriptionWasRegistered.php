<?php

namespace Saegeul\Subscription\Events ;

use Saegeul\Subscription\Models\Subscription;
use Saegeul\Series\Models\Series;

class SubscriptionWasRegistered 
{
    public $subscription;
    public $series;

    public function __construct(Subscription $subscription, Series $series)
    {
        $this->subscription = $subscription;
        $this->series = $series;
    }
}
