<?php namespace App\Http\Controllers\Oauth;

use App\Http\Controllers\OauthController;

class FacebookController extends OauthController {
    protected $provider = 'facebook';
}
