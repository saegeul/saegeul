<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class SeriesPostTest extends TestCase
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    }

    public function testGetPosts()
    {
        #given
        $user = $this->signinByCollaborator();

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $this->signin();
        #when
        $prefix = 'api/v1/series/'.$series->getKey().'/posts';
        $response = $this->call('GET',$prefix,[]);

        #then
        $this->assertEquals(200, $response->status());
        /*
        $result = $response->getContent();
        $arr = json_decode($result, true);
        dd($arr);
         */
    }

    /*
    public function test_failed_case_to_get_posts_no_login()
    {
        #given
        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        #when
        $prefix = 'api/v1/series/'.$series->getKey().'/posts';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_get_posts_invalid_series_id()
    {
        #given
        $this->signin();

        $seriesId = 111;

        #when
        $prefix = 'api/v1/series/'.$seriesId.'/posts';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(404, $response->status());
    }
     */
}
