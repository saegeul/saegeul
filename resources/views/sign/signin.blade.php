@extends('layouts.default')

@section('contents')
<div class="signup-wrapper">
    <div class="logo-wrapper" >
        <img src="/img/logo/saegeul_wordmark_lg.png" />
    </div>

    {!! Form::open(['class' =>'mini-paper', 'method' => 'post', 'url' => '/signin']) !!}
        <input type="hidden" name="return_url" value="{{ $returnUrl }}">
        <div class="field-group">
            <label>Email</label>
            <div>
                <input type="text" name="email" required="required">
            </div>
        </div>
        <div class="field-group">
            <label>Password</label>
            <div>
                <input type="password" name="password" required="required">
            </div>
        </div> 
        <button class="button expand" type="submit" name="btn-signin">Sign in</button>
        <div class="divider divider-or"></div> 

        <a class="button expand facebook" href="/authorize/facebook"><span class="fi-social-facebook" title="social facebook" aria-hidden="true"></span>&nbsp;Facebook</a>
    {!! Form::close() !!}
    <div class="mini-paper no-padding transparent clearfix" style="margin-top:10px;">
        <div class="left">
                <a class="mini-anchor" href="{{route('forgot-password')}}">Forgot password?</a>
        </div>
        <div class="right">
                <a class="mini-anchor" href="{{route('signup')}}">Do you want to join on saegeul?</a>
        </div> 
    </div>
</div>
@stop
