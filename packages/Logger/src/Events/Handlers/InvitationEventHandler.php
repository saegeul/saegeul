<?php

namespace Saegeul\Logger\Events\Handlers ;

use Saegeul\Logger\Events\Handlers\EventHandler ;

use Saegeul\Invitation\Events\UserWasInvitedRole;
use Saegeul\Invitation\Events\UserWasJoinedRole;
use Saegeul\Invitation\Events\UserWasDetachedRole;

class InvitationEventHandler extends EventHandler
{
    public function whenUserWasInvitedRole(UserWasInvitedRole $event)
    {
        $this->handle($event->invitation,__FUNCTION__);
    }

    public function whenUserWasJoinedRole(UserWasJoinedRole $event)
    {
        $this->handle($event->invitation,__FUNCTION__);
    }

    public function whenUserWasDetachedRole(UserWasDetachedRole $event)
    { 
        $invitation = $event->invitation;

        $this->handle($event->invitation,__FUNCTION__);
    }
}
