<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;

class MypageController extends ApiController 
{
    protected $glauth ;
    protected $user ;

	public function __construct(Glauth $glauth)
    {
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
    }

    public function get($provider)
    {
        if(!$this->user){
            return $this->respondUnauthorized() ;
        }

        $link = $this->user->getSocialByProvider($provider);

        if(!isset($link)){
            return $this->respondUnauthorized('Unauthorized instagram') ;
        }

        return $this->respond(
            array(
                'user'=>$this->user,
                'link'=>$link
            )
        );
    }

    public function getSocials()
    {
        if(!$this->user){
            return $this->respondUnauthorized() ;
        }

        $links = $this->user->getSocials();

        return $this->respond($links);
    }

    public function logout(Request $request, $provider)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $link = $this->user->getSocialByProvider($provider);
            if(!isset($link->oauth2_access_token)){
                return $this->respondUnauthorized('Unauthorized instagram') ;
            }

            \DB::beginTransaction();
            $isDelete = $link->delete();
            \DB::commit();

            return $this->respond([
                'is_delete' => $isDelete
            ]);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    } 

}
}
