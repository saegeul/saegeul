@extends("layouts.series")

@section('contents')
<div class="workspace-wrapper container mini-container">
    <br/>

    @foreach($result as $item)
    <div class="row">
        <div class="text-section">
            <h6 class="title"><a>{{ $item->series->title }}</a></h6> 
            <p>{{$item->series->description}}</p> 
            <div class="datetime">
                Last updated {{ date('M j, Y',strtotime($item->series->updated_at)) }}
            </div>
        </div> 
    </div>
    @endforeach
    @foreach (array_chunk($result,3) as $row)
    <div class="row">
        @foreach ($row as $item)
        <div class="small-6 columns">
            <div class="card">
                <div class="media-section"> 
                    <a><img src="{{ @($item->series->series_image_url)?$item->series->series_image_url:'http://lorempixel.com/800/450/fashion/2' }}" /></a>
                </div> 
                <div class="text-section">
                    <h6 class="title"><a>{{ $item->series->title }}</a></h6> 
                    <div class="datetime">
                        Last updated {{ date('M j, Y',strtotime($item->series->updated_at)) }}
                    </div>
                </div> 
                <div class="footer-section clearfix"> 
                    @if(count($item->collaborators) == 0)
                    <a class="button sg-collaborator-modal" data-series-id="{{ $item->series->getKey() }}" data-reveal-id="collaborator-modal" href="#"><span class="fi-person-genderless" title="person genderless" aria-hidden="true"></span><span>&nbsp;INVITE COLLABORATOR</span></a>
                    @else 
                    <div class="left">
                        <a class="button left sg-collaborator-modal" data-series-id="{{ $item->series->getKey() }}" data-reveal-id="collaborator-modal" >
                            <span class="fi-plus" title="plus" aria-hidden="true"></span> 
                        </a>
                        <ul class="avatar-list">
                            <?php $collaborators = array_slice($item->collaborators->toArray(),0,3); ?>
                            @foreach($collaborators as $user)
                            <li><a class="avatar"><img src="{{ @($user['profile_image'])?$user['profile_image']:'https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg' }}" /></a></li>
                            @endforeach
                            @if(count($item->collaborators) > 3)
                            <li><a class="avatar sg-collaborator-modal" data-series-id="{{ $item->series->getKey() }}" data-reveal-id="collaborator-modal" ><span class="fi-ellipses" title="ellipses" aria-hidden="true"></span></a></li>
                            @endif
                        </ul>
                    </div>
                    @endif
                    <!--<a class="button right"><span class="fi-cog" title="cog" aria-hidden="true"></span></a>-->
                    <a class="button right" data-reveal-id="share-modal" ><span class="fi-share-boxed" title="share boxed" aria-hidden="true"></span></a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endforeach
</div>

@include('workspace.series.partials.share_modal') 
@include('workspace.series.partials.collaborator_modal') 
@stop


@section('links')
@include('workspace.series.partials.links')
@stop

@section('scripts') 
<script>
jQuery(function($){
        var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
        var collaboratorIds = [];
        var deletionUserIds = [];
        var invitationEmails = [];
        var deletionWaitingEmails = [];

        var makeView = function(profileImage,email,description,type,userId){
        var str = '';
        str += '<li class="collaborator">'
        +'<div class="row small-uncollapse medium-uncollapse large-uncollapse">'
        + '<div class="columns small-9">'
        + '<div class="avatar-wrapper">'
        + '<div class="avatar">'
        + '<img src="'+ profileImage + '" />'
        + '</div>'
        + '<div class="description">'
        + '<span class="email">' + email + '</span>'
        + '<small>' + description +'</small>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="columns small-3">'
        + '<a class="button right sg-btn-close" data-user_id="' + userId + '" data-email="' + email + '" data-type="' + type + '">X</a>'
        + '</div>'
        + '</div>'
        + '</li>';

var $str = $(str) ;
$str.appendTo('.collaborators').find('.sg-btn-send-email').click(function(){
        var seriesId = $('input[name="series_id"]').val();
        var email = $(this).data('email');
        var roleId = $('input[name="role_id"]').val();
        $.ajax({
url:'/api/v1/series/'+seriesId+'/invitation',
method:'post',
data: { 'email':email, 'role_id':roleId },
success:function(data){
alert('success');
//console.log(data);
},
error:function(response){
alert('fail');
console.log(response.statusText);
}
});
        });
$str.find('.sg-btn-close').click(function(){
        var userId = $(this).data('user_id'); 
        var email = $(this).data('email'); 
        var type = $(this).data('type'); 
        var row = $(this).parent().parent().parent();

        if(!row.hasClass('removed')){
        row.addClass('removed');
        if(type == 'collaborator'){
        deletionUserIds.push(userId);
        collaboratorIds.splice(jQuery.inArray(userId, collaboratorIds), 1);
        }else if(type == 'waiting'){
        deletionWaitingEmails.push(email);
        }else if(type == 'invitator'){
        invitationEmails.splice(jQuery.inArray(email, invitationEmails), 1);
        }
        }else {
        row.removeClass('removed');
        if(type == 'collaborator'){
        collaboratorIds.push(userId);
        deletionUserIds.splice(jQuery.inArray(userId, deletionUserIds), 1);
        }else if(type == 'waiting'){
            deletionWaitingEmails.splice(jQuery.inArray(email, deletionWaitingEmails), 1);
        }else if(type == 'invitator'){
            invitationEmails.push(email);
        }
        }
});
}

var createCollaboratorsModal = function(collaborators){
    jQuery.each(collaborators, function(i, val){
            var temp = new Date(val.updated_at);
            var acceptedAt =  temp.getDate() + ',' + months[temp.getMonth()] + ',' + temp.getFullYear();
            var description = 'Accepted in ' + acceptedAt;
            makeView(val.profile_image,val.email,description,'collaborator',val.id); 
            collaboratorIds.push(val.id);
            });
};

var createWaitingsModal = function(waitings){
    jQuery.each(waitings, function(i, val){
            var profileImage = 'https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg';
            var temp = new Date(val.updated_at);
            var requestedAt =  temp.getDate() + ',' + months[temp.getMonth()] + ',' + temp.getFullYear();
            var description = 'Requested in ' + requestedAt + ' or <a class="sg-btn-send-email" data-email="' + val.email +'">Re-send a mail</a>';
            makeView(profileImage,val.email,description,'waiting',null); 
            });
};

$('.sg-btn-search-email').click(function(){
        var email = $('input[name="email"]').val();
        var profileImage = 'https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg';
        var description = 'NEW COLLABORATOR';
        if((jQuery.inArray(email, invitationEmails) < 0)){
        invitationEmails.push(email);
        makeView(profileImage,email,description,'invitator',null); 
        }
        });

$('.sg-collaborator-modal').click(function(){
        var seriesId = $(this).data('series-id');
        $('input[name="series_id"]').val(seriesId);

        $.ajax({
url:'/api/v1/series/'+seriesId,
method:'get',
data: { },
success:function(data){
collaboratorIds = [];
deletionUserIds = [];
invitationEmails = [];
deletionWaitingEmails = [];

$('.collaborators').html('');
$('input[name="series_id"]').val('');
$('input[name="role_id"]').val('');
createCollaboratorsModal(data.collaborators);  
createWaitingsModal(data.waitings);
$('input[name="series_id"]').val(seriesId);
$('input[name="role_id"]').val(data.role.id);
},
error:function(response){
console.log(response.statusText);
}
});
}); 

$('.sg-btn-save').click(function(){
        var seriesId = $('input[name="series_id"]').val();
        $.ajax({
url:'/api/v1/series/collaboration',
method:'post',
data: {
'series_id':seriesId,
'collaborator_ids':collaboratorIds,
'deletion_user_ids':deletionUserIds,
'deletion_waiting_emails':deletionWaitingEmails,
'invitation_emails':invitationEmails
},
success:function(data){
alert('success');
location.reload();
console.log(data);
},
error:function(response){
console.log(response.statusText);
}
});
}); 
});
</script>
@stop


