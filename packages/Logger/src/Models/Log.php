<?php
namespace Saegeul\Logger\Models ;

class Log extends \Eloquent
{
    protected $fillable = ['series_id', 'post_id','commit_uuid', 'user_id', 'command','invitation_id','pull_request_id' ] ;

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }
}
