<?php

namespace Saegeul\Vcs\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Vcs\Contracts\ActivityRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Saegeul\Post\Models\Post;

class ActivityRepository extends IlluminateRepository implements ActivityRepositoryInterface
{
    protected $model = 'Saegeul\Vcs\Models\Activity' ;
    

    public function countActivity(Post $post, $userId)
    {
        $activation = $this->findByPostId($post->getKey(),$userId);
        if($activation == 0){
            $data = array();
            $data['post_id'] = $post->getKey();
            $data['series_id'] = $post->series_id;
            $data['user_id'] = $userId;
            $activation = $this->store($data);
        }
        $activation->count = $activation->count + 1;
        $activation->save();
    }

    public function findByPostId($postId,$userId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('post_id', $postId);
        $model = $model->where('user_id', $userId);

        return $model->count();
    }
}
