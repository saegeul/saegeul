<?php

namespace Saegeul\Favorite\Events\Handlers ;

use Saegeul\Favorite\Events\FavoriteWasRegistered ;
use Saegeul\Favorite\Events\FavoriteWasDeleted;
use Saegeul\Favorite\Favorite;
use Saegeul\Series\Series;
use Saegeul\Post\Post;
use Saegeul\Vcs\Vcs;

class FavoriteEventHandler
{
    protected $favoriteService;
    protected $seriesService;
    protected $postService;
    protected $vcsService;

    public function __construct(
        Favorite $favoriteService, 
        Series $seriesService, 
        Post $postService,
        Vcs $vcsService
    )
    {
        $this->favoriteService = $favoriteService;
        $this->seriesService = $seriesService;
        $this->postService = $postService;
        $this->vcsService = $vcsService;
    }

    public function whenFavoriteWasRegistered(FavoriteWasRegistered $event)
    {
        $favorite = $event->favorite;
        $this->countFavorite($favorite);
    }

    public function whenFavoriteWasDeleted(FavoriteWasDeleted $event)
    {
        $favorite = $event->favorite;
        $this->countFavorite($favorite);
    }

    public function countFavorite($favorite)
    {
        $favoriteCount = $favorite->countFavorite();

        if($favorite->favorite_type == 'Saegeul\Series\Models\Series'){
            $series = $this->seriesService->show($favorite->favorite_id);
            $series->updateFavorite($favoriteCount);
        }else if($favorite->favorite_type == 'Saegeul\Post\Models\Post'){
            $post = $this->postService->show($favorite->favorite_id);
            $post->updateFavorite($favoriteCount);

            $revisionRepository = $this->vcsService->getRevisionRepository();
            $revision = $revisionRepository->findByUuid($post->commit_uuid);
            $revision->updateFavorite($favoriteCount);
        }
    }
}
