<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Subscription\Models\Subscription;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;

class SubscriptionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user'];

    public function transform(Subscription $subscription)
    {
        return [
            'id' => (int) $subscription->id,
            'email' => $subscription->email,
            'user_id' => $subscription->user_id,
            'series_id' => $subscription->series_id,
            'activated' => $subscription->activated,
            'created_at' => $subscription->created_at,
            'updated_at' => $subscription->updated_at
        ];
    }

    public function includeUser(Subscription $subscription)
    {
        $user = $subscription->user;

        return $this->item($user, new UserTransformer);
    }
}
