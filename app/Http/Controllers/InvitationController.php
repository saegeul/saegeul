<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Saegeul\Invitation\Invitation;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;

class InvitationController extends Controller 
{
    protected $invitationManager;
    protected $glauth;

    public function __construct(Invitation $invitationManager, Glauth $glauth)
    {
        $this->invitationManager = $invitationManager;
        $this->glauth = $glauth;
        $this->user = $this->glauth->check();
    }

    public function visit(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'invitation_code' => 'required',
            ]);

            if ($v->fails()){
                return redirect()->back()->withErrors($v->errors());
            }

            $invitationCode = $request->input('invitation_code');
            $invitationRepository = $this->invitationService->getRepository();
            if(!$invitation = $invitationRepository->findByCode($invitationCode)){
                return redirect()->back()->withErrors('not found invitation');
            }

            if(!$this->user){
                $data = ['email'=> $invitation->email];
                $userService = $this->glauth->getUserService();
                $userRepository = $userService->getUserRepository();
                $user = $userRepository->findByCredentials($data);
                if(!$user){
                    $returnUrl = '/signnup?return_url='.$request->url().'?invitation_code='.$invitationCode;
                }else{
                    $returnUrl = '/signin?return_url='.$request->url().'?invitation_code='.$invitationCode;
                }
                return redirect()->to($returnUrl);
            }
            $invitedModel = $invitation->invited;

            return view('invitation.visit',compact('invitationCode','invitedModel'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }

    public function join(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'invitation_code' => 'required',
            ]);

            if ($v->fails()){
                return redirect()->back()->withErrors($v->errors());
            }

            if(!$this->user){
                return redirect()->back()->withErrors($v->errors());
            }

            $invitationCode = $request->input('invitation_code');
            $invitation = $this->invitationManager->joinRole($invitationCode,$this->user->getKey());
            $invitedModel = $invitation->invited;

            return view('invitation.join',compact('invitation','invitedModel'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }
}
