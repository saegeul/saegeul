import React from 'react' ; 
import TrixEditor from './TrixEditor.jsx' ;

export default class TrixEditorContainer extends React.Component {

    constructor(props) {
        super(props) ; 
        this.state =  {
            html: '<b>Edit me</b>'
        }
    } 

    handleChange(innerHTML) {
        this.setState({html: innerHTML})
    }

    render() {
        const {html} = this.state ; 

        return (
            <TrixEditor value={html} onChange={this.handleChange.bind(this)} />
        ); 
    }

}
