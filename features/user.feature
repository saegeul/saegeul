Feature: user 
    Saegeul

    Scenario: Home Page
        Given I am on the homepage
        Then I should see "Laravel 5"

    Scenario: User sign up 
        When I register "root@saegeul.com" "1q2w3e" "1q2w3e" "root"
        Then I go to "complete-to-register" 

    Scenario: Failed sign up by duplicate email
        When I register "root@saegeul.com" "1q2w3e" "1q2w3e" "root"
        Then I move backward one page 
        And I should see "The email has already been taken." 

    Scenario: Failed sign up by unique link
        When I register "aaa@saegeul.com" "1q2w3e" "1q2w3e" "root"
        Then I move backward one page 
        And I should see "The unique link has already been taken."

    Scenario: Failed confirm activation
        When I go to "activation/confirm?hashCode=111&code=Q8iN4aKAPTZmBXcHDUBXDRDd6y4gWhZB"
        Then I should see "Invalidate Hash Code"

    Scenario: Failed sign in by missing activation 
        When I sign in "root@saegeul.com" "1q2w3e"
        Then I should see "Your account has not been activated yet." 

    Scenario: USER sign in 
        Given I confirm user activation "root@saegeul.com"
        And I go to "signin"
        When I sign in "root@saegeul.com" "1q2w3e"
        Then I go to "/"

    Scenario: Failed sign in by invalid data
        When I sign in "root@saegeul.com" "222"
        Then I move backward one page 
        Then I should see "Incorrect email or password" 

