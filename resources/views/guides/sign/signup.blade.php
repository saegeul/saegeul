@extends("layouts.default_for_guide")

@section('contents')
<div class="signup-wrapper">
    <div class="logo-wrapper" >
        <img src="/img/logo/saegeul_wordmark_lg.png" />
    </div>

    <form class="mini-paper">
        <div class="field-group">
            <label>Email</label>
            <div>
                <input type="text" />
            </div>
        </div>
        <div class="field-group">
            <label>Password</label>
            <div>
                <input type="password" />
            </div>
        </div>
        <div class="field-group">
            <label>Confirm Password</label>
            <div>
                <input type="password" />
            </div>
        </div> 
        <div class="field-group">
            <label>Your URL</label>
            <div id="url-wrapper">
                <span class="prefix">http://www.saegeul.com/@</span>
                <input type="text" />
            </div>
        </div>

        <a class="button expand "><i></i>Just Write</a>
        <div class="divider divider-or"></div> 

        <a class="button expand facebook"><span class="fi-social-facebook" title="social facebook" aria-hidden="true"></span>&nbsp;Facebook</a>
    </form>
    <p class="welcome-text">If you already are a memeber, <a href="/signin">sign in</a>.</p>
</div>
@stop
