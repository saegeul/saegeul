<?php
namespace Saegeul\Notification;

use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Saegeul\Notification\Contracts\NotificationRepositoryInterface',
            'Saegeul\Notification\Repositories\NotificationRepository');

        //$this->app->bind('Saegeul\Notification\Notification') ;
    }
}
