import React from 'react' ; 
import FancySectionList from './FancySectionList.jsx' ;

export default class CoverBuilder extends React.Component { 

    constructor(props) {
        super(props); 
    } 

    render() {
        return (
            <div> 
                <FancySectionList sectionList = {this.props.sectionList}></FancySectionList>
            </div>
        );
    } 
}
