<?php
namespace Saegeul\Logger;
use Saegeul\Logger\UnitTester;
use Saegeul\Logger\Logger ;
use Saegeul\Logger\Contracts\LoggableInterface ;
use Artgrafii\Glauth\Glauth ;
use \Codeception\Util\Stub ;

class LoggerCest
{
    public function _before(UnitTester $I)
    {
    }

    public function _after(UnitTester $I)
    {
    }

    // tests
    public function testToLog(UnitTester $I)
    { 

        $user = Stub::make('Artgrafii\Glauth\Models\User', ['getKey' => 1]); 
        $glauth = Stub::make('Artgrafii\Glauth\Glauth', ['check' => $user]); 
        
        \App::bind('Saegeul\Logger\Logger',function() use($glauth){
            return new \Saegeul\Logger\Logger(\App::make('Saegeul\Logger\Repositories\LogRepository') , $glauth) ;
        });

        $logger = \App::make('Saegeul\Logger\Logger') ; 
        $eventName = "whenPostWasRegistered" ; 
        $data = [ 
            'series_id' => 1 ,
            'post_id' => 1  ,
        ] ;

        $loggableData = new LoggableData($data) ;

        $ret = $logger->log($loggableData,$eventName) ;

        $I->assertEquals($data['series_id'] , $ret->series_id) ;
        $I->assertEquals($user->getKey()  , $ret->user_id) ;
        $I->assertEquals($eventName , $ret->command) ;
        $I->assertEquals($data['post_id'] , $ret->post_id) ;
    }
}

class LoggableData implements LoggableInterface
{
    protected $data ;
    protected $user ;

    public function __construct($data)
    {
        $this->data = $data ;
    }

    public function getLoggedData()
    {
        return $this->data ;
    }
}
