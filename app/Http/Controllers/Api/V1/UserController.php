<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController;
use Illuminate\Http\Request;
use Artgrafii\Glauth\Glauth ;

class UserController extends ApiController 
{
    protected $user;
    protected $glauth ;

    public function __construct(Glauth $glauth)
    {
        $this->glauth = $glauth;
        $this->user = $this->glauth->check() ;
    }

    public function findByEmail(Request $request)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $v = \Validator::make($request->all(), [
                'email' => 'required|email',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $email = $request->input('email');
            $userService = $this->glauth->getUserService();
            $userRepository = $userService->getUserRepository();
            $user = $userRepository->findByCredentials(compact('email'));
            if(!$user){
                return $this->respondNotFound();
            }
            return $this->respond($user);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
