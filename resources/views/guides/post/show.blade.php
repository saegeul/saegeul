@extends("layouts.post_layout_for_guide")

@section('contents')

<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap"> 
        <div>
            <img src="/img/logo/saegeul_wordmark.png" />
        </div>
      <div>
          <nav class="top-bar" data-topbar role="navigation"> 
              <ul class="title-area">
                  <li class="name">
                  <h1><a class="left-off-canvas-toggle" href="#" ><span class="fi-menu" title="menu" aria-hidden="true"></span></a></h1>
                  </li>
                  <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
              </ul> 
              
              <section class="top-bar-section">
                  <ul class="right">
                      <li>
                      <a href="#" class="button small">Subscribe</a>
                      </li>
                  </ul> 
              </section>
          </nav>
      </div>
        <aside class="left-off-canvas-menu">
            <ul class="table-of-contents">
                <li> <a>Introduce. Superstar K</a> </li>
                <li> <a>Let's talk about K-Pop</a> </li>
                <li> <a>Do you know Gian T</a>  </li>
                <li> <a>Introduce. Superstar K</a> </li>
            </ul>
            <p>
                This series was published by saegeul.
            </p>
            <div>
                <a class="button">My Workspace</a>
            </div>
        </aside>
        <div class="post-wrapper">
            <div class="header post-header" style="background-image:url('')" >
                <div class="header-inner"> 
                    <span>in Laravel Super Guide</span>
                    <h2>Introduce. Superstar K </h2>
                    <p>
                    </p>
                </div>
            </div>
            <div class="post-content">
                <blockquote>
                    “the way to create art is to burn and destroy
                    ordinary concepts and to substitute them
                    with new truths that run down from the top of the head
                    and out of the heart” -Charles Bukowski
                </blockquote>
                <p>
                Those of us living on the west coast of the United States have been experiencing a pretty severe drought this year, with many places — including California — having had this last for many years. Have a listen to Josh Ritter sing about one of the inevitable consequences of drought, Wildfires:
                </p>
                <p>
                <iframe width="700" height="393"  src="https://www.youtube.com/embed/oXK-0Cie57s" frameborder="0" allowfullscreen></iframe>
                </p>
                <p>
                while you consider the fact that prolonged droughts, when accompanied by long, hot summers, often lead to easily flammable terrain. And that’s when the aforementioned disaster — wildfires — tend to be at their worst.
                </p> 
                <div class="comment-wrapper">
                    <div class="comment-inner">
                        @include('guides.post.comment')
                    </div>
                </div>
            </div> 
        </div>
        <!-- main content goes here -->

        <!-- close the off-canvas menu -->
        <a class="exit-off-canvas"></a>

    </div>
</div>
@stop
