<?php namespace App\Http\Controllers\Api\V1 ;

use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController 
{
    protected $statusCode = 200 ; 

    public function getStatusCode()
    {
        return $this->statusCode ;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode ;
        return $this ;
    }

    public function respondInvalidData($message = 'Invalid Data!')
    {
        return $this->setStatusCode(422)->respondWithError($message) ; 
    }

    public function respondNotFound($message = 'Not Found!')
    {
        return $this->setStatusCode(404)->respondWithError($message) ; 
    }

    public function respondInternalError($message = 'Internal error!')
    {
        return $this->setStatusCode(500)->respondWithError($message) ; 
    }

    public function respondUnauthorized($message = 'Unauthorized!')
    {
        return $this->setStatusCode(401)->respondWithError($message) ; 
    }

    public function respond($data, $headers = [])
    {
        return response($data, $this->getStatusCode());
    } 

    public function respondWithError($message)
    {
        return response()->json([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ],$this->getStatusCode());
    }
}
