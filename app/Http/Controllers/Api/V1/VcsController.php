<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Post\Post ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Vcs\Vcs;
use Saegeul\Series\Series ;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\PostTransformer;
use App\Http\Controllers\Api\V1\Transformers\PullRequestTransformer;
use App\Http\Controllers\Api\V1\Transformers\RevisionTransformer;

class VcsController extends ApiController 
{
    protected $postService;
    protected $glauth;
    protected $user;
    protected $seriesService;
    protected $vcsService;
    protected $fractal;

    public function __construct(
        Post $postService, 
        Glauth $glauth, 
        Series $seriesService, 
        Vcs $vcsService,
        Manager $fractal
    )
	{
        $this->postService = $postService ;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->seriesService = $seriesService ;
        $this->vcsService = $vcsService;
        $this->fractal = $fractal;
	}

    public function fork(Request $request, $id)
    {
        try {
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $post = $this->postService->show($id);
            $series = $this->seriesService->show($post->series_id);
            if(!$series->isWriteableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $postRepository = $this->postService->getRepository();
            if($postRepository->isExisted($this->user->getKey(),$post->getKey())){
                return $this->respondInvalidData('Already exist post');
            }

            \DB::beginTransaction();

            $newPost = $this->vcsService->fork($post->getKey(),$this->user->getKey());

            \DB::commit();

            $resource = new Fractal\Resource\Item($newPost, new PostTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function checkDirty(Request $request, $id)
    {
        try{
            $post = $this->postService->show($id);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            } 
            
            $revisionRepository = $this->vcsService->getRevisionRepository();
            $sourceRevision = $revisionRepository->findByUuid($post->source_uuid);
            $originPost = $this->postService->show($sourceRevision->post_id);

            $dirtyStatus = false;
            if($post->source_uuid != $originPost->commit_uuid){
                $dirtyStatus = true;
            }

            return $this->respond(['dirtyStatus' => $dirtyStatus]);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function commit(Request $request, $id)
    {
        try {
            $post = $this->postService->show($id);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            } 
            
            \DB::beginTransaction();

            $newPost = $this->vcsService->commit($post->getKey());

            \DB::commit();

            $resource = new Fractal\Resource\Item($newPost, new PostTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function rollback(Request $request, $id)
    {
        try{
            $v = \Validator::make($request->all(), [
                'uuid' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }
            $uuid = $request->input('uuid');
            
            $post = $this->postService->show($id);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            } 
            
            \DB::beginTransaction();

            $newPost = $this->vcsService->rollback($post->getKey(),$uuid);

            \DB::commit();

            $resource = new Fractal\Resource\Item($newPost, new PostTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function pullRequest(Request $request, $id)
    {
        try{
            $v = \Validator::make($request->all(), [
                'series_id' => 'required',
                'comment' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $post = $this->postService->show($id);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            \DB::beginTransaction();

            $pullRequestRepository = $this->vcsService->getPullRequestRepository();
            $pullRequest = $pullRequestRepository->getByPostId($post->getKey());
            if($pullRequest){
                $pullRequestRepository->delete($pullRequest->getKey());
            }

            $formData = array();
            $formData['post_id'] = $post->getKey();
            $formData['target_id'] = $post->origin_id;
            $formData['series_id'] = $request->input('series_id');
            if(is_null($post->origin_id)){
                $formData['status'] = 'import';
            }else{
                $formData['status'] = 'merge';
            }
            $formData['user_id'] = $this->user->getKey();
            $formData['comment'] = $request->input('comment');  
            
            $pullRequest = $this->vcsService->pullRequest($formData);
            \DB::commit();

            $resource = new Fractal\Resource\Item($pullRequest, new PullRequestTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function merge(Request $request, $id)
    {
        try{
            $v = \Validator::make($request->all(), [
                'from_post_id' => 'required',
                'title' => 'required',
                'description' => 'required',
                'content' => 'required',
                'html' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $toPost = $this->postService->show($id);
            if(!$toPost->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $fromPostId = $request->input('from_post_id');
            $fromPost = $this->postService->show($fromPostId);
            if(!$fromPost){
                return $this->respondNotFound();
            }

            $series = $this->seriesService->show($fromPost->series_id);
            if(!$series){
                return $this->respondNotFound();
            }

            if($series->getKey() != $toPost->series_id){
                return $this->respondUnauthorized() ;
            }  

            $formData = array();
            $formData['title'] = $request->input('title');
            $formData['description'] = $request->input('description');
            $formData['content'] = $request->input('content');
            $formData['html'] = $request->input('html');
            $formData['post_image_url'] = $request->input('post_image_url');
            $formData['post_image'] = $request->input('post_image');
            
            \DB::beginTransaction();

            $newPost = $this->vcsService->merge($toPost,$fromPost,$formData);

            \DB::commit();

            $resource = new Fractal\Resource\Item($newPost, new PostTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        }catch(\Exception $e) {
            \DB::rollback();
            dd($e->getMessage());
            return $this->respondInternalError() ; 
        }
    }

    public function import(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'pull_request_id' => 'required'
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $pullRequestId = $request->input('pull_request_id');
            $pullRequestRepository = $this->vcsService->getPullRequestRepository();
            $pullReqeust = $pullRequestRepository->find($pullRequestId);
            if(!$pullReqeust){
                return $this->respondNotFound();
            }

            $series = $this->seriesService->show($pullReqeust->series_id);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            \DB::beginTransaction();

            $newPost = $this->vcsService->import($pullRequestId,$this->user->getKey());

            \DB::commit();

            $resource = new Fractal\Resource\Item($newPost, new PostTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }
}
