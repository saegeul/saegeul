<?php

namespace Saegeul\Finder\Contracts;

interface FinderServiceInterface 
{
    public function find($keyword,$page);
}
