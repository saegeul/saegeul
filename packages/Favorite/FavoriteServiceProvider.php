<?php

namespace Saegeul\Favorite;

use Illuminate\Support\ServiceProvider ;

class FavoriteServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->app->bind('Saegeul\Favorite\Contracts\FavoriteRepositoryInterface',
            'Saegeul\Favorite\Repositories\FavoriteRepository');

        $this->registerEventListener();
    }

    private function registerEventListener()
    {
        $registerConf = $this->app['config']->get('event.register-favorite'); 
        \Event::listen($registerConf['name'],$registerConf['handler']);

        $deleteConf = $this->app['config']->get('event.delete-favorite'); 
        \Event::listen($deleteConf['name'],$deleteConf['handler']);
    }
}
