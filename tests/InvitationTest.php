<?php

use Ramsey\Uuid\Uuid;
use Artgrafii\Glauth\Services\HashCodeService ;

class InvitationTest extends TestCase 
{ 
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->all()->first();

        $result = $glauth->signin($user->email,'1q2w3e') ;
    }

    public function testInviteRole()
    {
        $this->signin();

        $glauth = $this->app->make('Artgrafii\Glauth\Glauth');
        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;

        $role = $roleRepository->createModel()->where('name','collaborator')->first();  

        $seriesRepository = $this->app->make('Saegeul\Series\Contracts\SeriesRepositoryInterface') ;
        $series = $seriesRepository->lists(1,'')->first();

        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->all()->first();
        $email = $user->email;

        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $invitation = $invitationService->inviteRole($role->getKey(),$email,$series);

        $this->assertInstanceOf('Saegeul\Invitation\Models\Invitation',$invitation);
        $this->assertEquals($invitation->email,$email) ; 
        $this->assertEquals($invitation->role_id,$role->getKey()) ; 
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_invite_role_with_invalid_role_id()
    {
        $invalidRoleId = 31;
        $email = '222@artgrafii.com';

        $seriesRepository = $this->app->make('Saegeul\Series\Contracts\SeriesRepositoryInterface') ;
        $series = $seriesRepository->lists(1,'')->first();

        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $result = $invitationService->inviteRole($invalidRoleId,$email,$series);
    }

    /**
     * @expectedException ErrorException
     **/
    public function test_failed_case_to_invite_role_with_invalid_data()
    {
        $invalidRoleId = 31;

        $seriesRepository = $this->app->make('Saegeul\Series\Contracts\SeriesRepositoryInterface') ;
        $series = $seriesRepository->lists(1,'')->first();

        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $result = $invitationService->inviteRole($invalidRoleId,$email,$series);
    }

    public function testJoinRole()
    {   
        $this->signin();

        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->all()->first();
        $email = $user->email;

        $invitationRepository = $this->app->make('Saegeul\Invitation\Contracts\InvitationRepositoryInterface') ;
        $invitation = $invitationRepository->lists(1,'')->first();

        $hashCodeService = $this->app->make('Artgrafii\Glauth\Services\HashCodeService');
        $invitationCode = $hashCodeService->encode($invitation->getKey());

        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $joinedInvitation = $invitationService->joinRole($invitationCode,$user->getKey());

        $this->assertInstanceOf('Saegeul\Invitation\Models\Invitation',$joinedInvitation);
        $this->assertEquals($joinedInvitation->getKey(),$invitation->getKey()) ; 
        $this->assertEquals($joinedInvitation->response,1) ; 
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_join_role_with_invalid_user_id()
    {
        $invitationRepository = $this->app->make('Saegeul\Invitation\Contracts\InvitationRepositoryInterface') ;
        $invitation = $invitationRepository->lists(1,'')->first();

        $hashCodeService = $this->app->make('Artgrafii\Glauth\Services\HashCodeService');
        $invitationCode = $hashCodeService->encode($invitation->getKey());

        $userId = '1111';
        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $joinedInvitation = $invitationService->joinRole($invitationCode,$userId);
    }

    /**
     * @expectedException Artgrafii\Glauth\Exceptions\InvalidHashCodeException
     **/
    public function test_failed_case_to_join_role_with_invalid_invitation_code()
    {
        $userKey = '1111';
        $invitationCode = '1111';
        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $joinedInvitation = $invitationService->joinRole($invitationCode,$userKey);
    }

    public function testDetachRole()
    {   
        $this->signin();

        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->all()->first();

        $glauth = $this->app->make('Artgrafii\Glauth\Glauth');
        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;
        $role = $roleRepository->createModel()->where('name','collaborator')->first();  

        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $detachedInvitation = $invitationService->detachRole($user->getKey(),$role->getKey());

        $invitationRepository = $this->app->make('Saegeul\Invitation\Contracts\InvitationRepositoryInterface') ;
        $invitation = $invitationRepository->lists(1,'')->first();

        $this->assertInstanceOf('Cartalyst\Sentinel\Roles\EloquentRole',$detachedInvitation);
        $this->assertEquals($detachedInvitation->getKey(),$invitation->role_id) ; 
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_detach_role_with_invalid_user_id()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth');
        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;
        $role = $roleRepository->createModel()->where('name','collaborator')->first();  

        $userId = '1111';
        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $joinedInvitation = $invitationService->detachRole($userId,$role->getKey());
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_detach_role_with_invalid_role_id()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->all()->first();

        $roleId = 2222;
        $invitationService = $this->app->make('Saegeul\Invitation\Invitation') ;
        $joinedInvitation = $invitationService->detachRole($user->getKey(),$roleId);
    }
}
