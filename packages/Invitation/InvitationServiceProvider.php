<?php

namespace Saegeul\Invitation;

use Illuminate\Support\ServiceProvider ;

class InvitationServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->app->bind('Saegeul\Invitation\Contracts\InvitationRepositoryInterface',
            'Saegeul\Invitation\Repositories\InvitationRepository');

        $this->registerEventListener();
    }

    private function registerEventListener()
    {
        $inviteConf = $this->app['config']->get('event.invite-role'); 
        foreach($inviteConf['handler'] as $val){
            \Event::listen($inviteConf['name'],$val);
        }

        $joinConf = $this->app['config']->get('event.join-role'); 
        \Event::listen($joinConf['name'],$joinConf['handler']);

        $detachConf = $this->app['config']->get('event.detach-role'); 
        \Event::listen($detachConf['name'],$detachConf['handler']);
    }
}
