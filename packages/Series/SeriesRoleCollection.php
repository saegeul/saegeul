<?php

namespace Saegeul\Series ;

class SeriesRoleCollection
{
    protected $roleCollection = [
        'owner' => ['create','view','update','delete','write'], 
        'collaborator' => ['view','write'], 
        'viewer' => ['view']
    ];

    public function get()
    {
        return $this->roleCollection ;
    } 

    public function getOwner()
    {
        return 'owner';
    }

    public function getCollaborator()
    {
        return 'collaborator';
    }
}
