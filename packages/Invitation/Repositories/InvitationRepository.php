<?php

namespace Saegeul\Invitation\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Artgrafii\Glauth\Services\HashCodeService ;
use Saegeul\Database\Repository\Contracts\ModelFactoryInterface ;

class InvitationRepository extends IlluminateRepository implements InvitationRepositoryInterface
{
    protected $model = 'Saegeul\Invitation\Models\Invitation' ;
    protected $hashCodeService;

    public function __construct(ModelFactoryInterface $modelFactory, HashCodeService $hashCodeService)
    {
        parent::__construct($modelFactory);

        $this->hashCodeService = $hashCodeService;
    }

    public function findByCode($code)
    {
        $invitationId  = $this->hashCodeService->decode($code);
        return $this->find($invitationId);
    }

    public function findOnWaitingByEmail($email, $roleId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('role_id', $roleId);
        $model = $model->where('response', 0);
        return $model->Email($email)->first();
    }

    public function lists($page, $keyword)
    {
        $model = $this->modelFactory->create($this->model) ;
        if($keyword != ""){
            $model = $model->where('title', 'like', "%$keyword%");
        }
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function findOnWaiting($roleId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('role_id', $roleId);
        $model = $model->where('response', 0);

        return $model->get();
    }

    public function findByUserEmailOnRole($email, $roleId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->Email($email);
        $model = $model->where('role_id', $roleId);
        $model = $model->where('response', 1);

        return $model->first();
    }
}
