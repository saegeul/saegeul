<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class PostPullRequestTest extends TestCase
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    }

    public function testGetPosts()
    {
        #given
        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;

        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postArray['series_id'] = 1;
        $postService = $this->app->make('Saegeul\Post\Post') ;

        $this->signin();
        $originPost = $postService->register($postArray) ;
        $this->signout();

        $user = $this->signinByCollaborator();
        $newPost = $vcsService->fork($originPost->getKey(),$user->getKey());
        $data = [
            'post_id' => $newPost->getKey(),
            'target_id' => $newPost->origin_id,
                'user_id' => $user->getKey(),
                'series_id' => 1,
                'status' => 'merge',
                'comment' => 'post 1 에 입력을 원합니다.'
            ];
        $pullRequest = $vcsService->pullRequest($data);
        $this->signout();
        
        $this->signin();
        #when
        $prefix = 'api/v1/posts/'.$pullRequest->target_id.'/pullrequest';
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(200, $response->status());
        /*
        $result = $response->getContent();
        $arr = json_decode($result, true);
        dd($arr['data']);
         */
    }

    public function test_failed_case_to_get_posts_no_login()
    {
        #given
        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/pullrequest';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_get_posts_invalid_post_id()
    {
        #given
        $this->signin();
        $postId = 111;

        #when

        $prefix = 'api/v1/posts/'.$postId.'/pullrequest';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(404, $response->status());
    }
}
