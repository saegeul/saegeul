<?php namespace App\Http\Controllers\Api\V1\Finder;

use App\Http\Controllers\Api\V1\FinderController;
use Illuminate\Http\Request ;
use Saegeul\Finder\Finder;
use Artgrafii\Glauth\Glauth ;

class InstagramController extends FinderController
{
    protected $provider = 'instagram';

    public function find(Request $request)
    {
        if(!$this->user){
            return $this->respondUnauthorized() ;
        }

        $link = $this->user->getSocialByProvider($this->provider);
        if(!isset($link->oauth2_access_token)){
            return $this->respondInternalError('instagram error');
        }

        $this->options =  [
            'accessToken' => $link->oauth2_access_token
        ];

        return parent::find($request,$this->provider);
    }
}
