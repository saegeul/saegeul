<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Cover\Cover;
use Saegeul\Series\Series ;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\CoverTransformer;

class CoverController extends ApiController 
{
    protected $glauth;
    protected $user;
    protected $coverService;
    protected $seriesService;
    protected $fractal;

    public function __construct(
        Glauth $glauth, 
        Series $seriesService,
        Cover $coverService,
        Manager $fractal
    )
	{
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->coverService = $coverService ;
        $this->seriesService = $seriesService ;
        $this->fractal = $fractal;
	}

    public function index(Request $request, $seriesId)
    {
        
    }

    public function show(Request $request, $seriesId, $coverId)
    {
        try{
            $cover = $this->coverService->show($coverId);

            $resource = new Fractal\Resource\Item($cover, new CoverTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function store(Request $request, $seriesId)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $v = \Validator::make($request->all(), [
                'content' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $series = $this->seriesService->show($seriesId);
            if(!$series->isUpdatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $formData = [];
            $formData['content'] = $request->input('content');
            $formData['series_id'] = $seriesId;
            $cover = $this->coverService->createWithUser($formData);

            $resource = new Fractal\Resource\Item($cover, new CoverTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function update(Request $request, $seriesId, $coverId)
    {
        try {
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $series = $this->seriesService->show($seriesId);
            if(!$series->isUpdatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $data = $request->all();
            $cover = $this->coverService->update($coverId,$data);

            $resource = new Fractal\Resource\Item($cover, new CoverTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function destroy(Request $request, $seriesId, $coverId)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $series = $this->seriesService->show($seriesId);
            if(!$series->isUpdatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $result = $this->coverService->delete($coverId);

            $resource = new Fractal\Resource\Item($result, new CoverTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
