<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\NotificationTransformer;
use Saegeul\Notification\Notification ;

class NotificationController extends ApiController 
{
    protected $glauth ;
    protected $user ;
    protected $notificationService;
    protected $fractal;

    public function __construct(
        Notification $notificationService,
        Glauth $glauth,
        Manager $fractal
    )
	{
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->notificationService = $notificationService;
        $this->fractal = $fractal;
	}

    public function index(Request $request)
    {
        try {
            $page = $request->input('page')?$request->input('page'):1;
            $items = $this->notificationService->getNotificationWithReceiver($page);

            $resource = new Fractal\Resource\Collection($items, new NotificationTransformer);
            $resource->setPaginator(new IlluminatePaginatorAdapter($items));
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function show(Request $request,$id)
    {
        try {
            $notification = $this->notificationService->show($id);

            $resource = new Fractal\Resource\Item($notification, new NotificationTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
