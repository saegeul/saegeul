<!DOCTYPE html>
<html lang="en" class="no-js" >
    <head>
        <meta charset="utf-8">
        <title>Saegeul</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="/sgpack/flat-ui/images/favicon.ico">
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    </head>

<body > 
<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap"> 
      <div>
          <nav class="top-bar" data-topbar role="navigation"> 
              <ul class="title-area">
                  <li class="name">
                  <h1><a class="left-off-canvas-toggle" href="#" ><span class="fi-menu" title="menu" aria-hidden="true"></span></a></h1>
                  </li>
                  <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
              </ul> 
              
              <section class="top-bar-section">
                  <ul class="right">
                  </ul> 
              </section>
          </nav>
      </div>
        <aside class="left-off-canvas-menu"> 
            <div class="logo">
                <img src="/img/logo/saegeul_wordmark.png" />
            </div>
            <div class="avatar-area">
                <div class="avatar-wrapper">
                    <img src="/img/avatar.png" />
                </div>
                <div>
                    {{'@'}}{{$user->unique_link}}
                </div> 
            </div> 
            <div>
                <ul>
                    <li>HOME</li>
                    <li>SERIES</li>
                    <li>FOLLOWING</li>
                </ul>
            </div>
            <div> 
                MY SERIES
                <a>CREATE</a>
            </div>

            <div> 
                FOLLOWING SERIES
            </div>
        </aside>
        <div class="content-wrapper">
            @yield('contents')
        </div>
        <!-- main content goes here -->

        <!-- close the off-canvas menu -->
        <a class="exit-off-canvas"></a>

    </div>
</div>



<script src="/bower_components/foundation/js/vendor/jquery.js"></script>

@yield('scripts')

@include('partials.guide-script')

<script src="/bower_components/foundation/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>

</body>
</html>
