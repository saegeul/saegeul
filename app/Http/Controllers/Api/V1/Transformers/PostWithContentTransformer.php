<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Post\Models\Post;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;
use App\Http\Controllers\Api\V1\Transformers\SeriesTransformer;
use App\Http\Controllers\Api\V1\Transformers\TagTransformer;

class PostWithContentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user','series','tag'];

    public function transform(Post $post)
    {
        return [
            'id' => (int) $post->id,
            'origin_id' => $post->origin_id,
            'origin_owner' => $post->origin_owner,
            'commit_uuid' => $post->commit_uuid,
            'source_uuid' => $post->source_uuid,
            'title' => $post->title,
            'description' => $post->description,
            'content' => $post->content,
            'html' => $post->html,
            'image_id' => $post->post_image_id,
            'image_url' => $post->post_image_url,
            'status' => $post->status,
            'pull_request_status' => $post->pull_request_status,
            'series_id' => $post->series_id,
            'user_id' => $post->user_id,
            'favorite' => $post->favorite,
            'created_at' => $post->created_at,
            'updated_at' => $post->updated_at
        ];
    }

    public function includeUser(Post $post)
    {
        $user = $post->user;

        return $this->item($user, new UserTransformer);
    }

    public function includeSeries(Post $post)
    {
        $series = $post->series;
        if(!$series){
            return ;
        }

        return $this->item($series, new SeriesTransformer);
    }

    public function includeTag(Post $post)
    {
        $tags = $post->tags;
        if(!$tags){
            return ;
        }

        return $this->collection($tags, new TagTransformer);
    }
}
