@extends("layouts.default_for_guide")

@section('contents')
<div class="workspace-wrapper">
    @include('guides.mypage.partials.header') 
    <br/>

    <div class="row">
        <div class="large-4 columns">
            <div class="card">
                <div class="media-section"> 
                    <a><img src="http://lorempixel.com/800/450/fashion/2" /></a>
                </div> 
                <div class="text-section">
                    <h6 class="title"><a href="{{route('guide.series.show',array(1))}}">Laravel Guide for AdvancedLaravel Guide for Advanced</a></h6> 
                    <div class="datetime">
                        Last updated Jul 24, 2015
                    </div>
                </div> 
                <div class="footer-section clearfix"> 
                    <a class="button" data-reveal-id="collaborator-modal" ><span class="fi-list-rich" title="list rich" aria-hidden="true"></span><span>&nbsp;11 POSTS</span></a>
                    <a class="button right" data-reveal-id="share-modal" ><span class="fi-share-boxed" title="share boxed" aria-hidden="true"></span></a>
                </div>
            </div>
        </div>

        <div class="large-4 columns end">
            <div class="card">
                <div class="media-section"> 
                    <a> <img src="http://lorempixel.com/800/450/fashion/5" /> </a>
                </div> 
                <div class="text-section">
                    <h6 class="title"><a>Laravel Guide for AdvancedLaravel Guide for Advanced</a></h6> 
                    <div class="datetime">
                        Last updated Jul 24, 2015
                    </div>
                </div> 
                <div class="footer-section clearfix"> 
                    <a class="button" data-reveal-id="collaborator-modal" ><span class="fi-list-rich" title="list rich" aria-hidden="true"></span><span>&nbsp;5 POSTS</span></a>
                    <a class="button right" data-reveal-id="share-modal" ><span class="fi-share-boxed" title="share boxed" aria-hidden="true"></span></a>
                </div>

            </div>
        </div> 
    </div> 

    <div class="row">
        <div class="large-12 columns">
            <div class="section-divider">
                <h6 class="caption">SERIES YOU CONTRIBUTE</h6>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="large-4 columns">
            <div class="card">
                <div class="media-section"> 
                    <a> <img src="http://lorempixel.com/800/450/fashion/5" /> </a>
                </div> 
                <div class="text-section">
                    <h6 class="title"><a>Laravel Guide for AdvancedLaravel Guide for Advanced</a></h6> 
                    <div class="datetime">
                        Last updated Jul 24, 2015
                    </div>
                </div> 
                <div class="footer-section clearfix"> 
                    <a class="button" data-reveal-id="collaborator-modal" ><span class="fi-list-rich" title="list rich" aria-hidden="true"></span><span>&nbsp;7 POSTS</span></a>
                    <a class="button right" data-reveal-id="share-modal" ><span class="fi-share-boxed" title="share boxed" aria-hidden="true"></span></a>
                </div>

            </div>
        </div>

        <div class="large-4 columns end">
            <div class="card">
                <div class="media-section"> 
                    <a> <img src="http://lorempixel.com/800/450/fashion/3" /> </a>
                </div> 
                <div class="text-section">
                    <h6 class="title"><a>Laravel Guide for AdvancedLaravel Guide for Advanced</a></h6> 
                    <div class="datetime">
                        Last updated Jul 24, 2015
                    </div>
                </div> 
                <div class="footer-section clearfix"> 
                    <a class="button" data-reveal-id="collaborator-modal" ><span class="fi-list-rich" title="list rich" aria-hidden="true"></span><span>&nbsp;11 POSTS</span></a>
                    <a class="button right" data-reveal-id="share-modal" ><span class="fi-share-boxed" title="share boxed" aria-hidden="true"></span></a>
                </div>

            </div>
        </div> 
    </div> 

</div>

@include('guides.workspace.series.partials.share_modal') 
@include('guides.workspace.series.partials.collaborator_modal') 
@stop 

@section('links')
@include('guides.workspace.series.partials.links')
@stop

@section('scripts')
@stop
