@extends("layouts.post")

@section('contents') 

<div id="paper" data-post-id="{{$post->getKey()}}"> 
</div>


@stop 

@section('scripts')
<script src="/trix/trix.js"></script>
<script src="/trix/attachments.js"></script>
<script src="/fine-uploader/fine-uploader.min.js"></script>
<script src="/webapp/build/bundle.js"></script>

<script type="text/template" id="qq-template">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div><a><i class="material-icons">wallpaper</i></a></div>
        </div>
        <ul class="qq-upload-list-selector qq-upload-list" style="list-style:none;padding:0px;margin:0px;" aria-live="polite" aria-relevant="additions removals">
            <li>
                <img class="qq-thumbnail-selector" qq-max-size="730" qq-server-scale>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
            </li>
        </ul> 
    </div>
</script>



<script>
var useCase = new UseCase() ;
useCase.visit('posts.edit') ;
</script>
@stop
