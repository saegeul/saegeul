<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Post\Post ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Series\Series ;
use Saegeul\Favorite\Favorite;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\FavoriteTransformer;
use App\Http\Controllers\Api\V1\Transformers\FavoritePostTransformer;

class PostFavoriteController extends ApiController 
{
    protected $postService;
    protected $glauth;
    protected $user;
    protected $seriesService;
    protected $favoriteService;
    protected $fractal;

    public function __construct(
        Post $postService, 
        Glauth $glauth, 
        Favorite $favoriteService,
        Series $seriesService,
        Manager $fractal
    )
	{
        $this->postService = $postService ;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->seriesService = $seriesService ;
        $this->favoriteService = $favoriteService;
        $this->fractal = $fractal;
	}

    public function index(Request $request, $postId)
    {
        try{
            $page = $request->input('page')?$request->input('page'):1;
            $post = $this->postService->show($postId);
            $items = $post->favorites($page);

            $resource = new Fractal\Resource\Collection($items, new FavoriteTransformer);
            $resource->setPaginator(new IlluminatePaginatorAdapter($items));
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function show(Request $request, $id, $favoriteId)
    {
    }

    public function store(Request $request, $postId)
    {
        try{
            $post = $this->postService->show($postId);

            $favoriteRepository = $this->favoriteService->getRepository() ;
            $isExisted = $favoriteRepository->findByFavoriteIdOnTypeForUser($post->getKey(),$post->getClassName(),$this->user->getKey());
            if($isExisted){
                return $this->respondInvalidData('Already exists');
            }

            \DB::beginTransaction();

            $favorite = $this->favoriteService->register($post,$this->user);

            \DB::commit();

            $resource = new Fractal\Resource\Item($favorite, new FavoriteTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function update(Request $request, $id, $favoriteId)
    {
    }

    public function destroy(Request $request, $postId, $favoriteId)
    {
        try{
            $post = $this->postService->show($postId);

            $favoriteRepository = $this->favoriteService->getRepository() ;
            $favorite = $favoriteRepository->find($favoriteId);
            if(!$favorite){
                return $this->respondInvalidData();
            }  

            if($favorite->user_id != $this->user->getKey()){
                return $this->respondUnauthorized() ;
            }

            \DB::beginTransaction();

            $result = $this->favoriteService->delete($favorite->getKey());

            \DB::commit();

            $resource = new Fractal\Resource\Item($result, new FavoriteTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function getByUser(Request $request)
    {
        try{
            $favoriteType = 'Saegeul\Post\Models\Post';
            $page = $request->input('page')?$request->input('page'):1;
            $favoriteRepository = $this->favoriteService->getRepository();
            $items = $favoriteRepository->findByTypeOnUserId($this->user->getKey(), $favoriteType, $page);

            $resource = new Fractal\Resource\Collection($items, new FavoritePostTransformer);
            $resource->setPaginator(new IlluminatePaginatorAdapter($items));
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
