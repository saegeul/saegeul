<?php

return [
    'font' => [
        'family' => public_path().'/fonts/noto_sans/NotoSans-Regular.ttf',
        'family_ko' => public_path().'/fonts/noto_sans_cjk/NotoSansCJKkr-Regular.otf',
    ],
    'margin' => [
        'top' => 20,
        'bottom' => 20,
        'right' => 20,
        'left' => 20
    ],
    'tag' => ['div','em','strong','del','blockquote','a','img','p','span','br']
];
