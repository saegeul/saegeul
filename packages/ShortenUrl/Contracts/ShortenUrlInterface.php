<?php

namespace Saegeul\ShortenUrl\Contracts;

interface ShortenUrlInterface 
{
    public function register($url);
}
