@extends('layouts.default')

@section('contents')
<br/>
<div class="container">

    <div class="mini-form-wrapper">
        {!! Form::open(['class' =>'clearfix signup-form', 'method' => 'post', 'url' => '/invitation/join']) !!}
        <input type="hidden" name="invitation_code" value="{{ $invitationCode }}">
        <div> 
            Hi Welcome to {{ $invitedModel->getTable() }}
            <br>Title : {{ $invitedModel->title }}
            <br>Description : {{ $invitedModel->description }}
            <br>OwnerName : {{ $invitedModel->user->first_name }} {{ $invitedModel->user->last_name }}
            <br>OwnerEmail : {{ $invitedModel->user->email }}
        </div> 
        <button class="btn btn-block ghost-btn" type="submit">Join </button> 
        {!! Form::close() !!}
    </div>
</div> 
@stop 
