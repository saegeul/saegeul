<?php

namespace Saegeul\Post\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class PostRepository extends IlluminateRepository implements PostRepositoryInterface
{
    protected $model = 'Saegeul\Post\Models\Post' ;
    protected $limit = 10;

    public function lists($page, $keyword = null)
    {
        $model = $this->modelFactory->create($this->model) ;
        if($keyword !== "" && $keyword !== null){
            $model = $model->where('title', 'like', "%$keyword%");
        }
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->with('user');
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function findBySeries($seriesId,$userId,$status = 'all')
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('series_id', $seriesId);
        if($status != 'all'){
            $model = $model->where('status', $status);
        }
        //$model = $model->where('user_id', $userId);
        return $model->get();
    }

    public function findByUser($userId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('user_id', $userId);

        return $model->get();
    }

    public function isExisted($userId,$originId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('user_id', $userId);
        $model = $model->where('origin_id', $originId);

        return $model->first();
    }
}
