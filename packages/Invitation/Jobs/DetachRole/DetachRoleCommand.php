<?php

namespace Saegeul\Invitation\Jobs\DetachRole;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Events\Dispatcher;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface;
use Saegeul\Invitation\Events\UserWasDetachedRole;

class DetachRoleCommand implements SelfHandling
{
    public $userId;    
    public $roleId;    

    public function __construct($userId, $roleId)
    {
        $this->userId = $userId;
        $this->roleId = $roleId;
    }

    public function handle(
        Dispatcher $dispatcher, 
        Glauth $glauth, 
        InvitationRepositoryInterface $invitationRepository
    )
    {
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        if(!$user = $userRepository->findById($this->userId)){
            throw New ModelNotFoundException('Not found user email');
        }

        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;
        $role = $roleRepository->findById($this->roleId);  
        if(!$role){
            throw New ModelNotFoundException('Not found role id');
        }else if(!$user->inRole($role)){
            throw New ModelNotFoundException('Unauthorized user');
        }

        $result = $roleService->detachRole($user->getKey(),$role->getKey());

        $invitation = $invitationRepository->findByUserEmailOnRole($user->email,$role->getKey());

        $dispatcher->fire(new UserWasDetachedRole($invitation));

        return $result;
    }
}
