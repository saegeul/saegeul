<?php

namespace spec\Saegeul\Series\Jobs\Update;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Series\Events\SeriesWasUpdated;

class UpdateCommandSpec extends ObjectBehavior
{
    protected $seriesId;
    protected $data;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Series\Jobs\Update\UpdateCommand');
    }

    public function let()
    {
        $seriesId = 1;
        $this->seriesId = $seriesId;
        $data = [
            'description' => "bebe"
        ];
        $this->data = $data;
        $this->beConstructedWith($seriesId,$data);
    }

    public function it_trigger_handle(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository, SeriesModel $series)
    {
        $seriesRepository->find($this->seriesId)->shouldBeCalled()->willReturn($series);
        $data = [
            'description' => "bebe"
        ];
        $seriesRepository->update($this->seriesId,$data)->shouldBeCalled()->willReturn($series);

        $dispatcher->fire(new SeriesWasUpdated($series->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$seriesRepository)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }
}
