<?php

namespace Saegeul\Subscription\Jobs\Confirm;

use Illuminate\Events\Dispatcher;
use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface;
use Saegeul\Subscription\Events\SubscriberWasActivated;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class ConfirmCommand implements SelfHandling
{
    public $subscriptionCode;

    public function __construct($subscriptionCode)
    {
        $this->subscriptionCode = $subscriptionCode;
    }

    public function handle(
        Dispatcher $dispatcher, 
        SubscriptionRepositoryInterface $subscriptionRepository,
        Glauth $glauth
    )
    {
        $subscription = $subscriptionRepository->findByCode($this->subscriptionCode);
        if(!$subscription){
            throw New ModelNotFoundException('Not found subscription id');
        }else if($subscription->activated == 1){
            return $subscription;
        }

        $subscription->activated = 1;
        $subscription->save();

        $dispatcher->fire(new SubscriberWasActivated($subscription)); 

        return $subscription;
    }
}
