import React from 'react' ; 

export default class Card extends React.Component {
    render() {
        return (
            <div className="card">
                <div className="media-section"> 
                    <a><img src=""/></a>
                </div> 
                <div className="text-section">
                    <div className="datetime">
                        Last updated Jul 24, 2015
                    </div>
                </div> 
            </div> 
        ); 
    }
}
