<?php

namespace Saegeul\Permission;

class PermissionGenerator
{
    public function getSeriesIdBySlug($slug, $role)
    {
        $arr = explode('/',$slug);
        if($arr[0] == 'series' && $arr[2] == $role){
            return $arr[1];
        }

        return false;
    }

    public function generateSlug($model,$roleName)
    {
        return $model->getTable().'/'.$model->getKey().'/'.$roleName;
    }

    public function generatePermissions($model,$psermissions)
    {
        $returnData = [];
        foreach($psermissions as $val){
            $key = strtolower($model->getTable()).'.'.$model->getKey().'.'.$val;
            $returnData[$key] = true ;
        }
        return $returnData ;
    }

    public function generatePermissionsToArray($role)
    {
        $permissions = [];
        foreach($role['permissions'] as $val){
            $key = strtolower($role['documentType']).'.'.$role['documentId'].'.'.$val;
            $permissions[] = $key ;
        }
        return $permissions ;
    }
}
