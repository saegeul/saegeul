<?php

namespace Saegeul\Vcs\Jobs\Commit;

use Illuminate\Contracts\Bus\SelfHandling ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Artgrafii\Uuid\UuidService; 
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasCommited;

class CommitCommand implements SelfHandling
{

    public $postId;

    public function __construct($postId)
    {
        $this->postId = $postId;
    }

    public function handle(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository, 
        UuidService $uuidService
    ){
        $post = $postRepository->find($this->postId);

        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        }

        $data = $post->toArray();
        unset($data['id']);
        unset($data['created_at']);
        unset($data['updated_at']);
        $data['uuid'] = $uuidService->getTimebasedUuid();
        $data['post_id'] = $post->getKey();

        $revision = $revisionRepository->store($data);
        $post->commit_uuid = $revision->uuid;
        $post->save();

        $dispatcher->fire(new PostWasCommited($post));

        return $post;
    }
}
