<?php

namespace Saegeul\Vcs\Events ;

use Saegeul\Post\Models\Post ;

class PostWasRollbacked
{
    public $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }
}
