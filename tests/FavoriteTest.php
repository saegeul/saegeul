<?php

use Laracasts\TestDummy\Factory;

class FavoriteTest extends TestCase 
{
	public function testRegister()
	{
        $user = \Artgrafii\Glauth\Models\User::first()  ;

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $series = $seriesRepository->lists(1,'')->first();

        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favorite = $favoriteService->register($series,$user);

        $this->assertInstanceOf('Saegeul\Favorite\Models\Favorite',$favorite);
        $this->assertEquals($favorite->user_id,$user->getKey()) ; 

        $this->assertEquals($favorite->favorite_id,$series->getKey()) ;
        $this->assertEquals($favorite->favorite_type,$series->getClassName()) ;
    }

    public function testRegisterForPost()
	{
        $user = \Artgrafii\Glauth\Models\User::first()  ;

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository() ;
        $post = $postRepository->lists(1,'')->first();

        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favorite = $favoriteService->register($post,$user);

        $this->assertInstanceOf('Saegeul\Favorite\Models\Favorite',$favorite);
        $this->assertEquals($favorite->user_id,$user->getKey()) ; 

        $this->assertEquals($favorite->favorite_id,$post->getKey()) ;
        $this->assertEquals($favorite->favorite_type,$post->getClassName()) ;
    }

    public function testShow()
    {
        $user = \Artgrafii\Glauth\Models\User::first()  ;

        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favoriteRepository = $favoriteService->getRepository();
        $favoriteType = 'Saegeul\Series\Models\Series';
        $favoriteId = $favoriteRepository->findByTypeOnUserId($user->getKey(),$favoriteType,1)->first()->getKey();

        $favorite = $favoriteService->show($favoriteId);

        $this->assertInstanceOf('Saegeul\Favorite\Models\Favorite',$favorite);
        $this->assertEquals($favorite->getKey(),$favoriteId) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_show_favorite_invalid_favorite_id()
    {
        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favoriteId = 22222;

        $favorite = $favoriteService->show($favoriteId);
    }

    public function testDelete()
    {
        $user = \Artgrafii\Glauth\Models\User::first()  ;

        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favoriteRepository = $favoriteService->getRepository();
        $favoriteType = 'Saegeul\Series\Models\Series';
        $favoriteId = $favoriteRepository->findByTypeOnUserId($user->getKey(),$favoriteType,1)->first()->getKey();

        $favorite = $favoriteService->delete($favoriteId);

        $this->assertInstanceOf('Saegeul\Favorite\Models\Favorite',$favorite);
        $this->assertEquals($favorite->getKey(),$favoriteId) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_delete_favorite_invalid_favorite_id()
    {
        $favoriteId = 22222;

        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favorite = $favoriteService->delete($favoriteId);
    }
}
