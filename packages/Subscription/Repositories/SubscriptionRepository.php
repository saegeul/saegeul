<?php

namespace Saegeul\Subscription\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface ;
use Artgrafii\Glauth\Services\HashCodeService ;
use Saegeul\Database\Repository\Contracts\ModelFactoryInterface ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class SubscriptionRepository extends IlluminateRepository implements SubscriptionRepositoryInterface
{
    protected $model = 'Saegeul\Subscription\Models\Subscription' ;

    public function __construct(ModelFactoryInterface $modelFactory, HashCodeService $hashCodeService)
    {
        parent::__construct($modelFactory);

        $this->hashCodeService = $hashCodeService;
    }

    public function findByCode($code)
    {
        $subscriptionId  = $this->hashCodeService->decode($code);
        return $this->find($subscriptionId);
    }

    public function findByEmailForSeries($email,$seriesId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('email',$email);
        $model = $model->where('series_id',$seriesId);
        $model = $model->where('activated',1);
        return $model->first();
    }

    public function findToBeExsited($email,$seriesId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('email',$email);
        $model = $model->where('series_id',$seriesId);
        return $model->first();
    }

    public function findBySeriesId($seriesId,$page)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('series_id',$seriesId);
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function findByEmail($email)
    {
        $model = $this->modelFactory->create($this->model) ;
        return $model->email($email)->first();
    }

    public function findByUserId($userId,$page)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('user_id',$userId);
        $model = $model->where('activated',1);
        $totalCount = $model->count() ; 

        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }
}
