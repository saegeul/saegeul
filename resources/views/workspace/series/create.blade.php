@extends("layouts.default_v2")

@section('contents')
@include('workspace.partials.header') 

<div class="profile-content">
    {!! Form::open(['class' => 'mini-paper clearfix', 'method' => 'post', 'enctype' => 'multipart/form-data', 'url' => '/workspace/series/create'] ) !!}
    <div class="field-group">
        <label>Title</label>
        <div>
            <input type="text" name="title"/>
        </div>
    </div>
    <div class="field-group">
        <label>Description</label>
        <div>
            <textarea name="description"></textarea>
        </div>
    </div>
    <div class="field-group">
        <label>Collaborator</label>
        <div>
            <a href="#" data-reveal-id="myModal">click</a>
            <input type="hidden" name="collaborators"/>
        </div>
    </div>
    <div class="field-group">
        <table>
            <thead>
                <tr>
                    <th>email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="selected-collaborators">
            </tbody>
        </table>
    </div>
    <div class="field-group">
        <button class="button primary right" type="submit">Create</button>
    </div>
    {!! Form::close() !!}
    <div id="myModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h2 id="modalTitle">Append Collaborator</h2>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>

        {!! Form::open(['class' => 'clearfix', 'method' => 'post', 'url' => '/workspace/series']) !!}
        <div class="field-group">
            <div class="row collapse">
                <div class="small-10 columns">
                    <input type="text" name="email" placeholder="Email">
                </div>
                <div class="small-2 columns">
                    <a class="button postfix" id="sg-btn-search">Search</a>
                </div>
            </div>
            <div id="search-collaboartors-list"></div>
        </div>
        {!! Form::close() !!}
    </div>

    @stop

    @section('scripts')
    <script>
    jQuery(function($){
        var that = {};
        var collaborators = [];

        $(document).foundation();

        that.selectCollaborator = function (user) {
            if($.inArray(user.id,collaborators) == -1){
                collaborators.push(user.id);

                $('input:hidden[name=collaborators]').val(collaborators);
                var str = '<tr><td>'+user.email+'</td><td><a class="button tiny alert remove-collaborator" data-user_id="'+user.id+'">제거</a></td></tr>';
                $(str).appendTo('#selected-collaborators').find('.remove-collaborator').click(function(){
                    var userId = $(this).data('user_id');
                    var key = $.inArray(userId,collaborators);
                    collaborators.splice(key,1);
                    $('input:hidden[name=collaborators]').val(collaborators);
                    $(this).parent().parent().remove();   
                });
            }
        };

        $("#sg-btn-search").click(function(){
            var email = $('input:text[name=email]').val();
            $.ajax({
                url:'/api/v1/user/search',
                method:'get',
                data: { 'email': email },
                success:function(data){
                    that.selectCollaborator(data);
                    $('input:text[name=email]').val('');
                    $('#myModal').foundation('reveal', 'close');
                    $('#search-collaboartors-list').html('');
                },
                error:function(response){
                    $('#search-collaboartors-list').html(response.statusText);                
                    console.log(response.statusText);
                }
            });
        });
    });
    </script>
    @stop
