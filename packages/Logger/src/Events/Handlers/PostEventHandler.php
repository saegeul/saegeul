<?php
namespace Saegeul\Logger\Events\Handlers ;

use Saegeul\Logger\Events\Handlers\EventHandler ; 
use Saegeul\Post\Events\PostWasRegistered ;
use Saegeul\Post\Events\PostWasUpdated ;
use Saegeul\Post\Events\PostWasDeleted ;

class PostEventHandler extends EventHandler
{ 
    public function whenPostWasRegistered(PostWasRegistered $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    }

    public function whenPostWasUpdated(PostWasUpdated $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    }

    public function whenPostWasDeleted(PostWasDeleted $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    }
}
