<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class SeriesFavoriteTest extends TestCase
{
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }
 
    public function testRegisterFavorite()
    {
        $this->signin();

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $prefix = 'api/v1/series/'.$series->getKey().'/favorites';
        $response = $this->call('POST',$prefix,[], []);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);

        $this->assertEquals($arr['data']['user_id'], $this->loginUser->getKey());
        $this->assertEquals($arr['data']['favorite_id'], $series->getKey());
        $this->assertEquals($arr['data']['favorite_type'], $series->getClassName());

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $updatedSeries = $seriesRepository->find($series->getKey());
        $this->assertEquals($updatedSeries->favorite, 1);
    }

    public function test_failed_case_to_register_with_aleready_existed_user()
    {
        $this->signin();

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $prefix = 'api/v1/series/'.$series->getKey().'/favorites';
        $response = $this->call('POST',$prefix,[], []);        
        $this->assertEquals(422, $response->status());
    }

    public function test_failed_case_to_register_no_login()
    {
        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $prefix = 'api/v1/series/'.$series->getKey().'/favorites';
        $response = $this->call('POST',$prefix,[], []);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_register_invalid_series_id()
    {
        $this->signin();

        $prefix = 'api/v1/series/100/favorites';
        $response = $this->call('POST',$prefix,[], []);
        $this->assertEquals(404, $response->status());
    }

    public function testGetFavoritesForSeriesId()
    {
        $this->signin();

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $prefix = 'api/v1/series/'.$series->getKey().'/favorites';
        $response = $this->call('GET',$prefix,[], []);        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);
    } 

    public function test_failed_case_to_index_no_login()
    {
        $series = Factory::build('Saegeul\Series\Models\Series')->first();
        $prefix = 'api/v1/series/'.$series->getKey().'/favorites';
        $response = $this->call('GET',$prefix,[], []);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_show_invalid_favorite_id()
    {
        $this->signin();

        $prefix = 'api/v1/series/111/favorites';
        $response = $this->call('GET',$prefix);        
        $this->assertEquals(404, $response->status());
    }

    public function test_failed_case_to_delete_no_login()
    {
        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $prefix = 'api/v1/series/'.$series->getKey().'/favorites/1';
        $response = $this->call('DELETE',$prefix,[],[]);        
        $this->assertEquals(401, $response->status());
    }

    public function testDeleteFavorite()
    {
        $this->signin();

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favoriteRepository = $favoriteService->getRepository() ;
        $favorite = $favoriteRepository->findByFavoriteIdOnTypeForUser($series->getKey(),$series->getClassName(),$this->loginUser->getKey());

        $prefix = 'api/v1/series/'.$series->getKey().'/favorites/'.$favorite->getKey();
        $response = $this->call('DELETE',$prefix,[],[]);        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);

        $favoriteService = $this->app->make('Saegeul\Favorite\Favorite') ;
        $favoriteRepository = $favoriteService->getRepository() ;
        $favorite = $favoriteRepository->find($arr['data']['id']) ;
        $this->assertEmpty($favorite);
    } 
}
