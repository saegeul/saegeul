<?php

namespace spec\Saegeul\Series\Jobs\Delete;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Series\Events\SeriesWasDeleted ;

class DeleteCommandSpec extends ObjectBehavior
{
    protected $seriesId ;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Series\Jobs\Delete\DeleteCommand');
    }

    public function let()
    {
        $seriesId = 1;
        $this->seriesId = $seriesId;
        $this->beConstructedWith($seriesId);
    }

    public function it_trigger_handle(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository, SeriesModel $series)
    {
        $seriesRepository->find($this->seriesId)->shouldBeCalled()->willReturn($series);

        $seriesRepository->delete($this->seriesId)->shouldBeCalled()->willReturn($series);

        $dispatcher->fire(new SeriesWasDeleted($series->getWrappedObject()))->shouldBeCalled();
        $this->handle($dispatcher,$seriesRepository)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }
}
