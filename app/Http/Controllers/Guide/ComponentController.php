<?php
namespace App\Http\Controllers\Guide;

use App\Http\Controllers\Controller ;

class ComponentController extends Controller {

    public function index()
    {
        return view('guides.components.index') ;
    }
}
