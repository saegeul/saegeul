<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Saegeul\VideoParser\VideoParser;
use Saegeul\VideoParser\Exceptions\InvalidHostException;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\LinkParseTransformer;

class LinkController extends ApiController 
{
    protected $videoParser;
    protected $glauth ;
    protected $user ;
    protected $fractal;

    public function __construct(
        VideoParser $videoParser, 
        Glauth $glauth,
        Manager $fractal
    )
    {
        $this->videoParser = $videoParser;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->fractal = $fractal;
    }

    public function parse(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'uri' => 'required'
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $uri = $request->input('uri');
            $item = $this->videoParser->parse($uri);

            $resource = new Fractal\Resource\Item($item, new LinkParseTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (InvalidHostException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            dd($e->getMessage());
            return $this->respondInternalError() ; 
        } 
    }
}
