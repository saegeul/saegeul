<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('command');
			$table->integer('series_id')->nullable();
			$table->integer('invitation_id')->nullable();
			$table->integer('post_id')->nullable();
			$table->string('commit_uuid')->nullable();
			$table->integer('user_id');
			$table->timestamps();
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
