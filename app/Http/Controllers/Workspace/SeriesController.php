<?php namespace App\Http\Controllers\Workspace;

use App\Http\Controllers\Controller ;
use Illuminate\Http\Request ;
use Saegeul\Series\Series ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use Artgrafii\SgDrive\DriveFactory ;
use Saegeul\Invitation\Invitation;
use Saegeul\Series\SeriesRoleCollection ;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Saegeul\Post\Post;
use Saegeul\Cover\Repositories\CoverRepository ;

class SeriesController extends Controller {

    protected $seriesService;
    protected $glauth;
    protected $postService;
    public $user;
    protected $factory ;
    protected $invitationService;

    public function __construct(
        Series $seriesService, 
        Glauth $glauth, 
        Post $postService,
        DriveFactory $factory,
        Invitation $invitationService
    )
	{
        $this->seriesService = $seriesService ;
        $this->postService = $postService;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->factory = $factory ;
        $this->invitationService = $invitationService;
	}

    public function index(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
        if(!$this->user){
            return redirect()->to('/signin?return_url='.$returnUrl);
        }

        $seriesRepository = $this->seriesService->getRepository();
        $items = $seriesRepository->seriesByUser($this->user->getKey());
        /*
        if(count($items) == 0){
            return view('workspace.series.empty');
        }
         */

        $this->roleService = $this->glauth->getRoleService();
        $roleRepository = $this->roleService->getRoleRepository() ;

        $result = [];
        foreach($items as $series){
            $role = $roleRepository->findBySlug($series->slugForCollaborator());
            $temp = new \stdClass ;
            $temp->series = $series;
            $temp->collaborators = $role->users;
            $result[] = $temp;
        }

        $currentTab ='series'  ;
        return view('workspace.series.index',compact('result','currentTab'));
    }

    public function getCoworkingSeries(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
        if(!$this->user){
            return redirect()->to('/signin?return_url='.$returnUrl);
        }

        $seriesRepository = $this->seriesService->getRepository();
        $items = $seriesRepository->findByCoworking($this->user->roles);
        /*
        if(count($items) == 0){
            return view('workspace.series.empty');
        }
         */

        $this->roleService = $this->glauth->getRoleService();
        $roleRepository = $this->roleService->getRoleRepository() ;

        $result = [];
        foreach($items as $series){
            $role = $roleRepository->findBySlug($series->slugForCollaborator());
            $temp = new \stdClass ;
            $temp->series = $series;
            $temp->collaborators = $role->users;
            $result[] = $temp;
        }

        $currentTab = 'coworking' ;

        return view('workspace.series.coworking',compact('result','currentTab'));
    }

    public function editForm(Request $request, $id=null)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
        if(!$this->user){
            return redirect()->to('/signin?return_url='.$returnUrl);
        }

        $series = null;
        if($id != null){
            $series = $this->seriesService->show($id);
            if(!$series->isUpdatableByUser($this->user)){
                return redirect()->to('/series/'.$id);
            }
        }

        return view('workspace.series.create',compact('series'));
    }

    public function store(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
        try {
            if(!$this->user){
                return redirect()->to('/signin?return_url='.$returnUrl);
            }

            $v = \Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
            ]);

            if ($v->fails()){
                return redirect()->back()->withErrors($v->errors());
            }


            $formData = [];
            $formData['title'] = $request->input('title');
            $formData['description'] = $request->input('description');
            $formData['user_id'] = $this->user->getKey();

            $file = $request->file('series_image') ;
            if($file){
                $seriesImage = $this->registerSeriesImage($file);
                $formData['series_image'] = $seriesImage['id'];
                $formData['series_image_url'] = $seriesImage['url'];
            }

            $series = $this->seriesService->register($formData);

            $ids = $request->input('collaborators');
            if($ids != "" && ($collaboratorIds = explode(',',$ids))){
                $roleService = $this->glauth->getRoleService();
                $roleRepository = $roleService->getRoleRepository() ;
                $role = $roleRepository->findBySlug($series->slugForCollaborator());  
                $this->inviteRole($collaboratorIds,$role,$series);
            }

            return redirect()->to('series/'.$series->getKey());
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }

    private function inviteRole($collaboratorIds, $role, $series)
    {
        $userService = $this->glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        foreach($collaboratorIds as $id){
            $user = $userRepository->findById($id);
            $this->invitationService->inviteRole($role->getKey(),$user->email,$series);
        }
    }

    private function registerSeriesImage($file)
    {
        $drive = $this->factory->makeDrive($this->user);
        $fileObject = $drive->writeOnDB($file);
        $thumbPath = $drive->makeThumbnail($fileObject,340);
        $thumbFile = new UploadedFile($thumbPath,basename($thumbPath)) ;
        $thumbUrl = $fileObject->putOnCDN($drive,$thumbFile);

        $seriesImage = [];
        $seriesImage['id'] = $fileObject->getKey();
        $seriesImage['url'] = config('filesystems.disks.softlayer-cdn.cdn-url').$thumbUrl;

        return $seriesImage;
    }

    public function show(CoverRepository $coverRepo , $seriesId)
    { 
        $postRepository =  $this->postService->getRepository();
        $posts =  $postRepository->findBySeries($seriesId,null);
        $first = $posts->first() ; 
        $posts = $posts->toArray() ;

        $cover = $coverRepo->findBySeries($seriesId) ;

        try {
            $series = $this->seriesService->show($seriesId);
            $currentTab = 'cover' ; 
            return view('workspace.series.cover',compact('series','currentTab','posts','first','cover'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function posts($id)
    {
        try {
            $series = $this->seriesService->show($id);
            $currentTab = 'posts' ; 
            return view('workspace.series.posts',compact('series','currentTab'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } 
    }

    public function collaborators($id)
    {
        try {
            $series = $this->seriesService->show($id);
            $currentTab = 'collaborators' ; 
            return view('workspace.series.collaborators',compact('series','currentTab'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
