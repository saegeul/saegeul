<?php

namespace spec\Saegeul\Vcs\Jobs\Fork;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Vcs\Models\Revision as RevisionModel ;
use Saegeul\Post\Models\Post as PostModel ;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasForked;

class ForkCommandSpec extends ObjectBehavior
{
    protected $postId;
    protected $userId;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Vcs\Jobs\Fork\ForkCommand');
    }

    public function let()
    {
        $this->postId = 1;
        $this->userId = 2;
        $this->beConstructedWith($this->postId,$this->userId);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository, 
        RevisionModel $revision,
        UuidService $uuidService,
        PostModel $post,
        PostModel $newPost,
        RevisionModel $newRevision
    )
    {
        $postRepository->find($this->postId)->shouldBeCalled()->willReturn($post);

        $data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
            'user_id' => 1,
            'series_id' => 1
        ];
        $originId = 1;
        $post->getKey()->shouldBeCalled()->willReturn($originId);
        $data['origin_id'] = $originId;
        $originOwner = 1;
        $post->getAttribute('user_id')->willReturn($originOwner);
        $data['origin_owner'] = $originOwner;
        $post->toArray()->shouldBeCalled()->willReturn($data);
        $data['user_id'] = $this->userId;
        $data['source_uuid'] = '222';
        $post->getAttribute('commit_uuid')->willReturn($data['source_uuid']);
        $postRepository->store($data)->shouldBeCalled()->willReturn($newPost);

        $newPost->toArray()->shouldBeCalled()->willReturn($data);
        $newPost->getKey()->shouldBeCalled()->willReturn(2);
        $data['post_id'] = 2;
        $uuid = '3333';
        $uuidService->getTimebasedUuid()->shouldBeCalled()->willReturn($uuid);
        $data['uuid'] = $uuid;
        $data['command'] = 'fork';
        $revisionRepository->store($data)->shouldBeCalled()->willReturn($newRevision);
        $newRevision->getAttribute('uuid')->willReturn($uuid);
        $newPost->setAttribute('commit_uuid',$uuid)->shouldBeCalled();
        $newPost->save()->shouldBeCalled();

        $dispatcher->fire(new PostWasForked($newPost->getWrappedObject()))->shouldBeCalled();
    
        $this->handle($dispatcher,$revisionRepository,$postRepository,$uuidService)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
