<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Series\Models\Series;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;
use App\Http\Controllers\Api\V1\Transformers\TagTransformer;

class SeriesTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user','tag'];

    public function transform(Series $series)
    {
        return [
            'id' => (int) $series->id,
            'title' => $series->title,
            'description' => $series->description,
            'subscribers' => $series->subscribers,
            'owner' => $series->user_id,
            'image_id' => $series->series_image_id,
            'image_url' => $series->series_image_url,
            'status' => $series->status,
            'created_at' => $series->created_at,
            'updated_at' => $series->updated_at
        ];
    }

    public function includeUser(Series $series)
    {
        $user = $series->user;

        return $this->item($user, new UserTransformer);
    }

    public function includeTag(Series $series)
    {
        $tags = $series->tags;
        if(!$tags){
            return ;
        }

        return $this->collection($tags, new TagTransformer);
    }
}
