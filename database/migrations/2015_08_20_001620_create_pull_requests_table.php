<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePullRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pull_requests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id');
			$table->integer('series_id');
			$table->string('status')->default('draft');
			$table->integer('user_id');
			$table->mediumText('comment');
			$table->timestamps();
		});
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('pull_requests');
    }
}
