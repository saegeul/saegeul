<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class CoverTest extends TestCase
{
    private $prefix = 'api/v1/series/1/covers';
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    public function testRegisterCover()
    {
        $this->signin();

        $data = Factory::build('Saegeul\Cover\Models\Cover');

        $response = $this->call('POST',$this->prefix,$data->toArray(), []);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['content'], $data['content']);
        $this->assertEquals($arr['data']['series_id'], 1);
    }

    public function test_failed_case_to_register_no_login()
    {
        $data = Factory::build('Saegeul\Cover\Models\Cover');
        $response = $this->call('POST',$this->prefix,$data->toArray(), []);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_register_invalid_data()
    {
        $this->signin();

        $data = [];

        $response = $this->call('POST',$this->prefix,$data, []);        
        $this->assertEquals(422, $response->status());
    }

    public function testGetCover()
    {
        $coverId = 1;
        $response = $this->call('GET',$this->prefix.'/'.$coverId);        
        $this->assertEquals(200, $response->status());
        $result = $response->getContent();
        $arr = json_decode($result, true);

        $this->assertEquals($arr['data']['id'], $coverId);
    } 

    public function testUpdate()
    {
        $this->signin();

        $coverService = $this->app->make('Saegeul\Cover\Cover') ;

        $data = [
            'status' => 'published'
            ];
        $coverId = 1;
        $response = $this->call('PUT',$this->prefix.'/'.$coverId,$data);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['status'], $data['status']);
    }

    public function test_failed_case_to_update_no_login()
    {
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;

        $data = [
            'status' => 'published'
            ];
        $coverId = 1;
        $response = $this->call('PUT',$this->prefix.'/'.$coverId,$data);
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_update_invalid_cover_id()
    {
        $this->signin();

        $coverService = $this->app->make('Saegeul\Cover\Cover') ;

        $data = [
            'status' => 'published'
            ];
        $coverId = 222;
        $response = $this->call('PUT',$this->prefix.'/'.$coverId,$data);
        $this->assertEquals(404, $response->status());
    }

    public function testDeleteSeries()
    {
        $this->signin();

        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $coverId = 1;
        $response = $this->call('DELETE',$this->prefix.'/'.$coverId);
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['id'], $coverId);

        $coverRepsitory = $this->app->make('Saegeul\Cover\Contracts\CoverRepositoryInterface') ;
        $cover = $coverRepsitory->find($arr['data']['id']) ;
        $this->assertEmpty($cover);
    }


    public function test_failed_case_to_delete_no_login()
    {
        $coverId = 1;
        $response = $this->call('DELETE',$this->prefix.'/'.$coverId);
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_delete_invalid_series_id()
    {
        $this->signin();

        $coverId = 222;
        $response = $this->call('DELETE',$this->prefix.'/'.$coverId);
        $this->assertEquals(404, $response->status());
    }
}
