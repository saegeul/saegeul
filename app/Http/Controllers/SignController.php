<?php namespace App\Http\Controllers;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Ramsey\Uuid\Uuid ; 
use Illuminate\Database\QueryException ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException ;

class SignController extends Controller {

    protected $glauth ;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Glauth $glauth)
	{
		$this->middleware('guest');
        $this->glauth = $glauth ;
	}

    public function signinForm(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):"";
        return view('sign.signin',compact('returnUrl'));
    }

    public function signin(Request $request)
    {
        try {
            $returnUrl = $request->input('return_url')?$request->input('return_url'):"/workspace/series";

            $v = \Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $email = $request->input('email');
            $password = $request->input('password');

            $user = $this->glauth->signin($email,$password);

            return redirect()->to($returnUrl);
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (NotActivatedException $e) {
            return redirect('activation/need-to-confirm?return_url='.$returnUrl)->withErrors($e->getMessage());
        }
    }

    public function signupForm(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):"";
        return view('sign.signup',compact('returnUrl'));
    }    

    public function signup(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'email' => 'required|email|unique:users,email',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
                'unique_link' => 'required|unique:users,unique_link',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $email = $request->input('email');
            $password = $request->input('password');
            $uniqueLink = $request->input('unique_link');
            $uuid = Uuid::uuid4()->toString();

            $user = $this->glauth->signup($email,$password,$uniqueLink,$uuid);

            return redirect()->to('complete-to-register');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }

    public function completeToRegister(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):"";
        return view('activation.activation-form');
    }

    public function signout(Request $request)
    {
        $this->glauth->signout();
        return redirect()->to('/');
    }
}
