@extends("layouts.series")

@section('header')
@include('workspace.series.partials.header')
@endsection

@section('contents')
<div id="post-area" data-series-id="{{$series->getKey()}}">
</div> 
@stop

@section('scripts') 
<script>
var useCase = new UseCase ;
useCase.visit('series.posts') ;
</script> 
@stop 
