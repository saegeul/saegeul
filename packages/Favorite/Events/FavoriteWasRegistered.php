<?php

namespace Saegeul\Favorite\Events ;

use Saegeul\Favorite\Models\Favorite ;

class FavoriteWasRegistered 
{
    public $favorite;

    public function __construct(Favorite $favorite)
    {
        $this->favorite = $favorite;
    }
}
