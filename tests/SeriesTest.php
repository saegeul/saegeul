<?php

use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Laracasts\TestDummy\Factory;

class SeriesTest extends TestCase 
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testRegister()
	{
        $this->signin();

        $data = Factory::build('Saegeul\Series\Models\Series');

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->register($data->toArray());

        $this->assertInstanceOf('Saegeul\Series\Models\Series',$series);
        $this->assertEquals($series->title,$data['title']) ; 
        $this->assertEquals($series->description,$data['description']) ;
        $this->assertEquals($series->user_id,$data['user_id']) ;
    } 

    public function testCreatedByUser()
    { 
        $this->signin();

        //Given
        $user = \Artgrafii\Glauth\Models\User::first()  ;
        $seriesData = factory(Saegeul\Series\Models\Series::class)->make() ;
        $seriesService = $this->app->make('Saegeul\Series\Series') ;

        //When
        $createdSeries = $seriesService->createWithUser($seriesData->toArray(),$user) ; 

        $this->assertEquals($createdSeries->user_id, $user->getKey()) ;
        $this->assertEquals($createdSeries->title, $seriesData->title) ;
        $this->assertEquals($createdSeries->description, $seriesData->description) ; 
    }

    /**
     * @expectedException Illuminate\Database\QueryException
     **/
    public function test_failed_case_to_register_series_invalid_data()
	{
        $data = Factory::build('Saegeul\Series\Models\Series');
        $data['temp'] = '111';

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->register($data->toArray());
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_update_series_invalid_series_id()
    {
        $seriesId = 1111;
        $data = Factory::build('Saegeul\Series\Models\Series');

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->update($seriesId,$data);
    }

    /**
     * @expectedException Illuminate\Database\QueryException
     **/
    public function test_failed_case_to_update_series_invalid_data()
	{
        $seriesRepository = $this->app->make('Saegeul\Series\Contracts\SeriesRepositoryInterface') ;
        $seriesId = $seriesRepository->lists(1,'')->first()->getKey();
        $data = [
            'description' => "bebe"
        ];
        $data['temp'] = '111';

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->update($seriesId,$data);
    }

    public function testUpdate()
    {
        $this->signin();
        
        $seriesRepository = $this->app->make('Saegeul\Series\Contracts\SeriesRepositoryInterface') ;
        $seriesId = $seriesRepository->lists(1,'')->first()->getKey();
        $data = [
            'description' => "bebe"
        ];

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->update($seriesId,$data);

        $this->assertInstanceOf('Saegeul\Series\Models\Series',$series);
        $this->assertEquals($series->description,$data['description']) ; 
    }

    public function testShow()
    {
        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $seriesId = $seriesRepository->lists(1,'')->first()->getKey();

        $series = $seriesService->show($seriesId);

        $this->assertInstanceOf('Saegeul\Series\Models\Series',$series);
        $this->assertEquals($series->getKey(),$seriesId) ; 
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_show_series_invalid_series_id()
    {
        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $seriesId = 22222;

        $series = $seriesService->show($seriesId);
    }

    public function testDelete()
    {
        $this->signin();

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $seriesId = $seriesRepository->lists(1,'')->first()->getKey();

        $series = $seriesService->delete($seriesId);

        $this->assertInstanceOf('Saegeul\Series\Models\Series',$series);
        $this->assertEquals($series->getKey(),$seriesId) ; 
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_delete_series_invalid_series_id()
    {
        $seriesId = 22222;

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->delete($seriesId);
    }
}
