<?php

namespace Saegeul\Invitation\Jobs\JoinRole;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Events\Dispatcher;
use Artgrafii\Glauth\Glauth ;
use Artgrafii\Glauth\Services\HashCodeService ;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Saegeul\Invitation\Events\UserWasJoinedRole;

class JoinRoleCommand implements SelfHandling
{
    public $invitationCode;
    public $userId;

    public function __construct($invitationCode, $userId)
    {
        $this->invitationCode = $invitationCode;
        $this->userId = $userId;
    }

    public function handle(
        Dispatcher $dispatcher,
        Glauth $glauth, 
        HashCodeService $hashCodeService,
        InvitationRepositoryInterface $invitationRepository
    )
    {
        $invitation = $invitationRepository->findByCode($this->invitationCode);
        if(!$invitation){
            throw New ModelNotFoundException('Not found invitation id');
        }

        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        if(!$user = $userRepository->findById($this->userId)){
            throw New ModelNotFoundException('Not found user email');
        }else if($invitation->email != $user->email){
            throw New ModelNotFoundException('Not found invitation');
        }

        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;
        $role = $roleRepository->findById($invitation->role_id);  
        if(!$role){
            throw New ModelNotFoundException('Not found role id');
        }else if($user->inRole($role)){
            return $invitation;
        }

        $roleService->attachRole($this->userId,$role->getKey());

        $invitation->fill(['response'=>1]);
        $invitation->save();

        $dispatcher->fire(new UserWasJoinedRole($invitation));

        return $invitation;
    }
}
