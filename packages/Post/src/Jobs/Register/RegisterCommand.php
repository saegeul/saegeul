<?php

namespace Saegeul\Post\Jobs\Register;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Post\Events\PostWasRegistered ;

class RegisterCommand implements SelfHandling
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data ;
    }

    public function handle(Dispatcher $dispatcher, PostRepositoryInterface $postRepository)
    {
        if(isset($this->data['html'])){
            $temp = strip_tags($this->data['html']);
            $str = preg_replace(['/&nbsp;/','/&quot;/'],'',$temp);
            $this->data['description'] = mb_strimwidth($str,0,100,'...','utf-8');
        }
        $post = $postRepository->store($this->data);

        $dispatcher->fire(new PostWasRegistered($post));

        return $post ;
    }
}
