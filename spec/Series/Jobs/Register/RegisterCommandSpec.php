<?php

namespace spec\Saegeul\Series\Jobs\Register;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Series\Events\SeriesWasRegistered ;

class RegisterCommandSpec extends ObjectBehavior
{
    protected $data;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Series\Jobs\Register\RegisterCommand');
    }

    public function let()
    {
        $this->data = [
            'title' => 'Laravel5',
            'description' => "lalalalala",
            'user_id' => 1
        ];
        $this->beConstructedWith($this->data);
    }

    public function it_trigger_handle(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository, SeriesModel $series)
    {
        $seriesRepository->store($this->data)->shouldBeCalled()->willReturn($series);

        $dispatcher->fire(new SeriesWasRegistered($series->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$seriesRepository)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }
}
