@extends("layouts.default_for_guide")

@section('contents')
@include('guides.account_manager.partials.profile-header') 
<div class="profile-content">
    <form class="mini-paper clearfix">
        <div class="field-group">
            <label>Old Password</label>
            <div>
                <input type="password" />
            </div>
        </div>
        <div class="field-group">
            <label>New Password</label>
            <div>
                <input type="password" />
            </div>
        </div>
        <div class="field-group">
            <label>Confirm New Password</label>
            <div>
                <input type="password" />
            </div>
        </div>
        <a class="button alert right">Password Change</a>

    </form>
</div>

@stop
