<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Laracasts\Behat\Context\Migrator;
use Laracasts\Behat\Context\DatabaseTransactions;
use PHPUnit_Framework_Assert as PHPUnit;
use Artgrafii\Glauth\Services\HashCodeService ;

/**
 * Defines application features from the specific context.
 */
class UserFeatureContext extends MinkContext implements Context, SnippetAcceptingContext
{
    public function __construct()
    {
    }

    /**
     * @Given I register :email :password :passwordConf :uniqueLink
     */
    public function iRegister($email,$password,$passwordConf,$uniqueLink)
    {
        $this->visit('/signup');
        $this->fillField('email', $email);
        $this->fillField('password', $password);
        $this->fillField('password_confirmation', $passwordConf);
        $this->fillField('unique_link', $uniqueLink);
        $this->pressButton('btn-signup');
    }

    /**
     * @Given I confirm user activation :email
     */
    public function iConfirmUserActivation($email)
    {
        $glauth = app()->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->findByCredentials(['email' => $email]);

        $activationService = $glauth->getActivationService();
        $activationRepository = $activationService->getActivationRepository();
        $activation = $activationRepository->exists($user);
        $code = $activation->code ;

        $hashCodeService = new HashCodeService ;
        $hashCode = $hashCodeService->encode($user->getKey());

        $result = $activationService->confirm($hashCode,$code);
        PHPUnit::assertInstanceOf('Artgrafii\Glauth\Models\User',$result);
    }

    /**
     * @When I sign in :email :password
     */
    public function iSignIn($email,$password)
    {
        $this->visit('/signin');
        $this->fillField('email', $email);
        $this->fillField('password', $password);
        $this->pressButton('btn-signin');
    }
}
