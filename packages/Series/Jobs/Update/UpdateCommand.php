<?php

namespace Saegeul\Series\Jobs\Update;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Series\Events\SeriesWasUpdated;

class UpdateCommand implements SelfHandling
{

    public $seriesId;
    public $data;

    public function __construct($seriesId, $data)
    {
        $this->seriesId = $seriesId;
        $this->data = $data;
    }

    public function handle(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository)
    {
        $series = $seriesRepository->find($this->seriesId);
        if(!$series){
            throw New ModelNotFoundException('Not found series id');
        }
        $series = $seriesRepository->update($this->seriesId,$this->data);
        
        $dispatcher->fire(new SeriesWasUpdated($series));

        return $series ;
    }
}
