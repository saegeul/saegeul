<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class UserFavoriteTest extends TestCase
{
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }
 
    public function testGetSeries()
    {
        $this->signin();

        $series = Factory::build('Saegeul\Series\Models\Series')->first();

        $prefix = 'api/v1/series/'.$series->getKey().'/favorites';
        $response = $this->call('POST',$prefix,[], []);

        $prefix = 'api/v1/favorites/series';
        $response = $this->call('GET',$prefix,[], []);        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);
        $this->assertEquals($arr['data'][0]['favorite_id'],$series->getKey());
        $this->assertEquals($arr['data'][0]['favorite_type'],$series->getClassName());
    }

    public function test_failed_case_to_get_series_no_login()
    {
        $prefix = 'api/v1/favorites/series';
        $response = $this->call('GET',$prefix,[], []);        
        $this->assertEquals(401, $response->status());
    }

    public function testGetPost()
    {
        $this->signin();

        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        $prefix = 'api/v1/posts/'.$post->getKey().'/favorites';
        $response = $this->call('POST',$prefix,[], []);

        $prefix = 'api/v1/favorites/posts';
        $response = $this->call('GET',$prefix,[], []);        
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals(200, $response->status());
        $this->assertNotEmpty($arr['data']);
        $this->assertEquals($arr['data'][0]['favorite_id'],$post->getKey());
        $this->assertEquals($arr['data'][0]['favorite_type'],$post->getClassName());
    }

    public function test_failed_case_to_get_post_no_login()
    {
        $prefix = 'api/v1/favorites/posts';
        $response = $this->call('GET',$prefix,[], []);        
        $this->assertEquals(401, $response->status());
    }
}
