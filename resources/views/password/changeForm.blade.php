@extends("layouts.default")

@section('contents')
<section class="authenty signin-main">
    <div class="section-content">
        <div class="wrap">
            <div class="container">	  
                {!! Form::open(['class' =>'form-wrap', 'method' => 'post', 'url' => '/password/change']) !!}
                    <input type="hidden" name="hash_code" value="{{ $hashCode }}"/>
                    <input type="hidden" name="code" value="{{ $code }}"/>
                    <div class="row">
                        <div class="title" >
                            <h1>password</h1>
                        </div>
                        <div >
                            <div class="form-header">
                                <i class="fa fa-lock"></i>
                            </div>
                            <div class="form-main">
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" placeholder="password" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="repeat password" required="required">
                                </div> 

                                @if($errors->any())
                                <div class="alert alert-danger">
                                    <i class="fa fa-exclamation-circle"></i>&nbsp;{{$errors->first()}}
                                </div>
                                @endif
                                <button type="submit" class="btn btn-block signin">Password Change</button>
                            </div>
                            <div class="form-footer">
                                <div class="row"> 
                                    <div class="col-xs-12">
                                    </div>
                                </div>
                            </div>		
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section> 
@stop
