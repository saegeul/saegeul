@extends("layouts.default")

@section('contents')

@stop

@section('scripts')
<script>
jQuery(function($){
    if(window.opener){
    var name = '{{ $name }}';
    var provider = '{{ $provider }}';
    var id = '{{ $id }}';
        window.opener.$('.indicator-success').removeClass("hide");
        window.opener.$('.sg-info').removeClass('hide');
        window.opener.$('.sg-social-user-name').text('Hellow! '+name);
        window.opener.$('.sg-social-info').text('Your account connect with '+provider); 
        window.opener.$("input:checkbox[id='" + id + "']").prop('checked',true);
        window.close();
    }
});
</script>
@stop
