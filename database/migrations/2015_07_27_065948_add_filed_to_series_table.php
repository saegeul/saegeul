<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledToSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('series', function(Blueprint $table)
		{
			$table->integer('series_image')->nullable();
			$table->string('series_image_url')->nullable();
			$table->string('status')->default('draft');
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('series', function(Blueprint $table)
		{
			$table->dropColumn('series_image');
			$table->dropColumn('series_image_url');
			$table->dropColumn('status');
	    });
    }
}
