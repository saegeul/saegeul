<?php

use Illuminate\Database\Seeder;

class SeriesSeeder extends Seeder
{
    protected $roleService ;

    public function run()
    { 
        $glauth = App::make('Artgrafii\Glauth\Glauth') ; 
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository(); 
        $this->roleService = $glauth->getRoleService();

        factory(Saegeul\Series\Models\Series::class,5)->make()->each(function($u) use($userRepository,$glauth){
            $user = $userRepository->findById(1) ;  
            $glauth->signin($user->email,'1q2w3e') ;

            $seriesService = App::make('Saegeul\Series\Series') ;
            $series = $seriesService->createWithUser($u->toArray(),$user); 
        }) ; 

        $seriesCollection = \Saegeul\Series\Models\Series::all();

        foreach($seriesCollection as $series)
        {
            for($key=2; $key < 4 ; $key++)
            {
                $collaborator = $userRepository->findById(2) ;  
                $this->assignCollaboratorRole($collaborator, $series);
            }
        } 
    }

    private function assignCollaboratorRole($collaborator,$series)
    {
        $roleRepository = $this->roleService->getRoleRepository() ;
        $role = $roleRepository->findBySlug($series->slugForCollaborator());  

        $invitationService = App::make('Saegeul\Invitation\Invitation') ;
        $invitation = $invitationService->inviteRole($role->getKey(),$collaborator->email,$series);

        $hashCodeService = App::make('Artgrafii\Glauth\Services\HashCodeService');
        $invitationCode = $hashCodeService->encode($invitation->getKey());

        $invitationService->joinRole($invitationCode,$collaborator->getKey());
    }
}
