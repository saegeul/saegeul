<?php

namespace Saegeul\Subscription\Events ;

use Saegeul\Subscription\Models\Subscription;

class SubscriberWasActivated
{
    public $subscription;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }
}
