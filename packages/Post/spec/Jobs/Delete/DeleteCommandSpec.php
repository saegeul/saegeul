<?php

namespace spec\Saegeul\Post\Jobs\Delete;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Post\Models\Post as PostModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Post\Events\PostWasDeleted ;

class DeleteCommandSpec extends ObjectBehavior
{
    protected $postId ;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Post\Jobs\Delete\DeleteCommand');
    }

    public function let()
    {
        $postId = 1;
        $this->postId = $postId;
        $this->beConstructedWith($postId);
    }

    public function it_trigger_handle(Dispatcher $dispatcher, PostRepositoryInterface $postRepository, PostModel $post)
    {
        $postRepository->find($this->postId)->shouldBeCalled()->willReturn($post);

        $postRepository->delete($this->postId)->shouldBeCalled()->willReturn($post);

        $dispatcher->fire(new PostWasDeleted($post->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$postRepository)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
