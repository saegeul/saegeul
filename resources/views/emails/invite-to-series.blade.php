<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Invite you to series</h2>
        <?php 
            $ownerName = $owner->last_name . ' ' . $owner->first_name;
            $title = $invitableModel->title;
        ?>  
		<div>
            {{ $ownerName }} invite you to {{ $title }} , complete this form: {{ URL::to("invitation/visit?invitation_code=$invitationCode") }}
		</div>
	</body>
</html>
