import React from 'react' ; 
import FancyProgress from './util/FancyProgress.jsx' ;
import Request from 'superagent' ;
import Timemachine from './Timemachine.jsx' ;

export default class SaegeulEditor extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            progress: new FancyProgress, 
            cdnFullPath  : '',
            isForked : false ,
            imageId  : null,
            title : 'Tolstoy Says',
            isSidebarOpen : false ,
            series : { 
            },
            content : ''
        }; 
    }

    render() { 
        let buttonStyle = {
            'marginTop':'7px'
        };

        let historyStyle = { 
            position: 'absolute' , 
            top : '7px', 
            color: '#333',
            right: '4px' 
        };

        let historyIconStyle = {
            'fontSize' : '36px' ,
            'color' : '#333'
        };

        return (
            <div>
                <div className="editor-toolbar-wrapper">
                    <div className="mini-container">
                        <div>
                        </div>
                        <div className="row"> 
                            <div id="my-toolbar" className="editor-toolbar no-padding small-9 columns"></div> 
                            <div className="small-3  no-padding columns">
                                { this.state.isForked ?
                                <a className="ghost-btn right" style={buttonStyle}  onClick={this.onSuggestClick.bind(this)}>SUGGEST</a>
                                :
                                <a className="ghost-btn right" style={buttonStyle}  onClick={this.onPublishClick.bind(this)}>PUBLISH</a>
                                }
                                <a className="ghost-btn right" style={buttonStyle}  onClick={this.onSaveClick.bind(this)}>SAVE</a>
                                
                            </div>
                        </div>
                    </div>
                    <div style={historyStyle}> 
                        <a className="right" onClick={this.toggleSidebar.bind(this)} ><i className="material-icons" style={historyIconStyle} >restore</i></a>
                    </div>
                </div>
                <div className="post-wrapper mini-container">
                    {this.state.cdnFullPath ? 
                        <div className="wallpaper">
                            <img src={'http://'+this.state.cdnFullPath} />
                        </div>
                    :
                    <div className="wallpaper" id="wallpaper"> 
                        <div><i className="material-icons">wallpaper</i></div>
                    </div>
                    }
                    
                    <form>
                        <label><a href={this.state.series.uri} className="series-link">in {this.state.series.title} </a></label>
                        <input type="text" className="title" name="title" onChange={this.onChangeTitle.bind(this)} value={this.state.title} />
                        <trix-editor></trix-editor>
                        <input type="hidden" name="post_id" />
                    </form>
                </div>
                <Timemachine postId={this.props.post_id} isOpen={this.state.isSidebarOpen} toggler={this.toggleSidebar.bind(this)} reload={this.reload.bind(this)}  ></Timemachine>
            </div>
        );
    }

    onChangeTitle(event) {
        this.setState({ title : event.target.value});
    } 

    onSaveClick(event) { 
        let element = document.querySelector("trix-editor") ;
        let trixEditor = element.editor ; 
        let inputId =  element.getAttribute('input') ; 
        let title = this.state.title;
        let hiddenInput = document.getElementById(inputId) ;
        let content = JSON.stringify(trixEditor);

        const postId = this.props.post_id ; 

        this.state.progress.start() ;

        Request.put('/api/v1/posts/'+postId)
                .send({
                    'title' : title ,
                    'html' : hiddenInput.value,
                    'image_url' : this.state.cdnFullPath,
                    'image_id' : this.state.imageId,
                    'content' : content
                })
                .end(function(err,response){
                    this.state.progress.done() ;
                }.bind(this));

    }

    onPublishClick(event) { 
        let element = document.querySelector("trix-editor") ;
        let trixEditor = element.editor ; 
        let inputId =  element.getAttribute('input') ; 
        let title = this.state.title;
        let hiddenInput = document.getElementById(inputId) ;
        let content = JSON.stringify(trixEditor);

        const postId = this.props.post_id ; 

        this.state.progress.start() ;

        Request.put('/api/v1/posts/'+postId+'/publish')
                .send()
                .end(function(err,response){
                    this.state.progress.done() ;
                }.bind(this));

    }

    onSuggestClick(event) { 
        const postId = this.props.post_id ; 

        this.state.progress.start() ;

        Request.post('/api/v1/posts/'+postId+'/pullrequest')
                .send({
                    series_id : this.state.series.id , 
                    comment : 'pull-request'
                })
                .end(function(err,response){
                    this.state.progress.done() ;
                }.bind(this));

    }


    toggleSidebar() {
        if(this.state.isSidebarOpen ){
            this.setState({
                isSidebarOpen : false 
            });
        }else{
            this.setState({
                isSidebarOpen : true 
            }); 
        }
    }

    reload(revisionId){ 
        const postId = this.props.post_id ; 

        let element = document.querySelector("trix-editor") ;
        var trixEditor = element.editor ;

        Request.get('/api/v1/posts/'+postId+'/revisions/'+revisionId)
                .send()
                .end(function(err,response){
                    
                    let data = JSON.parse(response.text).data ; 
                    console.log(data) ;
                    this.setState({ title : data.title});
                    //this.setState({ series : data.series.data}); 
                    this.setState({ cdnFullPath : data.image_url});
                    this.setState({ imageId : data.post_image});
                    //this.setState({ series : data.series.data});
                    //this.state.series.uri = '/workspace/series/'+this.state.series.id+'/posts' ; 
                    this.setState({ series : this.state.series});

                    if(data.content != 'content'){
                        trixEditor.loadJSON(JSON.parse(data.content)) ;
                    }

                }.bind(this));
    }

    componentDidMount() {
        let element = document.querySelector("trix-editor") ;
        let myToolbar = document.getElementById("my-toolbar") ;
        let trixToolbar = document.querySelector("trix-toolbar") ; 
        var trixEditor = element.editor ;
        const postId = this.props.post_id ; 

        element.className = 'post-content' ;
        myToolbar.appendChild(trixToolbar) ;

        this.state.progress.start() ;

        if(postId) {
            Request.get('/api/v1/posts/'+postId)
                .send()
                .end(function(err,response){
                    let data = JSON.parse(response.text).data ; 
                    this.setState({ title : data.title});
                    this.setState({ series : data.series.data}); 
                    this.setState({ cdnFullPath : data.image_url});
                    this.setState({ imageId : data.post_image});
                    this.state.series.uri = '/workspace/series/'+this.state.series.id+'/posts' ; 
                    this.setState({ series : this.state.series});

                    console.log('series') ;
                    console.log(data.series.data) ;

                    if(data.series.data.owner  != data.user_id) {
                        this.setState({
                            isForked : true
                        }) ; 
                    }

                    if(data.content != 'content'){
                        trixEditor.loadJSON(JSON.parse(data.content)) ;
                    }
                    this.state.progress.done() ;
                }.bind(this)); 
        }

        var that = this ;

        var uploader = new qq.FineUploader({
            element: document.getElementById('wallpaper'),
            request: {
                endpoint: '/api/v1/images',
                inputName : 'upload' 

            }, 
            callbacks: {
                onComplete : function(id,fileName,jsonResponse){
                    let data = jsonResponse.data ;
                    console.log(data) ;
                    let cdnFullPath = data.cdn_full_path ;
                    that.setState({
                        'cdnFullPath' : cdnFullPath , 
                        'imageId' : data.id 
                    });
                }
            }
        });
    }
}
