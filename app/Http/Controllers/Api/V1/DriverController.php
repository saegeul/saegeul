<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Artgrafii\SgDrive\DriveFactory ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class DriverController extends ApiController 
{
    protected $glauth ;
    protected $user ;
    protected $factory ;

	public function __construct(Glauth $glauth, DriveFactory $factory)
    {
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        
        $this->factory = $factory ;
    }

    public function index(Request $request)
    {
        $page = $request->input('page')?$request->input('page'):1;
        $keyword = $request->input('keyword')?$request->input('keyword'):"";

        if(!$this->user){
            return $this->respondUnauthorized() ;
        }
        
        $drive = $this->factory->makeDrive($this->user);
        if($keyword == ""){
            $objects = $drive->lists($page);
        }else{
            $objects = $drive->findByName($keyword,$page);
        }

        return $this->respond($objects);
    }

    public function store(Request $request)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }
            $file = $request->file('upload') ;

            $drive = $this->factory->makeDrive($this->user);
            $object = $drive->writeOnDB($file);
            $result = $object->putOnCDN($drive);

            return $this->respond($result);
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function show($id)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $drive = $this->factory->makeDrive($this->user);
            $result = $drive->find($id);

            return $this->respond($result);
        } catch(ModelNotFoundException $e){
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function destroy($id)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $drive = $this->factory->makeDrive($this->user);
            $result = $drive->delete($id);

            return $this->respond($result);
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
