<div id="share-modal" class="reveal-modal modal small collaborator-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="modal-header">
        <h2>Share Series</h2>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="large-12 columns">
                <div class="row collapse">
                    <div class="small-8 medium-9 large-10 columns">
                        <input type="text" placeholder="url">
                    </div>
                    <div class="small-4 medium-3 large-2 columns">
                        <a href="#" class="button postfix">COPY</a>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

