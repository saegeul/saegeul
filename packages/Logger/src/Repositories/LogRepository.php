<?php
namespace Saegeul\Logger\Repositories ; 

use Saegeul\Database\Repository\IlluminateRepository ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class LogRepository extends IlluminateRepository
{
    protected $model = 'Saegeul\Logger\Models\Log' ;

    public function findBySeriesId($seriesId, $page)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('series_id',$seriesId); 
        $totalCount = $model->count() ; 

        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->with('user');
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function findByPostId($postId, $page)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('post_id',$postId); 
        $totalCount = $model->count() ; 

        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->with('user');
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }
}
