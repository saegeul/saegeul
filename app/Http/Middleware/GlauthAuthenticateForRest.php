<?php namespace App\Http\Middleware ;

use Artgrafii\Glauth\Glauth ;
use Closure ;

class GlauthAuthenticateForRest
{
    protected $statusCode = 200 ; 

    public function __construct(Glauth $glauth)
    {
        $this->glauth = $glauth ;
    }

    public function handle($request, Closure $next)
    {
        $user = $this->glauth->check();

        if($user == null){
            return $this->respondUnauthorized() ;
        }

		return $next($request);
    }

    public function respondWithError($message)
    {
        return response()->json([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ],$this->getStatusCode());
    }

    public function getStatusCode()
    {
        return $this->statusCode ;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode ;
        return $this ;
    }

    public function respondUnauthorized($message = 'Unauthorized!')
    {
        return $this->setStatusCode(401)->respondWithError($message) ; 
    }
}
