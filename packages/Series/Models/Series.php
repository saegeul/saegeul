<?php

namespace Saegeul\Series\Models;

use Illuminate\Database\Eloquent\Model;
use Saegeul\Permission\Accessor;
use Saegeul\Invitation\Contracts\InvitableInterface ;
use Saegeul\Permission\PermissionGenerator;
use Saegeul\Series\SeriesRoleCollection;
use Saegeul\Favorite\Contracts\FavoriteInterface;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Saegeul\Logger\Contracts\LoggableInterface ;
use Cartalyst\Tags\TaggableTrait;
use Cartalyst\Tags\TaggableInterface;

class Series extends Model implements InvitableInterface, FavoriteInterface, LoggableInterface, TaggableInterface
{
    use TaggableTrait;
    use Accessor ;

    protected $table = 'series' ;
    protected $guarded = array() ;

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }

    public function favorites($page=1)
    {
        $limit = 10;

        $model = $this->morphMany('Saegeul\Favorite\Models\Favorite', 'favorite')->getModel();
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$limit)->take($limit);
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$limit,$page) ;
    }

    public function collaborators()
    {
        $slug = $this->slugForCollaborator() ; 
    }

    public function image()
    {
        return $this->belongsTo('Artgrafii\SgDrive\Models\FileObject','series_image');
    }

    public function slugForCollaborator()
    {
        $permissionGenerator = new PermissionGenerator;
        $SeriesRoleCollection = new SeriesRoleCollection;

        return $permissionGenerator->generateSlug($this,$SeriesRoleCollection->getCollaborator());
    }
    public function invitation()
    {
        return $this->morphMany('Invitation','invited');
    }

    public function getClassName()
    {
        return $this->getMorphClass();
    }

    public function updateFavorite($favorite)
    {
        $this->favorite = $favorite;
        $this->save();

        return $this;
    }

    public function getLoggedData()
    {
        $registerData = array();
        $registerData['series_id'] = $this->id;

        return $registerData;
    }

    public function getTags($tag)
    {
        $namespace = $this->getEntityClassName();

        $tags = $this
            ->createTagsModel()
            ->whereNamespace($namespace)
            ->where('name','like','%'.$tag.'%')
            ->where('count','>',0);

        return $tags;
    }
}
