<?php

namespace Saegeul\Invitation;

use Illuminate\Bus\Dispatcher;
use Saegeul\Invitation\Contracts\InvitableInterface;
use Saegeul\Invitation\Jobs\InviteRole\InviteRoleCommand;
use Saegeul\Invitation\Jobs\VisitRole\VisitRoleCommand;
use Saegeul\Invitation\Jobs\JoinRole\JoinRoleCommand;
use Saegeul\Invitation\Jobs\DetachRole\DetachRoleCommand;
use Saegeul\Invitation\Models\Invitable;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface ;

class Invitation
{
    protected $dispatcher; 
    protected $invitationRepository;

    public function __construct(Dispatcher $dispatcher, InvitationRepositoryInterface $invitationRepository)
    {
        $this->dispatcher = $dispatcher;
        $this->invitationRepository = $invitationRepository; 
    }

    public function inviteRole($roleId, $email, InvitableInterface $invitableModel)
    {
        $command = new InviteRoleCommand($roleId,$email,$invitableModel); 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function joinRole($invitationCode, $userId)
    {
        $command = new JoinRoleCommand($invitationCode,$userId); 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function detachRole($userId, $roleId)
    {
        $command = new DetachRoleCommand($userId,$roleId); 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function getRepository()
    {
        return $this->invitationRepository ;
    }
}
