<?php

namespace Saegeul\Vcs\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;
use Saegeul\Post\Models\Post;

interface ActivityRepositoryInterface extends RepositoryInterface 
{
    public function countActivity(Post $post,$userId);
}
