@extends("layouts.default")

@section('contents')
<br/>
<div class="container"> 
    <div class="mini-form-wrapper">
        <h6>Welcome to saegeul</h6>
        <p class="welcome-text">We sent a mail to you. Verify your account. </p>
        
        {!! Form::open(['class' =>'clearfix signup-form']) !!}
            <a class="btn btn-block ghost-btn" href="/signin">&nbsp;Sign in </a>
        {!! Form::close() !!}
    </div>
</div> 
@stop 
