<!DOCTYPE html>
<html lang="en" class="no-js" >
    <head>
        <meta charset="utf-8">
        <title>Saegeul</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="/sgpack/flat-ui/images/favicon.ico">
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    </head>

<body > 
@yield('contents') 


<script src="/bower_components/foundation/js/vendor/jquery.js"></script>

@yield('scripts')

@include('partials.guide-script')

<script src="/bower_components/foundation/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>

</body>
</html>
