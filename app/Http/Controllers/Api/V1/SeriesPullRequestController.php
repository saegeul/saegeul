<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Series\Series ;
use Saegeul\Vcs\Vcs;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use League\Fractal ;
use League\Fractal\Manager;
use App\Http\Controllers\Api\V1\Transformers\PullRequestTransformer;

class SeriesPullRequestController extends ApiController 
{
    protected $seriesService;
    protected $vcsService;
    protected $glauth;
    protected $user;
    protected $fractal;

    public function __construct(
        Series $seriesService, 
        Vcs $vcsService,
        Glauth $glauth, 
        Manager $fractal
    )
	{
        $this->seriesService = $seriesService ;
        $this->vcsService = $vcsService;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->fractal = $fractal;
	}

    public function index(Request $request, $seriesId)
    {
        try{
            $series = $this->seriesService->show($seriesId);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $pullRequestRepository = $this->vcsService->getPullRequestRepository();
            $pullRequests = $pullRequestRepository->findBySeries($series->getKey());

            $resource = new Fractal\Resource\Collection($pullRequests, new PullRequestTransformer);
            $result = $this->fractal->createData($resource)->toJson();

            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function show(Request $request, $seriesId, $pullRequestId)
    {
        try{
            $series = $this->seriesService->show($seriesId);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $pullRequestRepository = $this->vcsService->getPullRequestRepository();
            $pullRequest = $pullRequestRepository->find($pullRequestId);
            if(!$pullRequest){
                return $this->respondNotFound();
            }

            $resource = new Fractal\Resource\Item($pullRequest, new PullRequestTransformer);
            $result = $this->fractal->createData($resource)->toJson();

            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
