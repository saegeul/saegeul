<?php
namespace Saegeul\Cover\Repositories;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Cover\Contracts\CoverRepositoryInterface ;


class CoverRepository extends IlluminateRepository implements CoverRepositoryInterface
{
    protected $model = 'Saegeul\Cover\Models\Cover' ;

    public function findBySeries($seriesId)
    {
        $model = $this->modelFactory->create($this->model) ;
        return $model->where('series_id',$seriesId)->first() ;
    }
}
