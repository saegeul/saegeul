@extends("layouts.default_for_guide")

@section('contents')
<div class="workspace-wrapper">
    @include('guides.workspace.posts.partials.header') 
    <br/> 

    <div class="row" >
        <div class="large-9 columns ">
            <a class="button" href="{{route('guide.workspace.series.posts.write',array(1))}}">Write</a>
            <div>
                <ul class="status">
                    <li>DRAFT</li>
                    <li>PUBLISHED</li>
                </ul>
            </div>
            <ul>
                <li>
                    <a>Introduction</a>
                    <ul>
                        <li><a>Why We Choose a Laravel</a></li>
                    </ul>
                </li> 
                <li>
                    <a>Architecture</a> 
                    <ul>
                        <li><a>The way to success</a></li>
                        <li><a>The Best way to develope a web</a></li>
                    </ul> 
                </li> 
                <li>
                    <a>Routing</a>
                    <ul>
                        <li><a>Fascinating Routing</a></li>
                        <li><a>Customization Route</a></li> 
                        <li><a>Build your route file</a></li> 
                    </ul> 
                </li> 
                <li>
                    <a>Controller</a>
                    <ul>
                        <li><a>Do Controller</a></li>
                        <li><a>Controller is Magic</a></li> 
                    </ul>
                </li> 
                <li>
                    <a>Template</a>
                </li>
                <li>
                    <a>Eloquent</a>
                </li> 
            </ul>
            <hr/>
            <h5>Waiting for joining this series</h5>
        </div>
        <div class="large-3 columns">
            <div>
                <h6>About this Sereis</h6>
                <p>Sageul is wrting platform. It is so sexy, fascinationg. It will allow you to write beautiful text.</p>
            </div>
            <div>
                <h6>Collaborators</h6>
                <ul class="collaborators">
                    <li class="collaborator">
                    <div class="row small-uncollapse medium-uncollapse large-uncollapse">
                        <div class="columns small-12">
                            <div class="avatar-wrapper">
                                <div class="avatar">
                                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/felipenogs/128.jpg" />
                                </div>
                                <div class="description"> 
                                    <span class="email">jaehee@artgrafii.com</span>
                                    <small>Accepted in 3,July,2015</small> 
                                </div>
                            </div>
                        </div>
                    </div>
                    </li>

                    <li class="collaborator removed">
                    <div class="row small-uncollapse">
                        <div class="columns small-12">
                            <div class="avatar-wrapper">
                                <div class="avatar">
                                    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg" />
                                </div>
                                <div class="description"> 
                                    <span class="email">sihyeong@artgrafii.com</span>
                                    <small>Accepted in 3,July,2015</small> 
                                </div>
                            </div>
                        </div>
                    </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>
<div>
@stop
