<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class VcsTest extends TestCase
{
    private $prefix = 'api/v1/posts';
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $glauth->signout();
    }

    private function signinForVcs()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(2);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    public function testFork()
    {
        $this->signinForVcs();

        $response = $this->call('POST',$this->prefix.'/1/fork');        
        $result = $response->getContent();
        $arr = json_decode($result,true);
        $this->assertEquals($arr['data']['origin_id'], 1);
        $this->assertEquals($arr['data']['status'], 'draft');
        $this->assertEquals(200, $response->status());
    }

    public function testCommit()
    {
        $this->signin();

        $response = $this->call('PUT',$this->prefix.'/1/commit');        
        $this->assertEquals(200, $response->status());
    }

    public function testCheckDirty()
    {
        $this->signin();
        $this->testCommit();
        $this->signout();
        $this->signinForVcs();

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $post = $postRepository->lists(1,'')->first();

        $response = $this->call('GET',$this->prefix.'/'.$post->getKey().'/dirty');        
        $this->assertEquals(200, $response->status());
    }

    public function testRollback()
    {
        $this->signinForVcs();

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $post = $postRepository->lists(1,'')->first();
        $response = $this->call('PUT',$this->prefix.'/'.$post->getKey().'/commit');        
        $this->assertEquals(200, $response->status());

        $vcs = $this->app->make('Saegeul\Vcs\Vcs') ;
        $revisionRepository = $vcs->getRevisionRepository();
        $uuid = $revisionRepository->history($post->getKey(),1)->first()->uuid;

        $response = $this->call('PUT',$this->prefix.'/'.$post->getKey().'/rollback',['uuid'=>$uuid]);        
        $this->assertEquals(200, $response->status());
    }

    public function testPullRequest()
    {
        $this->signinForVcs();

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $post = $postRepository->lists(1,'')->first();
        $data = [
            'series_id' => 1,
            'comment' => 'input the post 1'
            ];
        $response = $this->call('POST',$this->prefix.'/'.$post->getKey().'/pullrequest',$data);        
        $this->assertEquals(200, $response->status());
    }

    public function testMerge()
    {
        $this->signin();

        /*
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $post = $postRepository->lists(1,'')->first();
         */

        $mergeData = Factory::build('Saegeul\Post\Models\Post');

        $data = [
            'from_post_id' => 1,
            'title' => $mergeData['title'],
            'description' => $mergeData['description'],
            'html' => $mergeData['content'],
            'content' => $mergeData['content']
        ];

        $response = $this->call('PUT',$this->prefix.'/2/merge',$data);        
        $this->assertEquals(200, $response->status());
    }

    public function testImport()
    {
        $this->signinForVcs();

        // register new post
        $data = Factory::build('Saegeul\Post\Models\Post');
        $formData = $data->toArray();
        $formData['html'] = $data['content'];
        $path = public_path().'/img/logo/saegeul_small.png';
        $fileService = $this->app->make('Artgrafii\SgDrive\Services\FileService') ;
        $sereisImage = $fileService->makeFile($path);
        $temp = $this->call('POST',$this->prefix,$formData, [], ['post_image' => $sereisImage]);
        $result = $temp->getContent();
        $arr = json_decode($result);

        // register pull request
        $data = [
            'series_id' => 1,
            'comment' => 'input the post'
            ];
        $temp = $this->call('POST',$this->prefix.'/'.$arr->data->id.'/pullrequest',$data);
        $result = $temp->getContent();
        $arr = json_decode($result);

        // import
        $this->signout();
        $this->signin();
        $response = $this->call('POST',$this->prefix.'/import',['pull_request_id'=> $arr->data->id]);        
        $this->assertEquals(200, $response->status());
    }

    public function testRevisions()
    {
        $this->signout();
        $this->signin();

        $response = $this->call('GET',$this->prefix.'/1/revisions?page=1');        
        $this->assertEquals(200, $response->status());
    }
}
