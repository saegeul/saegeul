@extends("layouts.default_for_guide")

@section('contents')
<div class="signup-wrapper">
    <div class="logo-wrapper" >
        <img src="/img/logo/saegeul_wordmark_lg.png" />
    </div>

    <form class="mini-paper"> 
        <div class="">
            <label>Forgot your Password ?</label>
            <p><span>Enter your email</span>, we will send a reset link. </p>
        </div>
        <div class="field-group">
            <div>
                <input type="text" />
            </div>
        </div> 
        <div class="clearfix">
            <a class="button alert small expand">Send Reset Link</a>
        </div>
    </form>
    <p class="text-center">
        <a class="mini-anchor">Return to Login</a>
    </p>
</div>
@stop
