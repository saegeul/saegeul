<?php

namespace Saegeul\Subscription;

use Illuminate\Bus\Dispatcher ;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface ;
use Saegeul\Subscription\Jobs\Register\RegisterCommand ;
use Saegeul\Subscription\Jobs\Confirm\ConfirmCommand ;
use Saegeul\Subscription\Jobs\Cancel\CancelCommand ;
use Artgrafii\Glauth\Contracts\UserInterface ;

class Subscription
{
    protected $dispatcher; 

    public function __construct(Dispatcher $dispatcher, SubscriptionRepositoryInterface $subscriptionRepository)
    {
        $this->dispatcher = $dispatcher ;
        $this->subscriptionRepository = $subscriptionRepository ;
    }

    public function register(array $data, UserInterface $user)
    {
        $data['user_id'] = $user->getKey();
        $data['email'] = $user->getEmail();
        $command = new RegisterCommand($data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function confirm($subscriptionCode)
    {
        $command = new ConfirmCommand($subscriptionCode) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function cancel($email, $seriesId)
    {
        $command = new CancelCommand($email,$seriesId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function getRepository()
    {
        return $this->subscriptionRepository ;
    }
}
