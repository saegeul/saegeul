<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        factory(Artgrafii\Glauth\Models\User::class, 5)->make()->each(function($u){ 
            $glauth = App::make('Artgrafii\Glauth\Glauth') ;
            $userService = $glauth->getUserService();
            $userRepository = $userService->getUserRepository();

            $uuidService = App::make('Artgrafii\Uuid\UuidService') ;
            $uuid = $uuidService->getRandomUuid();
            $user = $glauth->signup($u->email,'1q2w3e',$u->unique_link,$uuid) ;

            $activationRepository = App::make('sentinel.activations') ;
            $activation = $activationRepository->exists($user);
            $code = $activation->code ;

            $hashCodeService = App::make('Artgrafii\Glauth\Services\HashCodeService');
            $hashCode = $hashCodeService->encode($user->getKey());

            $activationService = $glauth->getActivationService();
            $result = $activationService->confirm($hashCode,$code);

        }); 
    }
}
