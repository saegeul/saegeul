<?php

namespace spec\Saegeul\Subscription\Jobs\Cancel;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Subscription\Models\Subscription as SubscriptionModel ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Subscription\Events\SubscriptionWasCanceled;

class CancelCommandSpec extends ObjectBehavior
{
    protected $email = 'sihyeong@artgrafii.com' ;
    protected $seriesId = 1;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Subscription\Jobs\Cancel\CancelCommand');
    }

    public function let()
    {
        $this->beConstructedWith($this->email,$this->seriesId);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        SubscriptionRepositoryInterface $subscriptionRepository, 
        SubscriptionModel $subscription,
        SeriesRepositoryInterface $seriesRepository,
        SeriesModel $series
    )
    {
        $seriesRepository->find($this->seriesId)->shouldBeCalled()->willReturn($series);

        $subscriptionRepository->findByEmailForSeries($this->email,$this->seriesId)->shouldBeCalled()->willReturn($subscription);
        $subscription->getKey()->shouldBeCalled()->willReturn(1);
        $subscriptionRepository->delete(1)->shouldBeCalled()->willReturn($subscription);

        $dispatcher->fire(new SubscriptionWasCanceled($subscription->getWrappedObject(),$series->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$subscriptionRepository,$seriesRepository)->shouldReturnAnInstanceOf('Saegeul\Subscription\Models\Subscription');
    }
}
