<?php
namespace Saegeul\Notification\Events\Handlers ;

use Saegeul\Notification\Events\Handlers\EventHandler ;
use Saegeul\Vcs\Events\PostWasMerged;
use Saegeul\Vcs\Events\PostWasPullRequested;
use Saegeul\Vcs\Events\PullRequestWasDiscarded;
use Saegeul\Vcs\Vcs;
use Saegeul\Notification\Notification ;
use Illuminate\Mail\Mailer ;

class VcsEventHandler extends EventHandler
{
    protected $vcsService;

    public function __construct(
        Mailer $mailer, 
        Notification $notificationService,
        Vcs $vcsService
    )
    {
        parent::__construct($mailer,$notificationService);

        $this->vcsService = $vcsService;
    }

    public function whenPostWasPullRequested(PostWasPullRequested $event)
    {
        $pullRequest = $event->pullRequest;

        $post = $pullRequest->post;
        $series = $pullRequest->series;
        $sender = $post->user;
        $receiver = $series->user;

        $notification = $this->handle($pullRequest,__FUNCTION__) ;
        $notification->receiver = $receiver->getKey();
        $notification->save();

        $data = array();
        $data['series'] = $series;
        $data['post'] = $post;
        $data['sender'] = $sender;
        $data['receiver'] = $receiver;

        $mailInfo['email'] = $receiver->email;
        $mailInfo['title'] = "[Saegeul] Pull Requested For Your Series";
        $mailInfo['view'] = "emails.pull-request";

        $this->sendEmail($data,$mailInfo);
    }

    public function whenPostWasMerged(PostWasMerged $event)
    {
        $mergedPost = $event->post;

        $revisionRepository = $this->vcsService->getRevisionRepository();
        $revision = $revisionRepository->findByUuid($mergedPost->source_uuid);

        $pullRequestRepository = $this->vcsService->getPullRequestRepository();
        $pullRequest = $pullRequestRepository->getByPostId($revision->post_id);

        if($pullRequest){
            $post = $pullRequest->post;
            $series = $pullRequest->series;
            $sender = $mergedPost->user;
            $receiver = $pullRequest->user;

            $notification = $this->handle($pullRequest,__FUNCTION__) ;
            $notification->receiver = $receiver->getKey();
            $notification->save();

            $data = array();
            $data['series'] = $series;
            $data['post'] = $post;
            $data['sender'] = $sender;
            $data['receiver'] = $receiver;

            $mailInfo['email'] = $receiver->email;
            $mailInfo['title'] = "[Saegeul] Merged Your Post";
            $mailInfo['view'] = "emails.merge";

            $this->sendEmail($data,$mailInfo);
        }
    } 

    public function whenPullRequestWasDiscarded(PullRequestWasDiscarded $event)
    {
        $pullRequest = $event->pullRequest;

        $post = $pullRequest->post;
        $series = $pullRequest->series;
        $sender = $series->user;
        $receiver =  $post->user;

        $notification = $this->handle($pullRequest,__FUNCTION__) ;
        $notification->receiver = $receiver->getKey();
        $notification->save();

        $data = array();
        $data['series'] = $series;
        $data['post'] = $post;
        $data['sender'] = $sender;
        $data['receiver'] = $receiver;

        $mailInfo['email'] = $receiver->email;
        $mailInfo['title'] = "[Saegeul] Discard For Your Suggestion";
        $mailInfo['view'] = "emails.discard";

        $this->sendEmail($data,$mailInfo);
    }
}
