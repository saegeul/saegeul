<?php

namespace spec\Saegeul\Favorite\Jobs\Delete;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Saegeul\Favorite\Models\Favorite as FavoriteModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Favorite\Events\FavoriteWasDeleted ;

class DeleteCommandSpec extends ObjectBehavior
{
    protected $favoriteId ;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Favorite\Jobs\Delete\DeleteCommand');
    }

    public function let()
    {
        $favoriteId = 1;
        $this->favoriteId = $favoriteId;
        $this->beConstructedWith($favoriteId);
    }

    public function it_trigger_handle(Dispatcher $dispatcher, FavoriteRepositoryInterface $favoriteRepository, FavoriteModel $favorite)
    {
        $favoriteRepository->find($this->favoriteId)->shouldBeCalled()->willReturn($favorite);

        $favoriteRepository->delete($this->favoriteId)->shouldBeCalled()->willReturn($favorite);

        $dispatcher->fire(new FavoriteWasDeleted($favorite->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$favoriteRepository)->shouldReturnAnInstanceOf('Saegeul\Favorite\Models\Favorite');
    }
}
