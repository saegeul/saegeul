<?php

namespace Saegeul\Vcs\Jobs\Discard;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PullRequestWasDiscarded;

class DiscardCommand implements SelfHandling
{

    public $pullRequestId;

    public function __construct($pullRequestId)
    {
        $this->pullRequestId = $pullRequestId;
    }

    public function handle(
        Dispatcher $dispatcher, 
        PullRequestRepositoryInterface $pullRequestRepository
    )
    {
        $pullRequest = $pullRequestRepository->find($this->pullRequestId);
        if(!$pullRequest){
            throw New ModelNotFoundException('Not found pull request id');
        }

        $pullRequest->processed_status = 'discard';
        $pullRequest->save();

        $dispatcher->fire(new PullRequestWasDiscarded($pullRequest));

        return $pullRequest;
    }
}
