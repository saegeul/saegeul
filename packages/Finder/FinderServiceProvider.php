<?php

namespace Saegeul\Finder;

use Illuminate\Support\ServiceProvider;
use Saegeul\Finder\Services\YoutubeFinderService;
use Saegeul\Finder\Services\TwitterFinderService;
use Saegeul\Finder\Services\InstagramFinderService;
use TwitterOAuth\Auth\ApplicationOnlyAuth;
use TwitterOAuth\Serializer\ArraySerializer;
use Instagram\Instagram;

class FinderServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->registerYoutube();    
        $this->registerTwitter();    
        $this->registerInstagram();    
    }

    public function registerYoutube()
    {
        $credentials = $this->app['config']->get('finder.youtube'); 

        $this->app->bind('Saegeul\Finder\Services\YoutubeFinderService',function() use ($credentials) {
            $client = new \Google_Client();
            $client->setDeveloperKey($credentials);
            $service = new \Google_Service_YouTube($client); 
            $search = $service->search;

            return new YoutubeFinderService($search);
        });
    }

    public function registerTwitter()
    {
        $credentials = $this->app['config']->get('finder.twitter'); 

        $this->app->bind('Saegeul\Finder\Services\TwitterFinderService',function() use ($credentials) {
            $auth = new ApplicationOnlyAuth($credentials, new ArraySerializer());

            return new TwitterFinderService($auth);
        });
    }

    public function registerInstagram()
    {
        $this->app->bind('Saegeul\Finder\Services\InstagramFinderService',function($app, $parameters) {
            $instagram = new Instagram;
            $instagram->setAccessToken($parameters['accessToken']);

            return new InstagramFinderService($instagram);
        });
    }
}
