<?php

namespace Saegeul\Vcs\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface RevisionRepositoryInterface extends RepositoryInterface 
{
    public function history($postId,$page);
    public function findByUuid($uuid);
}
