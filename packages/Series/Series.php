<?php

namespace Saegeul\Series;

use Illuminate\Bus\Dispatcher ;
use Saegeul\Series\Jobs\Register\RegisterCommand ;
use Saegeul\Series\Jobs\Update\UpdateCommand ;
use Saegeul\Series\Jobs\Show\ShowCommand ;
use Saegeul\Series\Jobs\Delete\DeleteCommand ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Artgrafii\Glauth\Contracts\UserInterface ;

class Series
{
    protected $dispatcher; 
    protected $seriesRepository;

    public function __construct(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository)
    {
        $this->dispatcher = $dispatcher ;
        $this->seriesRepository = $seriesRepository ;
    }

    public function register($data)
    {
        $command = new RegisterCommand($data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function update($seriesId, $data)
    {
        $command = new UpdateCommand($seriesId,$data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function show($seriesId)
    {
        $command = new ShowCommand($seriesId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function delete($seriesId)
    {
        $command = new DeleteCommand($seriesId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function getRepository()
    {
        return $this->seriesRepository ;
    }

    public function createWithUser(array $data, UserInterface $user)
    {
        $data['user_id'] = $user->getKey() ;
        return $this->register($data) ;
    }

}
