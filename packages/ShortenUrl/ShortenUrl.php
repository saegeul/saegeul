<?php

namespace Saegeul\ShortenUrl;

use Saegeul\ShortenUrl\Contracts\ShortenUrlInterface;

class ShortenUrl implements ShortenUrlInterface
{
    protected $shortener;
    protected $url;

    public function __construct(\Google_Service_Urlshortener $shortener, \Google_Service_Urlshortener_Url $url)
    {
        $this->shortener = $shortener;
        $this->url = $url;
    }

    public function register($longUrl)
    {
        $this->url->longUrl = $longUrl;

        return $this->shortener->url->insert($this->url);
    }
}
