<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class PostRevisionTest extends TestCase
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    }

    public function testGetPosts()
    {
        #given
        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        $this->signin();
        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/revisions';
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(200, $response->status());
        /*
        $result = $response->getContent();
        $arr = json_decode($result, true);
         */
    }

    public function test_failed_case_to_get_posts_no_login()
    {
        #given
        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/revisions';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_get_posts_invalid_post_id()
    {
        #given
        $this->signin();
        $postId = 111;

        #when

        $prefix = 'api/v1/posts/'.$postId.'/revisions';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(404, $response->status());
    }

    public function testShow()
    {
        #given
        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        $this->signin();

        $prefix = 'api/v1/posts/'.$post->getKey().'/revisions';
        $response = $this->call('GET',$prefix);
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $revisionId = $arr['data'][0]['id'];

        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/revisions/'.$revisionId;
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(200, $response->status());
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['id'], $revisionId);
    }

    public function test_failed_case_to_show_no_login()
    {
        #given
        $this->signin();

        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        $prefix = 'api/v1/posts/'.$post->getKey().'/revisions';
        $response = $this->call('GET',$prefix);
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $revisionId = $arr['data'][0]['id'];

        $this->signout();
        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/revisions/'.$revisionId;
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_show_invalid_data()
    {
        #given
        $this->signin();

        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        $revisionId = '1000';

        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/revisions/'.$revisionId;
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(404, $response->status());
    }
}
