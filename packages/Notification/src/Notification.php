<?php
namespace Saegeul\Notification;

use Saegeul\Notification\Contracts\NotificationRepositoryInterface;
use Saegeul\Notification\Contracts\NotifiableInterface ;
use Artgrafii\Glauth\Glauth ;

class Notification
{
    protected $repository ;
    protected $glauth ; 

    public function __construct(NotificationRepositoryInterface $repository, Glauth $glauth)
    {
        $this->repository = $repository;
        $this->glauth = $glauth ;
    }

    public function register(NotifiableInterface $data , $eventName)
    { 
        $notificationData = $data->getNotificationData() ;
        $notificationData['command'] = $eventName;
        $notificationData['user_id'] = $this->glauth->check()->getKey();

        $ret = $this->repository->store($notificationData); 

        return $ret ; 
    }

    public function getNotificationWithReceiver($page = 1)
    {
        return $this->repository->findByReceiver($this->glauth->check()->getKey(),$page);
    }

    public function show($id)
    {
        $notification = $this->repository->find($id);
        if(!$notification->is_confirmed){
            $notification->is_confirmed = 1;
            $notification->notified_at = date('Y-m-d H:i:s');
            $notification->save();
        }

        return $notification;
    }
}
