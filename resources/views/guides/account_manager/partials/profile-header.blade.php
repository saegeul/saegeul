<div class="profile-header-wrapper">
    <div class="row">
        <div class="small-12">
            <div class="profile-header">
                <h1>{{trans('mypage.messages.hello',array('username'=>'Jaehee'))}} </h1>
            </div>
        </div>
    </div>
    <div class="row"> 
        <ul class="tabs simple-tabs">
            <li class="tab-title active"><a aria-selected="false" tabindex="-1" href="{{route('guide.account.profile')}}">Profile</a></li>
            <li class="tab-title"><a aria-selected="false" tabindex="-1" href="{{route('guide.account.password')}}">Password</a></li>
        </ul>
    </div>
</div>
