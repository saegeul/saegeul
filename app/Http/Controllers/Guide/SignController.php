<?php
namespace App\Http\Controllers\Guide ;

use App\Http\Controllers\Controller ;

class SignController extends Controller { 

    public function signup()
    {
        return view('guides.sign.signup') ;
    }

    public function signin()
    {
        return view('guides.sign.signin') ;
    } 

    public function forgotPassword()
    {
        return view('guides.sign.forgot-password') ;
    }

    public function activationForm()
    {
        return view('guides.sign.activation-form') ;
    } 
}
