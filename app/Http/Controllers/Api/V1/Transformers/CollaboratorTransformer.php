<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Cartalyst\Sentinel\Roles\EloquentRole;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;
use App\Http\Controllers\Api\V1\Transformers\InvitationTransformer;
use Artgrafii\Glauth\Models\User;

class CollaboratorTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user', 'invitation'];

    public function transform(EloquentRole $role)
    {
        return [
            'id' => (int) $role->id,
            'slug' => $role->slug,
            'name' => $role->name,
            'permissions' => $role->permissions,
            'created_at' => $role->created_at,
            'updated_at' => $role->updated_at
        ];
    }

    public function includeUser(EloquentRole $role)
    {
        $collaborators = $role->users;

        return $this->collection($collaborators, new UserTransformer);
    }

    public function includeInvitation(EloquentRole $role)
    {
        $invitationService = \App::make('Saegeul\Invitation\Invitation');
        $invitationRepository = $invitationService->getRepository();
        $waitings = $invitationRepository->findOnWaiting($role->getKey());

        return $this->collection($waitings, new InvitationTransformer);
    }
}
