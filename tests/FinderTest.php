<?php

use Artgrafii\Glauth\Services\HashCodeService ;

class FinderTest extends TestCase 
{
    public function testFactory()
    {
        $finderManager = $this->app->make('Saegeul\Finder\Finder') ;
        $provider = 'youtube';
        $options = [];
        $result = $finderManager->factory($provider,$options);
        $this->assertInstanceOf('Saegeul\Finder\Services\YoutubeFinderService',$result);
    }


    /**
     * @expectedException ReflectionException
     **/
    public function testFailedCaseWithNonProvider()
    {
        $finderManager = $this->app->make('Saegeul\Finder\Finder') ;
        $provider = 'empt';
        $options = [];
        $finderManager->factory($provider,$options);
    }

    public function testYoutubeFind()
    {
        $provider = 'youtube';
        $options = [];
        $finderManager = $this->app->make('Saegeul\Finder\Finder') ;
        $finderService = $finderManager->factory($provider,$options);
        $keyword = 'chrismas';
        $page = 1;
        $result = $finderService->find($keyword,$page);
        $this->assertArrayHasKey('nextPage',$result);
    }

    public function testTwitterFind()
    {
        $provider = 'twitter';
        $options = [];
        $finderManager = $this->app->make('Saegeul\Finder\Finder') ;
        $finderService = $finderManager->factory($provider,$options);
        $keyword = 'chrismas';
        $page = 1;
        $result = $finderService->find($keyword,$page);
        $this->assertArrayHasKey('nextPage',$result);
    }
}
