<?php

namespace Saegeul\Subscription\Models;

use Illuminate\Database\Eloquent\Model;
use Saegeul\Permission\Accessor;
use Saegeul\Notification\Contracts\NotifiableInterface ;

class Subscription extends Model implements NotifiableInterface
{
    protected $table = 'subscriptions' ;
    protected $guarded = array() ;

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }

    public function series()
    {
        return $this->belongsTo('Saegeul\Series\Models\Series');
    }

    public function countSubscription()
    {
        $model = $this->getModel();
        $model = $model->where('series_id',$this->series_id);
        $model = $model->where('activated',1);
        return $model->count();
    }

    public function getNotificationData() 
    {
        $registerData = array();
        $registerData['series_id'] = $this->series_id;

        return $registerData;
    }
}
