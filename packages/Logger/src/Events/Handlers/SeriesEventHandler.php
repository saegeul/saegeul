<?php

namespace Saegeul\Logger\Events\Handlers ;

use Saegeul\Logger\Events\Handlers\EventHandler ; 
use Saegeul\Series\Events\SeriesWasRegistered ;
use Saegeul\Series\Events\SeriesWasUpdated ;
use Saegeul\Series\Events\SeriesWasDeleted ; 

class SeriesEventHandler extends EventHandler
{
    public function whenSeriesWasRegistered(SeriesWasRegistered $event)
    {
        $this->handle($event->series,__FUNCTION__);
    }

    public function whenSeriesWasUpdated(SeriesWasUpdated $event)
    {
        $this->handle($event->series,__FUNCTION__);
    }

    public function whenSeriesWasDeleted(SeriesWasDeleted $event)
    {
        $this->handle($event->series,__FUNCTION__);
    } 
}
