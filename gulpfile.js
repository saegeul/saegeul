var elixir = require('laravel-elixir');
var gulp = require('gulp') ;
var gutil = require('gulp-util') ;
var webpack = require("webpack");
var merge = require("webpack-merge");
var WebpackDevServer = require("webpack-dev-server");
var path = require('path') ;
var HtmlWebpackPlugin = require('html-webpack-plugin') ; 
var ROOT_PATH = path.resolve(__dirname) ;

var TARGET = process.env.npm_lifecycle_event ; 

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */



elixir(function(mix) {
    mix.copy('public/bower_components/foundation/scss', 'resources/assets/sass');
    mix.sass(['app.scss']);
    mix.version('public/css/app.css') ; 
    //mix.task('webpack-dev-server');
}); 

/*
var webpackConfig = { 
    entry : path.resolve(ROOT_PATH , 'webapp/Main.js') , 
    output : {
        path : path.resolve(ROOT_PATH , 'build'),
        publicPath : '/assets/',
        filename: 'bundle.js'
    },
    devServer: {
        colors: true,
        historyApiFallback : true, 
        host: '0.0.0.0',
        port: 8080,
        hot :true, 
        inline : true, 
        progress : true
    },
    module : {
        loaders: [{
            test : /\.jsx?$/,
            loaders : ['babel'] ,
            include: path.resolve(ROOT_PATH , 'webapp')
        }]
    }, 
    plugins: [
        new webpack.HotModuleReplacementPlugin() 
        new HtmlWebpackPlugin({
          'title': 'example'
          })
    ]
};


if(TARGET == 'start' || !TARGET) { 
}

gulp.task('webpack',function(callback){
    webpack(webpackConfig, function(err, stats) {
        //if(err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        callback();
    });
});

gulp.task("webpack-dev-server", function(callback) {
    var compiler = webpack(webpackConfig);

    new WebpackDevServer(compiler, {
        // server and middleware options
    }).listen(8080, "0.0.0.0", function(err) {
        if(err) throw new gutil.PluginError("webpack-dev-server", err);
        // Server listening
        gutil.log("[webpack-dev-server]", "http://localhost:8080/webpack-dev-server/index.html");

        // keep the server alive or continue?
        // callback();
    });
});
*/
