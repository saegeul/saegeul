<?php

namespace Saegeul\Vcs\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface PullRequestRepositoryInterface extends RepositoryInterface 
{
}
