<?php

return [

    // series
    'register-series' => [
        'name' => 'Saegeul\Series\Events\SeriesWasRegistered',
        'handler' => ['Saegeul\Logger\Events\Handlers\SeriesEventHandler@whenSeriesWasRegistered','Saegeul\Series\Events\Handlers\SeriesEventHandler@whenSeriesWasRegistered']
    ],

    'update-series' => [
        'name' => 'Saegeul\Series\Events\SeriesWasUpdated',
        'handler' => 'Saegeul\Logger\Events\Handlers\SeriesEventHandler@whenSeriesWasUpdated'
    ],

    'delete-series' => [
        'name' => 'Saegeul\Series\Events\SeriesWasDeleted',
        'handler' => ['Saegeul\Logger\Events\Handlers\SeriesEventHandler@whenSeriesWasDeleted','Saegeul\Series\Events\Handlers\SeriesEventHandler@whenSeriesWasDeleted']
    ],

    // post
    'register-post' => [
        'name' => 'Saegeul\Post\Events\PostWasRegistered',
        'handler' => ['Saegeul\Logger\Events\Handlers\PostEventHandler@whenPostWasRegistered','Saegeul\Post\Events\Handlers\PostEventHandler@whenPostWasRegistered']
    ],

    // post
    'update-post' => [
        'name' => 'Saegeul\Post\Events\PostWasUpdated',
        'handler' => ['Saegeul\Logger\Events\Handlers\PostEventHandler@whenPostWasUpdated','Saegeul\Post\Events\Handlers\PostEventHandler@whenPostWasUpdated']
    ],

    'delete-post' => [
        'name' => 'Saegeul\Post\Events\PostWasDeleted',
        'handler' => ['Saegeul\Logger\Events\Handlers\PostEventHandler@whenPostWasDeleted','Saegeul\Post\Events\Handlers\PostEventHandler@whenPostWasDeleted']
    ],

    // vcs 
    'commit-post' => [
        'name' => 'Saegeul\Vcs\Events\PostWasCommited',
        'handler' => 'Saegeul\Logger\Events\Handlers\VcsEventHandler@whenPostWasCommited'
    ],

    'merge-post' => [
        'name' => 'Saegeul\Vcs\Events\PostWasMerged',
        'handler' => ['Saegeul\Logger\Events\Handlers\VcsEventHandler@whenPostWasMerged','Saegeul\Notification\Events\Handlers\VcsEventHandler@whenPostWasMerged','Saegeul\Vcs\Events\Handlers\VcsEventHandler@whenPostWasMerged']
    ],

    'fork-post' => [
        'name' => 'Saegeul\Vcs\Events\PostWasForked',
        'handler' => 'Saegeul\Logger\Events\Handlers\VcsEventHandler@whenPostWasForked'
    ],

    'import-post' => [
        'name' => 'Saegeul\Vcs\Events\PostWasImported',
        'handler' => ['Saegeul\Logger\Events\Handlers\VcsEventHandler@whenPostWasImported','Saegeul\Vcs\Events\Handlers\VcsEventHandler@whenPostWasImported']
    ],

    'pull-request-post' => [
        'name' => 'Saegeul\Vcs\Events\PostWasPullRequested',
        'handler' => ['Saegeul\Logger\Events\Handlers\VcsEventHandler@whenPostWasPullRequested','Saegeul\Notification\Events\Handlers\VcsEventHandler@whenPostWasPullRequested']
    ],

    'rollback-post' => [
        'name' => 'Saegeul\Vcs\Events\PostWasRollbacked',
        'handler' => 'Saegeul\Logger\Events\Handlers\VcsEventHandler@whenPostWasRollbacked'
    ],

    'discard-pull-request' => [
        'name' => 'Saegeul\Vcs\Events\PullRequestWasDiscarded',
        'handler' => ['Saegeul\Logger\Events\Handlers\VcsEventHandler@whenPullRequestWasDiscarded','Saegeul\Notification\Events\Handlers\VcsEventHandler@whenPullRequestWasDiscarded']
    ],

    // favorite
    'register-favorite' => [
        'name' => 'Saegeul\Favorite\Events\FavoriteWasRegistered',
        'handler' => 'Saegeul\Favorite\Events\Handlers\FavoriteEventHandler@whenFavoriteWasRegistered'
    ],

    'delete-favorite' => [
        'name' => 'Saegeul\Favorite\Events\FavoriteWasDeleted',
        'handler' => 'Saegeul\Favorite\Events\Handlers\FavoriteEventHandler@whenFavoriteWasDeleted'
    ],

    // invitation 
    'invite-role' => [
        'name' => 'Saegeul\Invitation\Events\UserWasInvitedRole',
        'handler' => ['Saegeul\Logger\Events\Handlers\InvitationEventHandler@whenUserWasInvitedRole','Saegeul\Invitation\Events\Handlers\InvitationEventHandler@whenUserWasInvitedRole']
    ],

    'join-role' => [
        'name' => 'Saegeul\Invitation\Events\UserWasJoinedRole',
        'handler' => 'Saegeul\Logger\Events\Handlers\InvitationEventHandler@whenUserWasJoinedRole'
    ],

    'detach-role' => [
        'name' => 'Saegeul\Invitation\Events\UserWasDetachedRole',
        'handler' => 'Saegeul\Logger\Events\Handlers\InvitationEventHandler@whenUserWasDetachedRole'
    ],

    // subscription
    'register-subscription' => [
        'name' => 'Saegeul\Subscription\Events\SubscriptionWasRegistered',
        'handler' => ['Saegeul\Subscription\Events\Handlers\SubscriptionEventHandler@whenSubscriptionWasRegistered','Saegeul\Notification\Events\Handlers\SubscriptionEventHandler@whenSubscriptionWasRegistered']
    ],

    'confirm-subscription' => [
        'name' => 'Saegeul\Subscription\Events\SubscriberWasActivated',
        'handler' => 'Saegeul\Subscription\Events\Handlers\SubscriptionEventHandler@whenSubscriberWasActivated'
    ],

    'cancel-subscription' => [
        'name' => 'Saegeul\Subscription\Events\SubscriptionWasCanceled',
        'handler' => 'Saegeul\Subscription\Events\Handlers\SubscriptionEventHandler@whenSubscriptionWasCanceled'
    ],

];
