<?php

namespace Saegeul\Invitation\Events ;

use Saegeul\Invitation\Models\Invitation ;
use Cartalyst\Sentinel\Roles\EloquentRole ;
use Saegeul\Invitation\Contracts\InvitableInterface;

class UserWasInvitedRole
{
    public $invitation;
    public $role;
    public $invitableModel ;    

    public function __construct(Invitation $invitation, EloquentRole $role, InvitableInterface $invitableModel)
    {
        $this->invitation = $invitation;
        $this->role = $role ;
        $this->invitableModel = $invitableModel;
    }
}
