@extends("layouts.default_for_guide")

@section('contents')
@include('guides.workspace.partials.header') 

<form class="mini-paper clearfix">
    
    <p>Welcome to workspace, <br/>
        Create First Series.<br/> 
        Then invite a collaborator. <br/> 
    </p>
    <a class="button alert right" href="{{route('guide.workspace.series.create')}}">New Series</a> 
</form>

@stop


@section('links')
@include('guides.workspace.series.partials.links')
@stop
