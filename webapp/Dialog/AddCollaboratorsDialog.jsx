import React from 'react' ;
import Request from 'superagent' ;

export default class AddCollaboratorsDialog extends React.Component {

    constructor(props){
        super(props) ;
        console.log(props) ;
        this.state = {
            email : '' , 
            users : []
        };
    }

    render() {
        var modalBgStyle = {
            display: 'block'
        };

        var modalStyle = {
            display: 'block',
            opacity: 1,
            visibility: 'visible'
        };

        return (
            <div>
                <div className="reveal-modal-bg" style={modalBgStyle}></div>

                <div className="reveal-modal small open" style={modalStyle}>
                    <h5>Invite Collaborators</h5>
                    <div>
                        <label>Email</label>
                        <input type="text" name="email" value={this.state.email} 
                                onChange={this.onChangeHandler.bind(this)} 
                                onKeyDown={this.onKeyPressHandler.bind(this)}  />
                    </div> 

                    <ul>
                    {
                    this.state.users.map((user,index) => {
                        return <li key={index}>{user}</li>
                    })
                    }
                    </ul>
                    <div>
                    </div>
                    <div>
                        <a onClick={this.sendInvitationEmail.bind(this)}>Invite</a>
                    </div>
                </div>
            </div>
        );
    }

    onChangeHandler(e) {
        this.setState({
            email : e.target.value
        });
    }

    onKeyPressHandler(e) {
        var ENTER = 13;
        
        if( e.keyCode == ENTER ) { 
            this.registerUser() ;
            this.setState({
                email : ''
            });
        } 
    } 

    registerUser() { 
        this.state.users.push(this.state.email)  ;
        this.setState({
            users  : this.state.users 
        });
    }

    sendInvitationEmail(){
        Request.post('/api/v1/series/'+this.props.seriesId+'/collaborators/process')
                .send({
                    'invitation_emails' : this.state.users
                })
                .end(function(err,response){
                    console.log(response) ;
                }.bind(this));
    }
}
