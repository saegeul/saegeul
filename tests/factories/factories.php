<?php

$factory('Artgrafii\Glauth\Models\User', [
    'email' => $faker->email,
    'password' => $faker->password ,
    'unique_link' => $faker->firstName
]);

$factory('Saegeul\Series\Models\Series', [
    'title' => $faker->title,
    'description' => $faker->text,
    'user_id' => 1
]);

$factory('Saegeul\Post\Models\Post', [
    'title' => $faker->title,
    'content' => $faker->text,
    'description' => $faker->text,
    'series_id' => 1,
    'user_id' => 1
]);
$factory('Saegeul\Post\Models\Post', [
    'title' => $faker->title,
    'content' => $faker->text,
    'description' => $faker->text,
    'series_id' => 1,
    'user_id' => 1
]);

$factory('Saegeul\Cover\Models\Cover', [
    'content' => $faker->text
]);
