<?php

namespace Saegeul\Vcs\Events\Handlers ;

use Saegeul\Vcs\Vcs;
use Saegeul\Vcs\Events\PostWasMerged ;
use Saegeul\Vcs\Events\PostWasImported;
use Saegeul\Post\Post;

class VcsEventHandler
{
    protected $vcsService;
    protected $postService;

    public function __construct(Vcs $vcsService, Post $postService)
    {
        $this->vcsService = $vcsService;
        $this->postService = $postService;
    }

    public function whenPostWasMerged(PostWasMerged $event)
    {
        //toPost
        $toPost = $event->post;
        $pullRequestRepository = $this->vcsService->getPullRequestRepository();
        $pullRequest = $pullRequestRepository->getByTargetId($toPost->getKey());
        if($pullRequest){
            $pullRequest->processed_status = 'merged';
            $pullRequest->save();
 
            $fromPost = $this->postService->show($pullRequest->post_id);
            $fromPost->pull_request_status = 'merged';
            $fromPost->save();

            $revisionRepository = $this->vcsService->getRevisionRepository();
            $revision = $revisionRepository->findByUuid($fromPost->commit_uuid);
            $revision->pull_request_status = 'merged';
            $revision->save();

            $activationRepository = $this->vcsService->getActivityRepository();
            $activationRepository->countActivity($toPost,$pullRequest->user_id);
        }
    }

    public function whenPostWasImported(PostWasImported $event)
    {
        $pullRequest = $event->pullRequest;
        $pullRequest->processed_status = 'imported';
        $pullRequest->save();

        $fromPost = $this->postService->show($pullRequest->post_id);
        $fromPost->pull_request_status = 'imported';
        $fromPost->save();

        $revisionRepository = $this->vcsService->getRevisionRepository();
        $revision = $revisionRepository->findByUuid($fromPost->commit_uuid);
        $revision->pull_request_status = 'imported';
        $revision->save();

        $toPost = $event->post;
        $activationRepository = $this->vcsService->getActivityRepository();
        $activationRepository->countActivity($toPost,$pullRequest->user_id);
    } 
}
