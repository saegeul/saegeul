<?php namespace App\Http\Middleware ;

use Artgrafii\Glauth\Glauth ;
use Closure ;

class GlauthAuthenticate
{

    public function __construct(Glauth $glauth)
    {
        $this->glauth = $glauth ;
    }

    public function handle($request, Closure $next)
    {
        $user = $this->glauth->check();

        if($user == null){
            return redirect('signin') ;
        }

		return $next($request);
    }
}
