@extends("layouts.series_viewer")

@section('header')
@endsection

@section('contents')
<div class="container mini-container">
    <img src="http://{{$post['post_image_url']}}">
    <br>
    <br>
    <div class="title">{{$post['title']}}</div>
    <br>
    <div class="post-content">{!! $post['html'] !!}</div>
    <br>
    <div id="disqus_thread"></div>
</div> 
@stop

@section('scripts') 
<script>
/**
 * * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 * * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
 * */
/*
 * var disqus_config = function () {
 * this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
 * this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
 * };
 * */
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');

s.src = '//saegeul.disqus.com/embed.js';

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@stop 
