@extends("layouts.series")

@section('header')
@include('workspace.partials.header')
@endsection

@section('contents')
<div class="container mini-container"> 
    <div id="button">
    </div> 

    <br/>

    <ul class="list pure-version">
    @foreach($result as $item)
    <li>
        <div class="left-column"> 
            <i class="material-icons">insert_photo</i>
        </div>
        <div class="right-column">
            <h6 class="title"><a href="{{ route('workspace.series.show',array( $item->series->getKey())) }}">{{ $item->series->title }}</a></h6> 
            <ul class="tag-list">
                <li class="tag">#design</li>
                <li class="tag">#ux</li>
                <li class="tag">#ui</li>
            </ul>
            <p class="description">{{$item->series->description}}</p> 
                
            <div class="additional-information clearfix">
                <div class="left">
                    <ul class="information">
                        <li>Followers <strong>132</strong></li>
                        <li>Like <strong>83</strong></li>
                    </ul>
                </div>
                <div class="right">
                    <span>Last updated {{ date('M j, Y',strtotime($item->series->updated_at)) }}</span>
                </div>
            </div>
        </div> 
    </li>
    @endforeach 
    </ul>
</div>
@stop

@section('scripts') 
<script>
var useCase = new UseCase ;
useCase.visit('series.index') ;
</script> 
@stop 
