<?php

namespace spec\Saegeul\Favorite\Jobs\Show;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Saegeul\Favorite\Models\Favorite as FavoriteModel ;

class ShowCommandSpec extends ObjectBehavior
{
    protected $favoriteId ;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Favorite\Jobs\Show\ShowCommand');
    }

    public function let()
    {
        $favoriteId = 1;
        $this->favoriteId = $favoriteId;
        $this->beConstructedWith($favoriteId);
    }

    public function it_trigger_handle(FavoriteRepositoryInterface $favoriteRepository, FavoriteModel $favorite)
    {
        $favorite = $favoriteRepository->find($this->favoriteId)->shouldBeCalled()->willReturn($favorite);
        $this->handle($favoriteRepository)->shouldReturnAnInstanceOf('Saegeul\Favorite\Models\Favorite');
    }
}
