php artisan vendor:publish --provider="Cartalyst\Sentinel\Laravel\SentinelServiceProvider"
php artisan vendor:publish --provider="Cartalyst\Sentinel\Addons\Social\Laravel\SocialServiceProvider"
php artisan vendor:publish --provider="Artgrafii\Glauth\GlauthServiceProvider"
php artisan vendor:publish --provider="Artgrafii\SgDrive\DriveServiceProvider"

php artisan migrate
