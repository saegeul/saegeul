<?php namespace App\Http\Controllers\Oauth;

use App\Http\Controllers\OauthController;
use Illuminate\Http\Request ;

class InstagramController extends OauthController {
    protected $provider = 'instagram';

    public function authorize(Request $request)
    {
        if(!$this->user){
            return redirect()->back();
        }
        return parent::authorize($request);
    }

}
