<?php namespace App\Http\Controllers;

use Illuminate\Http\Request ;
use Carbon\Carbon ;
use Saegeul\Series\Series ;
use Saegeul\Post\Post ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Cover\Repositories\CoverRepository ;


class SeriesController extends Controller 
{
    protected $seriesService;
    protected $postService;
    protected $glauth;
    public $user;

    public function __construct(
        Series $seriesService, 
        Post $postService, 
        Glauth $glauth
    ){
        $this->seriesService = $seriesService ;
        $this->postService = $postService ;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
	}

    public function index(Request $request)
    {
        $page = $request->input('page')?$request->input('page'):1;
        $keyword = $request->input('keyword')?$request->input('keyword'):"";
        
        $seriesRepository = $this->seriesService->getRepository();
        $series = $seriesRepository->lists($page,$keyword);
        $series->setPath('/series');

        $recentTime = Carbon::now();
        $recentTime->modify('-3 day');

        return view('series.list',compact('series','keyword','page','recentTime'));
    }

    public function show(Request $request, CoverRepository $coverRepo ,$seriesId)
    {
        try {
            $series = $this->seriesService->show($seriesId);
            /*
            if($series->status == 'draft'){
                return redirect()->back()->withErrors();
            }
             */
            $postRepository =  $this->postService->getRepository();
            $posts =  $postRepository->findBySeries($seriesId,null);
            $first = $posts->first() ; 
            $posts = $posts->toArray() ;

            $cover = $coverRepo->findBySeries($seriesId) ;

            $coverContent = (json_decode($cover->content)) ;

            $currentTab = 'cover' ; 
            return view('viewer.series.cover',compact('cover','series','currentTab','posts','first','coverContent'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function editForm(Request $request, $id=null)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
        if(!$this->user){
            return redirect()->to('/signin?return_url='.$returnUrl);
        }

        $series = null;
        if($id != null){
            $series = $this->seriesService->show($id);
            if(!$series->isUpdatableByUser($this->user)){
                return redirect()->to('/series/'.$id);
            }
        }

        return view('series.editForm',compact('series'));
    }

    public function store(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):"";
        try {
            if(!$this->user){
                return redirect()->to('/signin?return_url='.$returnUrl);
            }

            $v = \Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
            ]);

            if ($v->fails()){
                return redirect()->back()->withErrors($v->errors());
            }

            $title = $request->input('title');
            $description = $request->input('description');
            $userId= $this->user->getKey();

            $series = $this->seriesService->register($title,$description,$userId);

            return redirect()->to('series/'.$series->getKey());
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }

    public function update(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):"";
        try {
            if(!$this->user){
                return redirect()->to('/signin?return_url='.$returnUrl);
            }

            $v = \Validator::make($request->all(), [
                'id' => 'required',
                'title' => 'required',
                'description' => 'required',
            ]);

            if ($v->fails()){
                return redirect()->back()->withErrors($v->errors());
            }

            $seriesId = $request->input('id');
            $title = $request->input('title');
            $description = $request->input('description');

            $series = $this->seriesService->show($seriesId);
            if(!$series->isUpdatableByUser($this->user)){
                return redirect()->to('/series/'.$seriesId);
            }

            $series = $this->seriesService->register($seriesId, $title,$description);

            return redirect()->to('series/'.$series->getKey());
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
