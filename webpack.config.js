var path = require('path') ;
var webpack = require('webpack') ;
var HtmlWebpackPlugin = require('html-webpack-plugin') ;

var ROOT_PATH = path.resolve(__dirname) ;

module.exports = {
    entry : path.resolve(ROOT_PATH , 'webapp/Index.jsx') , 
    output : {
        path : path.resolve(ROOT_PATH , 'public/webapp/build'),
        publicPath : '/',
        library : 'UseCase',
        filename: 'bundle.js'
    },
    externals : {
        'trix' : 'Trix'
    },
    devServer: {
        colors: true,
        historyApiFallback : true, 
        host: '0.0.0.0',
        port: 8080,
        hot :true, 
        inline : true, 
        progress : true
    },
    module : {
        loaders: [{
            test : /\.jsx?$/,
            loaders : ['react-hot','babel'] ,
            include: path.resolve(ROOT_PATH , 'webapp')
        }]
    }, 
    plugins: [
        new webpack.HotModuleReplacementPlugin() 
        /*new HtmlWebpackPlugin({
          'title': 'example'
          })*/
    ]
};
