<?php

namespace Saegeul\Favorite\Jobs\Show;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class ShowCommand implements SelfHandling
{

    public $favoriteId ;

    public function __construct($favoriteId)
    {
        $this->favoriteId = $favoriteId ;
    }

    public function handle(FavoriteRepositoryInterface $favoriteRepository)
    {
        $favorite = $favoriteRepository->find($this->favoriteId);

        if(!$favorite){
            throw New ModelNotFoundException('Not found favorite id');
        }

        return $favorite ;
    }
}
