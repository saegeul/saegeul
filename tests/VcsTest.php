<?php

use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Artgrafii\Uuid\UuidService ;
use Artgrafii\Glauth\Services\HashCodeService;
use Laracasts\TestDummy\Factory;

class VcsTest extends TestCase 
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    } 

    public function testCommit()
    {
        //given
        $this->signin();

        $postRepository = $this->app->make('Saegeul\Post\Contracts\PostRepositoryInterface') ;
        $post = $postRepository->lists(1,'')->first();

        $postId = $post->getKey();
        $vcs = $this->app->make('Saegeul\Vcs\Vcs') ;

        //when
        $result = $vcs->commit($postId);

        //then
        $newPost = $postRepository->find($postId);
        $this->assertNotEquals($post->commit_uuid , $newPost->commit_uuid) ;
        $this->assertInstanceOf('Saegeul\Post\Models\Post',$result);
    }

    public function testFork()
    {
        //given
        $this->signin();

        $postRepository = $this->app->make('Saegeul\Post\Contracts\PostRepositoryInterface') ;
        $post = $postRepository->lists(1,'')->first();

        $postId = $post->getKey();
        $vcs = $this->app->make('Saegeul\Vcs\Vcs') ;
        $userId = 2;

        //when
        $result = $vcs->fork($postId,$userId);

        //then
        $this->assertInstanceOf('Saegeul\Post\Models\Post',$result);
        $this->assertEquals($result->source_uuid , $post->commit_uuid) ;
        $this->assertEquals($result->title , $post->title) ;
        $this->assertEquals($result->description , $post->description) ;
        $this->assertEquals($result->content , $post->content) ;
        $this->assertEquals($result->origin_id , $post->getKey()) ;
        $this->assertNotEquals($result->getKey() , $post->getKey()) ; 
    }

    public function testPullRequest()
    {
        //given
        $this->signinByCollaborator();

        $vcs = $this->app->make('Saegeul\Vcs\Vcs') ;

        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postArray['user_id'] = 1 ;
        $postArray['series_id'] = 1;
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->register($postArray) ;

        $data = [
            'post_id' => $post->getKey(),
                'series_id' => 1,
                'status' => 'import',
                'user_id' => 2,
                'comment' => 'post 1 에 입력을 원합니다.'
            ];

        //when
        $result = $vcs->pullRequest($data);

        //then
        $this->assertInstanceOf('Saegeul\Vcs\Models\PullRequest',$result);
    }

    public function testImport()
    {
        //given
        $user = $this->signinByCollaborator();

        $pullRequestRepository = $this->app->make('Saegeul\Vcs\Contracts\PullRequestRepositoryInterface') ;
        $pullRequest = $pullRequestRepository->lists(1)->first();
        $vcs = $this->app->make('Saegeul\Vcs\Vcs') ;

        //when
        $result = $vcs->import($pullRequest->getKey(),$user->getKey());

        //then
        $postRepository = $this->app->make('Saegeul\Post\Contracts\PostRepositoryInterface') ;
        $this->assertInstanceOf('Saegeul\Post\Models\Post',$result);
        $newPost = $postRepository->find($result->getKey()) ;
        $oldPost = $postRepository->find($pullRequest->post_id) ; //2 is key of post  which is from pull request

        $this->assertEquals($newPost->source_uuid, $oldPost->commit_uuid ) ;
        $this->assertEquals($newPost->content,$oldPost->content) ;
        $this->assertEquals($newPost->origin_id,$oldPost->getKey()) ;
        $this->assertEquals($newPost->title,$oldPost->title) ;
    }

    public function testMergeForCase1()
    {
        // case1 updated origin post merged new post from origin post
        
        //given 
        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;

        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postArray['series_id'] = 1;
        $postService = $this->app->make('Saegeul\Post\Post') ;

        $this->signin();
        $originPost = $postService->register($postArray) ;
        $this->signout();

        $user = $this->signinByCollaborator();
        $newPost = $vcsService->fork($originPost->getKey(),$user->getKey());
        $this->signout();
        
        $this->signin();
        $data = [
            'content' => "Update. So, before using Laravel, make sure you have Composer installed on your machine."
        ];

        $postService->update($originPost->getKey(),$data);
        $this->signout();

        $this->signinByCollaborator();
        $updateData = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "Update. So, before using Laravel, make sure you have Composer installed on your machine."
        ];
        $toPost = $postService->show($newPost->getKey());
        $fromPost = $postService->show($originPost->getKey());

        //when
        $result = $vcsService->merge($toPost,$fromPost,$updateData);

        //then
        $toPost = $postService->show($toPost->getKey());
        $this->assertEquals($toPost->source_uuid,$fromPost->commit_uuid) ;
        $this->assertEquals($toPost->description,$updateData['description']) ;
        $this->assertEquals($toPost->content,$updateData['content']) ;
        $this->assertEquals($toPost->title,$updateData['title']) ;
    }

    public function testMergeForCase2()
    {
        // case1 : pull request 
        
        //given 
        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postArray['series_id'] = 1;
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;

        $this->signin();
        $originPost = $postService->register($postArray) ;
        $this->signout();

        $user = $this->signinByCollaborator();
        $newPost = $vcsService->fork($originPost->getKey(),$user->getKey());

        $data = [
            'post_id' => $newPost->getKey(),
            'target_id' => $newPost->origin_id,
                'user_id' => $user->getKey(),
                'series_id' => 1,
                'status' => 'merge',
                'comment' => 'post 1 에 입력을 원합니다.'
            ];
        $pullRequest = $vcsService->pullRequest($data);
        $this->signout();

        $this->signin();
        $toPost = $postService->show($originPost->getKey());
        $fromPost = $postService->show($pullRequest->post_id);

        $updateData = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
        ];

        //when
        $result = $vcsService->merge($toPost,$fromPost,$updateData);

        //then
        $toPost = $postService->show($toPost->getKey());
        $this->assertEquals($toPost->source_uuid,$fromPost->commit_uuid) ;
        $this->assertEquals($toPost->description,$updateData['description']) ;
        $this->assertEquals($toPost->content,$updateData['content']) ;
        $this->assertEquals($toPost->title,$updateData['title']) ; 
    }

    public function testRollback()
    {
        $this->signin();

        $postRepository = $this->app->make('Saegeul\Post\Contracts\PostRepositoryInterface') ;
        $post = $postRepository->lists(1,'')->first();
        $vcs = $this->app->make('Saegeul\Vcs\Vcs') ;
        $revisionRepository = $vcs->getRevisionRepository();
        $revisionPost =  $revisionRepository->history($post->getKey())->first() ;
        $result = $vcs->rollback($post->getKey(),$revisionPost->uuid);

        $latestRevisionPost = $post->latestRevision() ;

        $updatedPost = $postRepository->find($post->getKey());

        $this->assertEquals($updatedPost->title, $revisionPost->title) ;
        $this->assertEquals($updatedPost->content, $revisionPost->content) ;
        $this->assertEquals($latestRevisionPost->from_rollback_uuid, $revisionPost->uuid) ; 
    }

    public function testDiscard()
    {
        //given
        $user = $this->signinByCollaborator();

        $postArray = Factory::build('Saegeul\Post\Models\Post')->toArray() ;
        $postArray['series_id'] = 1 ;
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->createWithUser($postArray,$user) ;

        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;
        $data = [
            'post_id' => $post->getKey(),
                'series_id' => 1,
                'status' => 'import',
                'user_id' => 2,
                'comment' => 'post 3 에 입력을 원합니다.'
            ];

        $result = $vcsService->pullRequest($data);
        $this->assertInstanceOf('Saegeul\Vcs\Models\PullRequest',$result);
        $this->signout();

        $this->signin();
        //when
        $result = $vcsService->discard($result->getKey());

        //then
        $this->assertInstanceOf('Saegeul\Vcs\Models\PullRequest',$result);
        $this->assertEquals($result->processed_status, 'discard') ;
    }
}
