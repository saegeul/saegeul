<?php

$factory->define(Artgrafii\Glauth\Models\User::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'password' => $faker->password ,
        'unique_link' => $faker->firstName 
    ];
}); 

$factory->define(Saegeul\Series\Models\Series::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->text
    ];
});

$factory->define(Saegeul\Post\Models\Post::class,function(Faker\Generator $faker){
    return  [
        'title' => $faker->title,
        'content' => $faker->text,
        'html' => $faker->text,
        'description' => $faker->text
    ];
});
