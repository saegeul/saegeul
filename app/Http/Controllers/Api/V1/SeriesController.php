<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Series\Series ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Invitation\Invitation;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\SeriesTransformer;

class SeriesController extends ApiController 
{
    protected $seriesService;
    protected $glauth;
    protected $user;
    protected $invitationService;
    protected $fractal;

    public function __construct(
        Series $seriesService, 
        Glauth $glauth, 
        Invitation $invitationService,
        Manager $fractal
    )
	{
        $this->seriesService = $seriesService ;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->invitationService = $invitationService;
        $this->fractal = $fractal;
	}

    public function index(Request $request)
    { 
        try{
            $seriesRepository = $this->seriesService->getRepository();
            if($request->input('mode') == 'coworking'){
                $items = $seriesRepository->findByCoworking($this->user->roles);
            }else{
                $items = $seriesRepository->seriesByUser($this->user->getKey());
            }

            $resource = new Fractal\Resource\Collection($items, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function show(Request $request, $id)
    {
        try{
            $series = $this->seriesService->show($id);

            $resource = new Fractal\Resource\Item($series, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function store(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $formData = [];
            $formData['title'] = $request->input('title');
            $formData['description'] = $request->input('description');
            $formData['user_id'] = $this->user->getKey();
            $imageId = $request->input('image_id');
            $imageUrl = $request->input('image_url');
            if($imageId != '' && $imageUrl != ''){
                $formData['series_image'] = $imageId;
                $formData['series_image_url'] = config('filesystems.disks.softlayer-cdn.cdn-url').$imageUrl;
            }

            \DB::beginTransaction();

            $series = $this->seriesService->register($formData);  

            $invitationEmails = $request->input('invitation_emails');
            if($invitationEmails != "" && is_array($invitationEmails)){
                $roleService = $this->glauth->getRoleService();
                $roleRepository = $roleService->getRoleRepository();
                $role = $roleRepository->findBySlug($series->slugForCollaborator());

                $this->inviteRole($invitationEmails,$role,$series);
            } 

            \DB::commit();

            $updatedSeries = $this->seriesService->show($series->getKey());

            $resource = new Fractal\Resource\Item($updatedSeries, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    } 

    private function inviteRole($invitationEmails,$role,$series)
    {
        foreach($invitationEmails as $email){
            $this->invitationService->inviteRole($role->getKey(),$email,$series);
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $v = \Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required'
            ]);

            if ($v->fails()){
                return $this->respondInvalidData() ;
            }

            $series = $this->seriesService->show($id);
            if(!$series->isUpdatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $formData = [];
            $formData['title'] = $request->input('title');
            $formData['description'] = $request->input('description');
            $formData['user_id'] = $this->user->getKey();

            $imageId = $request->input('image_id');
            $imageUrl = $request->input('image_url');
            if($imageId != '' && $imageUrl != ''){
                $formData['series_image'] = $imageId;
                $formData['series_image_url'] = config('filesystems.disks.softlayer-cdn.cdn-url').$imageUrl;
            }

            \DB::beginTransaction();

            $series = $this->seriesService->update($id,$formData);

            \DB::commit();

            $updatedSeries = $this->seriesService->show($series->getKey());

            $resource = new Fractal\Resource\Item($updatedSeries, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function destroy($id)
    {
        try{
            $series = $this->seriesService->show($id);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            \DB::beginTransaction();

            $deletedSeries = $this->seriesService->delete($id);
            
            \DB::commit();

            $resource = new Fractal\Resource\Item($deletedSeries, new SeriesTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            dd($e->getMessage());
            return $this->respondInternalError() ; 
        }
    } 
}
