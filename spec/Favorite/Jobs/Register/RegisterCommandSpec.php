<?php

namespace spec\Saegeul\Favorite\Jobs\Register;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Saegeul\Favorite\Models\Favorite as FavoriteModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Favorite\Events\FavoriteWasRegistered ;

class RegisterCommandSpec extends ObjectBehavior
{
    protected $data;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Favorite\Jobs\Register\RegisterCommand');
    }

    public function let()
    {
        $this->data = [
            'favorite_id' => 1,
            'favorite_type' => 'Saegeul\Series\Models\Series',
            'user_id' => 1
        ];
        $this->beConstructedWith($this->data);
    }

    public function it_trigger_handle(Dispatcher $dispatcher, FavoriteRepositoryInterface $favoriteRepository, FavoriteModel $favorite)
    {
        $favoriteRepository->store($this->data)->shouldBeCalled()->willReturn($favorite);

        $dispatcher->fire(new FavoriteWasRegistered($favorite->getWrappedObject()))->shouldBeCalled();
        
        $this->handle($dispatcher,$favoriteRepository)->shouldReturnAnInstanceOf('Saegeul\Favorite\Models\Favorite');
    }
}
