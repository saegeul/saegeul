<?php

namespace Saegeul\Invitation\Events ;

use Saegeul\Invitation\Models\Invitation ;

class UserWasDetachedRole
{
    public $invitation;

    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }
}
