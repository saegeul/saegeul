@extends("layouts.default")

@section ('contents')
<section id="password_recovery" class="authenty password-recovery">
    <div class="section-content">
        <div class="wrap">
            <div class="container">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 brand" data-animation="fadeInUp">
                            <h2>saegeul</h2>
                            <p>All things you want to read,write,see</p>
                        </div>
                        <div class="col-sm-1 hidden-xs">
                            <div class="horizontal-divider"></div>
                        </div>
                        <div class="col-xs-12 col-sm-8 main" data-animation="fadeInLeft" data-animation-delay=".5s">
                            <h2>Password Reset Complete !</h2>
                            <p>We sent reset code to your email.</p>
                            <div>
                                <div class="alert alert-success">
                                    Check your email.
                                </div>
                                <div class="row">
                                    
                                    
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
                                        <a class="btn btn-block ghost-btn reset" href="/signin">Signin</a>
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section> 
@stop
