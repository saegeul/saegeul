@extends("layouts.default")

@section('contents')
@include('workspace.partials.header') 

<form class="mini-paper clearfix">
    
    <p>Welcome to workspace, <br/>
        Create First Series.<br/> 
        Then invite a collaborator. <br/> 
    </p>
    <a class="button alert right" href="{{route('workspace.series.create')}}">New Series</a> 
</form>

@stop


@section('links')
@include('workspace.series.partials.links')
@stop
