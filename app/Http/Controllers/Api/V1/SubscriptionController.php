<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Series\Series ;
use Saegeul\Subscription\Subscription;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\SubscriptionTransformer;
use App\Http\Controllers\Api\V1\Transformers\SubscriptionSeriesTransformer;

class SubscriptionController extends ApiController 
{
    protected $glauth ;
    protected $user ;
    protected $subscriptionService;
    protected $seriesService;
    protected $fractal;

    public function __construct(
        Subscription $subscriptionService,
        Glauth $glauth,
        Series $seriesService,
        Manager $fractal
    )
	{
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->subscriptionService = $subscriptionService ;
        $this->seriesService = $seriesService ;
        $this->fractal = $fractal;
	}

    public function follow(Request $request,$id)
    {
        try {
            $subscriptionRepository = $this->subscriptionService->getRepository();
            $existedSubscription = $subscriptionRepository->findByEmailForSeries($this->user->email,$id);
            if($existedSubscription){
                return $this->respondInvalidData('Already completed subscription or waiting subscription');
            }
            $formData = array();
            $formData['series_id'] = $id;
            $formData['activated'] = 1;

            \DB::beginTransaction();

            $subscription = $this->subscriptionService->register($formData,$this->user);  

            \DB::commit();

            $resource = new Fractal\Resource\Item($subscription, new SubscriptionTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        } 
    }

    public function unfollow(Request $request,$id)
    {
        try {

            \DB::beginTransaction();

            $subscription = $this->subscriptionService->cancel($this->user->email,$id);  

            \DB::commit();

            $resource = new Fractal\Resource\Item($subscription, new SubscriptionTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        } 
    }

    public function getFollowing(Request $request)
    {
        try {
            $page = $request->input('page')?$request->input('page'):1;
            $subscriptionRepository = $this->subscriptionService->getRepository();
            $items = $subscriptionRepository->findByUserId($this->user->getKey(),$page);
            $resource = new Fractal\Resource\Collection($items, new SubscriptionSeriesTransformer);
            $resource->setPaginator(new IlluminatePaginatorAdapter($items));
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (\Exception $e) {
            return $this->respondInternalError();
        } 
    }
}
