<?php 
namespace Saegeul\Cover;

use Saegeul\Cover\Contracts\CoverRepositoryInterface ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Series\Series;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Contracts\UserInterface ;

class Cover 
{
    protected $coverRepository ;
    protected $seriesService;
    protected $glauth ; 

    public function __construct(
        CoverRepositoryInterface $coverRepository, 
        Series $seriesService,
        Glauth $glauth
    )
    {
        $this->coverRepository = $coverRepository;
        $this->seriesService = $seriesService;
        $this->glauth = $glauth;
    }

    public function register($data)
    {
        $this->seriesService->show($data['series_id']);

        $cover = $this->coverRepository->store($data);
        return $cover;
    }

    public function createWithUser(array $data)
    {
        $data['user_id'] = $this->glauth->check()->getKey()  ;

        return $this->register($data) ;
    }

    public function show($id)
    {
        $cover = $this->coverRepository->find($id);
        if(!$cover){
            throw New ModelNotFoundException('Not found cocer id');
        }
        return $cover;
    }

    public function update($id, $data)
    {
        $cover = $this->coverRepository->find($id);
        if(!$cover){
            throw New ModelNotFoundException('Not found cocer id');
        }

        $updatedCover = $this->coverRepository->update($id,$data);

        return $updatedCover;
    }

    public function delete($id)
    {
        $cover = $this->coverRepository->find($id);
        if(!$cover){
            throw New ModelNotFoundException('Not found cover id');
        }

        $this->coverRepository->delete($id);

        return $cover;
    }
}
