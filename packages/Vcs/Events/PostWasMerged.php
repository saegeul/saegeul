<?php

namespace Saegeul\Vcs\Events ;

use Saegeul\Post\Models\Post ;

class PostWasMerged
{
    public $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }
}
