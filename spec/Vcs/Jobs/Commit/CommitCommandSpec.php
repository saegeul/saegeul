<?php

namespace spec\Saegeul\Vcs\Jobs\Commit;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Vcs\Models\Revision as RevisionModel ;
use Saegeul\Post\Models\Post as PostModel ;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasCommited;

class CommitCommandSpec extends ObjectBehavior
{
    protected $postId;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Vcs\Jobs\Commit\CommitCommand');
    }

    public function let()
    {
        $this->postId = 1;
        $this->beConstructedWith($this->postId);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository, 
        RevisionModel $revision,
        UuidService $uuidService,
        PostModel $post
    )
    {
        $postRepository->find($this->postId)->shouldBeCalled()->willReturn($post);

        $data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
            'user_id' => 1,
            'series_id' => 1
        ];
        $post->toArray()->shouldBeCalled()->willReturn($data);
        $uuid = '222';
        $uuidService->getTimebasedUuid()->shouldBeCalled()->willReturn($uuid);

        $post->getKey()->shouldBeCalled()->willReturn(1);
        $data['post_id'] = 1;
        $data['uuid'] = $uuid;
        $revisionRepository->store($data)->shouldBeCalled()->willReturn($revision);

        $revision->getAttribute('uuid')->willReturn($uuid);
        $post->setAttribute('commit_uuid',$uuid)->shouldBeCalled();
        $post->save()->shouldBeCalled();
    
        $dispatcher->fire(new PostWasCommited($post->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$revisionRepository,$postRepository,$uuidService)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
