import React from 'react' ;
import FancyRowList from './FancyRowList.jsx' ;
import uuid from 'uuid' ;

export default class FancySection extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rows : [{
                    columnStyle : 2,
                    posts : [{
                        title : 'Hello saegeul' ,
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    },{
                        title : 'Hello saegeul',
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    }]
            }]
        }; 
    } 

    render() {
        return (
            <div className="container mini-container">
                <FancyRowList rows={this.state.rows} ref="rows" addRow={this.addRow.bind(this)} deleteRow={this.deleteRow.bind(this)} ></FancyRowList> 
            </div>
        )
    }

    addRow(event) { 
        this.state.rows.push({
                    columnStyle : 2,
                    posts : [{
                        title : 'Hello saegeul' ,
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    },{
                        title : 'Hello saegeul',
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    }]
            }) ; 
        this.setState({ 
            rows : this.state.rows
        });
    } 

    deleteRow(row) {
        const uniqueKey =(row.id) ;
        var foundIndex = -1 ;

        this.state.rows.forEach( (row,index) => {
            if(row.id == uniqueKey){
                foundIndex = index ;
            }
        }); 

        if(foundIndex >= 0 ) {
            this.state.rows.splice(foundIndex,1) ;
            this.setState({rows :this.state.rows}) ; 
        }
    } 

    getData() {
        var data = [] ; 

        /*
        this.state.rows.map((row,index) =>{
            data.push({
            });
        }) ;
        */

        var data = this.refs.rows.getData() ;

        return data ;
    }

    componentWillReceiveProps(nextProps, nextState) {
        this.setState({rows: nextProps.section.rows}) ; 
    }
}
