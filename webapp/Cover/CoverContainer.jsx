import React from 'react' ; 
import FancySectionList from './FancySectionList.jsx' ; 
import PostItem from './PostItem.jsx' ; 
import HTML5Backend from 'react-dnd-html5-backend' ; 
import {DragDropContext} from 'react-dnd' ; 
import Request from 'superagent' ; 

class CoverContainer extends React.Component {

    constructor(props){
        super(props) ; 
        this.state = {
            posts : [] ,
            sectionList : [{
                'rows' : [{
                    columnStyle : 2,
                    posts : [{
                        title : 'Hello saegeul' ,
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    },{
                        title : 'Hello saegeul',
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    }]
                },{
                    columnStyle : 2,
                    posts : [{
                        title : 'Hello saegeul' ,
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    },{
                        title : 'Hello saegeul',
                        image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
                    }]
                }]
            }] 
        } ; 

        this.refreshCover() ;
    }

    render() {
        return (
            <div>
                <div className="container mini-container clearfix">
                    <a className="ghost-btn right" onClick={this.save.bind(this)}>SAVE</a>
                </div>

                <div> 
                    <FancySectionList 
                        sectionList={this.state.sectionList} 
                        addSection={this.addSection.bind(this)}
                        removeSection={this.removeSection.bind(this)}
                        ref="sectionList"
                        />
                </div>
                <div className="right-fixed-bar margin-top">
                    <div className="header">
                        <h6><a>Published Posts</a></h6>
                    </div> 
                    <div>
                        <ul className="items"> 
                            {this.state.posts.map((post,index) => { 
                                return (
                                    <PostItem key={index} post={post}></PostItem>
                                )
                            })}

                        </ul>
                    </div>
                </div>
            </div>
        );
    }

    addSection() { 
        let section = {
            rows : [] 
        } ;

        this.state.sectionList.push(section) ; 

        this.setState({
            sectionList : this.state.sectionList
        });
    }

    removeSection(index) { 
        console.log(index) ;

        this.state.sectionList.splice(index,1) ;

        this.setState({
            sectionList : this.state.sectionList
        });
    }


    componentDidMount() {
        this.refreshItems() ;
    }

    save() {
        let coverContent = JSON.stringify(this.refs.sectionList.getData()) ;

        if(this.props.coverId) {
        Request.put('/api/v1/series/'+this.props.seriesId+'/covers/' + this.props.coverId)
                .send({
                    series_id : this.props.seriesId , 
                    content : coverContent
                })
                .end((err,response)=>{
                     
                });

        }else{
        Request.post('/api/v1/series/'+this.props.seriesId+'/covers')
                .send({
                    series_id : this.props.seriesId , 
                    content : coverContent
                })
                .end((err,response)=>{
                     
                });
        }

    }

    refreshItems() {
        Request.get('/api/v1/series/'+this.props.seriesId+'/posts')
                .send()
                .end((err,response)=>{
                    let data = JSON.parse(response.text).data ;
                    this.setState({
                        posts : data
                    }); 
                });
    }

    refreshCover() {
        if(this.props.coverId) {
        Request.get('/api/v1/series/'+this.props.seriesId+'/covers/'+this.props.coverId)
                .send()
                .end((err,response)=>{
                    let data = JSON.parse(response.text).data ;
                    let sections = JSON.parse(data.content) ;
                    this.setState({
                        sectionList : sections
                    });
                });
        }
    }
}

export default DragDropContext(HTML5Backend)(CoverContainer)
