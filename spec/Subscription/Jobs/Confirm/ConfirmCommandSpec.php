<?php

namespace spec\Saegeul\Subscription\Jobs\Confirm;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface ;
use Saegeul\Subscription\Models\Subscription as SubscriptionModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Subscription\Events\SubscriberWasActivated;
use Artgrafii\Glauth\Glauth ;
use Artgrafii\Glauth\Services\UserService ;
use Cartalyst\Sentinel\Users\UserRepositoryInterface ;
use Cartalyst\Sentinel\Users\EloquentUser ;


class ConfirmCommandSpec extends ObjectBehavior
{

    protected $subscriptionCode = 'ql8aBb4evGVgdM9Y';

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Subscription\Jobs\Confirm\ConfirmCommand');
    }

    public function let()
    {
        $this->beConstructedWith($this->subscriptionCode);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        Glauth $glauth,
        UserService $userService,
        UserRepositoryInterface $userRepository,
        EloquentUser $user, 
        SubscriptionRepositoryInterface $subscriptionRepository, 
        SubscriptionModel $subscription
    )
    {
        $email= 'root@saegeul.com';
        $subscriptionRepository->findByCode($this->subscriptionCode)->shouldBeCalled()->willReturn($subscription);
        $subscription->getAttribute('activated')->willReturn(0);

        $subscription->setAttribute('activated',1)->shouldBeCalled();
        $subscription->save()->shouldBeCalled();

        $dispatcher->fire(new SubscriberWasActivated($subscription->getWrappedObject()))->shouldBeCalled();
        
        $this->handle($dispatcher,$subscriptionRepository,$glauth)->shouldReturnAnInstanceOf('Saegeul\Subscription\Models\Subscription');
    }
}
