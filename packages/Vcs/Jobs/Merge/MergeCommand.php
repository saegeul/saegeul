<?php
namespace Saegeul\Vcs\Jobs\Merge;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Post\Models\Post;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasMerged;

class MergeCommand implements SelfHandling
{
    public $toPost;
    public $fromPost;
    public $data;

    public function __construct(Post $toPost, Post $fromPost, array $data)
    {
        $this->toPost = $toPost;
        $this->fromPost = $fromPost;
        $this->data = $data;
    }

    public function handle(Dispatcher $dispatcher, RevisionRepositoryInterface $revisionRepository, PostRepositoryInterface $postRepository, UuidService $uuidService)
    {
        $this->data['source_uuid'] = $this->fromPost->commit_uuid;
        $updatedPost = $postRepository->update($this->toPost->getKey(),$this->data);

        $newRevisionData = $this->toPost->toArray();
        unset($newRevisionData['id']);
        unset($newRevisionData['created_at']);
        unset($newRevisionData['updated_at']);
        $newRevisionData['uuid'] = $uuidService->getTimebasedUuid();
        $newRevisionData['post_id'] = $this->toPost->getKey();
        $newRevisionData['command'] = 'merge';

        $revision = $revisionRepository->store($newRevisionData);
        $updatedPost->commit_uuid = $revision->uuid;
        $updatedPost->save();

        $dispatcher->fire(new PostWasMerged($updatedPost));

        return $updatedPost;
    }
}
