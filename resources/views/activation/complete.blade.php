@extends("layouts.default")

@section ('content')

<section class="authenty password-recovery">
    <div class="section-content">
        <div class="wrap">
            <div class="container">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 brand" data-animation="fadeInUp">
                            <h2>saegeul</h2>
                            <p>All things you want to read,write,see</p>
                        </div>
                        <div class="col-sm-1 hidden-xs">
                            <div class="horizontal-divider"></div>
                        </div>
                        <div class="col-xs-12 col-sm-8 main" data-animation="fadeInLeft" data-animation-delay=".5s">
                            <h2>Complete!</h2>
                            <p>All Experience about saegeul</p>
                            <div class="row">
                                @if($errors->any())
                                <div class="col-xs-12">
                                    <div class="alert alert-danger">
                                        <i class="fa fa-exclamation-circle"></i>&nbsp;{{$errors->first()}}
                                    </div>
                                </div>
                                @endif
                                <div class="col-xs-12 col-sm-4 col-sm-offset-8">
                                    <a class="btn btn-block ghost-btn reset" href="/signin">return to signin</a>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section> 
@stop
