<?php
namespace Saegeul\Notification\Repositories ; 

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Notification\Contracts\NotificationRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class NotificationRepository extends IlluminateRepository implements NotificationRepositoryInterface
{
    protected $model = 'Saegeul\Notification\Models\Notification' ;

    public function findByReceiver($receiverId, $page)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('receiver', $receiverId);
        $model = $model->where('is_confirmed', 0);
        $totalCount = $model->count() ; 

        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->with('user');
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }
}
