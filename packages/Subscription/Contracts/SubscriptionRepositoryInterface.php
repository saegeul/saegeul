<?php

namespace Saegeul\Subscription\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface SubscriptionRepositoryInterface extends RepositoryInterface 
{
    public function findByCode($code);
    public function findByEmailForSeries($email,$seriesId);
    public function findBySeriesId($seriesId,$page);
    public function findByEmail($email);
}
