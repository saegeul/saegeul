<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;

class LinkParseTransformer extends TransformerAbstract
{
    public function transform($link)
    {
        return [
            'type' => $link->type,
            'link' => $link->link,
            'imageUrl' => $link->imageUrl
        ];
    }
}
