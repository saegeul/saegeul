@extends("layouts.default")

@section('contents')
<div class="signup-wrapper">
    <div class="logo-wrapper" >
        <img src="/img/logo/saegeul_wordmark_lg.png" />
    </div>

    {!! Form::open(['class' =>'mini-paper', 'method' => 'post', 'url' => '/signup']) !!}
        <div class="field-group">
            <label>Email</label>
            <div>
                <input type="text"  name="email" required="required">
            </div>
        </div>
        <div class="field-group">
            <label>Password</label>
            <div>
                <input type="password" name="password" required="required">
            </div>
        </div>
        <div class="field-group">
            <label>Confirm Password</label>
            <div>
                <input type="password" name="password_confirmation" required="required">
            </div>
        </div> 
        <div class="field-group">
            <label>Your URL</label>
            <div id="url-wrapper">
                <span class="prefix">http://www.saegeul.com/@</span>
                <input type="text" name="unique_link" required="required">
            </div>
        </div>

        <button class="button expand" type="submit" name="btn-signup">Signup</button>
        <div class="divider divider-or"></div> 

        <a class="button expand facebook" href="/authorize/facebook"><span class="fi-social-facebook" title="social facebook" aria-hidden="true"></span>&nbsp;Facebook</a>
    {!! Form::close() !!}
    <p class="welcome-text">If you already are a memeber, <a href="/signin">sign in</a>.</p>
</div>
@stop
