@extends("layouts.default_for_guide")

@section('contents')
@include('guides.account_manager.partials.profile-header') 
<div class="profile-content">
    <form class="mini-paper">
        <div class="field-group">
            <label><span class="fi-link-intact" title="link intact" aria-hidden="true"></span>&nbsp;Your URL</label>
            <div>
                saegeul.com/@jaehee
            </div>
        </div>
        <div class="field-group">
            <label><span class="fi-envelope-closed" title="envelope closed" aria-hidden="true"></span>&nbsp;Email</label>
            <div>
                jaehee@artgrafii.com     
            </div>
        </div>
        <hr/>
        <div class="field-group">
            <label><span class="fi-social-facebook" title="social facebook" aria-hidden="true"></span>&nbsp;Facebook </label>
            <div class="row"> 
                <div class="large-9 columns">
                    <p class="description"></p>
                </div>
                <div class="large-3 columns">
                    <div class="switch large round right">
                        <input id="exampleCheckboxSwitch" type="checkbox" checked="checked">
                        <label for="exampleCheckboxSwitch"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="field-group">
            <label><span class="fi-social-instagram" title="social instagram" aria-hidden="true"></span>&nbsp;Instagram </label>
            <div class="row"> 
                <div class="large-9 columns">
                    <p class="description"> </p>
                </div>
                <div class="large-3 columns">
                    <div class="switch large round right">
                        <input id="exampleCheckboxSwitch2" type="checkbox" checked="checked">
                        <label for="exampleCheckboxSwitch2"></label>
                    </div>
                </div>
            </div> 
        </div> 
    </form>
</div>

<div id="myModal" class="reveal-modal tiny callback-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="indicator indicator-success"> 
        <span class="fi-check-thin" title="check thin" aria-hidden="true"></span>
    </div> 

    <br/>
    <br/>
    <p class="lead">{{trans('mypage.messages.hello',array('username'=>'Jaehee'))}}</p>
    <p>{{trans('mypage.messages.user-was-connected-with-facebook')}}</p>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
@stop

@section('scripts') 
<script>
jQuery(function($){
        $('#myModal').foundation('reveal', 'open');
        });
    $('.sample').click(function(){
        var win = window.open('/','hello') ;
        $(win).bind('beforeunload',function(){
            alert("test") ;
        });
});

</script>
@stop
