<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Series\Series ;
use Saegeul\Post\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use League\Fractal ;
use League\Fractal\Manager;
use App\Http\Controllers\Api\V1\Transformers\PostTransformer;

class SeriesPostController extends ApiController 
{
    protected $seriesService;
    protected $postService;
    protected $glauth;
    protected $user;
    protected $fractal;

    public function __construct(
        Series $seriesService, 
        Post $postService,
        Glauth $glauth, 
        Manager $fractal
    )
	{
        $this->seriesService = $seriesService ;
        $this->postService = $postService;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->fractal = $fractal;
	}

    public function index(Request $request, $seriesId)
    {
        try{
            $series = $this->seriesService->show($seriesId);
            if(!$series->isViewableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $postRepository =  $this->postService->getRepository();

            $status = 'all';
            if($request->input('status') == 'publish'){
                $status = 'publish';
            }else if($request->input('status') == 'draft') {
                $status = 'draft';
            }

            $posts =  $postRepository->findBySeries($series->getKey(),$series->user_id,$status);
        

            $resource = new Fractal\Resource\Collection($posts, new PostTransformer);
            $result = $this->fractal->createData($resource)->toJson();

            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
