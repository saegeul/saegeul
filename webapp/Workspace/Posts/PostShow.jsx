import React from 'react' ;
import FancyProgress from './../../util/FancyProgress.jsx' ;
import Request from 'superagent' ;

class PostShow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            progress: new FancyProgress, 
            cdnFullPath  : '',
            imageId  : null,
            title : 'Tolstoy Says',
            isSidebarOpen : false ,
            series : { 
            },
            html : ''
        }; 
    }

    render() {
        let buttonStyle = {
            'marginTop':'7px'
        };

        return (
            <div>
                <div className="editor-toolbar-wrapper">
                    <div className="mini-container">
                        <div>
                        </div>
                        <div className="row"> 
                            <div id="my-toolbar" className="editor-toolbar no-padding small-9 columns"></div> 
                            <div className="small-3  no-padding columns">
                                <a className="ghost-btn right" style={buttonStyle} onClick={this.forkPost.bind(this)} >Make a Suggestion</a>
                                <a className="ghost-btn right" style={buttonStyle} onClick={this.mergePost.bind(this)} >Accept</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="post-wrapper mini-container">
                    {this.state.cdnFullPath ? 
                        <div className="wallpaper">
                            <img src={'http://'+this.state.cdnFullPath} />
                        </div>
                    :
                    <div className="wallpaper" id="wallpaper"> 
                    </div>
                    }

                    <label><a href={this.state.series.uri} className="series-link">in {this.state.series.title} </a></label>
                    <h5 className="title">{this.state.title}</h5>
                    <div dangerouslySetInnerHTML={{__html: this.state.html}}></div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        const postId = this.props.post_id ; 

        this.state.progress.start() ;
        Request.get('/api/v1/posts/'+postId)
                .send()
                .end(function(err,response){
                    let data = JSON.parse(response.text).data ; 
                    this.setState({ title : data.title});
                    this.setState({ series : data.series.data}); 
                    this.setState({ cdnFullPath : data.image_url});
                    this.setState({ description : data.description});
                    this.setState({ imageId : data.post_image});
                    this.state.series.uri = '/workspace/series/'+this.state.series.id+'/posts' ; 
                    this.setState({ series : this.state.series}); 
                    this.setState({ originId : data.origin_id}); 
                    this.setState({ html : data.html}); 
                    this.setState({ content : data.content}); 
                    
                    this.state.progress.done() ;
                }.bind(this)); 

    } 

    forkPost() {
        const postId = this.props.post_id ; 
        
        this.state.progress.start() ;

        Request.post('/api/v1/posts/'+postId+'/fork')
                .send()
                .end(function(err,response){ 
                    let data = JSON.parse(response.text).data ; 
                    location.href = '/workspace/series/'+ data.series_id + '/posts/' + data.id +'/edit';
                    this.state.progress.done() ;
                }.bind(this)); 

    }

    mergePost() { 
        const postId = this.props.post_id ; 
        
        this.state.progress.start() ;

        Request.put('/api/v1/posts/'+this.state.originId+'/merge')
                .send({
                    title : this.state.title,
                    description : this.state.description,
                    post_image_url : this.state.cdnFullPath , 
                    post_image : this.state.imageId ,
                    html : this.state.html, 
                    from_post_id : postId ,
                    content : this.state.content
                })
                .end(function(err,response){ 
                    let data = JSON.parse(response.text).data ; 
                    location.href = '/workspace/series/'+ data.series_id + '/posts/' + data.id +'/edit';
                    this.state.progress.done() ;
                }.bind(this)); 

    } 

}

export default PostShow ;
