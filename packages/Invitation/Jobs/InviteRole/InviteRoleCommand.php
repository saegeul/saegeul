<?php

namespace Saegeul\Invitation\Jobs\InviteRole;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Events\Dispatcher;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Invitation\Events\UserWasInvitedRole;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface;
use Saegeul\Invitation\Contracts\InvitableInterface;

class InviteRoleCommand implements SelfHandling
{
    public $roleId;    
    public $email;    
    public $invitableModel ;    

    public function __construct($roleId, $email, InvitableInterface $invitableModel)
    {
        $this->roleId = $roleId;
        $this->email = $email;
        $this->invitableModel = $invitableModel;
    }

    public function handle(
        Dispatcher $dispatcher, 
        Glauth $glauth, 
        InvitationRepositoryInterface $invitationRepository
    )
    {
        $roleService = $glauth->getRoleService();
        $roleRepository = $roleService->getRoleRepository() ;
        $role = $roleRepository->findById($this->roleId);  
        if(!$role){
            throw New ModelNotFoundException('Not found role id');
        }

        $formatter = [];
        $formatter['email'] = $this->email;
        $formatter['role_id'] = $this->roleId;
        $formatter['invited_id'] = $this->invitableModel->getKey();
        $formatter['invited_type'] =  $this->invitableModel->getClassName() ;

        $invitation = $invitationRepository->findOnWaitingByEmail($formatter['email'], $formatter['role_id']);
        if(!$invitation){
            $invitation = $invitationRepository->store($formatter);
        }

        $dispatcher->fire(new UserWasInvitedRole($invitation,$role,$this->invitableModel));

        return $invitation ;
    }
}
