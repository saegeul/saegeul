<?php

namespace Saegeul\Invitation\Events ;

use Saegeul\Invitation\Models\Invitation ;
use Cartalyst\Sentinel\Roles\EloquentRole ;

class UserWasJoinedRole
{
    public $invitation;

    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }
}
