<?php namespace App\Http\Controllers;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Services\HashCodeService ;
use Artgrafii\Glauth\Exceptions\InvalidHashCodeException;
use Saegeul\Subscription\Subscription;

class SubscriptionController extends Controller {

    public $user;
    protected $glauth ;
    protected $hashCodeService ; 
    protected $subscriptionService;

    public function __construct(
        Subscription $subscriptionService,
        Glauth $glauth,
        HashCodeService $hashCodeService
    )
	{
		$this->middleware('guest');
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->hashCodeService = $hashCodeService ;
        $this->subscriptionService = $subscriptionService ;
	}

    public function confirm(Request $request,$id)
    {
        try {
            $subscriptionCode = $request->input('subscription_code');
            $subscription = $this->subscriptionService->confirm($subscriptionCode);
            dd($subscription);
        }catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }catch (InvalidHashCodeException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
