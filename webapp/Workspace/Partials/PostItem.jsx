import React from 'react' ; 

export default class PostItem extends React.Component {

    render() {
        return (
            <li className={this.props.className}>
                <div className="left-column"> 
                    <i className="material-icons">insert_photo</i>
                </div>
                <div className="right-column">
                    <h6 className="title"><a href={this.getPostUri(this.props.post.id)}>{this.props.post.title}</a></h6>
                    <p>{this.props.post.description}</p>
                    <ul className="tag-list">
                        <li className="tag">#design</li>
                        <li className="tag">#ux</li>
                        <li className="tag">#ui</li>
                    </ul> 
                    <div className="additional-information clearfix">
                        <div className="left">
                            <ul className="information">
                                <li>Like <strong>83</strong></li>
                            </ul> 
                        </div>
                    </div>
                </div> 
            </li>
        )
    }

    getPostUri(postId) { 
        return '/workspace/series/'+this.props.post.series_id+'/posts/'+postId +'/edit-or-view' ;
    } 
}
