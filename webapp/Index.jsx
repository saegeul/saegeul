import React from 'react' ; 
import ReactDOM from 'react-dom' ; 
import SaegeulEditor from './SaegeulEditor.jsx';
import CoverContainer from './Cover/CoverContainer.jsx';
import CreatePostButton from './Button/CreatePostButton.jsx';
import PostsTab from './Workspace/PostsTab.jsx';
import CollaboratorsTab from './Workspace/CollaboratorsTab.jsx';
import SeriesTab from './Workspace/SeriesTab.jsx';
import PostShow from './Workspace/Posts/PostShow.jsx';
import TrixEditorContainer from './TrixEditorContainer.jsx';

export default class UseCase
{
    visit(place) { 
        switch(place) {
            case 'posts.edit' : 
            this.whenUserVisitedPost() ;
            break; 

            case 'posts.show' : 
            this.whenUserVisitedPostShow() ; 
            break;


            case 'series.cover' : 
            this.whenUserVisitedCoverOfSeries() ; 
            break;

            case 'series.posts' : 
            this.whenUserVisitedPostsOnSeries() ; 
            break; 

            case 'series.collaborators' : 
            this.whenUserVisitedCollaboratorsOnSeries() ; 
            break; 

            case 'series.index' : 
            this.whenUserVisitedSeries() ;
            break; 
        }
    }

    whenUserVisitedPost() {
        let paper = document.getElementById('paper') ;
        let postId = paper.getAttribute('data-post-id') ;
        ReactDOM.render(<SaegeulEditor post_id={postId} />, paper) ;
        //ReactDOM.render(<TrixEditorContainer post_id={postId} />, paper) ;
    }

    whenUserVisitedPostShow() {
        let paper = document.getElementById('paper') ;
        let postId = paper.getAttribute('data-post-id') ;
        ReactDOM.render(<PostShow post_id={postId} />, paper) ;
    }


    whenUserVisitedCoverOfSeries() {
        let freespace = document.getElementById('freespace') ;
        //let postId = paper.getAttribute('data-post-id') ;
        let seriesId = freespace.getAttribute('data-series-id') ;
        let coverId = freespace.getAttribute('data-cover-id') ;
        ReactDOM.render(<CoverContainer seriesId={seriesId} coverId={coverId} />, freespace) ; 
    }

    whenUserVisitedSeries() {
        let freespace = document.getElementById('freespace') ;

        ReactDOM.render(<SeriesTab></SeriesTab>, freespace) ; 
    }

    whenUserVisitedPostsOnSeries() { 
        let postArea = document.getElementById('post-area') ; 
        let seriesId = postArea.getAttribute('data-series-id') ;

        ReactDOM.render(<PostsTab seriesId={seriesId}></PostsTab> , postArea) ; 
    }

    whenUserVisitedCollaboratorsOnSeries() { 
        let area = document.getElementById('collaborators-area') ; 
        let seriesId = area.getAttribute('data-series-id') ;

        ReactDOM.render(<CollaboratorsTab seriesId={seriesId}></CollaboratorsTab> , area) ; 
    } 
}

module.exports = UseCase ;
