<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Cover\Models\Cover;
use Artgrafii\Glauth\Models\User;

class CoverTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user'];

    public function transform(Cover $cover)
    {
        return [
            'id' => (int) $cover->id,
            'content' => $cover->content,
            'series_id' => $cover->series_id,
            'status' => $cover->status,
            'created_at' => $cover->created_at,
            'updated_at' => $cover->updated_at
        ];
    }

    public function includeUser(Cover $cover)
    {
        $user = $cover->user;

        return $this->item($user, new UserTransformer);
    }
}
