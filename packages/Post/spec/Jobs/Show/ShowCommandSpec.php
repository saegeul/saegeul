<?php

namespace spec\Saegeul\Post\Jobs\Show;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Post\Models\Post as PostModel ;

class ShowCommandSpec extends ObjectBehavior
{
    protected $postId ;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Post\Jobs\Show\ShowCommand');
    }

    public function let()
    {
        $postId = 1;
        $this->postId = $postId;
        $this->beConstructedWith($postId);
    }

    public function it_trigger_handle(PostRepositoryInterface $postRepository, PostModel $post)
    {
        $post = $postRepository->find($this->postId)->shouldBeCalled()->willReturn($post);
        $this->handle($postRepository)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
