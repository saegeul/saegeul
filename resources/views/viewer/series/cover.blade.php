@extends("layouts.series_viewer")

@section('header')
@endsection

@section('contents')
<div class="container mini-container">
    <h3 style="text-align:center;"><a href="/pdf/series/{{$series['id']}}"><i class="material-icons">cloud_download</i></a> {{$series['title']}}</h3>
    <p style="text-align:center;color:#888;">{{$series['description']}}</p>
</div> 

<div class="container mini-container">
    @foreach($coverContent as $section)
    <div>
        @foreach($section->rows as $row) 
            @if($row->columnStyle == 1)
            <?php
                $post = $row->posts[0] ; 
            ?>
            <div class="row">
                <div class="columns small-12">
                    <div class="card" >
                        <div class="media-section">
                            <img src={{'http://'.$row->posts[0]->image_url }} />
                        </div>
                        <div class="text-section">
                            <h6><a href="/series/{{$post->series_id}}/posts/{{$post->id}}">{{$row->posts[0]->title }}</a></h6>
                        </div>
                    </div>
                </div>
            </div>
 
            @elseif($row->columnStyle == 2)
            <br/>
            <div class="row">
                @foreach($row->posts as $post)
                <div class="columns small-6">
                    <div class="card" >
                        <div class="media-section">
                            <img src={{'http://'.$post->image_url }} />
                        </div>
                        <div class="text-section">
                            <h6><a href="/series/{{$post->series_id}}/posts/{{$post->id}}">{{$post->title }}</a></h6>
                        </div>
                    </div>
                </div>
                @endforeach 
            </div> 
            @endif
        @endforeach
    </div>
    @endforeach
</div>
@stop

@section('scripts') 
<script>
</script> 
@stop 
