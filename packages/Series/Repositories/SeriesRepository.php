<?php

namespace Saegeul\Series\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Saegeul\Series\SeriesRoleCollection;
use Saegeul\Permission\PermissionGenerator;

class SeriesRepository extends IlluminateRepository implements SeriesRepositoryInterface
{
    protected $model = 'Saegeul\Series\Models\Series' ;

    public function lists($page, $keyword)
    {
        $model = $this->modelFactory->create($this->model) ;
        if($keyword != ""){
            $model = $model->where('title', 'like', "%$keyword%");
        }
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->with('user')->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function seriesByUser($userId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('user_id', $userId);

        return $model->get();
    }

    public function findByCoworking($roles)
    {
        $seriesRoleCollection = new SeriesRoleCollection;
        $permissionGenerator = new PermissionGenerator;
        $seriesIds = [];
        foreach($roles as $role){
            $seriesId = $permissionGenerator->getSeriesIdBySlug($role->slug,$seriesRoleCollection->getCollaborator());
            if($seriesId){
                $seriesIds[] = $seriesId;
            }
        }
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->whereIn('id',$seriesIds);

        return $model->get();
    }

    public function findByTagname($tag)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->withTag($tag);
        return $model->get();
    }

    public function getTags($tag)
    {
        $model = $this->modelFactory->create($this->model) ;
        $tags = $model->getTags($tag);
        return $tags->get();
    }
}
