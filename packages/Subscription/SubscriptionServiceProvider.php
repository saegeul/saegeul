<?php

namespace Saegeul\Subscription;

use Illuminate\Support\ServiceProvider ;

class SubscriptionServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->app->bind('Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface',
            'Saegeul\Subscription\Repositories\SubscriptionRepository');

        $this->registerEventListener();
    }

    private function registerEventListener()
    {
        $registerConf = $this->app['config']->get('event.register-subscription'); 
        foreach($registerConf['handler'] as $val){
            \Event::listen($registerConf['name'],$val);
        }

        $confirmConf = $this->app['config']->get('event.confirm-subscription'); 
        \Event::listen($confirmConf['name'],$confirmConf['handler']);

        $cancelConf = $this->app['config']->get('event.cancel-subscription'); 
        \Event::listen($cancelConf['name'],$cancelConf['handler']);
    }
}
