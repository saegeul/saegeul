<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Laracasts\TestDummy\Factory;

class NotificationTest extends TestCase
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $glauth->signout();
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    public function testGet()
    {
        #given
        $user = $this->signinByCollaborator();
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $post = $postRepository->find(1);
        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;
        $newPost = $vcsService->fork($post->getKey(),$user->id);

        $data = [
            'post_id' => $newPost->getKey(),
            'target_id' => $newPost->origin_id,
                'series_id' => $newPost->series_id,
                'status' => 'merge',
                'user_id' => 2,
                'comment' => 'post 3 에 입력을 원합니다..'
            ];

        $pullRequest = $vcsService->pullRequest($data);
        $this->assertInstanceOf('Saegeul\Vcs\Models\PullRequest',$pullRequest);
        $this->signout();

        $this->signin();
        #when
        $prefix = 'api/v1/notifications';
        $response = $this->call('GET',$prefix);        
        
        #then
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data'][0]['command'], 'whenPostWasPullRequested');
    } 

    public function test_failed_case_to_get_no_login()
    {
        #when
        $prefix = 'api/v1/notifications';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function testShow()
    {
        #given
        $this->signin();

        #when
        $prefix = 'api/v1/notifications/1';
        $response = $this->call('GET',$prefix);        
        
        #then
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['is_confirmed'], 1);
    }

    public function test_failed_case_to_show_no_login()
    {
        #when
        $prefix = 'api/v1/notifications/1';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }
}
