<?php

use Laracasts\TestDummy\Factory;

class PostTest extends TestCase 
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->all()->first();

        $result = $glauth->signin($user->email,'1q2w3e') ;
    }
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testRegister()
	{
        $this->signin();

        $data = Factory::build('Saegeul\Post\Models\Post')->toArray();

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->register($data) ;

        $this->assertInstanceOf('Saegeul\Post\Models\Post',$post) ;
        $this->assertEquals($post->title,$data['title']) ; 
        $this->assertEquals($post->description,$data['description']) ;
        $this->assertEquals($post->content,$data['content']) ;
        $this->assertEquals($post->series_id,$data['series_id']) ;
        $this->assertEquals($post->user_id,$data['user_id']) ;
    }

    /**
     * @expectedException Illuminate\Database\QueryException
     **/
    public function test_failed_case_to_register_post_invalid_data()
	{
        $data = Factory::build('Saegeul\Post\Models\Post')->toArray();
        $data['temp'] = '111';

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->register($data);
    }

    public function testUpdate()
    {
        $this->signin();

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $postId = $postRepository->lists(1,'')->first()->getKey();
        $data = [
            'content' => "Laravel utilizes Composer to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine."
        ];

        $post = $postService->update($postId,$data);

        $this->assertInstanceOf('Saegeul\Post\Models\Post',$post);
        $this->assertEquals($post->getKey(),$postId) ;
        $this->assertEquals($post->content,$data['content']) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_update_post_invalid_post_id()
    {
        $postId = 1111;
        $data = [
            'content' => "Laravel utilizes Composer to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine."
        ];

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->update($postId,$data);
    }

    /**
     * @expectedException Illuminate\Database\QueryException
     **/
    public function test_failed_case_to_update_post_invalid_data()
	{
        $postRepository = $this->app->make('Saegeul\Post\Contracts\PostRepositoryInterface') ;
        $postId = $postRepository->lists(1,'')->first()->getKey();
        $data = [
            'description' => "bebe"
        ];
        $data['temp'] = '111';

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->update($postId,$data);
    }

    public function testShow()
    {
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $postId = $postRepository->lists(1,'')->first()->getKey();

        $post = $postService->show($postId);

        $this->assertInstanceOf('Saegeul\Post\Models\Post',$post);
        $this->assertEquals($post->getKey(),$postId) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_show_post_invalid_post_id()
    {
        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postId = 22222;

        $post = $postService->show($postId);
    }

    public function testDelete()
    {
        $this->signin();

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $postId = $postRepository->lists(1,'')->first()->getKey();

        $post = $postService->delete($postId);

        $this->assertInstanceOf('Saegeul\Post\Models\Post',$post);
        $this->assertEquals($post->getKey(),$postId) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_delete_post_invalid_post_id()
    {
        $postId = 22222;

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $post = $postService->delete($postId);
    }
}
