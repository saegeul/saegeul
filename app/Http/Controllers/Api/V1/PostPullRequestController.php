<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Post\Post;
use Saegeul\Series\Series;
use Saegeul\Vcs\Vcs;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use League\Fractal ;
use League\Fractal\Manager;
use App\Http\Controllers\Api\V1\Transformers\PullRequestTransformer;

class PostPullRequestController extends ApiController 
{
    protected $seriesService;
    protected $postService;
    protected $vcsService;
    protected $glauth;
    protected $user;
    protected $fractal;

    public function __construct(
        Series $seriesService, 
        Post $postService, 
        Vcs $vcsService,
        Glauth $glauth, 
        Manager $fractal
    )
	{
        $this->seriesService = $seriesService;
        $this->postService = $postService;
        $this->vcsService = $vcsService;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->fractal = $fractal;
	}

    public function index(Request $request, $postId)
    {
        try{
            $post = $this->postService->show($postId);
            $series = $this->seriesService->show($post->series_id);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $page = $request->input('page')?$request->input('page'):1;
            $pullRequestRepository = $this->vcsService->getPullRequestRepository();
            $pullRequests = $pullRequestRepository->findByPost($post->getKey());

            $resource = new Fractal\Resource\Collection($pullRequests, new PullRequestTransformer);
            $result = $this->fractal->createData($resource)->toJson();

            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
