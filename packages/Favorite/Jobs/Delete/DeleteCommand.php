<?php

namespace Saegeul\Favorite\Jobs\Delete;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Favorite\Events\FavoriteWasDeleted;

class DeleteCommand implements SelfHandling
{

    public $favoriteId ;

    public function __construct($favoriteId)
    {
        $this->favoriteId = $favoriteId ;
    }

    public function handle(Dispatcher $dispatcher, FavoriteRepositoryInterface $favoriteRepository)
    {
        $favorite = $favoriteRepository->find($this->favoriteId);
        if(!$favorite){
            throw New ModelNotFoundException('Not found favorite id');
        }
        $deletedFavorite = $favoriteRepository->delete($this->favoriteId);
        if($deletedFavorite){
            $dispatcher->fire(new FavoriteWasDeleted($deletedFavorite));
        }

        return $deletedFavorite;
    }
}
