<?php

namespace Saegeul\Vcs\Events ;

use Saegeul\Vcs\Models\PullRequest;

class PullRequestWasDiscarded
{
    public $pullRequest;

    public function __construct(PullRequest $pullRequest)
    {
        $this->pullRequest = $pullRequest;
    }
}
