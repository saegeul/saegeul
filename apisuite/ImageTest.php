<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImageTest extends TestCase
{
    private $prefix = 'api/v1/images';

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    } 

    public function testRegister()
    {
        $this->signin();

        $path = public_path().'/img/logo/saegeul_small.png';
        $fileService = $this->app->make('Artgrafii\SgDrive\Services\FileService') ;
        $upload = $fileService->makeFile($path);

        $response = $this->call('POST',$this->prefix,[],[], ['upload' => $upload]);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['original_name'], $upload->getClientOriginalName());
    } 

    public function test_failed_case_to_register_no_login()
    {
        $path = public_path().'/img/logo/saegeul_small.png';
        $fileService = $this->app->make('Artgrafii\SgDrive\Services\FileService') ;
        $upload = $fileService->makeFile($path);

        $response = $this->call('POST',$this->prefix,[],[], ['upload' => $upload]);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_register_invalid_data()
    {
        $this->signin();

        $response = $this->call('POST',$this->prefix,[]);        
        $this->assertEquals(422, $response->status());
    }

    public function test_failed_case_to_register_invalid_file()
    {
        $this->signin();

        $path = public_path().'/robots.txt';
        $fileService = $this->app->make('Artgrafii\SgDrive\Services\FileService') ;
        $upload = $fileService->makeFile($path);

        $response = $this->call('POST',$this->prefix,[],[], ['upload' => $upload]);
        $this->assertEquals(422, $response->status());
    }

    public function testGetImages()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix);        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);
    }

    public function testGetImage()
    {
        $this->signin();

        $fileObjectRepository = $this->app->make('Artgrafii\SgDrive\Contracts\FileObjectRepositoryInterface') ;
        $fileObject = $fileObjectRepository->createModel()->first();

        $response = $this->call('GET',$this->prefix.'/'.$fileObject->getKey());        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['id'], $fileObject->getKey());
    } 

    public function test_failed_case_to_show_no_login()
    {
        $response = $this->call('GET',$this->prefix.'/1');        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_show_invalid_image_id()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix.'/311');        
        $this->assertEquals(404, $response->status());
    }

    public function testDeleteImage()
    {
        $this->signin();

        $fileObjectRepository = $this->app->make('Artgrafii\SgDrive\Contracts\FileObjectRepositoryInterface') ;
        $fileObject = $fileObjectRepository->createModel()->first();

        $response = $this->call('DELETE',$this->prefix.'/'.$fileObject->getKey());        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);

        $fileobject = $fileObjectRepository->createModel()->find($fileObject->getKey()) ;
        $this->assertEmpty($fileobject);
    }

    public function test_failed_case_to_delete_no_login()
    {
        $response = $this->call('DELETE', $this->prefix.'/1');
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_delete_invalid_post_id()
    {
        $this->signin();

        $response = $this->call('DELETE', $this->prefix.'/311');
        $this->assertEquals(404, $response->status());
    }   
}
