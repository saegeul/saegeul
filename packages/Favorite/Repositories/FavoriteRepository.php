<?php

namespace Saegeul\Favorite\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class FavoriteRepository extends IlluminateRepository implements FavoriteRepositoryInterface
{
    protected $model = 'Saegeul\Favorite\Models\Favorite' ;

    public function findByTypeOnUserId($userId, $favoriteType, $page)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->userId($userId)->favoriteType($favoriteType)->with('favorite');
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->get();
        
        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function findByFavoriteIdOnType($favoriteId, $favoriteType)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->favoriteId($favoriteId)->favoriteType($favoriteType);
        $items = $model->get();
        
        return $items;
    }

    public function findByFavoriteIdOnTypeForUser($favoriteId, $favoriteType, $userId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->userId($userId)->favoriteId($favoriteId)->favoriteType($favoriteType);
        return $model->first();
    }
}
