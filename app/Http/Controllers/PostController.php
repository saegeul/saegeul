<?php namespace App\Http\Controllers;

use Illuminate\Http\Request ;
use Carbon\Carbon ;
use Saegeul\Series\Series ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Post\Post ;
use Artgrafii\Glauth\Glauth ;

class PostController extends Controller 
{
    protected $seriesService;
    protected $postService ; 
    protected $glauth ; 

    public function __construct(
        Series $seriesService, 
        Post $postService,
        Glauth $glauth
    )
    {
        $this->seriesService = $seriesService ;
        $this->postService = $postService ;
        $this->glauth = $glauth ;
    }

    public function show(Request $request, $seriesId, $postId)
    {
        try {
            $post = $this->postService->show($postId);

            $postRepository =  $this->postService->getRepository();
            $series = $this->seriesService->show($seriesId);
            $posts =  $postRepository->findBySeries($seriesId,null);

            $currentTab = 'post' ; 
            return view('viewer.series.post',compact('series','post','currentTab','posts'));
        } catch (ModelNotFoundException $e) {
            dd($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function edit($postId)
    { 
        $post = $this->postService->show($postId);
        return view('posts.edit',compact('post'));
    }

    public function create()
    {
        $user = $this->glauth->check()  ;

        $formData = [];
        $formData['title'] = 'title';
        $formData['description'] = 'description';
        $formData['content'] = 'content';
        $formData['series_id'] = 1;
        $formData['user_id'] = $user->getKey(); 

        $post = $this->postService->register($formData);  

        return redirect()->route('posts.edit', array($post->getKey())) ; 
    }
}
