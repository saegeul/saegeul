<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Vcs\Models\PullRequest;
use App\Http\Controllers\Api\V1\Transformers\PostTransformer;

class PullRequestTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['post'];

    public function transform(PullRequest $pullRequest)
    {
        return [
            'id' => (int) $pullRequest->id,
            'series_id' => $pullRequest->series_id,
            'target_id' => $pullRequest->target_id,
            'status' => $pullRequest->status,
            'comment' => $pullRequest->comment,
            'processed_status' => $pullRequest->processed_status,
            'created_at' => $pullRequest->created_at,
            'updated_at' => $pullRequest->updated_at
        ];
    }

    public function includePost(PullRequest $pullRequest)
    {
        $post = $pullRequest->post;

        return $this->item($post, new PostTransformer);
    }
}
