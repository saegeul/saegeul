<?php

namespace Saegeul\Subscription\Jobs\Cancel;

use Illuminate\Events\Dispatcher;
use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Subscription\Contracts\SubscriptionRepositoryInterface;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Saegeul\Subscription\Events\SubscriptionWasCanceled;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class CancelCommand implements SelfHandling
{
    public $email; 
    public $seriesId;

    public function __construct($email, $seriesId)
    {
        $this->email = $email;
        $this->seriesId = $seriesId;
    }

    public function handle(
        Dispatcher $dispatcher, 
        SubscriptionRepositoryInterface $subscriptionRepository,
        SeriesRepositoryInterface $seriesRepository
    )
    {
        $series = $seriesRepository->find($this->seriesId);
        if(!$series){
            throw New ModelNotFoundException('Not found series id');
        }

        $subscription = $subscriptionRepository->findByEmailForSeries($this->email,$this->seriesId);
        if(!$subscription){
            throw New ModelNotFoundException('Not found subscription id');
        }
        $subscriptionRepository->delete($subscription->getKey());

        $dispatcher->fire(new SubscriptionWasCanceled($subscription,$series));

        return $subscription;
    }
}
