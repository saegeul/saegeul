<?php

namespace Saegeul\Finder;

use Illuminate\Foundation\Application; 

class Finder
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function factory($provider, $options = null)
    {
        $finderName = ucfirst($provider).'FinderService';
        $finderPath = 'Saegeul\\Finder\\Services\\'.$finderName;

        $finder = $this->app->make($finderPath,$options); 

        return $finder;
    }
}
