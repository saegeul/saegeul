<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('origin_id')->nullable();
			$table->string('origin_owner')->nullable();
			$table->string('commit_uuid')->nullable();
			$table->string('source_uuid')->nullable();
			$table->string('title');
			$table->text('content');
			$table->text('html');
			$table->string('description');
			$table->integer('post_image')->nullable();
			$table->string('post_image_url')->nullable();
			$table->string('status')->default('draft');
			$table->string('pull_request_status')->nullable();
			$table->integer('user_id');
			$table->integer('series_id')->nullable();
			$table->integer('favorite')->default(0);
			$table->integer('is_forked')->default(0);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('posts');
    }
}
