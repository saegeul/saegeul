<?php

namespace Saegeul\Finder\Services;

use Saegeul\Finder\Contracts\FinderServiceInterface;
use Instagram\Instagram;

class InstagramFinderService implements FinderServiceInterface 
{
    protected $listCount = 20;
    protected $instagram;

    public function __construct(Instagram $instagram)
    {
        $this->instagram = $instagram;
    }

    public function find($keyword,$page)
    {
        $searchOptions = $this->getSearchOptions($keyword,$page);        
        $response = $this->instagram->getTagMedia($keyword,$searchOptions);

        $result = $this->getReturnData($response);
        return $result ;      
    }

    private function getSearchOptions($page)
    {
        $searchOptions = array();
        $searchOptions['count'] = $this->listCount ;

        if(isset($page) && $page != 1 && $page != ""){
            $searchOptions['MAX_TAG_ID'] = $page ;
        }
        return $searchOptions ;
    }

    private function getReturnData($response)
    {
        $result = array() ;
        $items = array();
        foreach($response->getData() as $val)
        {
            $items[] = $val->getData() ;
        }
        $result['items'] = $items ;
        $result['nextPage'] = $response->getNextMaxTagId() ;

        return $result ;
    }
}
