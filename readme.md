## saegeul 
Once Write , Publish Anytype
새글은 온라인상에서 누구나 손쉽게 글을 작성하고, 공유하는 것을 도와주는 창작 플랫폼이다. 


### Requirements

* PHP latest version
* mysql 5.x

### 반드시 숙지하세요.

* Rest API 작성하기
    * [읽어보기#1](https://speakerdeck.com/leewin12/rest-api-seolgye)
    * [읽어보기#2](http://www.slipp.net/wiki/pages/viewpage.action?pageId=12878219)
    * [읽어보기#3](http://spoqa.github.io/2012/02/27/rest-introduction.html)
* PHPSpec
    * [Laracast 강의](https://laracasts.com/search?q=phpspec)
    * [Phpspec Cheatsheet](https://github.com/yvoyer/phpspec-cheat-sheet)
    * [Phpspec Cheatsheet](https://github.com/yvoyer/phpspec-cheat-sheet)
    * [https://github.com/phpspec/prophecy](https://github.com/phpspec/prophecy)

### Contributor

* Jaehee Hwang
* Sihyeong Lee 


```bash
source publish_provider.sh
```

* DB설정을 자신에 맞게 설정을 해준다. 

###### app/config/database.php
```php
<?php 

return [
	'default' => 'mysql',

	'connections' => [
        'mysql' => [
			'driver'    => 'mysql',
			'host'      => env('DB_HOST', 'localhost'),
			'database'  => env('DB_DATABASE', 'saegeul'),
			'username'  => env('DB_USERNAME', 'homestead'),
			'password'  => env('DB_PASSWORD', 'secret'),
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => 'SG_',
			'strict'    => false,
		],
	],
];
```

* 회원가입 인증 및 메일 서비스를 이용하기 위해서 아래와 같은 자신에 맞게 설정을 해준다. 

###### app/config/mail.php
```php
<?php 

return [

	'driver' => env('MAIL_DRIVER', 'mandrill'),
	'host' => env('MAIL_HOST', 'smtp.mandrillapp.com'),
	'port' => env('MAIL_PORT', 465),
	'from' => ['address'=>'no-reply@saegeul.com', 'name'=>'no-reply'],
	'encryption' => 'ssl',
	'username' => env('MAIL_USERNAME'),
	'password' => env('MAIL_PASSWORD'),
	'sendmail' => '/usr/sbin/sendmail -bs',
	'pretend' => false,

];
```

###### app/config/mail.php
```php
<?php 

return [
	'mandrill' => [
		'secret' => 'Your Secret Key',
	],
];
```

* Social oauth설정을 자신에 맞게 설정을 해준다. 

###### app/config/cartalyst.sentinel-addons.social.php
```php
<?php 

return [
    'connections' => [

        'facebook' => [
            'driver'     => 'Facebook',
            'identifier' => '',
			'secret'     => '',
			'scopes'     => [
				'email','public_profile'
			],
        ],

    'instagram' => [
        'driver'     => 'Instagram',
        'identifier' => '####',
        'secret'     => '####',
        'scopes'     => [
            'basic',
        ],
    ],
];
```

* filesystem(Softlayer) 설정을 자신에 맞게 설정을 해준다. 

###### app/config/filesystems.php
```php
<?php 

return [
    'default' => 'local',

    'cloud' => 'softlayer',

    'cloud-cdn' => 'softlayer-cdn',

    'disks' => [
        'softlayer' => [
            'driver' => 'softlayer',
            'credentials' => [
                'host' => 'https://dal05.objectstorage.softlayer.net',
                'username' => '####',
                'password' => '####'
            ],
            'container' => 'SG',
            'cdn-prefix' => '####',
        ],

        'softlayer-cdn' => [
            'driver' => 'softlayer-cdn',
            'credentials' => [
                'host' => 'https://dal05.objectstorage.softlayer.net',
                'username' => '####',
                'password' => '####'
            ],
            'container' => 'CDN',
            'cdn-prefix' => '####',
            'cdn-url' => '####',
        ],

    ],
];
```

* finder 설정을 자신에 맞게 설정을 해준다. 

###### app/config/finder.php
```php
<?php 

return array(

    'twitter' => array(
        'consumer_key' => '',
        'consumer_secret' => ''
    ),
    
    'youtube' =>'' 
);

```
* https://apps.twitter.com/
* https://console.developers.google.com/

### Finder REST API 

###### Youtube
http://saegeul.app:8000/api/v1/finder/youtube?keyword=psy&page=1

###### Twitter
http://saegeul.app:8000/api/v1/finder/twitter?keyword=psy&page=1

###### Instagram
http://saegeul.app:8000/api/v1/finder/instagram?keyword=psy&page=1

* instagram은 로그인과, saegeul.app:8000/authorize/instagram 에서 oauth를 이용한 access token을 가지고 와야 한다.
access token이 없을 경우 error 가 발생하고 message는 Unauthorized!이고 status_code는 401이다.