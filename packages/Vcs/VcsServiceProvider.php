<?php

namespace Saegeul\Vcs;

use Illuminate\Support\ServiceProvider ;

class VcsServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->app->bind('Saegeul\Vcs\Contracts\RevisionRepositoryInterface',
            'Saegeul\Vcs\Repositories\RevisionRepository');

        $this->app->bind('Saegeul\Vcs\Contracts\PullRequestRepositoryInterface',
            'Saegeul\Vcs\Repositories\PullRequestRepository');

        $this->app->bind('Saegeul\Vcs\Contracts\ActivityRepositoryInterface',
            'Saegeul\Vcs\Repositories\ActivityRepository');

        $this->registerEventListener();
    }

    private function registerEventListener()
    {
        $commitConf = $this->app['config']->get('event.commit-post'); 
        \Event::listen($commitConf['name'],$commitConf['handler']);

        $mergeConf = $this->app['config']->get('event.merge-post'); 
        foreach($mergeConf['handler'] as $val){
            \Event::listen($mergeConf['name'],$val);
        }

        $forkConf = $this->app['config']->get('event.fork-post'); 
        \Event::listen($forkConf['name'],$forkConf['handler']);

        $importConf = $this->app['config']->get('event.import-post'); 
        foreach($importConf['handler'] as $val){
            \Event::listen($importConf['name'],$val);
        }

        $pullRequestConf = $this->app['config']->get('event.pull-request-post'); 
        foreach($pullRequestConf['handler'] as $val){
            \Event::listen($pullRequestConf['name'],$val);
        }

        $rollbackConf = $this->app['config']->get('event.rollback-post'); 
        \Event::listen($rollbackConf['name'],$rollbackConf['handler']);

        $discardConf = $this->app['config']->get('event.discard-pull-request'); 
        foreach($discardConf['handler'] as $val){
            \Event::listen($discardConf['name'],$val);
        }
    }
}
