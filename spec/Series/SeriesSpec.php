<?php

namespace spec\Saegeul\Series;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Bus\Dispatcher ;
use Saegeul\Series\Jobs\Register\RegisterCommand ;
use Saegeul\Series\Jobs\Update\UpdateCommand ;
use Saegeul\Series\Jobs\Show\ShowCommand ;
use Saegeul\Series\Jobs\Delete\DeleteCommand ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Artgrafii\Glauth\Contracts\UserInterface ;

class SeriesSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Series\Series');
    }

    public function let(Dispatcher $dispatcher, SeriesRepositoryInterface $seriesRepository)
    {
        $this->beConstructedWith($dispatcher,$seriesRepository);
    }

    public function it_register(Dispatcher $dispatcher, SeriesModel $series)
    {
        $data = [
            'title' => 'Laravel5',
            'description' => "lalalalala",
            'user_id' => 1
        ];
        $command = new RegisterCommand($data) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series) ; 
        $this->register($data)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }

    public function it_create_series_with_user(Dispatcher $dispatcher ,UserInterface $user,SeriesModel $series)
    { 
        $user->getKey()->willReturn(1) ;

        $data = [
            'title' => 'Laravel5',
            'description' => "lalalalala",
            'user_id' => 1 
        ]; 
        
        $command = new RegisterCommand($data) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series) ; 
        
        $this->createWithUser($data,$user)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }

    public function it_update(Dispatcher $dispatcher, SeriesModel $series)
    {
        $seriesId = 1;
        $data = [
            'description' => "bebe"
        ];
        $command = new UpdateCommand($seriesId,$data) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series) ; 
        $this->update($seriesId,$data)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }

    public function it_show(Dispatcher $dispatcher, SeriesModel $series)
    {
        $seriesId = 1;
        $command = new ShowCommand($seriesId) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series) ; 
        $this->show($seriesId)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }

    public function it_delete(Dispatcher $dispatcher, SeriesModel $series)
    {
        $seriesId = 1;
        $command = new DeleteCommand($seriesId) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($series) ; 
        $this->delete($seriesId)->shouldReturnAnInstanceOf('Saegeul\Series\Models\Series');
    }

    public function it_get_repository(SeriesRepositoryInterface $seriesRepository)
    {
        $this->getRepository()->shouldReturnAnInstanceOf('Saegeul\Series\Contracts\SeriesRepositoryInterface');
    }
}
