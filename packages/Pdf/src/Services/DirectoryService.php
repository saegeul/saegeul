<?php 
namespace Saegeul\Pdf\Services;

use Artgrafii\Uuid\UuidService;
use Artgrafii\Glauth\Glauth ;
use Artgrafii\Glauth\Services\HashCodeService ;

class DirectoryService
{
    protected $glauth ; 
    private $uuidService;
    private $hashCodeService;

    public function __construct(
        Glauth $glauth,
        HashCodeService $hashCodeService,
        UuidService $uuidService
    )
    {
        $this->glauth = $glauth;
        $this->uuidService = $uuidService;
        $this->hashCodeService = $hashCodeService;
    }

    public function getFileName($document)
    { 
        $userService = $this->glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $owner = $userRepository->find($document->user_id);

        $encryptedName = $this->getEncryptedName($document) ;
        return $encryptedName;
        /*
        $dirPath = storage_path().'/app/'.$owner->getUuid() . '/pdf/' ;
        $newPath = $dirPath.$encryptedName ;

        $pathParts = pathinfo($newPath);
        $fileDir = $pathParts['dirname'];
        if (!file_exists($fileDir)) {
            mkdir($fileDir, 0777, true);
        }

        return $newPath;
         */
    }

    public function getEncryptedName($document)
    {
        $documentType = substr(substr(strrchr($document->getClassName(), "\\"), 1),0,1);
        $code = $this->hashCodeService->encode($document->getKey());
        $updatedTime = strtotime($document->updated_at);
        $extension = '.pdf';

        return  $documentType . $code . $updatedTime .$extension ;
    }
}
