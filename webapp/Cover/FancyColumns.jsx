import React from 'react' ;
import PostElement from './PostElement.jsx' ;

export default class FancyColumns extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            posts : [{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            },{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            },{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            },{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            }]
        };
    }

    render() {
        if(this.props.columns == 2){
            return this.twoColumns() ;
        }else if(this.props.columns == 1){
            return this.oneColumns() ;
        }
    }

    twoColumns() {
        var postElements = [] ; 

        for(let i = 0 ; i < 2 ; i++){
            postElements.push( 
                <div className="columns small-6" key={i}>
                    <PostElement ref={'postElement'+i} onDrop={(item)=>{this.dropHandler(i,item)}} post={this.state.posts[i]}></PostElement> 
                </div> 
            );
        }

        return (
            <div className="row"> 
                {postElements}
            </div> 
        ) 
    }

    oneColumns() {
        var postElements = [] ; 

        for(let i = 0 ; i < 1 ; i++){
            postElements.push( 
                <div className="columns small-12" key={i}>
                    <PostElement ref={'postElement'+i} onDrop={(item)=>{this.dropHandler(i,item)}} post={this.state.posts[i]}></PostElement> 
                </div> 
            );
        }

        return (
            <div className="row"> 
                {postElements}
            </div> 
        ) 
    }

    dropHandler(index,item) {
        console.log(index) ;
        console.log(item) ;
        this.state.posts[index] = item ;

        this.setState({
            posts : this.state.posts
        });

    }

    getData() {
        var data = [] ; 

        if(this.props.columns == 2){
            data.push( this.refs.postElement0.props.post )
            data.push( this.refs.postElement1.props.post )
        }else if(this.props.columns == 1){
            data.push( this.refs.postElement0.props.post )
        }

        let ret = {
            'columnStyle' : this.props.columns,
            'posts' : data
        }

        return ret ; 
    }

    componentWillReceiveProps(nextProps, nextState) {
        console.log('column') ;
        console.log(nextProps) ;
        console.log('column end') ;

        //this.setState({columns: nextProps.row.columnStyle}) ; 
        this.setState({posts: nextProps.posts}) ; 
    }


}
