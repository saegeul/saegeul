<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Subscription\Models\Subscription;
use App\Http\Controllers\Api\V1\Transformers\SeriesTransformer;

class SubscriptionSeriesTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['series'];

    public function transform(Subscription $subscription)
    {
        return [
            'id' => (int) $subscription->id,
            'email' => $subscription->email,
            'user_id' => $subscription->user_id,
            'series_id' => $subscription->series_id,
            'activated' => $subscription->activated,
            'created_at' => $subscription->created_at,
            'updated_at' => $subscription->updated_at
        ];
    }

    public function includeSeries(Subscription $subscription)
    {
        $series = $subscription->series;

        return $this->item($series, new SeriesTransformer);
    }
}
