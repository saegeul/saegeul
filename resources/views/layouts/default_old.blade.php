<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Saegeul</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="/sgpack/flat-ui/images/favicon.ico">
        <link rel="stylesheet" href="/sgpack/flat-ui/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/sgpack/flat-ui/css/flat-ui.css"> 
        <link rel="stylesheet" href="/sgpack/assets/css/style.css">
        <link rel="stylesheet" href="/assets/iconic/font/css/iconic-bootstrap.css"> 
        <link rel="stylesheet" href="/sgpack/dist/css/saegeul.min.css" />
    </head>

    <body> 

    <script src="/sgpack/common-files/js/jquery-1.10.2.min.js"></script>
    <script src="/sgpack/flat-ui/js/bootstrap.min.js"></script>
    <script src="/sgpack/common-files/js/modernizr.custom.js"></script>
    </body>
</html>
