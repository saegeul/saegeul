<?php

namespace spec\Saegeul\Vcs\Jobs\Rollback;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface ;
use Saegeul\Vcs\Models\Revision as RevisionModel ;
use Saegeul\Vcs\Models\PullRequest as PullRequestModel ;
use Saegeul\Post\Models\Post as PostModel ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasRollbacked;

class RollbackCommandSpec extends ObjectBehavior
{
    public $postId;
    public $fromUuid;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Vcs\Jobs\Rollback\RollbackCommand');
    }

    public function let()
    {
        $this->postId = 1;
        $this->fromUuid = '111-222';
        $this->beConstructedWith($this->postId,$this->fromUuid);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository, 
        RevisionModel $revision,
        UuidService $uuidService,
        PostModel $post,
        PostModel $updatedPost,
        RevisionModel $newRevision
    )
    {
        $postRepository->find($this->postId)->shouldBeCalled()->willReturn($post);
        $revisionRepository->findByUuid($this->fromUuid)->shouldBeCalled()->willReturn($revision);

        $data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
            'series_id' => 1
        ];
        
        $revision->toArray()->shouldBeCalled()->willReturn($data);
        $postRepository->update($this->postId,$data)->shouldBeCalled()->willReturn($updatedPost);
        $updatedPost->getKey()->shouldBeCalled()->willReturn($this->postId);
        $uuid = '222-333';
        $uuidService->getTimebasedUuid()->shouldBeCalled()->willReturn($uuid);

        $post->getAttribute('favorite')->willReturn(1) ;
        $data['post_id'] = $this->postId;
        $data['uuid'] = $uuid;
        $data['from_rollback_uuid'] = $this->fromUuid;
        $data['command'] = 'rollback';
        $data['favorite'] = 1;
        $revisionRepository->store($data)->shouldBeCalled()->willReturn($newRevision);

        $dispatcher->fire(new PostWasRollbacked($updatedPost->getWrappedObject()))->shouldBeCalled();
        $this->handle($dispatcher,$revisionRepository,$postRepository,$uuidService)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
