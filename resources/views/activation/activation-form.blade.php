@extends("layouts.default")

@section('contents')
<div class="signup-wrapper">
    <div class="logo-wrapper" >
        <img src="/img/logo/saegeul_wordmark_lg.png" />
    </div>

    {!! Form::open(['class' =>'mini-paper', 'method' => 'post', 'url' => '/activation/request']) !!}
        <div class="">
            <label>Verify Your Email</label>
            <p><span>Enter your email</span>, we will send a reset link. </p>
        </div>
        <div class="field-group">
            <div>
                <input type="text" name="email" required="required">
            </div>
        </div> 
        <div class="clearfix">
            <button type="submit" class="button success small expand">Verify Your Email</button>
        </div>
    </form>
    <p class="text-center">
        <a class="mini-anchor" href="/signin">Return to Login</a>
    </p>
</div>
@stop
