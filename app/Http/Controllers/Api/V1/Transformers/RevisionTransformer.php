<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Vcs\Models\Revision;
use App\Http\Controllers\Api\V1\Transformers\UserTransformer;

class RevisionTransformer extends TransformerAbstract
{
    //protected $defaultIncludes = ['user'];

    public function transform(Revision $revision)
    {
        return [
            'id' => (int) $revision->id,
            'origin_id' => $revision->origin_id,
            'origin_owner' => $revision->origin_owner,
            'commit_uuid' => $revision->commit_uuid,
            'source_uuid' => $revision->source_uuid,
            'title' => $revision->title,
            'description' => $revision->description,
            'content' => $revision->content,
            'image_id' => $revision->revision_image_id,
            'image_url' => $revision->revision_image_url,
            'status' => $revision->status,
            'pull_request_status' => $revision->pull_request_status,
            'user_id' => $revision->user_id,
            'series_id' => $revision->series_id,
            'uuid' => $revision->uuid,
            'post_id' => $revision->post_id,
            'from_rollback_uuid' => $revision->from_rollback_uuid,
            'command' => $revision->command,
            'favorite' => $revision->favorite,
            'created_at' => (string)$revision->created_at,
            'updated_at' => (string)$revision->updated_at
        ];
    }
}
