<?php

namespace Saegeul\Favorite\Contracts;

interface FavoriteInterface
{ 
    public function getKey() ;
    public function getClassName() ;
    public function updateFavorite($favorite);
}
