<?php
namespace Saegeul\Vcs\Models;

use Saegeul\Logger\Contracts\LoggableInterface ;
use Saegeul\Notification\Contracts\NotifiableInterface ;

use Illuminate\Database\Eloquent\Model;

class PullRequest extends Model implements LoggableInterface, NotifiableInterface
{
    protected $table = 'pull_requests' ;
    protected $guarded = array() ;

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }

    public function series()
    {
        return $this->belongsTo('Saegeul\Series\Models\Series');
    }

    public function post()
    {
        return $this->belongsTo('Saegeul\Post\Models\Post');
    }

    public function getLoggedData()
    {
        $registerData = array();
        $registerData['pull_request_id'] = $this->getKey();
        $registerData['post_id'] = $this->post_id;
        $registerData['series_id'] = $this->series_id;

        $post = $this->post;
        $registerData['commit_uuid'] = $post->commit_uuid;

        return $registerData;
    }

    public function getNotificationData() 
    {
        $registerData = array();
        $registerData['pull_request_id'] = $this->getKey();
        $registerData['post_id'] = $this->post_id;
        $registerData['series_id'] = $this->series_id;

        $post = $this->post;
        $registerData['commit_uuid'] = $post->commit_uuid;

        return $registerData;
    }
}
