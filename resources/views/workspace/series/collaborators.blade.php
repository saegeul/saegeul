@extends("layouts.series")

@section('header')
@include('workspace.series.partials.header')
@endsection

@section('contents')
<div id="collaborators-area" data-series-id="{{$series->getKey()}}">
</div> 
<div class="content-wrapper">
    <div class="container mini-container">
        <div class="row" style="padding:15px;">
            <i class="material-icons">group</i> 2 Collaborators
        </div>
    </div>
    <div class="container mini-container">
        <div class="row">
            <div class="columns small-6">
                <ul style="margin:0px;" class="list white-theme">
                    <li class="draft">
                    <div class="left-column">
                        <i class="material-icons">account_box</i>
                    </div>
                    <div class="right-column">
                        <h6 class="title">
                            <a href="/workspace/series/1/posts/1/edit">Sihyeong</a>
                        </h6>
                        <ul class="tag-list" style="list-style: none;">
                            <li>sihyeong@artgrafii.com</li>
                            <li>joined at 12, Nov, 2015</li>
                        </ul>
                        <div class="additional-information clearfix">
                        </div>
                    </div>
                    </li>
                </ul>
            </div>
            <div class="columns small-6">
                <ul style="margin:0px;" class="list white-theme">
                    <li class="draft">
                    <div class="left-column">
                        <i class="material-icons">account_box</i>
                    </div>
                    <div class="right-column">
                        <h6 class="title">
                            <a href="/workspace/series/1/posts/1/edit">Aran</a>
                        </h6>
                        <ul class="tag-list" style="list-style: none;">
                            <li>aran@artgrafii.com</li>
                            <li>joined at 12, Nov, 2015</li>
                        </ul>
                        <div class="additional-information clearfix">
                        </div> 
                    </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts') 
<script>
var useCase = new UseCase ;
useCase.visit('series.collaborators') ;
</script> 
@stop 
