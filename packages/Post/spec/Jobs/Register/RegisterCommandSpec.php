<?php

namespace spec\Saegeul\Post\Jobs\Register;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Post\Models\Post as PostModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Post\Events\PostWasRegistered ;

class RegisterCommandSpec extends ObjectBehavior
{
    protected $data;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Post\Jobs\Register\RegisterCommand');
    }

    public function let()
    {
        $this->data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
            'user_id' => 1,
            'series_id' => 1
        ];
        $this->beConstructedWith($this->data);
    }

    public function it_trigger_handle(Dispatcher $dispatcher, PostRepositoryInterface $postRepository, PostModel $post)
    {
        $postRepository->store($this->data)->shouldBeCalled()->willReturn($post);

        $dispatcher->fire(new PostWasRegistered($post->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$postRepository)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
