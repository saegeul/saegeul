<?php

namespace Saegeul\Post\Jobs\Delete;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Post\Events\PostWasDeleted ;

class DeleteCommand implements SelfHandling
{

    public $postId ;

    public function __construct($postId)
    {
        $this->postId = $postId ;
    }

    public function handle(Dispatcher $dispatcher, PostRepositoryInterface $postRepository)
    {
        $post = $postRepository->find($this->postId);
        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        }

        $deletedPost = $postRepository->delete($this->postId);
        if($deletedPost){
            $dispatcher->fire(new PostWasDeleted($deletedPost));
        }

        return $deletedPost;
    }
}
