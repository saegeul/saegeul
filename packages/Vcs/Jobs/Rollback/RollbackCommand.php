<?php
namespace Saegeul\Vcs\Jobs\Rollback;

use Illuminate\Contracts\Bus\SelfHandling ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasRollbacked;

class RollbackCommand implements SelfHandling
{
    public $postId;
    public $fromUuid;

    public function __construct($postId, $fromUuid)
    {
        $this->postId = $postId;
        $this->fromUuid = $fromUuid;
    }

    public function handle(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository, 
        UuidService $uuidService
    ){ 
        $post = $postRepository->find($this->postId);
        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        }

        $revision = $revisionRepository->findByUuid($this->fromUuid);
        if(!$revision){
            throw New ModelNotFoundException('Not found uuid');
        }

        $data = $revision->toArray();
        unset($data['id']);
        unset($data['created_at']);
        unset($data['updated_at']);
        unset($data['post_id']);
        unset($data['uuid']);
        unset($data['from_rollback_uuid']);
        unset($data['command']);
        unset($data['favorite']);
        $updatedPost = $postRepository->update($this->postId,$data);
        
        $data['post_id'] = $updatedPost->getKey();
        $data['uuid'] = $uuidService->getTimebasedUuid();
        $data['from_rollback_uuid'] = $this->fromUuid;
        $data['command'] = 'rollback';
        $data['favorite'] = $post->favorite;
        $newRevision = $revisionRepository->store($data);

        $dispatcher->fire(new PostWasRollbacked($updatedPost));

        return $updatedPost;
    }
}
