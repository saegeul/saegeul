<?php namespace App\Http\Controllers;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\QueryException ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Services\HashCodeService ;

class AccountController extends Controller {

    protected $glauth ;
    protected $user;
    protected $hashCodeService ;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Glauth $glauth, HashCodeService $hashCodeService)
	{
        $this->glauth = $glauth ;
        $this->hashCodeService = $hashCodeService ;
        $this->user = $this->glauth->check() ;
	}

    public function profile(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
        $user = $this->user;

        if(!$user){
            return redirect()->to('/signin?return_url='.$returnUrl);
        }

        $links = $user->getSocials()->toJson();

        $action = "profile";
        return view('account.profile',compact('user','links','action')) ;
    }

    public function password(Request $request)
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):$request->url();
        $user = $this->user;
        if(!$user){
            return redirect()->to('/signin?return_url='.$returnUrl);
        }
        $hashCode = $this->hashCodeService->encode($user->getKey());

        $action = "password";
        return view('account.password',compact('user','hashCode','action')) ;
    }

    public function change(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'hash_code' => 'required',
                'old_password' => 'required',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }
            $oldPassword = $request->input('old_password');
            $this->glauth->signin($this->user->email,$oldPassword);

            $password = $request->input('password');
            $userService = $this->glauth->getUserService();
            $user = $userService->update($this->user->getKey(),compact('password'));

            return redirect()->back();
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
