<?php

namespace Saegeul\Post\Jobs\Update;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Post\Events\PostWasUpdated;

class UpdateCommand implements SelfHandling
{

    public $postId;
    public $data;

    public function __construct($postId, $data)
    {
        $this->postId = $postId;
        $this->data = $data;
    }

    public function handle(Dispatcher $dispatcher, PostRepositoryInterface $postRepository)
    {
        $post = $postRepository->find($this->postId);
        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        }

        if(isset($this->data['html'])){
            $temp = strip_tags($this->data['html']);
            $str = preg_replace(['/&nbsp;/','/&quot;/'],'',$temp);
            $this->data['description'] = mb_strimwidth($str,0,170,'...','utf-8');
        }

        $post = $postRepository->update($this->postId,$this->data);

        $dispatcher->fire(new PostWasUpdated($post));

        return $post ;
    }
}
