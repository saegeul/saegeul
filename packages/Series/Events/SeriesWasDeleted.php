<?php

namespace Saegeul\Series\Events ;

use Saegeul\Series\Models\Series ;

class SeriesWasDeleted 
{
    public $series;

    public function __construct(Series $series)
    {
        $this->series = $series;
    }
}
