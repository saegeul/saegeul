<?php 
namespace Saegeul\VideoParser;

use Illuminate\Foundation\Application; 
use Saegeul\VideoParser\Exceptions\InvalidHostException;

class VideoParser
{
    public function parse($url, $thumbType=null)
    {
        $type = '';
        if (preg_match('/[http|https]+:\/\/(?:www\.|)youtube\.com\/watch\?(?:.*)?v=([a-zA-Z0-9_\-]+)/i', $url, $matches) || preg_match('/[http|https]+:\/\/(?:www\.|)youtube\.com\/embed\/([a-zA-Z0-9_\-]+)/i', $url, $matches) || preg_match('/[http|https]+:\/\/(?:www\.|)youtu\.be\/([a-zA-Z0-9_\-]+)/i', $url, $matches)) {
            $imageUrl = $this->getYoutubeThumbUrl($matches[1],$thumbType);
            $type = 'youtube';
        }else if (preg_match('#(https?://)?(www.)?(player.)?vimeo.com/([a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $matches)){
            if ($xml = simplexml_load_file('http://vimeo.com/api/v2/video/'.$matches[5].'.xml')) {
                $imageUrl = $this->getVimeoThumbUrl($xml,$thumbType);
                $type = 'vimeo';
            }
        }

        $response = $this->curl($url); 
        if(!isset($imageUrl) || !isset($matches[1]) || empty($response)){
            throw New InvalidHostException('Invalid Host');
        }

        $result = new \stdClass;
        $result->type = $type;
        $result->link = $url;
        $result->imageUrl = $imageUrl;

        return $result ;
    }

    private function curl($url = '') {
        if (empty($url)) {
            return false;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);

        $data = curl_exec($ch);
        return $data;
    }

    private function getVimeoThumbUrl($xml, $type='')
    {
        switch($type)
        {
        case 'large' :
            return $xml->video->thumbnail_large ? (string) $xml->video->thumbnail_large: (string) $xml->video->thumbnail_medium;
            break;
        case 'small' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/1.jpg';
            return $xml->video->thumbnail_small ? (string) $xml->video->thumbnail_small: (string) $xml->video->thumbnail_medium;
            break;
        default :
            return (string) $xml->video->thumbnail_medium;
        }
    }

    private function getYoutubeThumbUrl($youtubeId, $type='')
    {
        switch($type)
        {
        case '0' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/0.jpg';
            break;
        case '1' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/1.jpg';
            break;
        case '2' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/2.jpg';
            break;
        case '3' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/3.jpg';
            break;
        case 'hq' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/hqdefault.jpg';
            break;
        case 'mq' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/mqdefault.jpg';
            break;
        case 'sd' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/sddefault.jpg';
            break;
        case 'maxres' :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/maxresdefault.jpg';
            break;
        default :
            return 'https//img.youtube.com/vi/'.$youtubeId.'/default.jpg';
        }
    }
}
