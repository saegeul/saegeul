<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class PostLogTest extends TestCase
{
    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(1);

        return $glauth->signin($user->email,'1q2w3e') ;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    }

    public function testGetLogs()
    {
        #given
        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        $this->signin();
        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/logs';
        $response = $this->call('GET',$prefix);

        #then
        $this->assertEquals(200, $response->status());
        /*
        $result = $response->getContent();
        $arr = json_decode($result, true);
        dd($arr);
         */
    }

    public function test_failed_case_to_get_logs_no_login()
    {
        #given
        $post = Factory::build('Saegeul\Post\Models\Post')->first();

        #when
        $prefix = 'api/v1/posts/'.$post->getKey().'/logs';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_get_logs_invalid_series_id()
    {
        #given
        $this->signin();
        $postId = 111;

        #when
        $prefix = 'api/v1/posts/'.$postId.'/logs';
        $response = $this->call('GET',$prefix);        

        #then
        $this->assertEquals(404, $response->status());
    }
}
