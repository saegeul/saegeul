<?php

namespace Saegeul\Favorite\Events ;

use Saegeul\Favorite\Models\Favorite ;

class FavoriteWasDeleted
{
    public $favorite;

    public function __construct(Favorite $favorite)
    {
        $this->favorite = $favorite;
    }
}
