@extends("layouts.series")

@section('header')
@include('workspace.series.partials.header')
@endsection

@section('contents')
<div id="">
    <div id="freespace" @if($cover) data-cover-id="{{$cover->getKey()}}" @endif data-series-id="{{$series->getKey()}}"></div>
</div> 
@stop

@section('scripts') 
<script>
var useCase = new UseCase ;
useCase.visit('series.cover') ;
</script> 
@stop 
