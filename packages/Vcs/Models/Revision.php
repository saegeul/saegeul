<?php

namespace Saegeul\Vcs\Models;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    protected $table = 'revisions' ;
    protected $guarded = array() ;

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }

    public function updateFavorite($favorite)
    {
        $this->favorite = $favorite;
        $this->save();

        return $this;
    }
}
