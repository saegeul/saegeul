@extends("layouts.default")

@section('contents')
@include('account.profile-header') 
<div class="profile-content">
    {!! Form::open(['class' => 'mini-paper clearfix', 'method' => 'post', 'url' => '/account/change']) !!}
        <input type="hidden" name="hash_code" value="{{ $hashCode }}"/>
        <div class="field-group">
            <label>Old Password</label>
            <div>
                <input type="password" name="old_password"/>
            </div>
        </div>
        <div class="field-group">
            <label>New Password</label>
            <div>
                <input type="password" name="password"/>
            </div>
        </div>
        <div class="field-group">
            <label>Confirm New Password</label>
            <div>
                <input type="password" name="password_confirmation"/>
            </div>
        </div>
        <button class="button alert right" type="submit">Password Change</button>

    {!! Form::close() !!}
</div>

@stop
