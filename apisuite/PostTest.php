<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostTest extends TestCase
{
    private $prefix = 'api/v1/posts';
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    public function testRegisterPostWithoutSeries()
    {
        $this->signin();

        $data = factory(Saegeul\Post\Models\Post::class)->make()->toArray();
        $data['description'] = '<div>새글 개요</div><ul><li><strong>새글은 전문적으로 컨텐츠(책, 웹툰, 소설 등)를 제작 및 배포 할 수 있는 컨텐츠 플랫폼&nbsp;</strong>이다.&nbsp;</li><li>새글은 팀 단위로 작업을 할 수 있으며, 하나의 컨텐츠를 제작하기 위해서 다양한 사람들이 참여 할 수 있다.</li><li>개인 주도하에 이뤄지는 컨텐츠 마켓이 형성되어 기존에 시장에서 볼 수 없었던 다양한 카테고리의 증가가 기대된다.</li></ul><blockquote>함께하는것은 무엇일까? 생각해보라 &nbsp;<strong>참인것</strong>은?<ul><li>1번인지?</li><li>2번인지?</li><li>3번인지?</li></ul></blockquote><div>핵심소비자.</div><ul><li><strong>새글을 전문적으로 활용하는 소규모 위주의 컨텐츠 제작팀이 형성되어 다양한 카테고리의 컨텐츠가 제작됨</strong></li><li>다양한 카테고리를 구독하고 싶어하는 컨텐츠 구독자에게도 도움이 된다.&nbsp;</li></ul><div>새글의 주요 차별화 포인트</div><ul><li><strong>강력한 타임머신&nbsp;</strong>:&nbsp;<strong>타임머신 기능이란, 컨텐츠를 과거로 복구 할 수 있는 기능</strong>을 말한다.&nbsp;<strong>문서의 변경이력이 생길때마다 기록</strong>을 남겨, 컨텐츠 제작자는 상황에 따라서&nbsp;<strong>특정 시점으로 컨텐츠를 복원 할 수 있다.&nbsp;</strong></li><li><strong>타임머신 기능 때문에 협업이 더욱 강력해진다</strong>. 기존의 협업 시스템은 원본 컨텐츠를 수정하고 복원하는 형식이었기 때문에 원본의 훼손이 있을수 있다.&nbsp;<strong>새글은 원저작자만이 원본을 수정 할 수 있고, 협업자는 문서를 복사해서 수정하고 원저작자에게 제안하기 때문에 원저작자의 의도안에서 컨텐츠의 의도를 컨트롤 할 수 있다.</strong></li><li>컨텐츠유료화 : 컨텐츠 제작자는 제작된 컨텐츠를 새글 마켓을 통해서 유료로 배포가 가능하다. 그러므로&nbsp;<strong>컨텐츠 판매를 통해서 수익을 취할 수 있다.</strong></li><li>새글 에디터는 드래그앤 드랍 이미미지 첨부가 가능합니다.</li></ul><div><a href="http://11B21.http.dal05.cdn.softlayer.net/CDN/affba480-c2c0-45e4-91c7-fc45f8513642/113/151/020/e7b78be2151113083442.png" data-trix-attachment="{&quot;contentType&quot;:&quot;image/png&quot;,&quot;filename&quot;:&quot;새글 소개 화면.png&quot;,&quot;filesize&quot;:118493,&quot;height&quot;:617,&quot;href&quot;:&quot;http://11B21.http.dal05.cdn.softlayer.net/CDN/affba480-c2c0-45e4-91c7-fc45f8513642/113/151/020/e7b78be2151113083442.png&quot;,&quot;url&quot;:&quot;http://11B21.http.dal05.cdn.softlayer.net/CDN/affba480-c2c0-45e4-91c7-fc45f8513642/113/151/020/e7b78be2151113083442.png&quot;,&quot;width&quot;:680}" data-trix-content-type="image/png">';
        $response = $this->call('POST',$this->prefix,$data);
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result,true);
        $this->assertEquals($arr['data']['title'], $data['title']);
        $this->assertEquals($arr['data']['content'], $data['content']);

        $vcsService = $this->app->make('Saegeul\Vcs\Vcs') ;
        $revisionRepository = $vcsService->getRevisionRepository();
        $revision = $revisionRepository->findByUuid($arr['data']['commit_uuid']);
        $this->assertEquals($revision->uuid, $arr['data']['commit_uuid']);
    }

    public function test_failed_case_to_register_no_login()
    {
        $data = factory(Saegeul\Post\Models\Post::class)->make();

        $response = $this->call('POST',$this->prefix,$data->toArray(), []);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_register_invalid_data()
    {
        $this->signin();

        $response = $this->call('POST',$this->prefix,[], []);        
        $this->assertEquals(422, $response->status());
    }

    public function testRegisterPostWithSeries()
    {
        $this->signin();

        $data = factory(Saegeul\Post\Models\Post::class)->make();
        $seriesId = 1;

        $response = $this->call('POST',$this->prefix, array_merge($data->toArray(),array('series_id'=>$seriesId)), []);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['title'], $data['title']);
        $this->assertEquals($arr['data']['content'], $data['content']);
        $this->assertEquals($arr['data']['series_id'], $seriesId);
    }


    public function testGetPost()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix.'/1');        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['id'], 1);

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $post = $postRepository->find(1);
        $this->assertEquals($arr['data']['id'], $post->id);
    } 

    public function test_failed_case_to_show_no_login()
    {
        $response = $this->call('GET',$this->prefix.'/1');        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_show_invalid_post_id()
    {
        $this->signin();

        $response = $this->call('GET',$this->prefix.'/31');        
        $this->assertEquals(404, $response->status());
    }

    public function testUpdatePost()
    {
        $this->signin();

        $data = factory(Saegeul\Post\Models\Post::class)->make()->toArray();

        $response = $this->call('PUT', $this->prefix.'/6', $data, []);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['title'], $data['title']);
        $this->assertEquals($arr['data']['content'], $data['content']);
    }

    public function test_failed_case_to_update_no_login()
    {
        $data = factory(Saegeul\Post\Models\Post::class)->make();

        $response = $this->call('PUT', $this->prefix.'/1', $data->toArray(), []);
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_update_invalid_post_id()
    {
        $this->signin();

        $data = factory(Saegeul\Post\Models\Post::class)->make();

        $response = $this->call('PUT', $this->prefix.'/31', $data->toArray(), []);
        $this->assertEquals(404, $response->status());
    }

    public function test_failed_case_to_update_invalid_data()
    {
        $this->signin();

        $data = [];

        $response = $this->call('PUT', $this->prefix.'/31', $data, []);
        $this->assertEquals(422, $response->status());
    }

     public function testPublishPost()
    {
        $this->signin();

        $data = factory(Saegeul\Post\Models\Post::class)->make();

        $response = $this->call('PUT', $this->prefix.'/1/publish');        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['status'], 'publish');
    }

    public function test_failed_case_to_publish_no_login()
    {
        $data = factory(Saegeul\Post\Models\Post::class)->make();

        $response = $this->call('PUT', $this->prefix.'/1/publish');        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_publish_invalid_post_id()
    {
        $this->signin();

        $data = factory(Saegeul\Post\Models\Post::class)->make();

        $response = $this->call('PUT', $this->prefix.'/31/publish');        
        $this->assertEquals(404, $response->status());
    }

    public function testDeletePost()
    {
        $this->signin();

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository();
        $post = $postRepository->lists(1,'')->first();

        $response = $this->call('DELETE',$this->prefix.'/'.$post->getKey());        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);

        $postService = $this->app->make('Saegeul\Post\Post') ;
        $postRepository = $postService->getRepository() ;
        $post = $postRepository->find($arr['data']['id']) ;
        $this->assertEmpty($post);
    }

    public function test_failed_case_to_delete_no_login()
    {
        $response = $this->call('DELETE', $this->prefix.'/1');
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_delete_invalid_post_id()
    {
        $this->signin();

        $data = factory(Saegeul\Post\Models\Post::class)->make();

        $response = $this->call('DELETE', $this->prefix.'/31');
        $this->assertEquals(404, $response->status());
    } 
}
