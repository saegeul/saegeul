<?php

namespace Saegeul\Post\Jobs\Show;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class ShowCommand implements SelfHandling
{

    public $postId ;

    public function __construct($postId)
    {
        $this->postId = $postId ;
    }

    public function handle(PostRepositoryInterface $postRepository)
    {
        $post = $postRepository->find($this->postId);

        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        }

        return $post ;
    }
}
