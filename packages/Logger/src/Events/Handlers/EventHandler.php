<?php
namespace Saegeul\Logger\Events\Handlers ;

use Saegeul\Logger\Logger ;
use Saegeul\Logger\Contracts\LoggableInterface ;

class EventHandler
{ 
    protected $logger ;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger ;
    }


    public function handle(LoggableInterface $data , $eventName )
    {
        $this->logger->log($data,$eventName) ;
        return true ;
    }
}
