<?php
namespace Saegeul\Cover\Models;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $table = 'covers' ;
    protected $fillable = ['content', 'series_id', 'user_id', 'status'] ;

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }
}
