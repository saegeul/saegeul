<?php

namespace Saegeul\Post;

use Illuminate\Bus\Dispatcher ;
use Saegeul\Post\Jobs\Register\RegisterCommand ;
use Saegeul\Post\Jobs\Update\UpdateCommand ;
use Saegeul\Post\Jobs\Show\ShowCommand ;
use Saegeul\Post\Jobs\Delete\DeleteCommand ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Artgrafii\Glauth\Contracts\UserInterface ;

class Post
{
    protected $dispatcher; 

    public function __construct(Dispatcher $dispatcher, PostRepositoryInterface $postRepository)
    {
        $this->dispatcher = $dispatcher ;
        $this->postRepository = $postRepository ;
    }

    public function register($data)
    {
        $command = new RegisterCommand($data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function update($postId, $data)
    {
        $command = new UpdateCommand($postId,$data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function show($postId)
    {
        $command = new ShowCommand($postId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function delete($postId)
    {
        $command = new DeleteCommand($postId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function getRepository()
    {
        return $this->postRepository ;
    }

    public function createWithUser(array $data, UserInterface $user)
    {
        $data['user_id'] = $user->getKey() ;
        return $this->register($data) ;
    }
}
