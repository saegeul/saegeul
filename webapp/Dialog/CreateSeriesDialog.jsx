import React from 'react' ;
import Request from 'superagent' ;

export default class CreateSeriesDialog extends React.Component {

    constructor(props) {
        super(props) ;
        console.log(this.props.dialogStatus) ;

        this.state = {
            display:'none',
            modalBgStyle : {
                display: 'block'
            },
            modalStyle : {
                display: 'block',
                opacity: 1,
                visibility: 'visible' 
            },
            title : '' , 
            description : ''
        };

        this.openStyle =  {
            display: 'block',
            opacity: 1,
            visibility: 'visible' 

        }

        this.closeStyle = { 
            display: 'none'
        }
    }

    render() {
        return (
            <div>
                <div className="reveal-modal-bg" style={this.props.dialogStatus.isOpen ? this.openStyle : this.closeStyle}></div>

                <div className="reveal-modal sg-reveal-modal small open" style={this.props.dialogStatus.isOpen ? this.openStyle : this.closeStyle}>
                    <div className="modal-header">
                        <h5>Create Series</h5>
                        <small>Make your collection</small>
                    </div> 
                    <div className="modal-body">
                        <form className="form">
                            <div>
                                <input type="text" name="title" placeholder="Title"  onChange={this.onChangeTitle.bind(this)} value={this.state.title} />
                            </div>
                            <div>
                                <textarea rows="4" name="description" placeholder="Description" onChange={this.onChangeDescription.bind(this)}  value={this.state.description} />
                            </div> 
                        </form>
                        <div className="clearfix">
                            <div className="right">
                            <a onClick={this.props.closeDialog} className="ghost-btn">CANCEL</a>
                            <a onClick={this.createSeries.bind(this)} className="ghost-btn">CREATE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    createSeries() { 
        Request.post('/api/v1/series')
                .send({
                    'title' : this.state.title ,
                    'description' : this.state.description ,
                })
                .end(function(err,response){
                    this.props.closeDialog();
                    this.props.refreshItems() ;
                }.bind(this)); 
    } 

    onChangeTitle(event) {
        this.setState({ title : event.target.value});
    } 

    onChangeDescription(event) {
        this.setState({ description : event.target.value});
    } 
}
