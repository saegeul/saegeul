<?php

namespace Saegeul\Finder\Services;

use Illuminate\Foundation\Application; 
use Saegeul\Finder\Contracts\FinderServiceInterface;

class YoutubeFinderService implements FinderServiceInterface 
{
    protected $search;
    protected $listCount = 20;

    public function __construct(\Google_Service_YouTube_Search_Resource $search)
    {
        $this->search = $search;
    }

    public function find($keyword,$page)
    {
        $searchOptions = $this->getSearchOptions($keyword,$page);        
        $response = $this->search->listSearch('id,snippet', $searchOptions);

        $result = $this->getReturnData($response);
        return $result;
    }  

    private function getSearchOptions($keyword, $page)
    {
        $searchOptions = array();
        $searchOptions['q'] = $keyword ;
        $searchOptions['maxResults'] = $this->listCount ;

        if(isset($page) && $page != 1 && $page != ""){
            $searchOptions['pageToken'] = $page ;
        }
        return $searchOptions ;
    }

    private function getReturnData($response)
    {
        $result = array() ;
        $items = array();
        foreach($response->getItems() as $val)
        {
            $video = $val->getSnippet();
            $item['id'] = $val->getId()->getVideoId();
            $item['url'] = "youtube.com/watch?v=".$item['id'] ;
            $item['title'] = $video->getTitle(); 
            $item['description'] = $video->getDescription(); 
            $item['image'] = $video->getThumbnails()->getHigh()->getUrl();
            $item['publish_at'] = $video->getPublishedAt();
            $items[] = $item;
        }
        $result['items'] = $items ;
        $result['nextPage'] = $response->getNextPageToken();

        return $result ;
    }    
}
