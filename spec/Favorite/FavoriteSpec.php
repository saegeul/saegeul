<?php

namespace spec\Saegeul\Favorite;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Bus\Dispatcher ;
use Saegeul\Favorite\Jobs\Register\RegisterCommand ;
use Saegeul\Favorite\Jobs\Show\ShowCommand ;
use Saegeul\Favorite\Jobs\Delete\DeleteCommand ;
use Saegeul\Favorite\Models\Favorite;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Artgrafii\Glauth\Contracts\UserInterface ;
use Saegeul\Favorite\Contracts\FavoriteInterface;

class FavoriteSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Favorite\Favorite');
    }

    public function let(Dispatcher $dispatcher, FavoriteRepositoryInterface $favoriteRepository)
    {
        $this->beConstructedWith($dispatcher,$favoriteRepository);
    }

    public function it_register(Dispatcher $dispatcher, Favorite $favorite, UserInterface $user, FavoriteInterface $favoriteModel)
    {
        $favoriteModel->getKey()->shouldBeCalled()->willReturn(1) ;
        $favoriteModel->getClassName()->shouldBeCalled()->willReturn('Saegeul\Series\Models\Series') ;
        $user->getKey()->shouldBeCalled()->willReturn(1) ;
        $data = [
            'favorite_id' => 1,
            'favorite_type' => 'Saegeul\Series\Models\Series',
            'user_id' => 1
        ];

        $command = new RegisterCommand($data) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($favorite) ; 
        $this->register($favoriteModel,$user)->shouldReturnAnInstanceOf('Saegeul\Favorite\Models\Favorite');
    }

    public function it_show(Dispatcher $dispatcher, Favorite $favorite)
    {
        $favoriteId = 1;
        $command = new ShowCommand($favoriteId) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($favorite) ; 
        $this->show($favoriteId)->shouldReturnAnInstanceOf('Saegeul\Favorite\Models\Favorite');
    }

    public function it_delete(Dispatcher $dispatcher, Favorite $favorite)
    {
        $favoriteId = 1;
        $command = new DeleteCommand($favoriteId) ; 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($favorite) ; 
        $this->delete($favoriteId)->shouldReturnAnInstanceOf('Saegeul\Favorite\Models\Favorite');
    }

    public function it_get_repository(FavoriteRepositoryInterface $favoriteRepository)
    {
        $this->getRepository()->shouldReturnAnInstanceOf('Saegeul\Favorite\Contracts\FavoriteRepositoryInterface');
    }
}
