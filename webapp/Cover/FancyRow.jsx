import React from 'react' ;
import FancyColumns from './FancyColumns.jsx' ;

export default class FancyRow extends React.Component {

    constructor(props) { 
        super(props) ;
        this.state = {
            columns : 2, 
            posts : [{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            },{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            },{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            },{
                title : 'Title',
                image_url : 'placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150' 
            } ]
        }
    }
    render() {
        return (
            <div className="container mini-container">
                <div className="remocon-wrapper">
                    <ul className="remocon">
                        <li><a onClick={this.changeOneColumns.bind(this)} ><i className="material-icons">view_day</i></a></li>
                        <li><a onClick={this.changeOneColumns.bind(this)} ><i className="material-icons">view_module</i></a></li>
                        <li><a onClick={this.changeTwoColumns.bind(this)} ><i className="material-icons">view_list</i></a></li>
                        <li><a onClick={this.props.deleteRow} ><i className="material-icons">delete</i></a></li>
                    </ul>
                </div>
                <FancyColumns columns={this.state.columns} posts={this.state.posts} ref="columns"></FancyColumns>
                <br/>
            </div>
        )
    } 

    changeOneColumns() {
        this.changeColumns(1) ;
    }

    changeTwoColumns() {
        this.changeColumns(2) ;
    }

    changeThreeColumns() {
        this.changeColumns(3) ;
    }

    changeColumns(columns=1){
        this.setState({
            columns : columns
        });
    }

    getData() {
        return this.refs.columns.getData() ;
    }

    componentWillReceiveProps(nextProps, nextState) {

        this.setState({columns: nextProps.row.columnStyle}) ; 
        this.setState({posts: nextProps.row.posts}) ; 
    }

}
