import React from 'react' ; 
import SeriesItem from './Partials/SeriesItem.jsx' ;
import Request from 'superagent' ; 

class SeriesList extends React.Component {

    constructor(props) {
        super(props) ; 
    }

    render() {
        return (
            <ul className="list pure-version"> 
                {this.props.items.map((series,index) => {
                    return (
                        <SeriesItem series={series} key={index}></SeriesItem>
                    )
                })} 
            </ul>
        );
    }
}

export default SeriesList ;
