<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubscriptionTest extends TestCase
{
    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    public function testFollow()
    {
        $user = $this->signinByCollaborator();


        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->show(1);
        $subscribers = $series->subscribers;

        $prefix = 'api/v1/series/1/follow';
        $response = $this->call('POST',$prefix,[],[]);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
         
        $this->assertEquals($arr['data']['email'], $user->email);

        $series = $seriesService->show(1);
        $this->assertEquals($series->subscribers, $subscribers+1);
    } 

    public function test_failed_case_to_follow_invalid_no_login()
    {
        $prefix = 'api/v1/series/1/follow';
        $response = $this->call('POST',$prefix,[],[]);        
        $this->assertEquals(401, $response->status());
    } 

    public function test_failed_case_to_follow_invalid_series_id()
    {
        $this->signinByCollaborator();

        $prefix = 'api/v1/series/31/follow';
        $response = $this->call('POST',$prefix,[],[]);        
        $this->assertEquals(404, $response->status());
    } 

    public function test_failed_case_to_follow_already_complted_subscription()
    {
        $this->signinByCollaborator();

        $prefix = 'api/v1/series/1/follow'; 

        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscriptionRepository = $subscriptionService->getRepository();
        $subscription = $subscriptionRepository->findBySeriesId(1,1)->first();

        $response = $this->call('POST',$prefix,[],[]);        
        $this->assertEquals(422, $response->status());
    }

    public function test_failed_case_to_unfollow_invalid_no_login()
    {
        $prefix = 'api/v1/series/1/unfollow';
        $response = $this->call('DELETE',$prefix,[],[]);        
        $this->assertEquals(401, $response->status());
    } 

    public function test_failed_case_to_unfollow_invalid_series_id()
    {
        $this->signinByCollaborator();

        $prefix = 'api/v1/series/31/unfollow';
        $response = $this->call('DELETE',$prefix,[],[]);        
        $this->assertEquals(404, $response->status());
    }

    public function testUnfollow()
    {
        $this->signinByCollaborator();

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $series = $seriesService->show(1);
        $subscribers = $series->subscribers;

        $prefix = 'api/v1/series/1/unfollow'; 

        $response = $this->call('DELETE',$prefix,[],[]);        
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals(201, $response->status());

        $series = $seriesService->show(1);
        $this->assertEquals($series->subscribers, $subscribers-1);
    }
}
