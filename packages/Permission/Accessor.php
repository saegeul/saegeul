<?php

namespace Saegeul\Permission;

trait Accessor {

    public function isCreatableByUser($user)
    {
        $accessType = $this->table.'.'.$this->getKey().'.'.'create';
        return $user->hasAccess($accessType); 
    }

    public function isUpdatableByUser($user)
    {
        $accessType = $this->table.'.'.$this->getKey().'.'.'update';
        return $user->hasAccess($accessType); 
    }

    public function isDeletableByUser($user)
    {
        $accessType = $this->table.'.'.$this->getKey().'.'.'delete';
        return $user->hasAccess($accessType); 
    }

    public function isViewableByUser($user)
    {
        $accessType = $this->table.'.'.$this->getKey().'.'.'view';
        return $user->hasAccess($accessType); 
    }

    public function isWriteableByUser($user)
    {
        $accessType = $this->table.'.'.$this->getKey().'.'.'write';
        return $user->hasAccess($accessType); 
    }

}
