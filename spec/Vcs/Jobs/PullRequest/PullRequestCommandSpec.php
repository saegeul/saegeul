<?php

namespace spec\Saegeul\Vcs\Jobs\PullRequest;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Vcs\Models\PullRequest as PullRequestModel ;
use Saegeul\Post\Models\Post as PostModel ;
use Saegeul\Vcs\Models\Revision as RevisionModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasPullRequested;

class PullRequestCommandSpec extends ObjectBehavior
{
    protected $data;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Vcs\Jobs\PullRequest\PullRequestCommand');
    }

    public function let()
    {
        $this->data = [
            'post_id' => 2,
            'series_id' => 1,
            'status' => 'pull-request',
            'user_id' => 2,
            'comment' => 'post 1 에 입력을 원합니다.'
        ];
        $this->beConstructedWith($this->data);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        PullRequestRepositoryInterface $pullRequestRepository, 
        PostRepositoryInterface $postRepository, 
        RevisionRepositoryInterface $revisionRepository, 
        RevisionModel $revision,
        PostModel $post,
        PullRequestModel $pullRequest
    )
    {
        $pullRequestRepository->store($this->data)->shouldBeCalled()->willReturn($pullRequest);

        $pullRequest->getAttribute('post_id')->willReturn(2);
        $postRepository->find(2)->shouldBeCalled()->willReturn($post);
        $pullRequest->getAttribute('status')->willReturn('pull-request');
        $post->setAttribute('pull_request_status','pull-request')->shouldBeCalled();
        $post->save()->shouldBeCalled();

        $uuid = '111';
        $post->getAttribute('commit_uuid')->willReturn($uuid);
        $revisionRepository->findByUuid($uuid)->shouldBeCalled()->willReturn($revision);
        $revision->setAttribute('pull_request_status','pull-request')->shouldBeCalled();
        $revision->save()->shouldBeCalled();

        $dispatcher->fire(new PostWasPullRequested($pullRequest->getWrappedObject()))->shouldBeCalled();
        $this->handle($dispatcher,$pullRequestRepository,$revisionRepository,$postRepository)->shouldReturnAnInstanceOf('Saegeul\Vcs\Models\PullRequest');
    }
}
