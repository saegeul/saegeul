<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        $glauth = App::make('Artgrafii\Glauth\Glauth') ; 
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository(); 

        factory(Saegeul\Post\Models\Post::class,5)->make()->each(function($u) use($userRepository){
            $user = $userRepository->findById(1) ;  
            $data = $u->toArray();
            $data['series_id'] = 1;

            $postService = App::make('Saegeul\Post\Post') ;
            $post = $postService->createWithUser($data,$user); 
        }) ;
    }
}
