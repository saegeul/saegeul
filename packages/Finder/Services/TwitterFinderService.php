<?php

namespace Saegeul\Finder\Services;

use Saegeul\Finder\Contracts\FinderServiceInterface;
use TwitterOAuth\Auth\ApplicationOnlyAuth;
use TwitterOAuth\Serializer\ArraySerializer;

class TwitterFinderService implements FinderServiceInterface 
{
    protected $auth;
    protected $listCount = 20;
    protected $cardType = array(
        'summary' => 'Summary',
        'summary_large_image' => 'Large Image Summary',
        'photo' => 'Photo',
        'gallery' => 'Gallery',
        'app' => 'App'
    );

    public function __construct(ApplicationOnlyAuth $auth)
    {
        $this->auth = $auth;
    }

    public function find($keyword,$page)
    {
        $searchOptions = $this->getSearchOptions($keyword,$page);        
        $response = $this->auth->get('search/tweets', $searchOptions);

        $result = $this->getReturnData($response);
        return $result ;
    }

    private function getSearchOptions($keyword, $page)
    {
        $searchOptions = array();
        $searchOptions['q'] = $keyword ;
        $searchOptions['count'] = $this->listCount ;
        $searchOptions['result_type'] = 'recent' ;

        if(isset($page) && $page != 1 && $page != ""){
            $searchOptions['max_id'] = $page ;
        }
        return $searchOptions ;
    }

    private function getReturnData($response)
    {
        $result = array() ;
        $result['items'] = $response['statuses'];
        //$result['items'] = array_map([$this, 'generateMetaTag'], $response['statuses']);

        if(isset($response['search_metadata']['next_results'])){
            parse_str($response['search_metadata']['next_results'], $query) ;
            $result['nextPage'] = $query['?max_id'] ;
        }else{
            $result['nextPage'] = "" ;
        }

        return $result ;
    }

    private function generateMetaTag($item)
    {
        if(isset($item['entities']['media'][0]['media_url_https']) && $item['entities']['media'][0]['media_url_https'] != ""){
            $url = $item['entities']['media'][0]['expanded_url'];
        }else{
            $url = "https://twitter.com/".$item['user']['screen_name']."/status/".$item['id'];
        }
        $tag = "<blockquote class='twitter-tweet'><a href='".$url."'></a></blockquote>";
        return $tag;
    }
}
