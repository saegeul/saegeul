<?php

class ShortenUrlTest extends TestCase 
{
    public function testRegister()
    {
        $url = 'http://www.google.com/';
        $shortenService = $this->app->make('Saegeul\ShortenUrl\Contracts\ShortenUrlInterface') ;
        $result = $shortenService->register($url);

        $this->assertArrayHasKey('id',$result);
        $this->assertEquals($result['longUrl'],$url) ; 
    }
}
