<?php
namespace Saegeul\Cover\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface CoverRepositoryInterface extends RepositoryInterface
{
}
