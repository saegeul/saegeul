<?php

use Laracasts\TestDummy\Factory;

class CoverTest extends TestCase 
{

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testRegister()
	{
        $data = [
            'content' => "{'title':'Hi There',Main-Image:'http://www.image.com'}",
            'series_id' => 1,
            'status' => 'published'
            ];
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $cover = $coverService->register($data) ;

        $this->assertInstanceOf('Saegeul\Cover\Models\Cover',$cover) ;
        $this->assertEquals($cover->content,$data['content']) ;
        $this->assertEquals($cover->series_id,$data['series_id']) ;
        $this->assertEquals($cover->status,$data['status']) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_register_cover_invalid_series_id()
	{
        $data['series_id'] = 111111;
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $cover = $coverService->register($data) ;
    }

    public function testShow()
    {
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $id = 1;
        $cover = $coverService->show($id) ;
        $this->assertInstanceOf('Saegeul\Cover\Models\Cover',$cover) ;
        $this->assertEquals($cover->getKey(),$id) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_show_cover_invalid_cover_id()
	{
        $id = 111;
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $cover = $coverService->show($id) ;
    }

    public function testUpdate()
    {
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $id = 1;
        $data = [
            'content' => "{'title':'Ohhh ohhh'}",
            ];
        $cover = $coverService->update($id,$data) ;
        $this->assertInstanceOf('Saegeul\Cover\Models\Cover',$cover) ;
        $this->assertEquals($cover->content,$data['content']) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_update_cover_invalid_cover_id()
	{
        $id = 111;
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $data = [
            'content' => "{'title':'Ohhh ohhh'}",
            ];
        $cover = $coverService->update($id,$data) ;
    }

    public function testDelete()
    {
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $id = 1;
        $cover = $coverService->delete($id) ;
        $this->assertInstanceOf('Saegeul\Cover\Models\Cover',$cover) ;
        $this->assertEquals($cover->getKey(),$id) ;
    }

    /**
     * @expectedException Illuminate\Database\Eloquent\ModelNotFoundException
     **/
    public function test_failed_case_to_delete_cover_invalid_cover_id()
	{
        $id = 111;
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $cover = $coverService->delete($id) ;
    }

    public function testCreateWithUser()
	{
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $loginUser = $userRepository->find(1);

        $result = $glauth->signin($loginUser->email,'1q2w3e') ;
        $data = [
            'content' => "{'title':'Hi There',Main-Image:'http://www.image.com'}",
            'series_id' => 1,
            'status' => 'published'
            ];
        $coverService = $this->app->make('Saegeul\Cover\Cover') ;
        $cover = $coverService->createWithUser($data) ;

        $this->assertInstanceOf('Saegeul\Cover\Models\Cover',$cover) ;
        $this->assertEquals($cover->content,$data['content']) ;
        $this->assertEquals($cover->series_id,$data['series_id']) ;
        $this->assertEquals($cover->user_id,$loginUser->getKey()) ;
        $this->assertEquals($cover->status,$data['status']) ;
    }
}
