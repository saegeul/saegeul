<?php

namespace Saegeul\Invitation\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface InvitationRepositoryInterface extends RepositoryInterface 
{
    public function findByCode($code);
    public function findOnWaitingByEmail($email,$roleId);
}
