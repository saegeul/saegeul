<?php

    Route::get('/signup',array('as' => 'signup' , 'uses' => 'SignController@signupForm')) ;
    Route::post('/signup',array('as' => 'signup' , 'uses' => 'SignController@signup'));
    Route::get('/signin',array('as' => 'signin' , 'uses' => 'SignController@signinForm')) ;
    Route::post('/signin',array('as' => 'signin' , 'uses' => 'SignController@signin')) ;
    Route::get('/signout',array('as' => 'signout' , 'uses' => 'SignController@signout')) ;
