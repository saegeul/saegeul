import React from 'react' ; 
import FancyRow from './FancyRow.jsx' ;
import uuid from 'uuid' ;

export default class FancyRowList extends React.Component {

    constructor(props){
        super(props);
    } 

    render() {
        return (
            <div>
                {this.props.rows.map((o,key) => {
                    return <FancyRow uuid={o.id} ref={'row'+key} row={o}  key={key} deleteRow={this.props.deleteRow.bind(this,o)} ></FancyRow>
                })}
                
                <a onClick={this.props.addRow} >ADD ROW</a>
            </div>
        );
    }

    getData() {
        var data = [] ; 
        
        this.props.rows.map((o,index) => { 
            data.push(this.refs['row'+index].getData() )
        }) ;

        return data ; 
    }
}
