<?php
namespace App\Http\Controllers\Guide\Series ;

use App\Http\Controllers\Controller ;

class SeriesController extends Controller { 

    public function show($seriesId)
    {
        return view('guides.series.show') ;
    } 
}
