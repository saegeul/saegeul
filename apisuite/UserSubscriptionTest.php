<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class UserSubscriptionTest extends TestCase
{
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }
 
    public function testGetFollowing()
    {
        $this->signin();

        $subscriptionService = $this->app->make('Saegeul\Subscription\Subscription') ;
        $subscriptionRepository = $subscriptionService->getRepository();

        $prefix = 'api/v1/series/1/follow';
        $response = $this->call('POST',$prefix,[],[]);        
        $this->assertEquals(201, $response->status());
        $result = $response->getContent();
        $arr = json_decode($result, true);

        $subscription = $subscriptionRepository->find($arr['data']['id']);
        $subscription->activated = 1;
        $subscription->save();

        $prefix = 'api/v1/series/2/follow';
        $response = $this->call('POST',$prefix,[],[]);        
        $this->assertEquals(201, $response->status());
        $result = $response->getContent();
        $arr = json_decode($result, true);

        $subscription = $subscriptionRepository->find($arr['data']['id']);
        $subscription->activated = 1;
        $subscription->save();

        $prefix = 'api/v1/following/series';
        $response = $this->call('GET',$prefix,[], []);

        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertNotEmpty($arr['data']);
        $this->assertEquals($arr['data'][0]['user_id'],$this->loginUser->getKey());
    }

    public function test_failed_case_to_get_following_invalid_no_login()
    {
        $prefix = 'api/v1/following/series';
        $response = $this->call('GET',$prefix,[],[]);        
        $this->assertEquals(401, $response->status());
    }
}
