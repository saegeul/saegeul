<?php
namespace Saegeul\Vcs\Jobs\Fork;

use Illuminate\Contracts\Bus\SelfHandling ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasForked;

class ForkCommand implements SelfHandling
{

    public $postId;
    public $userId;

    public function __construct($postId,$userId)
    {
        $this->postId = $postId;
        $this->userId = $userId;
    }

    public function handle(Dispatcher $dispatcher, RevisionRepositoryInterface $revisionRepository, PostRepositoryInterface $postRepository, UuidService $uuidService)
    {
        $post = $postRepository->find($this->postId);

        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        } 

        $data = $post->toArray();
        $data['origin_id'] = $post->getKey();
        $data['origin_owner'] = $post->user_id;
        $data['user_id'] = $this->userId;
        $data['source_uuid'] = $post->commit_uuid;
        $data['is_forked'] = 1;
        $data['status'] = 'draft';
        unset($data['commit_uuid']);
        unset($data['created_at']);
        unset($data['updated_at']);
        unset($data['id']);
        $newPost = $postRepository->store($data);

        $newRevisionData = $newPost->toArray();
        $newRevisionData['post_id'] = $newPost->getKey();
        $newRevisionData['uuid'] = $uuidService->getTimebasedUuid();
        $newRevisionData['command'] = 'fork';
        unset($newRevisionData['id']);
        unset($newRevisionData['created_at']);
        unset($newRevisionData['updated_at']);
        $revision = $revisionRepository->store($newRevisionData);
        
        $newPost->commit_uuid = $revision->uuid;
        $newPost->save();

        $dispatcher->fire(new PostWasForked($newPost));

        return $newPost;
    }
}
