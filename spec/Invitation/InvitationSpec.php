<?php

namespace spec\Saegeul\Invitation;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Bus\Dispatcher;
use Saegeul\Invitation\Jobs\InviteRole\InviteRoleCommand;
use Saegeul\Invitation\Jobs\VisitRole\VisitRoleCommand;
use Saegeul\Invitation\Jobs\JoinRole\JoinRoleCommand;
use Saegeul\Invitation\Contracts\InvitableInterface;
use Saegeul\Invitation\Models\Invitation ;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface ;


class InvitationSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Invitation\Invitation');
    }

    public function let(Dispatcher $dispatcher, InvitationRepositoryInterface $invitationRepository)
    {
        $this->beConstructedWith($dispatcher,$invitationRepository);
    }

    public function it_invite(Dispatcher $dispatcher, InvitableInterface $invitableModel, Invitation $invitation)
    {
        $roleId = 1;
        $email = 'sihyeong@artgrafii.com';

        $command = new InviteRoleCommand($roleId,$email,$invitableModel->getWrappedObject()); 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($invitation) ; 
        $this->inviteRole($roleId,$email,$invitableModel)->shouldReturnAnInstanceOf('Saegeul\Invitation\Models\Invitation');
    }

    public function it_join(Dispatcher $dispatcher, Invitation $invitation)
    {
        $invitationCode = 'RDO1aV4l0r42kxXW';
        $userId = 2;

        $command = new JoinRoleCommand($invitationCode,$userId); 
        $dispatcher->dispatchNow($command)->shouldBeCalled()->willReturn($invitation) ; 

        $this->joinRole($invitationCode,$userId)->shouldReturnAnInstanceOf('Saegeul\Invitation\Models\Invitation');
    }

    public function it_get_repository(InvitationRepositoryInterface $invitationRepository)
    {
        $this->getRepository()->shouldReturnAnInstanceOf('Saegeul\Invitation\Contracts\InvitationRepositoryInterface');
    }
}
