<?php

Route::group(array('prefix'=>'activation'),function(){
    Route::get('confirm', 'ActivationController@confirm');
    Route::get('need-to-confirm', array('as' => 'activation/need-to-confirm' , 'uses' => 'ActivationController@needToConfirm'));
    Route::post('request', 'ActivationController@request');
}); 
