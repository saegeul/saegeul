<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "s3", "rackspace"
    |
     */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
     */

    'cloud' => 'softlayer',

    'cloud-cdn' => 'softlayer-cdn',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
     */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root'   => storage_path().'/app',
        ],

        's3' => [
            'driver' => 's3',
            'key'    => 'your-key',
            'secret' => 'your-secret',
            'region' => 'your-region',
            'bucket' => 'your-bucket',
        ],

        'rackspace' => [
            'driver'    => 'rackspace',
            'username'  => 'your-username',
            'key'       => 'your-key',
            'container' => 'your-container',
            'endpoint'  => 'https://identity.api.rackspacecloud.com/v2.0/',
            'region'    => 'IAD',
            'url_type'  => 'publicURL'
        ],

        'softlayer' => [
            'driver' => 'softlayer',
            'credentials' => [
                'host' => 'https://dal05.objectstorage.softlayer.net',
                'username' => 'IBMOS370146-2:sihyeong',
                'password' => '3f2fff5fa709776f98a37de2a9010de74e3dae93f2ef51add851486b8845f645'
            ],
            'container' => 'SG',
            'cdn-prefix' => '11B21.http.dal05.cdn.softlayer.net',
        ],

        'softlayer-cdn' => [
            'driver' => 'softlayer-cdn',
            'credentials' => [
                'host' => 'https://dal05.objectstorage.softlayer.net',
                'username' => 'IBMOS370146-2:sihyeong',
                'password' => '3f2fff5fa709776f98a37de2a9010de74e3dae93f2ef51add851486b8845f645'
            ],
            'container' => 'CDN',
            'cdn-prefix' => '11B21.http.dal05.cdn.softlayer.net',
            'cdn-url' => 'https://dal05.objectstorage.softlayer.net/v1/AUTH_6f6792cb-c06b-4b51-84f8-c96410694dce/CDN/',
        ],

    ],

];
