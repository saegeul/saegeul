<?php namespace App\Http\Controllers;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Rhumsaa\Uuid\Uuid ; 
use Illuminate\Database\QueryException ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class OauthController extends Controller {

    protected $glauth;
    protected $provider;
    protected $user;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Glauth $glauth)
	{
		$this->middleware('guest');
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
	}

    public function authorize(Request $request)
    {
        $callback = url('/callback/'.$this->provider);
        $url = $this->glauth->getOauthService()->getAuthorizationUrl($this->provider, $callback);

        return redirect()->to($url);
    }

    public function callback(Request $request)
    {
        $callback = $request->url();
        try{
            $user = $this->glauth->getOauthService()->authenticate($this->provider, $callback);

            if($this->provider == 'facebook' && ($user->unique_link == null || $user->unique_link == "")){
                return redirect()->to('/user/nickname');
            }

            $name = ($user->last_name)?$user->last_name:"";
            $id = $this->provider.'Val';
            $provider = ucfirst($this->provider);
		    return view('account.success-sns-signin',compact('name','provider','id'));
        }catch (Cartalyst\SentinelSocial\AccessMissingException $e){
            return redirect()->back()->withErrors($e->getError());
        }   
    }
}
