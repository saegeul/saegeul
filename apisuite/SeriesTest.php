<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class SeriesTest extends TestCase
{
    private $prefix = 'api/v1/series';
    private $loginUser;

    private function signin()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $this->loginUser = $userRepository->find(1);

        $result = $glauth->signin($this->loginUser->email,'1q2w3e') ;
    }

    private function signinByCollaborator()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;
        $userService = $glauth->getUserService();
        $userRepository = $userService->getUserRepository();
        $user = $userRepository->find(2);

        $glauth->signin($user->email,'1q2w3e') ;

        return $user;
    }

    private function signout()
    {
        $glauth = $this->app->make('Artgrafii\Glauth\Glauth') ;

        $result = $glauth->signout() ;
    }

    public function testIndex()
    {
        #given
        $this->signin();

        #when
        $response = $this->call('GET',$this->prefix);        

        #then
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals(200, $response->status());
    }

    public function test_failed_case_to_index_no_login()
    {
        #when
        $response = $this->call('GET',$this->prefix);        

        #then
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals(401, $response->status());
    }

    public function testCoworking()
    {
        #given
        $this->signinByCollaborator();

        #when
        $response = $this->call('GET',$this->prefix.'?mode=coworking');        

        #then
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals(200, $response->status());
    }

    public function test_failed_case_to_coworking_no_login()
    {
        #when
        $response = $this->call('GET',$this->prefix.'?mode=coworking');        

        #then
        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals(401, $response->status());
    }

    public function testGetSeries()
    {
        $seriesId = 1;
        $response = $this->call('GET',$this->prefix.'/'.$seriesId);        
        $this->assertEquals(200, $response->status());
        $result = $response->getContent();
        $arr = json_decode($result, true);

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $series = $seriesService->show($seriesId);

        $this->assertEquals($arr['data']['id'], $seriesId);
        $this->assertEquals($series->getKey(), $seriesId);
        $this->assertEquals($arr['data']['title'], $series->title);
    } 

    public function test_failed_case_to_show_invalid_series_id()
    {
        $seriesId = 31;
        $response = $this->call('GET',$this->prefix.'/'.$seriesId);        
        $this->assertEquals(404, $response->status());
    }

    public function testRegisterSeries()
    {
        $this->signin();

        $data = Factory::build('Saegeul\Series\Models\Series');

        $email = 'temp1@artgrafii.com';
        $response = $this->call('POST',$this->prefix,['title' => $data['title'], 'description' => $data['description'], 'invitation_emails' => [$email]], []);        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['title'], $data['title']);
        $this->assertEquals($arr['data']['description'], $data['description']);

        $invitationRepository = $this->app->make('Saegeul\Invitation\Contracts\InvitationRepositoryInterface') ;
        $invitation = $invitationRepository->lists(1,'')->first();
        $this->assertEquals($invitation->email, $email);
    }

    public function test_failed_case_to_register_no_login()
    {
        $email = 'temp1@artgrafii.com';
        $data = Factory::build('Saegeul\Series\Models\Series');
        $response = $this->call('POST',$this->prefix,['title' => $data['title'], 'description' => $data['description'], 'invitation_emails' => [$email]], []);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_register_invalid_data()
    {
        $this->signin();

        $data = Factory::build('Saegeul\Series\Models\Series');

        $email = 'temp1@artgrafii.com';
        $response = $this->call('POST',$this->prefix,['title1' => $data['title'], 'description' => $data['description'], 'invitation_emails' => [$email]], []);        
        $this->assertEquals(422, $response->status());
    }

    public function testUpdateSeries()
    {
        $this->signin();

        $data = Factory::build('Saegeul\Series\Models\Series');

        $path = public_path().'/img/logo/saegeul_small.png';
        $fileService = $this->app->make('Artgrafii\SgDrive\Services\FileService') ;
        $sereisImage = $fileService->makeFile($path);

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $series = $seriesRepository->lists(1,'')->first();

        $response = $this->call('PUT',$this->prefix.'/'.$series->getKey(),['title' => $data['title'], 'description' => $data['description']], []);        
        $this->assertEquals(200, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['title'], $data['title']);
        $this->assertEquals($arr['data']['description'], $data['description']);
    }

    public function test_failed_case_to_update_no_login()
    {
        $email = 'temp1@artgrafii.com';
        $data = Factory::build('Saegeul\Series\Models\Series');
        $response = $this->call('PUT',$this->prefix.'/1',['title' => $data['title'], 'description' => $data['description']], []);        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_update_invalid_series_id()
    {
        $this->signin();

        $email = 'temp1@artgrafii.com';
        $data = Factory::build('Saegeul\Series\Models\Series');
        $response = $this->call('PUT',$this->prefix.'/31',['title' => $data['title'], 'description' => $data['description']], []);        
        $this->assertEquals(404, $response->status());
    }

    public function test_failed_case_to_update_invalid_data()
    {
        $this->signin();

        $email = 'temp1@artgrafii.com';
        $data = Factory::build('Saegeul\Series\Models\Series');
        $response = $this->call('PUT',$this->prefix.'/1',['title1' => $data['title'], 'description' => $data['description']], []);        
        $this->assertEquals(422, $response->status());
    }

    public function testDeleteSeries()
    {
        $this->signin();

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository();
        $series = $seriesRepository->lists(1,'')->first();

        $response = $this->call('DELETE',$this->prefix.'/'.$series->getKey());        
        $this->assertEquals(201, $response->status());

        $result = $response->getContent();
        $arr = json_decode($result, true);
        $this->assertEquals($arr['data']['id'], $series->getKey());

        $seriesService = $this->app->make('Saegeul\Series\Series') ;
        $seriesRepository = $seriesService->getRepository() ;
        $series = $seriesRepository->find($arr['data']['id']) ;
        $this->assertEmpty($series);
    }

    public function test_failed_case_to_delete_no_login()
    {
        $response = $this->call('DELETE',$this->prefix.'/1');        
        $this->assertEquals(401, $response->status());
    }

    public function test_failed_case_to_delete_invalid_series_id()
    {
        $this->signin();

        $response = $this->call('DELETE',$this->prefix.'/31');        
        $this->assertEquals(404, $response->status());
    }
}
