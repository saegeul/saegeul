<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPullRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pull_requests', function(Blueprint $table)
		{
			$table->string('processed_status')->default('waiting');
			$table->integer('target_id')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pull_requests', function(Blueprint $table)
		{
			$table->dropColumn('processed_status');
			$table->dropColumn('target_id');
	    });
    }
}
