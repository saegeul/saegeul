<?php

return [

    'signup' => [
        'name' => 'sentinel.registered',
        'handler' => 'Artgrafii\Glauth\Events\Handlers\UserEventHandler@whenUserWasRegistered'
    ],

    'request-to-confirm' => [
        'name' => 'Artgrafii\Glauth\Events\UserWasActivated',
        'handler' => 'Artgrafii\Glauth\Events\Handlers\UserEventHandler@whenRequestToConfirm'
    ],

    'reset-password' => [
        'name' => 'Artgrafii\Glauth\Events\PasswordWasReset',
        'handler' => 'Artgrafii\Glauth\Events\Handlers\UserEventHandler@whenResetPassword'
    ],
];
