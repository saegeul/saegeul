<?php

namespace spec\Saegeul\Finder;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Foundation\Application; 
use Saegeul\Finder\Services\YoutubeFinderService ;

class FinderSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Finder\Finder');
    }

    public function let(Application $app)
    {
        $this->beConstructedWith($app);
    }

    public function it_factory(Application $app, YoutubeFinderService $youtubeFinderService)
    {
        $provider = 'youtube';
        $finderName = ucfirst($provider).'FinderService';
        $finderPath = 'Saegeul\\Finder\\Services\\'.$finderName;
        $options = array();

        $app->make($finderPath,$options)->shouldBeCalled()->willReturn($youtubeFinderService); 
        $this->factory($provider,$options)->shouldReturnAnInstanceOf('Saegeul\Finder\Services\YoutubeFinderService');
    }
}
