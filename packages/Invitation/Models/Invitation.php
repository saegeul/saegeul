<?php

namespace Saegeul\Invitation\Models;

use Illuminate\Database\Eloquent\Model ;
use Saegeul\Logger\Contracts\LoggableInterface ;

class Invitation extends Model implements LoggableInterface
{
    protected $table = 'invitations' ;
    protected $guarded = array() ;

    public function invited()
    {
        return $this->morphTo();
    }

    public function scopeEmail($query, $email)
    {
        return $query->where('email',$email);
    }

    public function scopeUserId($query, $userId)
    {
        return $query->where('user_id',$userId);
    }

    public function scopeMorph($query, $morphId, $morphType)
    {
        return $query->where('invited_id',$morphId)->where('invited_type',$morphType);
    }

    public function getLoggedData()
    {
        $registerData = array();
        $registerData['invitation_id'] = $this->id;
        $registerData['series_id'] = $this->invited_id;

        return $registerData;
    }
}
