@extends("layouts.default_for_guide")

@section('contents')
@include('guides.workspace.partials.header') 
<div class="profile-content">
    <form class="mini-paper clearfix">
        <div class="field-group">
            <label>Title</label>
            <div>
                <input type="text" />
            </div>
        </div>
        <div class="field-group">
            <label>Description</label>
            <div>
                <textarea></textarea>
            </div>
        </div>
        <div class="field-group">
            <label>Cover Image</label>
            <div>
                <input type="file" />
            </div>
        </div>
        <div class="field-group">
            <label>Collaborator</label>
            <div>
                <input type="file" />
            </div>
        </div> 
        <a class="button primary right">Create</a> 
    </form>
</div> 
@stop
