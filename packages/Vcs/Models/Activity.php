<?php
namespace Saegeul\Vcs\Models;

use Saegeul\Logger\Contracts\LoggableInterface ;
use Saegeul\Notification\Contracts\NotifiableInterface ;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model 
{
    protected $table = 'activities' ;
    protected $guarded = array() ;
}
