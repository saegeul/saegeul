<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


foreach(File::allFiles(__DIR__.'/Routes') as $partial)
{
    require_once $partial->getPathname();
} 

Route::get('/signup',array('as' => 'signup' , 'uses' => 'SignController@signupForm')) ;
Route::post('/signup',array('as' => 'signup' , 'uses' => 'SignController@signup'));
Route::get('/signin',array('as' => 'signin' , 'uses' => 'SignController@signinForm')) ;
Route::post('/signin',array('as' => 'signin' , 'uses' => 'SignController@signin')) ;
Route::get('/signout',array('as' => 'signout' , 'uses' => 'SignController@signout')) ;
Route::get('/', 'SignController@signinForm');

Route::get('home', 'HomeController@index');

Route::controllers([
    //'auth' => 'Auth\AuthController',
    //'password' => 'Auth\PasswordController',
]);

// SignController
Route::get('complete-to-register', 'SignController@completeToRegister');

// PasswordController
Route::group(array('prefix'=>'password'),function(){
    Route::post('reset', 'PasswordController@reset');
    Route::get('change', 'PasswordController@changeForm');
    Route::post('change', 'PasswordController@change');
});

Route::group(array('prefix'=>'series'),function(){
    /*
    Route::get('/', 'SeriesController@index');
    Route::get('create', 'SeriesController@editForm');
    Route::post('create', 'SeriesController@store');
    Route::get('edit/{id}', 'SeriesController@editForm');
    Route::post('edit/{id}', 'SeriesController@update');
     */
    Route::get('{id}', 'SeriesController@show');
    Route::get('/{seriesId}/posts/{postId}','PostController@show') ;
});

Route::group(array('prefix'=>'invitation'),function(){
    Route::get('visit', 'InvitationController@visit');
    Route::post('join', 'InvitationController@join');
});

Route::get('authorize/facebook','Oauth\FacebookController@authorize') ;
Route::get('callback/facebook','Oauth\FacebookController@callback') ;
Route::get('authorize/instagram','Oauth\InstagramController@authorize') ;
Route::get('callback/instagram','Oauth\InstagramController@callback') ;

Route::group(array('prefix'=>'user'),function(){
    Route::get('nickname', 'UserController@updateForm');
    Route::post('nickname', 'UserController@update');
    //Route::get('confirm', 'ActivationController@confirm');
});

 

Route::get('pdf/series/{seriesId}','PdfController@getPdfForSeries') ;
Route::get('pdf/posts/{postId}','PdfController@getPdfForPost') ;

//un login
Route::group(array('prefix'=>'api/v1','namespace'=>'Api\V1'),function(){ 
    Route::get('series/{seriesId}','SeriesController@show') ;
    Route::get('series/{seriesId}/covers/{coverId}','CoverController@show') ;
    Route::get('tags','TagController@findByTagname') ;
    Route::get('tags/series','TagController@findByTagForSeries') ;
});

//login
Route::group(array('prefix'=>'api/v1','namespace'=>'Api\V1','middleware' =>'auth.glauthForRest'),function(){ 

    Route::group(array('prefix'=>'series/{seriesId}/collaborators'),function(){ 
        Route::get('waitings','CollaboratorController@getWaitings') ;
        Route::post('process','CollaboratorController@processCollaborators') ;
    });

    Route::resource('series.collaborators','CollaboratorController') ;
    Route::put('series/{seriesId}/tag','TagController@registerForSeries');
    Route::post('series/{seriesId}/follow','SubscriptionController@follow');
    Route::delete('series/{seriesId}/unfollow','SubscriptionController@unfollow');
    Route::get('following/series','SubscriptionController@getFollowing');
    Route::get('series/{seriesId}/posts','SeriesPostController@index') ;
    Route::resource('series.pullrequests','SeriesPullRequestController');
    Route::resource('series.favorites','SeriesFavoriteController') ;
    Route::resource('series.covers','CoverController') ;
    Route::resource('series/{seriesId}/logs','SeriesLogController@index') ;
    Route::resource('series','SeriesController') ;

    Route::group(array('prefix'=>'posts/{id}'),function(){
        Route::post('fork','VcsController@fork');
        Route::get('dirty','VcsController@checkDirty');
        Route::put('commit','VcsController@commit');
        Route::put('rollback','VcsController@rollback');
        Route::put('merge','VcsController@merge');
        Route::post('pullrequest','VcsController@pullRequest');
        Route::put('publish','PostController@publish');
        Route::get('pullrequest','PostPullRequestController@index');
        Route::get('logs','PostLogController@index');
    });

    Route::post('posts/import','VcsController@import');
    Route::resource('posts.favorites','PostFavoriteController') ;
    Route::get('posts/{postId}/revisions','PostRevisionController@index') ;
    Route::get('posts/{postId}/revisions/{revisionId}','PostRevisionController@show') ;
    Route::resource('posts','PostController');
    Route::resource('images','ImageController') ;
    Route::get('favorites/posts','PostFavoriteController@getByUser') ;
    Route::get('favorites/series','SeriesFavoriteController@getByUser') ;
    Route::resource('notifications','NotificationController');
    Route::get('relay','RelayController@get') ;
    Route::get('links/parse','LinkController@parse') ;

});
