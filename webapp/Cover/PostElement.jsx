import React from 'react' ;
import {DropTarget} from 'react-dnd' ; 
import ItemTypes from './ItemTypes.jsx' ; 

const dropTargetSpec = { 
    drop(props,monitor) {
        let item = monitor.getItem() ;
        props.onDrop(item) ;
    }
}

var collect = function(connect,monitor){
    return {
        connectDropTarget : connect.dropTarget() ,
        isOver : monitor.isOver()
    }
}

class PostElement extends React.Component {
    render() {

        const {isOver,connectDropTarget} = this.props ;
        const hoverStyle = {
            backgroundColor : '#000'
        }

        return connectDropTarget(
            <div className="card" style={isOver ? hoverStyle : null} >
                <div className="media-section">
                    <img src={'http://'+this.props.post.image_url} />
                </div>
                <div className="text-section">
                    <h6>{this.props.post.title}</h6>
                </div>
            </div>
        )
    }

    getData() {
        return this.props.post ;
    }
}

export default DropTarget(ItemTypes.POST,dropTargetSpec,collect)(PostElement) ;
