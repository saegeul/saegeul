<!DOCTYPE html>
<html lang="en" class="no-js" >
    <head>
        <meta charset="utf-8">
        <title>Saegeul</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="/sgpack/flat-ui/images/favicon.ico">
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    </head>

<body style="background-color:#fff !important;"> 
<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap workspace-wrap" style="padding-top:60px;background:#fff;"> 
      <div>
          <nav class="top-bar fixed" data-topbar role="navigation" style="border-bottom:none;"> 
              <ul class="title-area">
                  <li class="name">
                  <h1><a class="left-off-canvas-toggle" href="#" ><span class="fi-menu" title="menu" aria-hidden="true"></span></a></h1>
                  </li>
                  <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
              </ul> 
                <div class="container mini-container">
                    <p style="text-align:center;padding-top:15px;">{{ $series['title'] }}</p>
                </div>
          </nav>
      </div>
        <aside class="left-off-canvas-menu"> 
            <div class="avatar-area">
                <div>
                    Table
                    <div style="position:relative"><a style="color:#888;font-size: small;position: absolute;right: 12px;bottom: -18px;" href="/series/{{$series['id']}}">return Cover</a></div>
                </div>
            </div> 
            <div class="sidebar-nav">
                <ul>
                @foreach ($posts as $post)
                    <li><a href="/series/{{$series['id']}}/posts/{{$post['id']}}">{{$post['title']}}</a></li>
                @endforeach
                </ul>
            </div> 
        </aside>
        <div class="content-wrapper">
            @yield('contents')
        </div>
        <!-- main content goes here -->

        <!-- close the off-canvas menu -->
        <a class="exit-off-canvas"></a>

    </div>
</div> 

<script src="/bower_components/foundation/js/vendor/jquery.js"></script> 
<script src="/bower_components/foundation/js/foundation.min.js"></script>
<script src="/webapp/build/bundle.js"></script> 

@yield('scripts')

<script>
$(document).foundation();
</script>

</body>
</html>
