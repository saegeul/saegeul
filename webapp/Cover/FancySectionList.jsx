import React from 'react' ;
import FancySection from './FancySection.jsx';

class FancySectionList extends React.Component {

    constructor(props) {
        super(props) ; 
    }

    render() {
        return (
            <div>
                {this.props.sectionList.map((section,index) =>{ 
                    console.log(section) ;
                    return (
                        <div key={index}>
                            <FancySection ref={'section'+index} section={section} ></FancySection>
                            <a onClick={this.props.addSection}><i className="material-icons">add_circle</i></a>
                            <a onClick={ () => { this.props.removeSection(index)}}><i className="material-icons">remove_circle</i></a>
                        </div>
                    )
                })}
            </div>
        );
    } 

    getData() {
        var data = []  ; 
        this.props.sectionList.map((section,index) => { 
            data.push({
                'rows' :this.refs['section'+index].getData() 
            }) 
        }) ; 

        return data ;
    }

}

export default FancySectionList
