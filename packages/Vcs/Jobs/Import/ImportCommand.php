<?php
namespace Saegeul\Vcs\Jobs\Import;

use Illuminate\Contracts\Bus\SelfHandling ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface ;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasImported;

class ImportCommand implements SelfHandling
{

    public $pullRequestId;
    public $userId;

    public function __construct($pullRequestId, $userId)
    {
        $this->pullRequestId = $pullRequestId;
        $this->userId = $userId;
    }

    public function handle(
        Dispatcher $dispatcher, 
        PullRequestRepositoryInterface $pullRequestRepository, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository, 
        UuidService $uuidService
    ){ 
        $pullRequest = $pullRequestRepository->find($this->pullRequestId);
        if(!$pullRequest){
            throw New ModelNotFoundException('Not found pull-request id');
        }

        $post = $postRepository->find($pullRequest->post_id);
        if(!$post){
            throw New ModelNotFoundException('Not found post id');
        }

        $newPostData = array();
        $newPostData['origin_id'] = $post->getKey();
        $newPostData['origin_owner'] = $post->user_id;
        $newPostData['title'] = $post->title;
        $newPostData['description'] = $post->description;
        $newPostData['content'] = $post->content;
        $newPostData['html'] = $post->html;
        $newPostData['source_uuid'] = $post->commit_uuid;
        $newPostData['series_id'] = $post->series_id;
        $newPostData['user_id'] = $this->userId;
        $newPost = $postRepository->store($newPostData);

        $newRevisionData = $newPost->toArray();
        unset($newRevisionData['id']);
        unset($newRevisionData['created_at']);
        unset($newRevisionData['updated_at']);
        $newRevisionData['uuid'] = $uuidService->getTimebasedUuid();
        $newRevisionData['post_id'] = $newPost->getKey();
        $newRevisionData['command'] = 'import';
        $revisionRepository->store($newRevisionData);

        $dispatcher->fire(new PostWasImported($newPost,$pullRequest));
        return $newPost;
    }
}
