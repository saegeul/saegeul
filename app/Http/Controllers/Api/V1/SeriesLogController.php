<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Series\Series ;
use Saegeul\Logger\Logger;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\LogTransformer;

class SeriesLogController extends ApiController 
{
    protected $seriesService;
    protected $loggerService;
    protected $glauth;
    protected $user;
    protected $fractal;

    public function __construct(
        Series $seriesService, 
        Logger $loggerService,
        Glauth $glauth, 
        Manager $fractal
    )
	{
        $this->seriesService = $seriesService ;
        $this->loggerService = $loggerService;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->fractal = $fractal;
	}

    public function index(Request $request, $seriesId)
    {
        try{
            $series = $this->seriesService->show($seriesId);
            if(!$series->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $page = $request->input('page')?$request->input('page'):1;
            $logs = $this->loggerService->findBySeries($seriesId,$page);

            $resource = new Fractal\Resource\Collection($logs, new LogTransformer);
            $resource->setPaginator(new IlluminatePaginatorAdapter($logs));
            $result = $this->fractal->createData($resource)->toJson();

            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
