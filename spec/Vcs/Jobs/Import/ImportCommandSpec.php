<?php

namespace spec\Saegeul\Vcs\Jobs\Import;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface ;
use Saegeul\Vcs\Models\Revision as RevisionModel ;
use Saegeul\Vcs\Models\PullRequest as PullRequestModel ;
use Saegeul\Post\Models\Post as PostModel ;
use Saegeul\Series\Models\Series as SeriesModel ;
use Artgrafii\Uuid\UuidService;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasImported;

class ImportCommandSpec extends ObjectBehavior
{
    public $pullRequestId;
    protected $userId;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Vcs\Jobs\Import\ImportCommand');
    }

    public function let()
    {
        $this->pullRequestId = 1;
        $this->userId = 2;
        $this->beConstructedWith($this->pullRequestId,$this->userId);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        PullRequestRepositoryInterface $pullRequestRepository, 
        RevisionRepositoryInterface $revisionRepository, 
        PostRepositoryInterface $postRepository, 
        RevisionModel $revision,
        UuidService $uuidService,
        PostModel $post,
        PostModel $newPost,
        RevisionModel $newRevision,
        PullRequestModel $pullRequest
    )
    {
        $data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
            'user_id' => 1,
            'series_id' => 1
        ];

        $pullRequestRepository->find($this->pullRequestId)->shouldBeCalled()->willReturn($pullRequest);
        $pullRequest->getAttribute('post_id')->willReturn(2);
        $postRepository->find(2)->shouldBeCalled()->willReturn($post);

        $newData = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
            'series_id' => 1
        ];
        $originId = 1;
        $post->getKey()->shouldBeCalled()->willReturn($originId);
        $newData['origin_id'] = $originId;
        $originOwner = 1;
        $post->getAttribute('user_id')->willReturn($originOwner);
        $newData['origin_owner'] = $originOwner;
        $post->getAttribute('title')->willReturn($newData['title']);
        $post->getAttribute('description')->willReturn($newData['description']);
        $post->getAttribute('content')->willReturn($newData['content']);
        $commit_uuid = '11112-22';
        $newData['source_uuid'] = $commit_uuid;
        $post->getAttribute('commit_uuid')->willReturn($commit_uuid);
        $post->getAttribute('series_id')->willReturn($newData['series_id']);
        $newData['user_id'] = $this->userId;
        $postRepository->store($newData)->shouldBeCalled()->willReturn($newPost);

        $newPostId = 3;
        $newPost->toArray()->shouldBeCalled()->willReturn($data);
        $uuid = '333';
        $uuidService->getTimebasedUuid()->shouldBeCalled()->willReturn($uuid);
        $data['uuid'] = $uuid;
        $newPost->getKey()->shouldBeCalled()->willReturn($newPostId);
        $data['post_id'] = $newPostId;
        $data['command'] = 'import';
        $revisionRepository->store($data)->shouldBeCalled()->willReturn($newRevision);

        $dispatcher->fire(new PostWasImported($newPost->getWrappedObject(),$pullRequest->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$pullRequestRepository,$revisionRepository,$postRepository,$uuidService)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
