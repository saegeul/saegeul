<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::table('users', function(Blueprint $table)
		{
			$table->string('unique_link')->unique();
			$table->string('uuid');
			$table->string('profile_image');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    Schema::table('users', function(Blueprint $table)
		{
            $table->dropColumn('unique_link');
            $table->dropColumn('uuid');
			$table->dropColumn('profile_image');
	    });
	}

}
