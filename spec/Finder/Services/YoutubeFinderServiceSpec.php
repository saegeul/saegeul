<?php

namespace spec\Saegeul\Finder\Services;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class YoutubeFinderServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Finder\Services\YoutubeFinderService');
    }

    public function let(\Google_Service_YouTube_Search_Resource $search)
    {
        $this->beConstructedWith($search);
    }

    public function it_find(
        \Google_Service_YouTube $service, 
        \Google_Service_YouTube_Search_Resource $search
         
    )
    {
        $keyword = 'chrismas';
        $page = 1;
        $searchOptions = [
            'q' => $keyword,
            'maxResults' => 20
        ];

        $response = new \Google_Service_YouTube_SearchListResponse ;

        $search->listSearch('id,snippet',$searchOptions)->shouldBeCalled()->willReturn($response);
        
        $this->find($keyword,$page)->shouldBeArray();
    }
}
