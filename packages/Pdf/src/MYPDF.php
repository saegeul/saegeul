<?php
namespace Saegeul\Pdf;

class MYPDF extends \TCPDF {

    //Page header
    public function Header() {
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetFont('helvetica', '', 8);
        // Page number
		$this->Cell(0, 5, 'By SAEGEUL', 0, false, 'C');

        $w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
		if (empty($this->pagegroups)) {
			$pagenumtxt = $w_page.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
		} else {
			$pagenumtxt = $w_page.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
		}
        $this->SetFont('helvetica', 'I', 8);
        if ($this->getRTL()) {
			$this->SetX($this->original_rMargin);
			$this->Cell(0, 5, $pagenumtxt, 'T', 0, 'L');
		} else {
			$this->SetX($this->original_lMargin);
			$this->Cell(0, 5, $this->getAliasRightShift().$pagenumtxt, 'T', 0, 'R');
		}
    }
}
