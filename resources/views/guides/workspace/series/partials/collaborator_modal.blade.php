<div id="collaborator-modal" class="reveal-modal modal small collaborator-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="modal-header">
        <h2>Collaborators</h2>
    </div>
    <div class="modal-body">
        <ul class="collaborators">
            <li class="collaborator">
            <div class="row small-uncollapse medium-uncollapse large-uncollapse">
                <div class="columns small-9">
                    <div class="avatar-wrapper">
                        <div class="avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/felipenogs/128.jpg" />
                        </div>
                        <div class="description"> 
                            <span class="email">jaehee@artgrafii.com</span>
                            <small>Accepted in 3,July,2015</small> 
                        </div>
                    </div>
                </div>
                <div class="columns small-3">
                    <a class="button right">X</a>
                </div>
            </div>
            </li>

            <li class="collaborator removed">
            <div class="row small-uncollapse">
                <div class="columns small-9">
                    <div class="avatar-wrapper">
                        <div class="avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg" />
                        </div>
                        <div class="description"> 
                            <span class="email">sihyeong@artgrafii.com</span>
                            <small>Accepted in 3,July,2015</small> 
                        </div>
                    </div>
                </div>
                <div class="columns small-3">
                    <a class="button right">X</a>
                </div>
            </div>
            </li> 

            <li class="collaborator">
            <div class="row small-uncollapse">
                <div class="columns small-9">
                    <div class="avatar-wrapper">
                        <div class="avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg" />
                        </div>
                        <div class="description"> 
                            <span class="email">aran@artgrafii.com</span>
                            <small>Requested in 12,Aug,2105 or <a>Re-send a mail</a></small> 
                        </div>
                    </div>
                </div>
                <div class="columns small-3">
                    <a class="button right">X</a>
                </div>
            </div>
            </li> 
            
            <li class="collaborator">
            <div class="row small-uncollapse">
                <div class="columns small-9">
                    <div class="avatar-wrapper">
                        <div class="avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg" />
                        </div>
                        <div class="description"> 
                            <span class="email">jaehee@saegeul.com</span>
                            <small>NEW COLLABORATOR</small> 
                        </div>
                    </div>
                </div>
                <div class="columns small-3">
                    <a class="button right">X</a>
                </div>
            </div>
            </li> 


        </ul>
        <div class="row">
            <div class="large-12 columns">
                <div class="row collapse">
                    <div class="small-8 medium-9 large-10 columns">
                        <input type="text" placeholder="email">
                    </div>
                    <div class="small-4 medium-3 large-2 columns">
                        <a href="#" class="button postfix"><span class="fi-plus" title="plus" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <div class="right">
            <a class="button">CANCEL</a>
            <a class="button">SAVE</a>
        </div>
    </div>
</div>
