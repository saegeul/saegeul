<?php namespace App\Http\Controllers;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\QueryException ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class UserController extends Controller {

    protected $glauth ;
    protected $user;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Glauth $glauth)
	{
		$this->middleware('guest');
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
	}

    public function updateForm()
    {
        $returnUrl = $request->input('return_url')?$request->input('return_url'):"";
        $user = $this->user;
        if(!$user){
            return redirect()->to('/signin?return_url='.$returnUrl);
        }

        return view('user.nickname',compact('user'));
    } 

    public function update(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'id' => 'required',
                'nickname' => 'required|unique:users,unique_link',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $userId = $request->input('id');
            $nickname = $request->input('nickname');
            $data = [
                'unique_link' => $nickname 
            ];

            $userService = $this->glauth->getUserService();
            $user = $userService->update($userId,$data);

            return redirect()->to('/');
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
