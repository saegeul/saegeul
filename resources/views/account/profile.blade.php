@extends("layouts.default_v2")

@section('contents')
@include('account.profile-header') 
<div class="profile-content">
    <div class="row">
        <div class="small-12 large-6 small-centered columns">
            <form class="mini-paper">
                <div class="row">
                    <div class="large-12 columns field-group">
                        <label><span class="fi-link-intact" title="link intact" aria-hidden="true"></span>&nbsp;Your URL</label>
                        <div>
                            saegeul.com/{{ '@'.$user->unique_link }}
                        </div>
                    </div>
                    <div class="large-12 columns field-group">
                        <label><span class="fi-envelope-closed" title="envelope closed" aria-hidden="true"></span>&nbsp;Email</label>
                        <div>
                            {{ $user->email }}
                        </div>
                    </div>
                </div> 
                <hr/>
                <div class="row">
                    <div class="large-12 columns field-group">
                        <label><span class="fi-social-facebook" title="social facebook" aria-hidden="true"></span>&nbsp;Facebook </label>
                        <div class="row"> 
                            <div class="large-9 columns">
                                <p class="description">You can connect your account to Facebook<br/>Just go facebook.</p>
                            </div>
                            <div class="large-3 columns">
                                <div class="switch large round right">
                                    <input class="sg-social-box" data-provider="facebook" id="facebookVal" name="facebookVal" type="checkbox">
                                    <label for="facebookVal"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="large-12 columns field-group">
                        <label><span class="fi-social-instagram" title="social instagram" aria-hidden="true"></span>&nbsp;Instagram </label>
                        <div class="row"> 
                            <div class="large-9 columns">
                                <p class="description">You can connect your account to Instagram<br/>Just go Instagram.</p>
                            </div>
                            <div class="large-3 columns">
                                <div class="switch large round right">
                                    <input class="sg-social-box" data-provider="instagram" id="instagramVal" name="instagramVal" type="checkbox">
                                    <label for="instagramVal"></label>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div> 
            </form>
        </div>
    </div>
</div>
<div id="myModal" class="reveal-modal tiny callback-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="indicator indicator-success hide"> 
        <span class="fi-check-thin" title="check thin" aria-hidden="true"></span>
    </div> 

    <br/>
    <br/>
    <div class="hide sg-info">
        <p class="lead sg-social-user-name"></p>
        <p class="sg-social-info"></p>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
</div>
@stop

@section('scripts')
<script>
jQuery(function($){
    var that = {};
    var _token = '{{ csrf_token() }}';

    that.init = function () { 
        var links = '{!! $links !!}' ;
        links = jQuery.parseJSON(links) ;

        jQuery.each(links, function(i, val){
            var checkBoxId = val.provider + 'Val';
            $("input:checkbox[id='" + checkBoxId + "']").prop('checked',true);
        });
    };

    $.oauthpopup = function(options, provider)
    {
        options.windowName = options.windowName || 'ConnectWithOAuth'; // should not include space for IE
        options.windowOptions = options.windowOptions || 'location=0,status=0,width=800,height=400';

        var that = this;
        that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
        $(that._oauthWindow).bind("unload", function() { 
            $('#myModal').foundation('reveal', 'open');
            $.ajax({
                url:'/api/v1/account/sns/get/'+provider,
                method:'get',
                success:function(data){
                    $('.indicator-success').removeClass("hide");
                    $('.sg-info').removeClass("hide");
                    $('.sg-social-user-name').text('Hello! '+((data.user.last_name == null)?"":data.user.last_name));
                    $('.sg-social-info').text('Your account connect with '+provider[0].toUpperCase() + provider.slice(1)); 

                    var checkBoxId = provider + 'Val';
                    $("input:checkbox[id='" + checkBoxId + "']").prop('checked',true);
                },
                error:function(response){
                    console.log(response.statusText);
                }
            });
        });
    };

    that.socialLogout = function (provider) { 
        $.ajax({
            url:'/api/v1/account/sns/'+provider+'/logout',
            method:'post',
            data: { '_token': _token },
            success:function(data){
                if(!data.is_delete){ 
                    var checkBoxId = provider + 'Val';
                    $("input:checkbox[id='" + checkBoxId + "']").prop('checked',true);
                }
            },
            error:function(response){
                console.log(response.statusText);
            }
        });
    };

    that.init();

    $(".sg-social-box").click(function(){
        var chk = $(this).is(":checked");
        var provider = $(this).data("provider");
        if(!chk){
            that.socialLogout(provider);
        }else{
            event.preventDefault();
            $.oauthpopup({
                path: '/authorize/'+provider
            },provider);
        }
    });
});
</script>
@stop
