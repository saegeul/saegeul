<?php

Route::group(array('prefix'=>'password'),function(){
    Route::post('reset', 'PasswordController@reset');
    Route::get('change', 'PasswordController@changeForm');
    Route::post('change', 'PasswordController@change');
});

Route::get('/forgot-password',array('as' => 'forgot-password' , 'uses' => 'PasswordController@forgotPassword')) ;
