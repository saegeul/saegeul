import Nprogress from 'nprogress';

export default class FancyProgress {

    start() {
        Nprogress.configure({easing : 'ease' , speed : 200 }) ;
        Nprogress.configure({ trickleRate: 0.05, trickleSpeed: 100 });
        Nprogress.configure({ showSpinner : false });
        Nprogress.start() ;
    } 

    done() {
        Nprogress.done() ;
    }
}
