<?php

namespace spec\Saegeul\Vcs\Jobs\Merge;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Post\Contracts\PostRepositoryInterface ;
use Saegeul\Post\Models\Post as PostModel ;
use Artgrafii\Uuid\UuidService;
use Saegeul\Vcs\Models\Revision as RevisionModel ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Vcs\Events\PostWasMerged;

class MergeCommandSpec extends ObjectBehavior
{
    protected $toPost;
    protected $fromPost;
    protected $data;


    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Vcs\Jobs\Merge\MergeCommand');
    }

    public function let(PostModel $toPost, PostModel $fromPost)
    {
        $this->data = [
            'title' => 'Laravel5 Installation',
            'description' => "Server Requirements lalalalala",
            'content' => "The Laravel framework has a few system requirements. Of course, all of these requirements are satisfied by the Laravel Homestead virtual machine",
        ];
        $this->toPost = $toPost;
        $this->fromPost = $fromPost;
        $this->beConstructedWith($this->toPost,$this->fromPost,$this->data);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository,
        PostRepositoryInterface $postRepository,
        UuidService $uuidService,
        PostModel $toPost, 
        PostModel $fromPost,
        RevisionModel $newRevision
    )
    {
        $uuid = '1111';
        $this->fromPost->getAttribute('commit_uuid')->willReturn($uuid);
        $this->data['source_uuid'] = $uuid;
        $this->toPost->getKey()->shouldBeCalled()->willReturn(1);
        $postRepository->update(1,$this->data)->shouldBeCalled()->willReturn($this->toPost);

        $this->toPost->toArray()->shouldBeCalled()->willReturn($this->data);
        $uuid2 = '2222';
        $uuidService->getTimebasedUuid()->shouldBeCalled()->willReturn($uuid2);
        $this->data['uuid'] = $uuid2;
        $this->data['command'] = 'merge';
        $this->toPost->getKey()->shouldBeCalled()->willReturn(1);
        $this->data['post_id'] = 1;
        $revisionRepository->store($this->data)->shouldBeCalled()->willReturn($newRevision);
        $newRevision->getAttribute('uuid')->willReturn($uuid2);
        $this->toPost->setAttribute('commit_uuid',$uuid2)->shouldBeCalled();
        $this->toPost->save()->shouldBeCalled();

        $dispatcher->fire(new PostWasMerged($this->toPost->getWrappedObject()))->shouldBeCalled();
        
        $this->handle($dispatcher,$revisionRepository,$postRepository,$uuidService)->shouldReturnAnInstanceOf('Saegeul\Post\Models\Post');
    }
}
