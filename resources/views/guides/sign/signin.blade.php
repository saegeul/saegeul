@extends("layouts.default_for_guide")

@section('contents')
<div class="signup-wrapper">
    <div class="logo-wrapper" >
        <img src="/img/logo/saegeul_wordmark_lg.png" />
    </div>

    <form class="mini-paper">
        <div class="field-group">
            <label>Email</label>
            <div>
                <input type="text" />
            </div>
        </div>
        <div class="field-group">
            <label>Password</label>
            <div>
                <input type="password" />
            </div>
        </div> 
        <a class="button expand "><i></i>Sign in</a>
        <div class="divider divider-or"></div> 

        <a class="button expand facebook"><span class="fi-social-facebook" title="social facebook" aria-hidden="true"></span>&nbsp;Facebook</a>
    </form>
    <div class="mini-paper no-padding transparent clearfix" style="margin-top:10px;">
        <div class="left">
                <a class="mini-anchor" href="{{route('guide.forgot-password')}}">Forgot password?</a>
        </div>
        <div class="right">
                <a class="mini-anchor" href="{{route('guide.signup')}}">Do you want to join on saegeul?</a>
        </div> 
    </div>
</div>
@stop
