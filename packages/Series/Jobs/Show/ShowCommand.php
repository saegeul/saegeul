<?php

namespace Saegeul\Series\Jobs\Show;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Series\Contracts\SeriesRepositoryInterface ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class ShowCommand implements SelfHandling
{

    public $seriesId ;

    public function __construct($seriesId)
    {
        $this->seriesId = $seriesId ;
    }

    public function handle(SeriesRepositoryInterface $seriesRepository)
    {
        $series = $seriesRepository->find($this->seriesId);
        if(!$series){
            throw New ModelNotFoundException('Not found series id');
        }

        return $series ;
    }
}
