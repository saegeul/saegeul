<?php namespace App\Http\Controllers;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\QueryException ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Exceptions\FailedToResetPasswordException ;
use Artgrafii\Glauth\Exceptions\FailedToChangePasswordException ;
use Artgrafii\Glauth\Services\HashCodeService ;
use Artgrafii\Glauth\Exceptions\InvalidHashCodeException ;

class PasswordController extends Controller {

    protected $glauth ;
    protected $hashCodeService ;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Glauth $glauth, HashCodeService $hashCodeService)
	{
		$this->middleware('guest');
        $this->glauth = $glauth ;
        $this->hashCodeService = $hashCodeService ;
	}

    public function forgotPassword()
    {
        return view('password.forgot-password');
    }

    public function reset(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $email = $request->input('email');

            $passwordService = $this->glauth->getPasswordService();
            $user = $passwordService->resetPassword($email);

            return view('password.resetPasswordComplete',compact('user'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (FailedToResetPasswordException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }

    public function changeForm(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'hash_code' => 'required',
                'code' => 'required',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $hashCode = $request->input('hash_code');
            $code = $request->input('code');
            $userId = $this->hashCodeService->decode($hashCode);

            return view('password.changeForm',compact('hashCode','code'));
        } catch (InvalidHashCodeException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }

    public function change(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'hash_code' => 'required',
                'code' => 'required',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $hashCode = $request->input('hash_code');
            $code = $request->input('code');
            $password = $request->input('password');

            $passwordService = $this->glauth->getPasswordService();
            $user = $passwordService->changePassword($hashCode,$code,$password);

            return redirect()->to('signin');
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (FailedToResetPasswordException $e) {
            return redirect()->back()->withErrors($e->getError());
        } catch (FailedToChangePasswordException $e) {
            return redirect()->back()->withErrors($e->getError());
        }
    }
}
