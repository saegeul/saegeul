<?php namespace App\Http\Controllers\Api\V1\Finder;

use App\Http\Controllers\Api\V1\FinderController;
use Illuminate\Http\Request ;
use Saegeul\Finder\Finder;
use Artgrafii\Glauth\Glauth ;

class TwitterController extends FinderController
{
    protected $provider = 'twitter';
}
