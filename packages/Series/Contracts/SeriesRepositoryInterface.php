<?php

namespace Saegeul\Series\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface SeriesRepositoryInterface extends RepositoryInterface 
{
}
