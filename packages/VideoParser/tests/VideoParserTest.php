<?php

use Laracasts\TestDummy\Factory;

class VideoParserTest extends TestCase 
{
    public function testParse()
    {
        $url = 'https://www.youtube.com/watch?v=QmwEHmFv3zs';
        //$url = 'https://youtu.be/tYI5CriE6XU';
        $videoParserService = $this->app->make('Saegeul\VideoParser\VideoParser') ;
        $result = $videoParserService->parse($url);
        $this->assertEquals($result->imageUrl,'https//img.youtube.com/vi/QmwEHmFv3zs/default.jpg');
    }

    public function testParseForVideo()
    {
        $url ='https://vimeo.com/channels/lexusshortfilms/108968882';
        $videoParserService = $this->app->make('Saegeul\VideoParser\VideoParser') ;
        $result = $videoParserService->parse($url);
        $this->assertEquals($result->imageUrl,'http://i.vimeocdn.com/video/494828514_200x150.jpg');
    }

    /**
     * @expectedException Saegeul\VideoParser\Exceptions\InvalidHostException
     **/
    public function test_failed_case_to_parse_invalid_host()
    {
        $url = 'https://www.nate.com/watch?v=QmwEHmFv3zs';
        //$url = 'https://youtu.be/tYI5CriE6XU';
        $videoParserService = $this->app->make('Saegeul\VideoParser\VideoParser') ;
        $result = $videoParserService->parse($url);
    }

    /**
     * @expectedException Saegeul\VideoParser\Exceptions\InvalidHostException
     **/
    public function test_failed_case_to_parse_invalid_id()
    {
        $url = 'https://youtu.be/tYI5CriE61221';
        $videoParserService = $this->app->make('Saegeul\VideoParser\VideoParser') ;
        $videoParserService->parse($url);
    }
}
