<?php

namespace Saegeul\Favorite\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model 
{
    protected $table = 'favorites' ;
    protected $guarded = array() ;

    public function favorite()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }

    public function scopeUserId($query,$userId)
    {
        return $query->where('user_id',$userId) ;
    }

    public function scopeFavoriteId($query,$favoriteId)
    {
        return $query->where('favorite_id',$favoriteId) ;
    }

    public function scopeFavoriteType($query,$favoriteType)
    {
        return $query->where('favorite_type',$favoriteType) ;
    }

    public function countFavorite()
    {
        $model = $this->getModel();
        $model = $model->where('favorite_id',$this->favorite_id);
        $model = $model->where('favorite_type',$this->favorite_type);
        return $model->count();
    }
}
