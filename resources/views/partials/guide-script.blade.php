<script src="/bower_components/jQuery.hotkeys/jquery.hotkeys.js"></script>
<script>
jQuery(function($){
    var guideBarIsOpened = false ; 
    $('#guide-bar').hide() ;
    $(document).bind('keydown','Ctrl+h', function(){ 
        if(guideBarIsOpened){
            guideBarIsOpened = false ;
            $('#guide-bar').hide() ;
        }else {
            guideBarIsOpened = true ;
            $('#guide-bar').show() ;
        }
    });
}); 
</script>
