<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\ShortenUrl\ShortenUrl;
use Saegeul\ShortenUrl\Contracts\ShortenUrlInterface;

class ShortenUrlController extends ApiController 
{
    public $service;

	public function __construct(ShortenUrlInterface $service)
    {
        $this->service = $service;
    }

    public function store(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'url' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondUnauthorized() ;
            }

            $url= $request->input('url');
            $shorten = $this->service->register($url);

            return $this->respond($shorten);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }  
}
