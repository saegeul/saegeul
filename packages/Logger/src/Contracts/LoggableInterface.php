<?php
namespace Saegeul\Logger\Contracts ; 

interface LoggableInterface
{
    public function getLoggedData() ;
}
