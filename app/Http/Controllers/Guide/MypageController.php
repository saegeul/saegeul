<?php
namespace App\Http\Controllers\Guide ;

use App\Http\Controllers\Controller ;

class MypageController extends Controller {

    public function index()
    {
        return view('guides.mypage.index') ;
    }
}
