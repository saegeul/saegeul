<?php
namespace App\ViewComposers ;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Contracts\View\View;

class ProfileComposer
{
    protected $glauth; 

    public function __construct(Glauth $glauth)
    {
        $this->glauth = $glauth ;
    }

    public function compose(View $view)
    {
        if($this->glauth->check()){
            $view->with('user', $this->glauth->check());
        }
    }
}
