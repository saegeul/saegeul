<?php

namespace Saegeul\Vcs\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class PullRequestRepository extends IlluminateRepository implements PullRequestRepositoryInterface
{
    protected $model = 'Saegeul\Vcs\Models\PullRequest' ;
    
    public function lists($page, $keyword=null)
    {
        $model = $this->modelFactory->create($this->model) ;
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->with('user')->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function getByPostId($postId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('post_id', $postId);
        $model = $model->where('processed_status', 'waiting');

        return $model->first();
    }

    public function getByTargetId($postId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('target_id', $postId);
        $model = $model->where('processed_status', 'waiting');

        return $model->first();
    }

    public function findBySeries($seriesId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('series_id', $seriesId);
        $model = $model->where('processed_status', 'waiting');
        $model = $model->with('user','post');

        return $model = $model->get();
    }

    public function findByPost($postId)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('target_id', $postId);
        $model = $model->where('processed_status', 'waiting');
        $model = $model->with('user','post');

        return $model = $model->get();
    }
}
