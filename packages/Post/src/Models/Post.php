<?php

namespace Saegeul\Post\Models;

use Illuminate\Database\Eloquent\Model;
use Saegeul\Favorite\Contracts\FavoriteInterface;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Saegeul\Logger\Contracts\LoggableInterface ;
use Saegeul\Notification\Contracts\NotifiableInterface ;
use Cartalyst\Tags\TaggableTrait;
use Cartalyst\Tags\TaggableInterface;

class Post extends Model implements FavoriteInterface, LoggableInterface, NotifiableInterface, TaggableInterface
{
    use TaggableTrait;

    protected $table = 'posts' ;
    protected $guarded = array() ;

    public function isCreatableByUser($user)
    {
        if($this->user_id == $user->getKey()){
            return true;
        }
        return false;
    }

    public function user()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User');
    }

    public function owner()
    {
        return $this->belongsTo('Artgrafii\Glauth\Models\User','origin_owner');
    }

    public function series()
    {
        return $this->belongsTo('Saegeul\Series\Models\Series','series_id');
    }

    public function favorites($page=1)
    {
        $limit = 10;

        $model = $this->morphMany('Saegeul\Favorite\Models\Favorite', 'favorite')->getModel();
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$limit)->take($limit);
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$limit,$page) ;
    }

    public function revisions()
    {
        return $this->hasMany('Saegeul\Vcs\Models\Revision') ;
    }

    public function latestRevision()
    {
        return $this->revisions()->orderBy('id','desc')->first() ;
    }

    public function getClassName()
    {
        return $this->getMorphClass();
    }

    public function updateFavorite($favorite)
    {
        $this->favorite = $favorite;
        $this->save();

        return $this;
    }

    public function getLoggedData()
    {
        $registerData = array();
        $registerData['post_id'] = $this->id;
        $registerData['series_id'] = $this->series_id;
        $registerData['commit_uuid'] = $this->commit_uuid;

        return $registerData;
    }

    public function getNotificationData() 
    {
        $registerData = array();
        $registerData['post_id'] = $this->getKey();
        $registerData['series_id'] = $this->series_id;
        $registerData['commit_uuid'] = $this->commit_uuid;

        return $registerData;
    }
}
