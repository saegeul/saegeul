<?php
namespace App\Http\Controllers\Guide\Series ;

use App\Http\Controllers\Controller ;

class PostController extends Controller { 

    public function show($seriesId, $postId)
    {
        return view('guides.post.show') ;
    } 
}
