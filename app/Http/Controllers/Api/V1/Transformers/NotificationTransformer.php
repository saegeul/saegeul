<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Notification\Models\Notification;
use Artgrafii\Glauth\Models\User;

class NotificationTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user'];

    public function transform(Notification $notification)
    {
        return [
            'id' => (int) $notification->id,
            'command' => $notification->command,
            'pull_request_id' => $notification->pull_request_id,
            'series_id' => $notification->series_id,
            'post_id' => $notification->post_id,
            'is_confirmed' => $notification->is_confirmed,
            'notified_at' => $notification->notified_at,
            'created_at' => $notification->created_at,
            'updated_at' => $notification->updated_at
        ];
    }

    public function includeUser(Notification $notification)
    {
        $user = $notification->user;

        return $this->item($user, new UserTransformer);
    }
}
