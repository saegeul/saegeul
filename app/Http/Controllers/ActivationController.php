<?php namespace App\Http\Controllers;

use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\QueryException ;
use Artgrafii\Glauth\Exceptions\FailedToConfirmUserException ;
use Artgrafii\Glauth\Exceptions\FailedToCreateActivationException ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class ActivationController extends Controller {

    protected $glauth ;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Glauth $glauth)
	{
		$this->middleware('guest');
        $this->glauth = $glauth ;
	}

    public function confirm(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'hash_code' => 'required',
                'code' => 'required',
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $hashCode = $request->input('hash_code');
            $code = $request->input('code');

            $activationService = $this->glauth->getActivationService();
            $user = $activationService->confirm($hashCode,$code);

            return view('activation.complete',compact('user'));
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (FailedToConfirmUserException $e) {
            return redirect()->to('signin')->withErrors($e->getError());
        }
    }

    public function needToConfirm(Request $request)
    {
        return view('activation.activation-form');
    }

    public function request(Request $request)
    {
        try {
            $v = \Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }

            $email = $request->input('email');

            $activationService = $this->glauth->getActivationService();
            $user = $activationService->requestToConfirm($email);

            return redirect()->to('/singin');
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        } catch (FailedToCreateActivationException $e) {
            return redirect()->to('activation/need-to-confirm')->withErrors($e->getError());
        }
    }
}
