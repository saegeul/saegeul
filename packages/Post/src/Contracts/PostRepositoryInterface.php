<?php

namespace Saegeul\Post\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface PostRepositoryInterface extends RepositoryInterface 
{
}
