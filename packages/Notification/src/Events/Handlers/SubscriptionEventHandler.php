<?php
namespace Saegeul\Notification\Events\Handlers ;

use Saegeul\Notification\Events\Handlers\EventHandler ;
use Saegeul\Notification\Notification ;
use Illuminate\Mail\Mailer ;
use Saegeul\Subscription\Events\SubscriptionWasRegistered ;

class SubscriptionEventHandler extends EventHandler
{
    public function whenSubscriptionWasRegistered(SubscriptionWasRegistered $event)
    {
        $subscription = $event->subscription;

        $series = $subscription->series;
        $sender = $subscription->user;
        $receiver = $series->user;

        $notification = $this->handle($subscription,__FUNCTION__) ;
        $notification->receiver = $receiver->getKey();
        $notification->save();

        $data = array();
        $data['series'] = $series;
        $data['sender'] = $sender;
        $data['receiver'] = $receiver;

        $mailInfo['email'] = $receiver->email;
        $mailInfo['title'] = "[Saegeul] following for your series";
        $mailInfo['view'] = "emails.following";

        $this->sendEmail($data,$mailInfo);
    }
}
