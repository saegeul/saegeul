<?php

namespace Saegeul\Post\Events\Handlers ;

use Saegeul\Post\Events\PostWasRegistered ;
use Saegeul\Post\Events\PostWasUpdated ;
use Saegeul\Post\Events\PostWasDeleted;
use Saegeul\Vcs\Vcs;
use Saegeul\Favorite\Favorite;

class PostEventHandler
{
    protected $vcsService;
    protected $favoriteService;

    public function __construct(Vcs $vcsService,Favorite $favoriteService)
    {
        $this->vcsService = $vcsService;
        $this->favoriteService = $favoriteService;
    }

    public function whenPostWasRegistered(PostWasRegistered $event)
    {
        $post = $event->post;
        $this->vcsService->commit($post->getKey());
    }

    public function whenPostWasUpdated(PostWasUpdated $event)
    {
        $post = $event->post;
        $this->vcsService->commit($post->getKey());
    }

    public function whenPostWasDeleted(PostWasDeleted $event)
    {
        $post = $event->post;

        $favoriteRepository = $this->favoriteService->getRepository();
        $favorites = $favoriteRepository->findByFavoriteIdOnType($post->getKey(),$post->getClassName());
        foreach($favorites as $key => $favorite){
            $favoriteRepository->delete($favorite->getKey());
        }
    }
}
