<?php
namespace App\Http\Controllers\Guide ;

use App\Http\Controllers\Controller ;

class AccountManager extends Controller { 

    public function profile()
    {
        return view('guides.account_manager.profile') ;
    }

    public function password()
    {
        return view('guides.account_manager.password') ;
    }
}
