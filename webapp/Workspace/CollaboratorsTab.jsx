import React from 'react' ; 
import Request from 'superagent' ;
import ReactDOM from 'react-dom' ;
import AddCollaboratorsDialog from './../Dialog/AddCollaboratorsDialog.jsx' ;

export default class CollaboratorsTab extends React.Component {

    constructor(props){
        super(props) ;
        this.div = null ;
    }

    render() {
        this.div = document.createElement('div') ;
        document.body.appendChild(this.div) ;
        this.getUsers() ;

        return (
            <div> 
                <div className="container mini-container"> 
                    <a className="button round icon first-btn green" onClick={this.showCollaboratorDialog.bind(this)} >
                        <i className="material-icons icon">person_add</i> 
                    </a> 
                </div> 
            </div>
        );
    }

    showCollaboratorDialog() {
        ReactDOM.render(<AddCollaboratorsDialog seriesId={this.props.seriesId} ></AddCollaboratorsDialog>, this.div) ; 
    }

    
    getUsers() {
        Request.get('/api/v1/relay')
                .query(
                    {
                        resource : 'collaborators' ,
                        url : '/api/v1/series/'+this.props.seriesId + '/collaborators/waitings'
                    }
                )
                .end((err,response)=> { 
                    console.log(response) ;
                });

    }
}
