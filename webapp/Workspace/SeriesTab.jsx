import React from 'react' ; 
import SeriesList from './SeriesList.jsx' ; 
import Request from 'superagent' ; 
import CreateSeriesDialog from './../Dialog/CreateSeriesDialog.jsx' ;

export default class SeriesTab extends React.Component {

    constructor(props){
        super(props) ;
        this.state = {
            dialogStatus : {
                isOpen:false
            },
            items : []
        };
    }

    render() {
        return (
            <div>
                <div className="container mini-container"> 
                    <a className="button round icon first-btn" onClick={this.showDialog.bind(this)} >
                        <i className="material-icons icon">create</i>
                    </a> 
                </div>
                <br/>
                <div className="container mini-container">
                    <SeriesList items={this.state.items}>
                    </SeriesList>
                </div> 
                <CreateSeriesDialog 
                        dialogStatus={this.state.dialogStatus} 
                        refreshItems={this.refreshItems.bind(this)} 
                        closeDialog={this.closeDialog.bind(this)}>
                </CreateSeriesDialog>
            </div>
        );
    }

    componentDidMount(){
        this.refreshItems() ;
    }

    refreshItems() {
        Request.get('/api/v1/series')
            .send({
            })
            .end((err,response) => {
                let data = JSON.parse(response.text) ;
                this.setState({
                    items : data.data
                });
            }); 
    }

    showDialog() {
        this.setState({
            dialogStatus : {
                isOpen : true
            }
        }); 
    }

    closeDialog() {
        this.setState({
            dialogStatus : {
                isOpen : false
            }
        }); 
    }
}
