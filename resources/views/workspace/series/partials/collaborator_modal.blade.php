<div id="collaborator-modal" class="reveal-modal modal small collaborator-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="modal-header">
        <h2>Collaborators</h2>
    </div>
    <div class="modal-body">
        <input type="hidden" name="series_id">
        <input type="hidden" name="role_id">
        <input type="hidden" name="collaborators">
        <input type="hidden" name="delete_users">
        <ul class="collaborators">

        </ul>
        <div class="row">
            <div class="large-12 columns">
                <div class="row collapse">
                    <div class="small-8 medium-9 large-10 columns">
                        <input type="text" name="email" placeholder="email">
                    </div>
                    <div class="small-4 medium-3 large-2 columns">
                        <a href="#" class="button postfix sg-btn-search-email"><span class="fi-plus" title="plus" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <div class="right">
            <a class="button">CANCEL</a>
            <a class="button sg-btn-save">SAVE</a>
        </div>
    </div>
</div>
