<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController;
use Illuminate\Http\Request;
use Saegeul\Finder\Finder;
use Artgrafii\Glauth\Glauth;
use TwitterOAuth\Exception\TwitterException;
use TwitterOAuth\Exception\MissingCredentialsException;
use Instagram\Core\ApiAuthException;
use Instagram\Core\ApiException;

class FinderController extends ApiController 
{
    protected $glauth ;
    protected $user ;
    protected $options;
    protected $provider;

	public function __construct(Finder $finder, Glauth $glauth)
    {
        $this->finder = $finder;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
    }

    public function find(Request $request)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $v = \Validator::make($request->all(), [
                'keyword' => 'required',
                'page' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $keyword = $request->input('keyword');
            $page= $request->input('page');
            $finderService = $this->finder->factory($this->provider,$this->options);
            $items = $finderService->find($keyword,$page);

            return $this->respond($items);
        } catch (\ReflectionException $e) {
            return $this->respondNotFound();
        } catch(\Google_Service_Exception $e){
            return $this->respondInternalError('youtube error');
        } catch(\Google_Exception $e){
            return $this->respondInternalError('youtube error');
        } catch (TwitterException $e) {
            return $this->respondInternalError('twitter error');
        } catch (MissingCredentialsException $e) {
            return $this->respondInternalError('twitter error');
        } catch (ApiAuthException $e) {
            return $this->respondInternalError('instagram error');
        } catch (ApiException $e) {
            return $this->respondInternalError('instagram error');
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        } 
    } 
}
