import React from 'react' ; 
import Request from 'superagent' ;
import PostItem from './Partials/PostItem.jsx' ;

export default class PostsTab extends React.Component { 

    constructor(props) {
        super(props) ; 
        this.createPostUri = '/workspace/series/'+props.seriesId+'/posts/create' ;
        this.state = {
            posts : []
        }
    }

    render() { 
        var style = {
            'margin' : '0px'
        };

        return (
            <div>
                <div className="container mini-container"> 
                    <a className="button round icon first-btn ocean" href={this.createPostUri}>
                        <i className="material-icons icon">create</i> 
                    </a> 
                </div> 
                <br/>
                <br/>
                <div className="container mini-container">
                    <ul style={style} className="list white-theme">
                        {this.state.posts.map((o,index) => {

                            let className = 'draft' ;

                            if(o.status == 'draft') {
                                className = 'draft' ;
                            }else {
                                className = 'published' ;
                            }

                            return ( 
                                <PostItem key={index} post={o} className={className}></PostItem>
                            )
                        })}
                    </ul>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.getPosts() ;
    } 
    
    getPosts() {
        Request.get('/api/v1/series/'+ this.props.seriesId + '/posts')
                .send()
                .end(function(err,response){
                    let parsedData = JSON.parse(response.text) ;
                    let data = parsedData.data ;
                    this.setState({
                        posts : data
                    });

                }.bind(this)); 
    }
}
