<?php namespace App\Http\Controllers\Api\V1\Transformers;

use League\Fractal\TransformerAbstract ;
use Saegeul\Favorite\Models\Favorite;
use App\Http\Controllers\Api\V1\Transformers\SeriesTransformer;

class FavoriteSeriesTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['series'];

    public function transform(Favorite $favorite)
    {
        return [
            'id' => (int) $favorite->id,
            'user_id' => $favorite->user_id,
            'favorite_id' => $favorite->favorite_id,
            'favorite_type' => $favorite->favorite_type,
            'created_at' => $favorite->created_at,
            'updated_at' => $favorite->updated_at
        ];
    }

    public function includeSeries(Favorite $favorite)
    {
        $series = $favorite->favorite;

        return $this->item($series, new SeriesTransformer);
    }
}
