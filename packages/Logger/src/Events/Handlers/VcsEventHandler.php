<?php
namespace Saegeul\Logger\Events\Handlers ;
use Saegeul\Logger\Events\Handlers\EventHandler ;
use Saegeul\Vcs\Events\PostWasCommited;
use Saegeul\Vcs\Events\PostWasMerged;
use Saegeul\Vcs\Events\PostWasForked;
use Saegeul\Vcs\Events\PostWasImported;
use Saegeul\Vcs\Events\PostWasPullRequested;
use Saegeul\Vcs\Events\PostWasRollbacked; 
use Saegeul\Vcs\Events\PullRequestWasDiscarded;

class VcsEventHandler extends EventHandler
{
    public function whenPostWasCommited(PostWasCommited $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    }

    public function whenPostWasMerged(PostWasMerged $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    }

    public function whenPostWasForked(PostWasForked $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    }

    public function whenPostWasImported(PostWasImported $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    }

    public function whenPostWasPullRequested(PostWasPullRequested $event)
    {
        $this->handle($event->pullRequest,__FUNCTION__) ;
    }

    public function whenPostWasRollbacked(PostWasRollbacked $event)
    {
        $this->handle($event->post,__FUNCTION__) ;
    } 

    public function whenPullRequestWasDiscarded(PullRequestWasDiscarded $event)
    {
        $this->handle($event->pullRequest,__FUNCTION__) ;
    }
}
