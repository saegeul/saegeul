import React from 'react' ; 

export default class SeriesItem extends React.Component {

    constructor(props){
        super(props) ;
        this.series = props.series ;
    }

    render() {
        return(
        <li> 
            <div className="left-column">
                <i className="material-icons">insert_photo</i>
            </div>
            <div className="right-column">
                <h6 className="title"><a href={this.route(this.series.id)}>{this.series.title}</a></h6>
                <ul className="tag-list">
                    <li className="tag">#design</li>
                    <li className="tag">#ux</li>
                    <li className="tag">#ui</li>
                </ul>
                <p className="description">{this.series.description}</p> 
                <div className="additional-information clearfix">
                    <div className="left">
                        <ul className="information">
                            <li>Followers <strong>0</strong></li>
                            <li>Like <strong>0</strong></li>
                        </ul>
                    </div>
                    <div className="right">
                        <span>Last updated Jul 8,2015</span>
                    </div>
                </div> 
            </div>
        </li>
        );
    }

    route(seriesId) {
        return '/workspace/series/'+seriesId;
    }
}
