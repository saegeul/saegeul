<div class="container mini-container">
    <h6 class="title">{{$user->unique_link}} library</h6>
    <div>
        <ul class="tabs simple-tabs">
            <li class="tab-title @if($currentTab == 'series') active @endif"><a href="/workspace/series">SERIES</a></li>
            <li class="tab-title @if($currentTab == 'coworking') active @endif"><a href="/workspace/series/coworking">COWORKING</a></li>
        </ul>
    </div>
</div> 
