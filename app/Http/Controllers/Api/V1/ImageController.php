<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Artgrafii\SgDrive\DriveFactory ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\SgDrive\Services\FileService;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\FileObjectTransformer;

class ImageController extends ApiController 
{
    protected $glauth ;
    protected $user ;
    protected $factory ;
    protected $fileService;
    protected $fractal;

    public function __construct(
        Glauth $glauth, 
        DriveFactory $factory,
        FileService $fileService,
        Manager $fractal
    )
    {
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->factory = $factory ;
        $this->fileService = $fileService;
        $this->fractal = $fractal;
    }

    public function index(Request $request)
    {
        $page = $request->input('page')?$request->input('page'):1;
        $keyword = $request->input('keyword')?$request->input('keyword'):"";

        if(!$this->user){
            return $this->respondUnauthorized() ;
        }
        
        $drive = $this->factory->makeDrive($this->user);
        if($keyword == ""){
            $objects = $drive->lists($page);
        }else{
            $objects = $drive->findByName($keyword,$page);
        }

        $resource = new Fractal\Resource\Collection($objects, new FileObjectTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($objects));
        $result = $this->fractal->createData($resource)->toJson();
        return $this->setStatusCode(200)->respond($result);
    }

    public function store(Request $request)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $v = \Validator::make($request->all(), [
                'upload' => 'required|image',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $file = $request->file('upload');

            $drive = $this->factory->makeDrive($this->user);
            $object = $drive->writeOnDB($file);
            $cdnObject = $object->putOnCDN($drive);

            $resource = new Fractal\Resource\Item($cdnObject, new FileObjectTransformer);
            $result = $this->fractal->createData($resource)->toJson();

            $result = json_decode($result) ;
            $result->success = true ;
            $result = json_encode($result) ;

            //$result = json_encode($obj) ;
            return $this->setStatusCode(201)->respond($result);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return $this->respondInternalError();
        }
    }

    public function show($id)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $drive = $this->factory->makeDrive($this->user);

            $fileObject = $drive->find($id);

            $resource = new Fractal\Resource\Item($fileObject, new FileObjectTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch(ModelNotFoundException $e){
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function destroy($id)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $drive = $this->factory->makeDrive($this->user);
            $fileObject = $drive->delete($id);
            \DB::commit();

            $resource = new Fractal\Resource\Item($fileObject['fileObject'], new FileObjectTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch(ModelNotFoundException $e){
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }
}
