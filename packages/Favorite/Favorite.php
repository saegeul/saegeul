<?php

namespace Saegeul\Favorite;

use Illuminate\Bus\Dispatcher ;
use Saegeul\Favorite\Jobs\Register\RegisterCommand ;
use Saegeul\Favorite\Jobs\Show\ShowCommand ;
use Saegeul\Favorite\Jobs\Delete\DeleteCommand ;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Artgrafii\Glauth\Contracts\UserInterface ;
use Saegeul\Favorite\Contracts\FavoriteInterface;

class Favorite
{
    protected $dispatcher; 

    public function __construct(Dispatcher $dispatcher, FavoriteRepositoryInterface $favoriteRepository)
    {
        $this->dispatcher = $dispatcher ;
        $this->favoriteRepository = $favoriteRepository ;
    }

    public function register(FavoriteInterface $favoriteModel, UserInterface $user)
    {
        $data = array();
        $data['favorite_id'] = $favoriteModel->getKey();
        $data['favorite_type'] = $favoriteModel->getClassName();
        $data['user_id'] = $user->getKey() ;

        $command = new RegisterCommand($data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function show($favoriteId)
    {
        $command = new ShowCommand($favoriteId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function delete($favoriteId)
    {
        $command = new DeleteCommand($favoriteId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function getRepository()
    {
        return $this->favoriteRepository ;
    }
}
