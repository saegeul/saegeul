#php artisan migrate:rollback
#php artisan migrate
#php artisan migrate --path=vendor/cartalyst/tags/resources/migrations
#php artisan db:seed

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

#printf "${GREEN}TEST:${NC} Finder\n"
#bin/phpunit tests/FinderTest.php

#printf "${GREEN}TEST:${NC} Series\n"
#bin/phpunit tests/SeriesTest.php

#printf "${GREEN}TEST:${NC} Invitation\n"
#bin/phpunit tests/InvitationTest.php

#printf "${GREEN}TEST:${NC} Post\n"
#bin/phpunit packages/Post/tests/PostTest.php

#printf "${GREEN}TEST:${NC} Shorten URL\n"
#bin/phpunit tests/ShortenUrlTest.php

#printf "${GREEN}TEST:${NC} Subscription\n"
#bin/phpunit tests/SubscriptionTest.php

#printf "${GREEN}TEST:${NC} Favorite\n"
#bin/phpunit tests/FavoriteTest.php

#printf "${GREEN}TEST:${NC} VCS\n"
#bin/phpunit tests/VcsTest.php

#printf "${GREEN}TEST:${NC} Cover\n"
#bin/phpunit packages/Cover/tests/CoverTest.php

#printf "${GREEN}TEST:${NC} Notification\n"
#bin/phpunit packages/Notification/tests/NotificationTest.php

printf "${GREEN}TEST:${NC} Pdf\n"
bin/phpunit packages/Pdf/tests/PdfTest.php

#printf "${GREEN}TEST:${NC}VideoParser\n"
#bin/phpunit packages/VideoParser/tests/VideoParserTest.php
