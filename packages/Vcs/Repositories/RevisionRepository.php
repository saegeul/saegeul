<?php

namespace Saegeul\Vcs\Repositories ;

use Saegeul\Database\Repository\IlluminateRepository ;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class RevisionRepository extends IlluminateRepository implements RevisionRepositoryInterface
{
    protected $model = 'Saegeul\Vcs\Models\Revision' ;
    protected $limit = 40;

    public function history($postId,$page=1)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('post_id', $postId);
        $totalCount = $model->count() ; 
        $model = $model->orderBy('id','desc')->skip(($page-1)*$this->limit)->take($this->limit);
        $items = $model->get();

        return new Paginator($items->all(),$totalCount,$this->limit,$page) ;
    }

    public function findByUuid($uuid)
    {
        $model = $this->modelFactory->create($this->model) ;
        $model = $model->where('uuid', $uuid);

        return $model->first();
    }
}
