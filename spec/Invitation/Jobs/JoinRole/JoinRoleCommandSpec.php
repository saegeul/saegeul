<?php

namespace spec\Saegeul\Invitation\Jobs\JoinRole;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Illuminate\Events\Dispatcher;
use Artgrafii\Glauth\Glauth ;
use Artgrafii\Glauth\Services\UserService ;
use Artgrafii\Glauth\Services\RoleService ;
use Artgrafii\Glauth\Services\HashCodeService ;
use Saegeul\Invitation\Contracts\InvitationRepositoryInterface ;
use Saegeul\Invitation\Models\Invitation ;
use Cartalyst\Sentinel\Roles\IlluminateRoleRepository ;
use Cartalyst\Sentinel\Roles\EloquentRole ;
use Cartalyst\Sentinel\Users\UserRepositoryInterface ;
use Cartalyst\Sentinel\Users\EloquentUser ;
use Saegeul\Invitation\Events\UserWasJoinedRole;

class JoinRoleCommandSpec extends ObjectBehavior
{
    protected $invitationCode = 'RDO1aV4l0r42kxXW';
    protected $userId = 1;

    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Invitation\Jobs\JoinRole\JoinRoleCommand');
    }

    public function let()
    {
        $this->beConstructedWith($this->invitationCode,$this->userId);
    }

    public function it_trigger_handle(
        Dispatcher $dispatcher,
        HashCodeService $hashCodeService,
        InvitationRepositoryInterface $invitationRepository,
        Glauth $glauth,
        UserService $userService,
        RoleService $roleService,
        IlluminateRoleRepository $roleRepository,
        UserRepositoryInterface $userRepository,
        Invitation $invitation,
        EloquentUser $user, 
        EloquentRole $role
    )
    {
        $this->beConstructedWith($this->invitationCode,$this->userId);

        $invitationRepository->findByCode($this->invitationCode)->shouldBeCalled()->willReturn($invitation);

        $glauth->getUserService()->shouldBeCalled()->willReturn($userService);
        $userService->getUserRepository()->shouldBeCalled()->willReturn($userRepository);
        $userRepository->findById($this->userId)->shouldBeCalled()->willReturn($user);

        $user->getAttribute('email')->willReturn('jaehee@artgrafii.com') ;
        $invitation->getAttribute('email')->willReturn('jaehee@artgrafii.com') ;

        $glauth->getRoleService()->shouldBeCalled()->willReturn($roleService);
        $roleService->getRoleRepository()->shouldBeCalled()->willReturn($roleRepository);
        $invitation->getAttribute('role_id')->willReturn(2) ;
        $roleRepository->findById(2)->shouldBeCalled()->willReturn($role);
        $role->getKey()->shouldBeCalled()->willReturn(2);
        $user->inRole($role)->shouldBeCalled()->willReturn(false);

        $roleService->attachRole(1,2)->shouldBeCalled()->willReturn($role);

        $data = ['response'=>1];
        $invitation->fill($data)->shouldBeCalled();
        $invitation->save()->shouldBeCalled();

        $dispatcher->fire(new UserWasJoinedRole($invitation->getWrappedObject()))->shouldBeCalled();

        $this->handle($dispatcher,$glauth,$hashCodeService,$invitationRepository)->shouldReturnAnInstanceOf('Saegeul\Invitation\Models\Invitation');
    }
}
