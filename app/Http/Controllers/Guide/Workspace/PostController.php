<?php
namespace App\Http\Controllers\Guide\Workspace ;

use App\Http\Controllers\Controller ;

class PostController extends Controller { 

    public function bySeries()
    {
        return view('guides.workspace.posts.index') ;
    } 

    public function writeForm()
    {
        return view('guides.workspace.posts.write') ;
    }
}
