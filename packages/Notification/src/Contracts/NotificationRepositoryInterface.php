<?php
namespace Saegeul\Notification\Contracts;

use Saegeul\Database\Repository\Contracts\RepositoryInterface ;

interface NotificationRepositoryInterface extends RepositoryInterface 
{
}
