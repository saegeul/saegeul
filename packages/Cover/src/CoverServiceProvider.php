<?php

namespace Saegeul\Cover;

use Illuminate\Support\ServiceProvider ;

class CoverServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->app->bind('Saegeul\Cover\Contracts\CoverRepositoryInterface',
            'Saegeul\Cover\Repositories\CoverRepository');
    }
}
