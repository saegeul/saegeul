<?php

namespace Saegeul\Subscription\Events\Handlers ;

use Illuminate\Mail\Mailer ;
use Saegeul\Subscription\Events\SubscriptionWasRegistered ;
use Saegeul\Subscription\Events\SubscriberWasActivated;
use Saegeul\Subscription\Events\SubscriptionWasCanceled;
use Artgrafii\Glauth\Services\HashCodeService ;
use Saegeul\Series\Series;

class SubscriptionEventHandler
{
    public $mailer ; 
    public $hashCodeService ; 
    protected $sereisService;

    public function __construct(
        Mailer $mailer,
        HashCodeService $hashCodeService,
        Series $sereisService
    )
    {
        $this->mailer = $mailer ;
        $this->hashCodeService = $hashCodeService ;
        $this->sereisService = $sereisService;
    }

    public function whenSubscriptionWasRegistered(SubscriptionWasRegistered $event)
    {
        $subscription = $event->subscription;

        $series = $event->series ;
        $owner = $series->user;

        $data = [
            'series' => $series,
            'owner' => $owner
        ] ;

        $mailInfo['email'] = $subscription->email;
        $mailInfo['title'] = "[Saegeul] Subscription to Series";
        $mailInfo['view'] = "emails.register-subscription-to-series";

        $this->sendSubscriptionEmail($data,$mailInfo);

        $subscriptionCount = $subscription->countSubscription();
        $series->subscribers = $subscriptionCount;
        $series->save();
    }

    private function sendSubscriptionEmail($data,$mailInfo)
    {
        $callback = function($message) use($mailInfo){
            $message->to($mailInfo['email'],  'User')->subject($mailInfo['title']);
        };

        $result = $this->mailer->send($mailInfo['view'],$data,$callback);
    }

    public function whenSubscriberWasActivated(SubscriberWasActivated $event)
    {
        $subscription = $event->subscription;
        $seriesRepository = $this->sereisService->getRepository();
        $series = $seriesRepository->find($subscription->series_id);

        $subscriptionCount = $subscription->countSubscription();
        $series->subscribers = $subscriptionCount;
        $series->save();
    }

    public function whenSubscriptionWasCanceled(SubscriptionWasCanceled $event)
    {
        $subscription = $event->subscription;

        $series = $event->series ;

        $owner = $series->user;

        $data = [
            'subscription' => $subscription,
            'series' => $series,
            'owner' => $owner
        ] ;

        $mailInfo['email'] = $subscription->email;
        $mailInfo['title'] = "[Saegeul] Not Activated Subscription to Series";
        $mailInfo['view'] = "emails.cancel-subscription-to-series";

        $this->sendSubscriptionEmail($data,$mailInfo);

        $subscriptionCount = $subscription->countSubscription();
        $series->subscribers = $subscriptionCount;
        $series->save();
    }
}
