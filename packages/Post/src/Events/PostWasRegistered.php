<?php

namespace Saegeul\Post\Events ;

use Saegeul\Post\Models\Post ;

class PostWasRegistered 
{
    public $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }
}
