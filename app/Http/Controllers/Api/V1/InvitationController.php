<?php namespace App\Http\Controllers\Api\V1 ;

use App\Http\Controllers\Api\V1\ApiController ;
use Saegeul\Series\Series ;
use Artgrafii\Glauth\Glauth ;
use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Saegeul\Invitation\Invitation;

class InvitationController extends ApiController 
{
    protected $invitationService;
    protected $seriesService;
    protected $glauth ;
    protected $user ;

    public function __construct(
        Invitation $invitationService, 
        Series $seriesService, 
        Glauth $glauth
    )
    {
        $this->invitationService = $invitationService;
        $this->seriesService = $seriesService ;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
    }

    public function inviteSeries(Request $request, $id)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $v = \Validator::make($request->all(), [
                'email' => 'email',
                'role_id' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData() ;
            }

            $series = $this->seriesService->show($id);
            if(!$series->isUpdatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $email = $request->input('email');
            $roleId = $request->input('role_id');
            \DB::beginTransaction();
            $invitation = $this->invitationService->inviteRole($roleId,$email,$series);
            \DB::commit();

            return $this->respond($invitation);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    } 

    public function destroy($id)
    {
        try{
            if(!$this->user){
                return $this->respondUnauthorized() ;
            }

            $invitationRepository = $this->invitationService->getRepository();
            $invitation = $invitationRepository->find($id);
            if($invitation->invited->getClassName() == 'Saegeul\Series\Models\Series'){
                $model = $this->seriesService->show($invitation->invited_id);
                if(!$model->isCreatableByUser($this->user)){
                    return $this->respondUnauthorized() ;
                }
            }

            \DB::beginTransaction();
            $result = $invitationRepository->delete($id);
            \DB::commit();

            return $this->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }
}
