<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ApiController ;
use Illuminate\Http\Request ;
use Saegeul\Post\Post ;
use Illuminate\Database\Eloquent\ModelNotFoundException ;
use Artgrafii\Glauth\Glauth ;
use Saegeul\Series\Series ;
use League\Fractal ;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Http\Controllers\Api\V1\Transformers\PostTransformer;
use App\Http\Controllers\Api\V1\Transformers\PostWithContentTransformer;


class PostController extends ApiController 
{
    protected $postService;
    protected $glauth;
    protected $user;
    protected $seriesService;
    protected $fractal;

    public function __construct(
        Post $postService, 
        Glauth $glauth, 
        Series $seriesService,
        Manager $fractal
    )
	{
        $this->postService = $postService ;
        $this->glauth = $glauth ;
        $this->user = $this->glauth->check() ;
        $this->seriesService = $seriesService ;
        $this->fractal = $fractal;
	}

    public function show(Request $request, $id)
    {
        try{
            $post = $this->postService->show($id);
            $series = $this->seriesService->show($post->series_id);
            if(!$series->isViewableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $resource = new Fractal\Resource\Item($post, new PostWithContentTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(200)->respond($result);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound();
        } catch(\Exception $e) {
            return $this->respondInternalError() ; 
        }
    }

    public function store(Request $request)
    {
        try{
            $v = \Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
                'content' => 'required',
                'html' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData();
            }

            $formData = [];
            $formData['title'] = $request->input('title');
            $formData['description'] = $request->input('description');
            $formData['content'] = $request->input('content');
            $formData['html'] = $request->input('html');
            $formData['series_id'] = $request->input('series_id');
            $formData['user_id'] = $this->user->getKey();

            $imageId = $request->input('image_id');
            $imageUrl = $request->input('image_url');
            if($imageId != '' && $imageUrl != ''){
                $formData['post_image'] = $imageId;
                $formData['post_image_url'] = $imageUrl;
            }

            \DB::beginTransaction();
            $post = $this->postService->register($formData);  
            \DB::commit();

            $updatedPost = $this->postService->show($post->getKey());

            $resource = new Fractal\Resource\Item($updatedPost, new PostWithContentTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $v = \Validator::make($request->all(), [
                'content' => 'required',
                'html' => 'required',
            ]);

            if ($v->fails()){
                return $this->respondInvalidData() ;
            }

            $post = $this->postService->show($id);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $formData = [];
            $formData['title'] = $request->input('title');
            $formData['description'] = $request->input('description');
            $formData['content'] = $request->input('content');
            $formData['html'] = $request->input('html');
            $formData['user_id'] = $this->user->getKey();

            $imageId = $request->input('image_id');
            $imageUrl = $request->input('image_url');
            if($imageId != '' && $imageUrl != ''){
                $formData['post_image'] = $imageId;
                $formData['post_image_url'] = $imageUrl;
            }

            \DB::beginTransaction();

            $post = $this->postService->update($id,$formData);
            \DB::commit();
            $updatedPost = $this->postService->show($post->getKey());

            $resource = new Fractal\Resource\Item($updatedPost, new PostWithContentTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function destroy($id)
    {
        try{
            $post = $this->postService->show($id);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            \DB::beginTransaction();

            $deletedPost = $this->postService->delete($id);

            \DB::commit();

            $resource = new Fractal\Resource\Item($deletedPost, new PostWithContentTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }

    public function publish(Request $request, $id)
    {
        try{
            $post = $this->postService->show($id);
            if(!$post->isCreatableByUser($this->user)){
                return $this->respondUnauthorized() ;
            }

            $formData = [];
            $formData['status'] = 'publish';

            \DB::beginTransaction();

            $post = $this->postService->update($id,$formData);

            \DB::commit();

            $updatedPost = $this->postService->show($post->getKey());

            $resource = new Fractal\Resource\Item($updatedPost, new PostWithContentTransformer);
            $result = $this->fractal->createData($resource)->toJson();
            return $this->setStatusCode(201)->respond($result);
        } catch (ModelNotFoundException $e) {
            \DB::rollback();
            return $this->respondNotFound();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->respondInternalError() ; 
        }
    }
}
