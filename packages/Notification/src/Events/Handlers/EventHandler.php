<?php
namespace Saegeul\Notification\Events\Handlers ;

use Saegeul\Notification\Notification ;
use Saegeul\Notification\Contracts\NotifiableInterface ;
use Saegeul\Notification\Events\NotificationWasRegistered;
use Illuminate\Mail\Mailer ;

class EventHandler
{ 
    protected $notificationService;
    protected $mailer ; 

    public function __construct(Mailer $mailer, Notification $notificationService)
    {
        $this->mailer = $mailer ;
        $this->notificationService = $notificationService;
    }

    public function handle(NotifiableInterface $data, $eventName)
    {
        return $this->notificationService->register($data,$eventName) ;
    }

    public function sendEmail($data,$mailInfo)
    {
        $callback = function($message) use($mailInfo){
            $message->to($mailInfo['email'],  'User')->subject($mailInfo['title']);
        };

        $this->mailer->send($mailInfo['view'],$data,$callback);
    }
}
