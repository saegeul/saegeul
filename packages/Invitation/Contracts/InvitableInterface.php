<?php

namespace Saegeul\Invitation\Contracts;

interface InvitableInterface
{ 
    public function getKey() ;
    public function getClassName() ;
}
