<?php

namespace Saegeul\Vcs;

use Illuminate\Bus\Dispatcher ;
use Saegeul\Vcs\Jobs\Commit\CommitCommand ;
use Saegeul\Vcs\Jobs\Fork\ForkCommand ;
use Saegeul\Vcs\Jobs\PullRequest\PullRequestCommand ;
use Saegeul\Vcs\Jobs\Merge\MergeCommand ;
use Saegeul\Vcs\Jobs\Import\ImportCommand ;
use Saegeul\Vcs\Jobs\Rollback\RollbackCommand;
use Saegeul\Vcs\Jobs\Discard\DiscardCommand;
use Saegeul\Vcs\Contracts\RevisionRepositoryInterface ;
use Saegeul\Vcs\Contracts\PullRequestRepositoryInterface;
use Saegeul\Vcs\Contracts\ActivityRepositoryInterface;
use Saegeul\Post\Models\Post;

class Vcs
{
    protected $dispatcher; 
    protected $revisionRepository;
    protected $pullRequestRepository;
    protected $activationRepository;

    public function __construct(
        Dispatcher $dispatcher, 
        RevisionRepositoryInterface $revisionRepository, 
        PullRequestRepositoryInterface $pullRequestRepository, 
        ActivityRepositoryInterface $activityRepository
    )
    {
        $this->dispatcher = $dispatcher ;
        $this->revisionRepository = $revisionRepository ;
        $this->pullRequestRepository = $pullRequestRepository ;
        $this->activityRepository = $activityRepository;
    }

    public function commit($postId)
    {
        $command = new CommitCommand($postId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function fork($postId,$userId)
    {
        $command = new ForkCommand($postId,$userId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    } 

    public function pullRequest($data)
    {
        $command = new PullRequestCommand($data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function merge(Post $toPost, Post $fromPost, array $data)
    {
        $command = new MergeCommand($toPost,$fromPost,$data) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function import($pullRequestId,$userId)
    {
        $command = new ImportCommand($pullRequestId,$userId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function rollback($postId,$fromUuid)
    {
        $command = new RollbackCommand($postId,$fromUuid) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function discard($pullRequestId)
    {
        $command = new DiscardCommand($pullRequestId) ; 
        return $this->dispatcher->dispatchNow($command) ; 
    }

    public function getRevisionRepository()
    {
        return $this->revisionRepository ;
    }

    public function getPullRequestRepository()
    {
        return $this->pullRequestRepository ;
    }

    public function getActivityRepository()
    {
        return $this->activityRepository;
    }
}
