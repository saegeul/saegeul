<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileObjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::create('fileobjects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('original_name');
			$table->string('encrypted_name');
			$table->integer('size');
			$table->string('full_path');
            $table->boolean('is_image');
			$table->string('type');
			$table->string('mime_type');
			$table->integer('height');
			$table->integer('width');
			$table->string('thumb_large_path');
			$table->string('thumb_middle_path');
			$table->string('thumb_small_path');
            $table->boolean('is_cdn')->default(0);
			$table->integer('user_id');
			$table->string('uuid');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fileobjects');
	}

}
