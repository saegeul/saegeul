<?php

namespace spec\Saegeul\Logger\Events\Handlers;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Saegeul\Logger\Logger ;
use Saegeul\Logger\Contracts\LoggableInterface ;

class EventHandlerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Logger\Events\Handlers\EventHandler');
    }

    public function let(Logger $logger)
    {
        $this->beConstructedWith($logger) ;
    }

    public function it_handle(Logger $logger, LoggableInterface $data)
    {
        $eventName = 'hello' ;
        $logger->log($data,$eventName)->shouldBeCalled() ;
        $this->handle($data,$eventName) ;
    }
}
