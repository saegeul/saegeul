<!DOCTYPE html>
<html lang="en" class="no-js" >
    <head>
        <meta charset="utf-8">
        <title>Saegeul</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="/sgpack/flat-ui/images/favicon.ico">
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
        <link rel="stylesheet" href="/redactor/css/redactor.css" />
    </head>

<body> 
<!--<nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
            <h1><a href="#"><img src="/img/logo/saegeul_small.png" /></a></h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
            <ul class="right">
                <li class="active"><a href="#">Go to My workspace</a></li>
                <li class="has-dropdown">
                <a href="#">jaehee@artgrafii.com</a>
                <ul class="dropdown">
                    <li><a href="#">First link in dropdown</a></li>
                    <li class="active"><a href="#">Active link in dropdown</a></li>
                </ul>
                </li>
            </ul> 
        </section>
    </nav>-->
@yield('contents') 

<!-- 실제 레이아웃으로 옮길때는 제거 only for guide -->
@include('partials.guidebar') 
<!-- end -->

<script src="/bower_components/foundation/js/vendor/jquery.js"></script>

@yield('scripts')

@include('partials.guide-script')

<script src="/bower_components/foundation/js/foundation.min.js"></script>
<script>
$(document).foundation();
</script>

</body>
</html>
