<?php

namespace Saegeul\Favorite\Jobs\Register;

use Illuminate\Contracts\Bus\SelfHandling ;
use Saegeul\Favorite\Contracts\FavoriteRepositoryInterface ;
use Illuminate\Events\Dispatcher ;
use Saegeul\Favorite\Events\FavoriteWasRegistered ;

class RegisterCommand implements SelfHandling
{

    public $data;

    public function __construct($data)
    {
        $this->data = $data ;
    }

    public function handle(Dispatcher $dispatcher, FavoriteRepositoryInterface $favoriteRepository)
    {
        $favorite = $favoriteRepository->store($this->data);

        $dispatcher->fire(new FavoriteWasRegistered($favorite));

        return $favorite ;
    }
}
