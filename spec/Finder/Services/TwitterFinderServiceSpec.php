<?php

namespace spec\Saegeul\Finder\Services;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TwitterOAuth\Auth\ApplicationOnlyAuth;
use TwitterOAuth\Serializer\ArraySerializer;

class TwitterFinderServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Saegeul\Finder\Services\TwitterFinderService');
    }

    public function let(ApplicationOnlyAuth $auth)
    {
        $this->beConstructedWith($auth);
    }

    public function it_find(
        ApplicationOnlyAuth $auth
    )
    {
        $keyword = 'chrismas';
        $page = 1;
        $searchOptions = [
            'q' => $keyword,
            'maxResults' => 20
        ];

        //$auth->get('search/tweets',$searchOptions)->shouldBeCalled()->willReturn($response);
        
        $this->find($keyword,$page)->shouldBeArray();
    }
}
