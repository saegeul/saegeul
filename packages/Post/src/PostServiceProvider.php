<?php

namespace Saegeul\Post;

use Illuminate\Support\ServiceProvider ;

class PostServiceProvider extends ServiceProvider 
{
    public function register()
    { 
        $this->app->bind('Saegeul\Post\Contracts\PostRepositoryInterface',
            'Saegeul\Post\Repositories\PostRepository');

        $this->registerEventListener();
    }

    private function registerEventListener()
    {
        $registerConf = $this->app['config']->get('event.register-post'); 
        foreach($registerConf['handler'] as $val){
            \Event::listen($registerConf['name'],$val);
        }

        $updateConf = $this->app['config']->get('event.update-post'); 
        foreach($updateConf['handler'] as $val){
            \Event::listen($updateConf['name'],$val);
        }

        $deleteConf = $this->app['config']->get('event.delete-post'); 
        foreach($deleteConf['handler'] as $val){
            \Event::listen($deleteConf['name'],$val);
        }
    }
}
