<?php

namespace Saegeul\Invitation\Events\Handlers ;

use Saegeul\Invitation\Events\UserWasInvitedRole ;
use Illuminate\Mail\Mailer ;
use Artgrafii\Glauth\Services\HashCodeService ;
use Saegeul\Invitation\Events\UserWasDetachedRole ;
use Saegeul\Invitation\Invitation ;

class InvitationEventHandler
{
    public $mailer ; 
    public $hashCodeService ; 
    public $invitationService ;
    public $invitationRepository ;

    public function __construct(
        Mailer $mailer,
        HashCodeService $hashCodeService ,
        Invitation $invitationService
    )
    {
        $this->mailer = $mailer ;
        $this->invitationService = $invitationService ;
        $this->invitationRepository = $invitationService->getRepository() ;
        $this->hashCodeService = $hashCodeService ;
    }

    public function whenUserWasInvitedRole(UserWasInvitedRole $event)
    {
        $invitation = $event->invitation;
        $invitation->touch();

        $invitationCode = $this->hashCodeService->encode($invitation->getKey());

        $invitableModel = $event->invitableModel;

        $owner = $invitableModel->user;
        $role = $event->role;

        $data = [
            'role' => $role, 
            'invitationCode' => $invitationCode,
            'invitableModel' => $invitableModel,
            'owner' => $owner
        ] ;

        $mailInfo['email'] = $invitation->email;
        if($invitableModel->getClassName() == 'Saegeul\Series\Models\Series'){
            $mailInfo['title'] = "[Saegeul] Invite to Series";
            $mailInfo['view'] = "emails.invite-to-series";
        }

        $this->sendInvitationEmail($data,$mailInfo);
    }

    public function whenUserWasDetachedRole(UserWasDeatchedRole $event)
    {
        $invitation = $event->invitation ;
        $this->invitationRepository->delete($invitation->getKey());
    }

    private function sendInvitationEmail($data,$mailInfo)
    {
        $callback = function($message) use($mailInfo){
            $message->to($mailInfo['email'],  'User')->subject($mailInfo['title']);
        };

        $this->mailer->send($mailInfo['view'],$data,$callback);
    }
}
